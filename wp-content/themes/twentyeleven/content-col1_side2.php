<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
?>
		<div id="col_1_sidebar_2">
            	<div id="sidebar_left">

                <div class="menu">

                	<?php
						simarine_page_hierarchy_menu(get_the_ID());

					?>
                    &nbsp;
			 </div><!-- .menu -->

                </div><!-- #sidebar_left -->
               <div id="sidebar_right">

                <div class="menu">
                	<div style="text-decoration:none;background-position:bottom !important;color: #2c2c2c !important;"><a style="color: #2c2c2c !important;"><?php _e('Find out more', 'twentyeleven'); ?></a></div>
                <?php
					if(get_post_meta( $post->ID, "poglej_se", true) != "")
				 		$menuid =  get_post_meta( $post->ID, "poglej_se", true);
				 	else
				 		$menuid = "1";
				 	$menu = wp_get_nav_menu_object('poglej_se'. $menuid);
					$menu_items = wp_get_nav_menu_items($menu->term_id, array('orderby' => 'menu_order'));

					foreach($menu_items as $item){
						?>
                	<div>
                    	<a href="<?php echo $item->url; ?>"><?php echo $item->title;?></a>
                    </div>
                    <?php
					}
					?>

                </div><!-- .menu -->

                </div><!-- #sidebar_right -->

           		<div id="main_left_col">
                    <span id="nav_pointer">
                    	<a href="<?php echo get_site_url();?>"><?php _e('Home', 'twentyeleven'); ?></a>
                    	<?php $anc = get_ancestors( get_the_ID(), 'page' );
							$anc = array_reverse($anc);
							foreach($anc as $id){
								$current++;
								$p = get_post($id);
								?>
								&gt; <a href="<?php echo get_permalink($p->ID);?>"><?php echo $p->post_title; ?></a>
								<?php
							}
						?>
                    	&gt; <a href="<?php echo get_permalink(get_the_ID()); ?>" class="active"><?php the_title(); ?></a>

                    </span>
                    <div class="clear"></div>

                    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                        <header class="entry-header">
                            <h1><?php the_title(); ?></h1>
                        </header><!-- .entry-header -->

                        <div class="entry-content">
                            <?php the_content(); ?>
                            <?php wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'twentyeleven' ) . '</span>', 'after' => '</div>' ) ); ?>
                        </div><!-- .entry-content -->
                        <footer class="entry-meta">
                            <?php edit_post_link( __( 'Edit', 'twentyeleven' ), '<span class="edit-link">', '</span>' ); ?>
                        </footer><!-- .entry-meta -->
                    </article><!-- #post-<?php the_ID(); ?> -->


                </div><!-- #main_left_col -->


            </div><!-- #col_1_sidebar_1 -->