( function( window, undefined ) {
	'use strict';

	jQuery( function( $ ) {

		// Browse calendar pages.
		$( '.simcal-default-calendar' ).each( function( e, i ) {

			var calendar     = $( i ),
				id           = calendar.data( 'calendar-id' ),
				offset       = calendar.data( 'offset' ),
				start        = calendar.data( 'events-first' ),
				end          = calendar.data( 'calendar-end' ),
				nav          = calendar.find( '.simcal-calendar-head' ),
				buttons      = nav.find( '.simcal-nav-button' ),
				spinner      = calendar.find( '.simcal-ajax-loader' ),
				current      = nav.find( '.simcal-current' ),
				currentTime  = current.data( 'calendar-current' ),
				currentMonth = current.find( 'span.simcal-current-month' ),
				currentYear  = current.find( 'span.simcal-current-year' ),
				currentDate  = new Date( currentTime * 1000 ),
				date,
				action;

			if ( calendar.hasClass( 'simcal-default-calendar-grid' ) ) {
				action = 'simcal_default_calendar_draw_grid';
				// Always use the first of the month in grid.
				date = new Date( currentDate.getFullYear(), currentDate.getMonth(), 1 );
			} else {
				action = 'simcal_default_calendar_draw_list';
				toggleListHeading( calendar );
			}

			// Navigate the calendar.
			buttons.on( 'click', function() {

				var direction = $( this ).hasClass( 'simcal-next' ) ? 'next' : 'prev';

				if ( action == 'simcal_default_calendar_draw_grid' ) {

					// Monthly grid calendars.

					var body = calendar.find( '.simcal-month' ),
						newDate,
						month,
						year;

					if ( 'prev' == direction ) {
						// Beginning of the previous month.
						newDate = new Date( date.setMonth( date.getMonth() - 1, 1 ) );
					} else {
						// Last day of next month.
						newDate = new Date( date.setMonth( date.getMonth() + 2, 1 ) );
						newDate.setDate( 0 );
						newDate.setHours( 23 );
						newDate.setMinutes( 59 );
						newDate.setSeconds( 59 );
					}

					month = newDate.getMonth();
					year = newDate.getFullYear();

					$.ajax({
						url       : simcal_default_calendar.ajax_url,
						method    : 'POST',
						dataType  : 'json',
						cache     : false,
						data      : {
							action: action,
							month : month + 1, // month count in PHP goes 1-12 vs 0-11 in JavaScript
							year  : year,
							id    : id
						},
						beforeSend: function() {
							spinner.fadeToggle();
						},
						success   : function( response ) {

							currentMonth.text( simcal_default_calendar.months.full[month] );
							currentYear.text( year );
							current.attr( 'data-calendar-current', ( newDate.getTime() / 1000 ) + offset + 1 );

							spinner.fadeToggle();

							date = newDate;

							body.replaceWith( response.data );

							calendarBubbles( calendar, list );
							expandEventsToggle();
						},
						error     : function( response ) {
							console.log(response );
						}
					} );

				} else {

					// List calendars.

					var list = calendar.find( '.simcal-events-list-container' ),
						prev = list.data( 'prev' ),
						next = list.data( 'next' ),
						timestamp = direction == 'prev' ? prev : next;

					$.ajax( {
						url       : simcal_default_calendar.ajax_url,
						method    : 'POST',
						dataType  : 'json',
						cache     : false,
						data      : {
							action: action,
							ts    : timestamp,
							id    : id
						},
						beforeSend: function() {
							spinner.fadeToggle();
						},
						success   : function( response ) {

							list.replaceWith( response.data );
							current.attr( 'data-calendar-current', timestamp );

							toggleListHeading( calendar );

							spinner.fadeToggle();
							expandEventsToggle();
						},
						error     : function( response ) {
							console.log( response );
						}
					});

				}

			} );
		} );

		/**
		 * Replace the list heading with current page.
		 *
		 * @param calendar Current calendar.
		 */
		function toggleListHeading( calendar ) {

			var current = $( calendar ).find( '.simcal-current' ),
				heading = $( calendar ).find( '.simcal-events-list-container' ),
				small   = heading.data( 'heading-small' ),
				large   = heading.data( 'heading-large' );

			if ( calendar.width() < 400 ) {
				current.html( '<h3>' + small + '</h3>' );
			} else {
				current.html( '<h3>' + large + '</h3>' );
			}
		}

		var gridCalendars = $( '.simcal-default-calendar-grid' );

		/**
		 * Default calendar grid event bubbles.
		 *
		 * Initializes tooltips for events in grid.
		 * Adjusts UI for mobile or desktop.
		 *
		 * @param calendar The calendar element.
		 */
		function calendarBubbles( calendar ) {

			var table		  = $( calendar ).find( '> table' ),
				thead		  = table.find( 'thead' ),
				weekDayNames  = thead.find( 'th.simcal-week-day' ),
				cells		  = table.find( 'td.simcal-day > div' ),
				eventsList	  = table.find( 'ul.simcal-events' ),
				eventTitles	  = eventsList.find( '> li > .simcal-event-title' ),
				eventsToggle  = table.find( '.simcal-events-toggle' ),
				eventsDots	  = table.find( 'span.simcal-events-dots' ),
				events		  = table.find( '.simcal-tooltip-content' ),
				hiddenEvents  = table.find( '.simcal-event-toggled' ),
				bubbleTrigger = table.data( 'event-bubble-trigger' ),
				width		  = cells.first().width();

			if ( width < 60 ) {

				weekDayNames.each( function( e, w ) {
					$( w ).text( $( w ).data( 'screen-small' ) );
				} );

				// Hide list of events titles and show dots.
				eventsList.hide();
				eventTitles.hide();
				if ( eventsToggle != 'undefined' ) {
					eventsToggle.hide();
					if ( hiddenEvents != 'undefined' ) {
						hiddenEvents.show();
					}
				}
				eventsDots.show();
				// Adapts cells to be more squareish on mobile.
				var minH = ( width - 10 ) + 'px';
				cells.css( 'min-height', minH );
				table.find( 'span.simcal-events-dots:not(:empty)' ).css( 'min-height', minH );

			} else {

				if ( width <= 240 ) {
					weekDayNames.each( function( e, w ) {
						$( w ).text( $( w ).data( 'screen-medium' ) );
					});
				} else {
					weekDayNames.each( function( e, w ) {
						$( w ).text( $( w ).data( 'screen-large' ) );
					} );
				}

				// Hide dots and show list of events titles and toggle.
				eventsList.show();
				eventTitles.show();
				if ( eventsToggle != 'undefined' ) {
					eventsToggle.show();
					if ( hiddenEvents != 'undefined' ) {
						hiddenEvents.hide();
					}
				}
				eventsDots.hide();

				// Cells default min-height value.
				cells.css( 'min-height', ( width ) + 'px' );
			}
			//SPREMEMMBA skocjan - forsiraj "medium" velikost imen dnevov
			weekDayNames.each( function( e, w ) {
				$( w ).text( $( w ).data( 'screen-medium' ) );
			} );
			//SPREMEMMBA skocjan - forsiraj "medium" velikost imen dnevov - konec

			// Create bubbles for each cell.
			cells.each( function( e, cell ) {

				var cellDots = $( cell ).find( 'span.simcal-events-dots' ),
					tooltips = $( cell ).find( '.simcal-tooltip' ),
					eventBubbles,
					content,
					last;

				// Mobile mode.
				if ( width < 60 ) {
					events.show();
					// Use a single bubble from dots as a whole.
					eventBubbles = cellDots;
				} else {
					events.hide();
					// Create a bubble for each event in list.
					eventBubbles = tooltips;
				}

				//SPREMEMMBA skocjan - izpis dogodkov (dodano)
				$( cell ).on( 'click', function() {
					// najdi dogodke
					// najprej preiski strukturo celic (lokacija preden se podatki premaknejo v tool tip bubble )
					var event_data = $( cell ).find('.simcal-events');
					// ce je celica prazna, poizkusi tool tip bubble
					if (event_data.size() < 1) {
						event_data = $( '#qtip-' + $( cell ).find('.simcal-events-dots').attr('data-hasqtip') + '-content' ).find('.simcal-events');
					}
					// ce imamo dogodke (<li> elemente), jih prikazemo
					if (event_data.size() > 0) {
						// pocisti potencialne prejsne oznacbe izbranih dni
						$( '.simcal-selected').each( function( idx, s) {
							$( s ).removeClass('simcal-selected');
						});
						// oznaci izbran dan
						$( cell ).parent().addClass('simcal-selected');

						// prikazi dogodke
						var event_data_dom = document.createElement('div');
						var event_data_dom_data = document.createElement('ul');
						var event_date_dom = document.createElement('li');

						event_data_dom_data.innerHTML = event_data.html();	// dodaj <li> v <ul>
						event_data_dom.appendChild(event_data_dom_data);	// dodaj <ul> v <div>

						// poisci se datum za enkratni izpis pred dogodki
						var event_data_span_lst = event_data_dom.getElementsByTagName('span');
						var lst_it;
						for (lst_it = 0; lst_it < event_data_span_lst.length; lst_it++) {
							if (event_data_span_lst[lst_it].getAttribute('class').search('simcal-event-start-time') >= 0) {
								var event_date = new Date(event_data_span_lst[lst_it].getAttribute('content'));
								event_date_dom.appendChild(document.createTextNode(event_date.getDate() +'. ' +  (event_date.getMonth() + 1) + '. ' + event_date.getFullYear()));
								event_date_dom.setAttribute('id', 'datum_dogodka');
								break;
							}
						}

						// sestavi vsebino
						event_data_dom_data.insertBefore(event_date_dom, event_data_dom_data.childNodes[0]);
						$( '#napovednik_okvir' ).find('#napovednik_dogodki').html(event_data_dom.innerHTML);
						$( '#napovednik_okvir' ).show();
					}
				} );
				//SPREMEMMBA skocjan - izpis dogodkov - konec
				//SPREMEMMBA skocjan - imamo svoj izpis dogodkov, ne potrebujemo izpisa v obliki pop-upov (komentirano)
				/**
				*eventBubbles.each( function( e, i ) {
				*	$( i ).qtip( {
				*		content : width < 60 ? $( cell ).find( 'ul.simcal-events' ) : $( i ).find( '> .simcal-tooltip-content' ),
				*		position: {
				*			my      : 'top center',
				*			at      : 'bottom center',
				*			target  : $(i),
				*			viewport: width < 60 ? $( window ) : true,
				*			adjust  : {
				*				method: 'shift',
				*				scroll: false
				*			}
				*		},
				*		style   : {
				*			def    : false,
				*			classes: 'simcal-default-calendar simcal-event-bubble'
				*		},
				*		show    : {
				*			solo  : true,
				*			effect: false,
				*			event : bubbleTrigger == 'hover' ? 'mouseenter' : 'click'
				*		},
				*		hide    : {
				*			fixed : true,
				*			effect: false,
				*			event : bubbleTrigger == 'click' ? 'unfocus' : 'mouseleave',
				*			delay: 100
				*		},
				*		events  : {
				*			show: function( event, current ) {
				*				// Hide when another tooltip opens:
				*				if ( last && last.id ) {
				*					if ( last.id != current.id ) {
				*						last.hide();
				*					}
				*				}
				*				last = current;
				*			}
				*		}
				*	} );
				*
				*} );
				*/
				//SPREMEMMBA skocjan - imamo svoj izpis dogodkov, ne potrebujemo izpisa v obliki pop-upov - end

			} );

		}

		// Event bubbles and calendar UI triggers.
		gridCalendars.each( function( e, calendar ) {
			calendarBubbles( calendar );
			$( calendar ).on( 'change', function() {
				calendarBubbles( this );
			} );
		} );
		// Viewport changes might require triggering calendar mobile mode.
		window.onresize = function() {
			gridCalendars.each( function( e, calendar ) {
				calendarBubbles( calendar );
			} );
		};

		/**
		 * Toggle to expand events.
		 */
		function expandEventsToggle() {
			$( '.simcal-events-toggle' ).each( function( e, button ) {

				var list    = $( button ).prev( '.simcal-events' ),
					toggled = list.find( '.simcal-event-toggled' ),
					arrow   = $( button ).find( 'i' );

				$( button ).on( 'click', function() {
					arrow.toggleClass( 'simcal-icon-rotate-180' );
					toggled.slideToggle();
				});

			});
		}
		expandEventsToggle();

	} );

	//SPREMEMMBA skocjan - manipuliraj izpis dogodkov iz koledarja
	var event_frame = jQuery( '#napovednik_okvir' );
	event_frame.find( '#napovednik_kontrola' ).find( '.napovednik_kontrola_zapri' ).on( 'click', function() {
		// skrij izpis dogodkov
		event_frame.hide();
		event_frame.find( '#napovednik_dogodki' ).html( '' );
		// pocisti oznacbo izbranega dneva
		jQuery( '.simcal-selected').each( function( idx, s) {
			jQuery( s ).removeClass('simcal-selected');
		});
	});
	//SPREMEMMBA skocjan - manipuliraj izpis dogodkov iz koledarja - konec
} )( this );
