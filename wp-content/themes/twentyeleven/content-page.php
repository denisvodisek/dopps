<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
?><div id="col_1_sidebar_1">
            	<div id="sidebar_left">

                <div class="menu">

                	<?php
						simarine_page_hierarchy_menu(get_the_ID());

					?>
                    &nbsp;
			 </div><!-- .menu -->

                </div><!-- #sidebar_left -->


           		<div id="main_left_col">
                    <span id="nav_pointer">
                    	<a href="<?php echo get_site_url();?>"><?php _e("Home", "twentyeleven"); ?></a>
                    	<?php $anc = get_ancestors( get_the_ID(), 'page' );
							$anc = array_reverse($anc);
							foreach($anc as $id){
								$current++;
								$p = get_post($id);
								?>
								&gt; <a href="<?php echo get_permalink($p->ID);?>"><?php echo $p->post_title; ?></a>
								<?php
							}
						?>
                    	&gt; <a href="<?php echo get_permalink(get_the_ID()); ?>" class="active"><?php the_title(); ?></a>

                    </span>
                    <div class="clear"></div>

                    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                        <header class="entry-header">
                            <h1><?php the_title(); ?></h1>
                        </header><!-- .entry-header -->

                        <div class="entry-content">
                            <?php the_content(); ?>
                            <?php wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'twentyeleven' ) . '</span>', 'after' => '</div>' ) ); ?>
                        </div><!-- .entry-content -->
                        <footer class="entry-meta">
                            <?php edit_post_link( __( 'Edit', 'twentyeleven' ), '<span class="edit-link">', '</span>' ); ?>
                        </footer><!-- .entry-meta -->
                    </article><!-- #post-<?php the_ID(); ?> -->


                </div><!-- #main_left_col -->


            </div><!-- #col_1_sidebar_1 -->

