<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
?>


<?php

//Variables used:
/*
$newsletter = array(
	'title' => '',
	'senderName' => '',
	'recipientName' => '',
	'recipientEmail' => '',
	'photoUrl' => '',
	'photoTitle' => '',
	'photoAuthor' => '',
	'senderText' => '',
	'isEmail' => false
);
*/
?>

<style>

	#newsletter_preview{
		border: 1px solid #fff;
		margin:10px;
		background:white;
	}
	#newsletter_header{
		background: #016ba7;
	}
	#newsletter_header img{
		margin:-3px;
	}
	#newsletter_title_holder{
		padding: 10px;
		background: #fff;
		font-size: 17px;
		color: #016ba7;
	}
	#newsletter_title_holder textarea{
		width:98%;
		min-width:98%;
		max-width:98%;
		font-size: 17px;
		line-height: 17px;
		height: 20px;
		background:none;
		color:#016ba7;
		border:	1px solid #fff;
	}

	#newsletter_content{
		margin:0px 10px 0px 10px;
		padding-bottom:20px;
	}
	#newsletter_meta{
		margin:10px 0px 10px 0px;
	}
	.news_green{
		color: #f8613b;
	}
	#newsletter_content input{
		color: #323232;
		border:	1px solid #EEE;
	}

	#newsletter_content img{
		width:100%;
	}
	#newsletter_text_holder{

	}
	#newsletter_content textarea{
		width:98%;
		min-width:98%;
		max-width:98%;
		border:none;
		font-size: 14px;
		color: #323232;
		border:	1px solid #fff;
	}
/* Oblikovanje dialoga */
#fading-background{
	position: absolute;
	z-index: 100;
	background-color: #999;
	opacity: 0.80;
	width: 440px;
	height: 100%;
	margin-top: 73px;
	display: none;
	padding: 15px;
	}
#fading{
	position: absolute;
	z-index: 200;
	top: 350px;
	text-align: center;
	width: 460px;
	background-color: #EFEDEE;
	padding: 5px;
	display: none;
}
#textbox{
	background-color: #FFF;
	margin: 10px;
	padding: 10px;
}
#text{
	font-size: 20px;
}

#newsletter_sendbutton{
	cursor: pointer;
	background: no-repeat center url('<?php echo get_stylesheet_directory_uri(); ?>/images/e-razglednice_submit<?php _e("_EN.jpg", "twentyeleven"); ?>');
	height: 25px;
	width: 91px;
	float: right;
}
#newsletter_sendbutton:hover{
	background: no-repeat center url('<?php echo get_stylesheet_directory_uri(); ?>/images/e-razgednice_submit_hover<?php _e("_EN.jpg", "twentyeleven"); ?>');
}


</style>

<?php if($newsletter['isEmail']){
	//Ves CSS mora bit inline!
?>
	<div class="info_board" style="margin-top: 10px; width:690px;background: #efedee; padding: 5px;">
		<div id="newsletter_preview" style="    border: 1px solid #fff;margin: 10px; background: white;">
			<div id="newsletter_header">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
					<img src="<?php bloginfo( 'template_url' ); ?>/bkg/skocjanski_zatok_logo.svg"  width="668" alt="Logo Škocjanski zatok" title="<?php bloginfo( 'description' ); ?>" />
				</a>
			</div>
			<div id="newsletter_title_holder" style=" padding: 10px;">
				<span name="newsletter_title" id="newsletter_title" style="line-height: 17px;padding: 10px; background: #fff;	font-size: 17px; color: #016ba7;"><?= $newsletter['title']; ?></span>
			</div>
			<div id="newsletter_content" style="margin: 0px 10px 0px 10px; padding-bottom: 20px;">
				<img id="newsletter_image" src="<?= $newsletter['photoUrl']; ?>" title="<?= $newsletter['photoTitle']; ?>" style="width:649px;margin: 3px auto 10px auto;" />
				<span id="newsletter_author" class="meta" style="color: #9b9b9b; font-size: 11px !important;"><?php _e('Photo', 'twentyeleven'); ?>: <?= $newsletter['photoAuthor']; ?> </span>
				<div class="bottom_border" style="border-bottom: 1px solid #c7c7c7; padding-bottom: 7px; margin-bottom: 11px;"></div>
				<div id="newsletter_text_holder">
					<span name="newsletter_text" id="newsletter_text" style="font-size: 14px; color: #323232;"><?= $newsletter['senderText']; ?></span>
					<br />
					<span id="nl_senders_name" style="float:right; margin: 0px 30px 30px 0px; "><?= $newsletter['senderName']; ?></span>
					<div style="clear: both;"></div>
				</div>
			</div>
		</div>

	</div><!-- .info_board -->
<?php }else{ ?>
	<div class="info_board">
		<div id="newsletter_preview">
			<div id="newsletter_header">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
					<img src="<?php bloginfo( 'template_url' ); ?>/bkg/skocjanski_zatok_logo_enotni.svg"  width="232.8" height="100" alt="Logo Škocjanski zatok" title="<?php bloginfo( 'description' ); ?>" />
				</a>
			</div>
			<div id="newsletter_title_holder">
				<textarea name="newsletter_title" id="newsletter_title" placeholder="<?php _e('Title', 'twentyeleven'); ?>" value="<?= $newsletter['title']; ?>"></textarea>
			</div>
			<div id="newsletter_content">
				<div id="newsletter_meta">
					<span class='news_green'><?php _e('Sender', 'twentyeleven'); ?>:</span> <span id="nl_yourname"><input onchange="podpis()" size="24" type="text" id="nl_senders_name" name="senders_name" placeholder="<?php _e("Your name", "twentyeleven");?>"  value="<?= $newsletter['senderName']; ?>"/></span> <br />
					<span class='news_green'><?php _e('Recipient', 'twentyeleven'); ?>:</span> <span id="nl_friendsname"><input type="text" id="ns_friends_name"  name="friends_name" placeholder="<?php _e("Your friend's name", "twentyeleven");?>"  value="<?= $newsletter['recipientName']; ?>"/></span> - <span id="nl_friendsemail"><input style="width: 150px;" type="text" id="nl_friends_email" name="friends_email" placeholder="<?php _e("Your friend's e-mail", "twentyeleven");?>"  value="<?= $newsletter['recipientEmail']; ?>"/></span>
				</div>
				<img id="newsletter_image" src="<?= $newsletter['photoUrl']; ?>" title="<?= $newsletter['photoTitle']; ?>" />
				<span id="newsletter_author" class="meta"><?php _e('Photo', 'twentyeleven'); ?>: <?= $newsletter['photoAuthor']; ?> </span>
				<div class="bottom_border"></div>
				<div id="newsletter_text_holder">
					<textarea name="newsletter_text" id="newsletter_text" placeholder="<?php _e('Write text here', 'twentyeleven'); ?>" value="<?= $newsletter['senderText']; ?>"></textarea>
				</div>
			</div>
            <div style="margin: 0 15px 10px 0; height: 30px">
            <p id="podpis"></p>
            </div>
			<input id="image_src" type="hidden" value="<?= $newsletter['photoUrl']; ?>" />
			<input id="image_title" type="hidden" value="<?= $newsletter['photoTitle']; ?>" />
			<input id="image_author" type="hidden" value="<?= $newsletter['photoAuthor']; ?>" />

			<div style="margin: 0 15px 10px 0; height: 30px">
				<div id="newsletter_sendbutton">
					&nbsp;
				</div>
			</div>
		</div>

	</div><!-- .info_board -->
<script>
    function podpis() {
            var sender = document.getElementById("nl_senders_name").value;
            document.getElementById("podpis").innerHTML = sender;
    }
</script>
<?php } ?>
