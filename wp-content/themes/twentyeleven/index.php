<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 */

get_header(); ?>

		<div id="col_1_sidebar_2">
            	<div id="sidebar_left">

                <div class="menu">
                	<?php
						//simarine_page_hierarchy_menu(get_the_ID());
				//	get_sidebar('left');
					//simarine_archives_menu();
					sz_simarine_archives_menu();
					?>
			 </div><!-- .menu -->

                </div><!-- #sidebar_left -->
               <div id="sidebar_right">

                <div class="menu">
                <span style="font-size:0.9em;">
                <?php

                	//simarine_posts_in_month();
					sz_simarine_posts_in_month();
				?>
                </span>
                </div><!-- .menu -->

                </div><!-- #sidebar_right -->

           		<div id="main_left_col">
                    <span id="nav_pointer">
                    	<a href="<?php echo get_site_url();?>"><?php _e('Home', 'twentyeleven'); ?></a>
                    	<?php $anc = get_ancestors( get_the_ID(), 'page' );
							$anc = array_reverse($anc);
							foreach($anc as $id){
								$current++;
								$p = get_post($id);
								?>
								> <a href="<?php echo get_permalink($p->ID);?>"><?php echo $p->post_title; ?></a>
								<?
							}
						?>
                    	> <a href="<?php echo get_permalink(get_the_ID()); ?>" class="active"><?php the_title(); ?></a>

                    </span>
                    <div class="clear"></div>

			<?php if ( have_posts() ) : ?>

				<?php twentyeleven_content_nav( 'nav-above' ); ?>

				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>

					<?php
					get_template_part( 'content', get_post_format() ); ?>

				<?php endwhile; ?>

				<?php twentyeleven_content_nav( 'nav-below' ); ?>

			<?php else : ?>

				<article id="post-0" class="post no-results not-found">
					<header class="entry-header">
						<h1 class="entry-title"><?php _e( 'Nothing Found', 'twentyeleven' ); ?></h1>
					</header><!-- .entry-header -->

					<div class="entry-content">
						<p><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'twentyeleven' ); ?></p>
						<?php get_search_form(); ?>
					</div><!-- .entry-content -->
				</article><!-- #post-0 -->

			<?php endif; ?>



                </div><!-- #main_left_col -->


            </div><!-- #col_1_sidebar_2 -->


<?php get_footer(); ?>