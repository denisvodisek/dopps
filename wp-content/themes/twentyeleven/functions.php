<?php
/**
 * Twenty Eleven functions and definitions
 *
 * Sets up the theme and provides some helper functions. Some helper functions
 * are used in the theme as custom template tags. Others are attached to action and
 * filter hooks in WordPress to change core functionality.
 *
 * The first function, twentyeleven_setup(), sets up the theme by registering support
 * for various features in WordPress, such as post thumbnails, navigation menus, and the like.
 *
 * When using a child theme (see http://codex.wordpress.org/Theme_Development and
 * http://codex.wordpress.org/Child_Themes), you can override certain functions
 * (those wrapped in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before the parent
 * theme's file, so the child theme functions would be used.
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are instead attached
 * to a filter or action hook. The hook can be removed by using remove_action() or
 * remove_filter() and you can attach your own function to the hook.
 *
 * We can remove the parent theme's hook only after it is attached, which means we need to
 * wait until setting up the child theme:
 *
 * <code>
 * add_action( 'after_setup_theme', 'my_child_theme_setup' );
 * function my_child_theme_setup() {
 *     // We are providing our own filter for excerpt_length (or using the unfiltered value)
 *     remove_filter( 'excerpt_length', 'twentyeleven_excerpt_length' );
 *     ...
 * }
 * </code>
 *
 * For more information on hooks, actions, and filters, see http://codex.wordpress.org/Plugin_API.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) )
	$content_width = 578;

/**
 * Tell WordPress to run twentyeleven_setup() when the 'after_setup_theme' hook is run.
 */
add_action( 'after_setup_theme', 'twentyeleven_setup' );

if ( ! function_exists( 'twentyeleven_setup' ) ):
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 *
 * To override twentyeleven_setup() in a child theme, add your own twentyeleven_setup to your child theme's
 * functions.php file.
 *
 * @uses load_theme_textdomain() For translation/localization support.
 * @uses add_editor_style() To style the visual editor.
 * @uses add_theme_support() To add support for post thumbnails, automatic feed links, and Post Formats.
 * @uses register_nav_menus() To add support for navigation menus.
 * @uses add_custom_background() To add support for a custom background.
 * @uses add_custom_image_header() To add support for a custom header.
 * @uses register_default_headers() To register the default custom header images provided with the theme.
 * @uses set_post_thumbnail_size() To set a custom post thumbnail size.
 *
 * @since Twenty Eleven 1.0
 */
function twentyeleven_setup() {

	/* Make Twenty Eleven available for translation.
	 * Translations can be added to the /languages/ directory.
	 * If you're building a theme based on Twenty Eleven, use a find and replace
	 * to change 'twentyeleven' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'twentyeleven', get_template_directory() . '/languages' );

	$locale = get_locale();
	$locale_file = get_template_directory() . "/languages/$locale.php";
	if ( is_readable( $locale_file ) )
		require_once( $locale_file );

	// This theme styles the visual editor with editor-style.css to match the theme style.
	add_editor_style();

	// Load up our theme options page and related code.
	require( get_template_directory() . '/inc/theme-options.php' );

	// Grab Twenty Eleven's Ephemera widget.
	require( get_template_directory() . '/inc/widgets.php' );

	// Add default posts and comments RSS feed links to <head>.
	add_theme_support( 'automatic-feed-links' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menu( 'primary', __( 'Primary Menu', 'twentyeleven' ) );
	register_nav_menu( 'secondary', __( 'Secondary Menu', 'twentyeleven' ) );
	register_nav_menu( 'top', __( 'Top Menu', 'twentyeleven' ) );
	register_nav_menu( 'side', __( 'Right Side Menu', 'twentyeleven' ) );

	// Add support for a variety of post formats
	add_theme_support( 'post-formats', array( 'aside', 'link', 'gallery', 'status', 'quote', 'image' ) );

	// Add support for custom backgrounds
	//add_custom_background();

	// This theme uses Featured Images (also known as post thumbnails) for per-post/per-page Custom Header images
	add_theme_support( 'post-thumbnails' );

	// The next four constants set how Twenty Eleven supports custom headers.

	// The default header text color
	define( 'HEADER_TEXTCOLOR', '000' );

	// By leaving empty, we allow for random image rotation.
	define( 'HEADER_IMAGE', '' );

	// The height and width of your custom header.
	// Add a filter to twentyeleven_header_image_width and twentyeleven_header_image_height to change these values.
	define( 'HEADER_IMAGE_WIDTH', apply_filters( 'twentyeleven_header_image_width', 1000 ) );
	define( 'HEADER_IMAGE_HEIGHT', apply_filters( 'twentyeleven_header_image_height', 288 ) );

	// We'll be using post thumbnails for custom header images on posts and pages.
	// We want them to be the size of the header image that we just defined
	// Larger images will be auto-cropped to fit, smaller ones will be ignored. See header.php.
	set_post_thumbnail_size( HEADER_IMAGE_WIDTH, HEADER_IMAGE_HEIGHT, true );

	// Add Twenty Eleven's custom image sizes
	add_image_size( 'large-feature', HEADER_IMAGE_WIDTH, HEADER_IMAGE_HEIGHT, true ); // Used for large feature (header) images
	add_image_size( 'small-feature', 500, 300 ); // Used for featured posts if a large-feature doesn't exist

	//simarine custom image sizes:
	add_image_size('frontpage_main_slider', 640, 328,true);
	add_image_size('newsletter_thumb_wide', 185, 120,true);
	add_image_size('newsletter_thumb', 120, 120,true);
	add_image_size('newsletter_full', 428);
	add_image_size('page_thumb', 175, 117,true);
	add_image_size('article_picture', 290);
	add_image_size('galery_preview', 166, 111,true);


	// Turn on random header image rotation by default.
	//add_theme_support( 'custom-header', array( 'random-default' => true ) );

	// Add a way for the custom header to be styled in the admin panel that controls
	// custom headers. See twentyeleven_admin_header_style(), below.
	//add_custom_image_header( 'twentyeleven_header_style', 'twentyeleven_admin_header_style', 'twentyeleven_admin_header_image' );

	// ... and thus ends the changeable header business.

	// Default custom headers packaged with the theme. %s is a placeholder for the theme template directory URI.
	register_default_headers( array(
		'wheel' => array(
			'url' => '%s/images/headers/wheel.jpg',
			'thumbnail_url' => '%s/images/headers/wheel-thumbnail.jpg',
			/* translators: header image description */
			'description' => __( 'Wheel', 'twentyeleven' )
		),
		'shore' => array(
			'url' => '%s/images/headers/shore.jpg',
			'thumbnail_url' => '%s/images/headers/shore-thumbnail.jpg',
			/* translators: header image description */
			'description' => __( 'Shore', 'twentyeleven' )
		),
		'trolley' => array(
			'url' => '%s/images/headers/trolley.jpg',
			'thumbnail_url' => '%s/images/headers/trolley-thumbnail.jpg',
			/* translators: header image description */
			'description' => __( 'Trolley', 'twentyeleven' )
		),
		'pine-cone' => array(
			'url' => '%s/images/headers/pine-cone.jpg',
			'thumbnail_url' => '%s/images/headers/pine-cone-thumbnail.jpg',
			/* translators: header image description */
			'description' => __( 'Pine Cone', 'twentyeleven' )
		),
		'chessboard' => array(
			'url' => '%s/images/headers/chessboard.jpg',
			'thumbnail_url' => '%s/images/headers/chessboard-thumbnail.jpg',
			/* translators: header image description */
			'description' => __( 'Chessboard', 'twentyeleven' )
		),
		'lanterns' => array(
			'url' => '%s/images/headers/lanterns.jpg',
			'thumbnail_url' => '%s/images/headers/lanterns-thumbnail.jpg',
			/* translators: header image description */
			'description' => __( 'Lanterns', 'twentyeleven' )
		),
		'willow' => array(
			'url' => '%s/images/headers/willow.jpg',
			'thumbnail_url' => '%s/images/headers/willow-thumbnail.jpg',
			/* translators: header image description */
			'description' => __( 'Willow', 'twentyeleven' )
		),
		'hanoi' => array(
			'url' => '%s/images/headers/hanoi.jpg',
			'thumbnail_url' => '%s/images/headers/hanoi-thumbnail.jpg',
			/* translators: header image description */
			'description' => __( 'Hanoi Plant', 'twentyeleven' )
		)
	) );
}
endif; // twentyeleven_setup


add_filter('image_size_names_choose', 'my_image_sizes');
function my_image_sizes($sizes) {
        $addsizes = array(
                "newsletter_full" => "1col_1side  2 imgs in a row"
                );

			/*
			//simarine custom image sizes:
			add_image_size('frontpage_main_slider', 640, 328,true);
			add_image_size('newsletter_thumb_wide', 185, 120,true);
			add_image_size('newsletter_thumb', 120, 120,true);
			add_image_size('newsletter_full', 428);
			add_image_size('page_thumb', 175, 117,true);
			add_image_size('article_picture', 290);
			add_image_size('galery_preview', 166, 111,true);
			*/

        $newsizes = array_merge($sizes, $addsizes);
        return $newsizes;
}


if ( ! function_exists( 'twentyeleven_header_style' ) ) :
/**
 * Styles the header image and text displayed on the blog
 *
 * @since Twenty Eleven 1.0
 */
function twentyeleven_header_style() {

	// If no custom options for text are set, let's bail
	// get_header_textcolor() options: HEADER_TEXTCOLOR is default, hide text (returns 'blank') or any hex value
	if ( HEADER_TEXTCOLOR == get_header_textcolor() )
		return;
	// If we get this far, we have custom styles. Let's do this.
	?>
	<style type="text/css">
	<?php
		// Has the text been hidden?
		if ( 'blank' == get_header_textcolor() ) :
	?>
		#site-title,
		#site-description {
			position: absolute !important;
			clip: rect(1px 1px 1px 1px); /* IE6, IE7 */
			clip: rect(1px, 1px, 1px, 1px);
		}
	<?php
		// If the user has set a custom color for the text use that
		else :
	?>
		#site-title a,
		#site-description {
			color: #<?php echo get_header_textcolor(); ?> !important;
		}
	<?php endif; ?>
	</style>
	<?php
}
endif; // twentyeleven_header_style

if ( ! function_exists( 'twentyeleven_admin_header_style' ) ) :
/**
 * Styles the header image displayed on the Appearance > Header admin panel.
 *
 * Referenced via add_custom_image_header() in twentyeleven_setup().
 *
 * @since Twenty Eleven 1.0
 */
function twentyeleven_admin_header_style() {
?>
	<style type="text/css">
	.appearance_page_custom-header #headimg {
		border: none;
	}
	#headimg h1,
	#desc {
		font-family: "Helvetica Neue", Arial, Helvetica, "Nimbus Sans L", sans-serif;
	}
	#headimg h1 {
		margin: 0;
	}
	#headimg h1 a {
		font-size: 32px;
		line-height: 36px;
		text-decoration: none;
	}
	#desc {
		font-size: 14px;
		line-height: 23px;
		padding: 0 0 3em;
	}
	<?php
		// If the user has set a custom color for the text use that
		if ( get_header_textcolor() != HEADER_TEXTCOLOR ) :
	?>
		#site-title a,
		#site-description {
			color: #<?php echo get_header_textcolor(); ?>;
		}
	<?php endif; ?>
	#headimg img {
		max-width: 1000px;
		height: auto;
		width: 100%;
	}
	</style>
<?php
}
endif; // twentyeleven_admin_header_style

if ( ! function_exists( 'twentyeleven_admin_header_image' ) ) :
/**
 * Custom header image markup displayed on the Appearance > Header admin panel.
 *
 * Referenced via add_custom_image_header() in twentyeleven_setup().
 *
 * @since Twenty Eleven 1.0
 */
function twentyeleven_admin_header_image() { ?>
	<div id="headimg">
		<?php
		if ( 'blank' == get_theme_mod( 'header_textcolor', HEADER_TEXTCOLOR ) || '' == get_theme_mod( 'header_textcolor', HEADER_TEXTCOLOR ) )
			$style = ' style="display:none;"';
		else
			$style = ' style="color:#' . get_theme_mod( 'header_textcolor', HEADER_TEXTCOLOR ) . ';"';
		?>
		<h1><a id="name"<?php echo $style; ?> onclick="return false;" href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php bloginfo( 'name' ); ?></a></h1>
		<div id="desc"<?php echo $style; ?>><?php bloginfo( 'description' ); ?></div>
		<?php $header_image = get_header_image();
		if ( ! empty( $header_image ) ) : ?>
			<img src="<?php echo esc_url( $header_image ); ?>" alt="" />
		<?php endif; ?>
	</div>
<?php }
endif; // twentyeleven_admin_header_image

/**
 * Sets the post excerpt length to 40 words.
 *
 * To override this length in a child theme, remove the filter and add your own
 * function tied to the excerpt_length filter hook.
 */
function twentyeleven_excerpt_length( $length ) {
	return 40;
}
add_filter( 'excerpt_length', 'twentyeleven_excerpt_length' );

/**
 * Returns a "Continue Reading" link for excerpts
 */
function twentyeleven_continue_reading_link() {
	return ' <a href="'. esc_url( get_permalink() ) . '">' . __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'twentyeleven' ) . '</a>';
}

/**
 * Replaces "[...]" (appended to automatically generated excerpts) with an ellipsis and twentyeleven_continue_reading_link().
 *
 * To override this in a child theme, remove the filter and add your own
 * function tied to the excerpt_more filter hook.
 */
function twentyeleven_auto_excerpt_more( $more ) {
	return ' &hellip;' . twentyeleven_continue_reading_link();
}
add_filter( 'excerpt_more', 'twentyeleven_auto_excerpt_more' );

/**
 * Adds a pretty "Continue Reading" link to custom post excerpts.
 *
 * To override this link in a child theme, remove the filter and add your own
 * function tied to the get_the_excerpt filter hook.
 */
function twentyeleven_custom_excerpt_more( $output ) {
	if ( has_excerpt() && ! is_attachment() ) {
		$output .= twentyeleven_continue_reading_link();
	}
	return $output;
}
add_filter( 'get_the_excerpt', 'twentyeleven_custom_excerpt_more' );

/**
 * Get our wp_nav_menu() fallback, wp_page_menu(), to show a home link.
 */
function twentyeleven_page_menu_args( $args ) {
	$args['show_home'] = true;
	return $args;
}
add_filter( 'wp_page_menu_args', 'twentyeleven_page_menu_args' );


function nav_menu_remove_last_seperator( $items, $args ) {
	$position = strrpos($items, $args->after, -1);
	$items = substr_replace($items, '', $position, strlen($args->after));
	return $items;
}
add_filter( 'wp_nav_menu_items', 'nav_menu_remove_last_seperator', 10, 2 );

/**
 * Register our sidebars and widgetized areas. Also register the default Epherma widget.
 *
 * @since Twenty Eleven 1.0
 */
function twentyeleven_widgets_init() {

	register_widget( 'Twenty_Eleven_Ephemera_Widget' );

	register_sidebar( array(
		'name' => __( 'Main Sidebar', 'twentyeleven' ),
		'id' => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => "</aside>",
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	register_sidebar( array(
		'name' => __( 'Showcase Sidebar', 'twentyeleven' ),
		'id' => 'sidebar-2',
		'description' => __( 'The sidebar for the optional Showcase Template', 'twentyeleven' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => "</aside>",
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	register_sidebar( array(
		'name' => __( 'Footer Area One', 'twentyeleven' ),
		'id' => 'sidebar-3',
		'description' => __( 'An optional widget area for your site footer', 'twentyeleven' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => "</aside>",
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	register_sidebar( array(
		'name' => __( 'Footer Area Two', 'twentyeleven' ),
		'id' => 'sidebar-4',
		'description' => __( 'An optional widget area for your site footer', 'twentyeleven' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => "</aside>",
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	register_sidebar( array(
		'name' => __( 'Footer Area Three', 'twentyeleven' ),
		'id' => 'sidebar-5',
		'description' => __( 'An optional widget area for your site footer', 'twentyeleven' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => "</aside>",
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
}
add_action( 'widgets_init', 'twentyeleven_widgets_init' );

if ( ! function_exists( 'twentyeleven_content_nav' ) ) :
/**
 * Display navigation to next/previous pages when applicable
 */
function twentyeleven_content_nav( $nav_id ) {
	global $wp_query;

	if ( $wp_query->max_num_pages > 1 ) : ?>
		<nav id="<?php echo $nav_id; ?>">
			<h3 class="assistive-text"><?php _e( 'Post navigation', 'twentyeleven' ); ?></h3>
			<div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Older posts', 'twentyeleven' ) ); ?></div>
			<div class="nav-next"><?php previous_posts_link( __( 'Newer posts <span class="meta-nav">&rarr;</span>', 'twentyeleven' ) ); ?></div>

		</nav><!-- #nav-above -->
	<?php endif;
}
endif; // twentyeleven_content_nav

/**
 * Return the URL for the first link found in the post content.
 *
 * @since Twenty Eleven 1.0
 * @return string|bool URL or false when no link is present.
 */
function twentyeleven_url_grabber() {
	if ( ! preg_match( '/<a\s[^>]*?href=[\'"](.+?)[\'"]/is', get_the_content(), $matches ) )
		return false;

	return esc_url_raw( $matches[1] );
}

/**
 * Count the number of footer sidebars to enable dynamic classes for the footer
 */
function twentyeleven_footer_sidebar_class() {
	$count = 0;

	if ( is_active_sidebar( 'sidebar-3' ) )
		$count++;

	if ( is_active_sidebar( 'sidebar-4' ) )
		$count++;

	if ( is_active_sidebar( 'sidebar-5' ) )
		$count++;

	$class = '';

	switch ( $count ) {
		case '1':
			$class = 'one';
			break;
		case '2':
			$class = 'two';
			break;
		case '3':
			$class = 'three';
			break;
	}

	if ( $class )
		echo 'class="' . $class . '"';
}

if ( ! function_exists( 'twentyeleven_comment' ) ) :
/**
 * Template for comments and pingbacks.
 *
 * To override this walker in a child theme without modifying the comments template
 * simply create your own twentyeleven_comment(), and that function will be used instead.
 *
 * Used as a callback by wp_list_comments() for displaying the comments.
 *
 * @since Twenty Eleven 1.0
 */
function twentyeleven_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case 'pingback' :
		case 'trackback' :
	?>
	<li class="post pingback">
		<p><?php _e( 'Pingback:', 'twentyeleven' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( __( 'Edit', 'twentyeleven' ), '<span class="edit-link">', '</span>' ); ?></p>
	<?php
			break;
		default :
	?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
		<article id="comment-<?php comment_ID(); ?>" class="comment">
			<footer class="comment-meta">
				<div class="comment-author vcard">
					<?php
						$avatar_size = 68;
						if ( '0' != $comment->comment_parent )
							$avatar_size = 39;

						echo get_avatar( $comment, $avatar_size );

						/* translators: 1: comment author, 2: date and time */
						printf( __( '%1$s on %2$s <span class="says">said:</span>', 'twentyeleven' ),
							sprintf( '<span class="fn">%s</span>', get_comment_author_link() ),
							sprintf( '<a href="%1$s"><time pubdate datetime="%2$s">%3$s</time></a>',
								esc_url( get_comment_link( $comment->comment_ID ) ),
								get_comment_time( 'c' ),
								/* translators: 1: date, 2: time */
								sprintf( __( '%1$s at %2$s', 'twentyeleven' ), get_comment_date(), get_comment_time() )
							)
						);
					?>

					<?php edit_comment_link( __( 'Edit', 'twentyeleven' ), '<span class="edit-link">', '</span>' ); ?>
				</div><!-- .comment-author .vcard -->

				<?php if ( $comment->comment_approved == '0' ) : ?>
					<em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'twentyeleven' ); ?></em>
					<br />
				<?php endif; ?>

			</footer>

			<div class="comment-content"><?php comment_text(); ?></div>

			<div class="reply">
				<?php comment_reply_link( array_merge( $args, array( 'reply_text' => __( 'Reply <span>&darr;</span>', 'twentyeleven' ), 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
			</div><!-- .reply -->
		</article><!-- #comment-## -->

	<?php
			break;
	endswitch;
}
endif; // ends check for twentyeleven_comment()

if ( ! function_exists( 'twentyeleven_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 * Create your own twentyeleven_posted_on to override in a child theme
 *
 * @since Twenty Eleven 1.0
 */
function twentyeleven_posted_on() {
	printf( __( '<span class="sep"><time class="entry-date" datetime="%1$s" pubdate>%2$s</time></span>', 'twentyeleven' ),
		esc_attr( get_the_time() ),
		esc_html( get_the_date('d.m.Y') )
	);
}
endif;

/**
 * Adds two classes to the array of body classes.
 * The first is if the site has only had one author with published posts.
 * The second is if a singular post being displayed
 *
 * @since Twenty Eleven 1.0
 */
function twentyeleven_body_classes( $classes ) {

	if ( function_exists( 'is_multi_author' ) && ! is_multi_author() )
		$classes[] = 'single-author';

	if ( is_singular() && ! is_home() && ! is_page_template( 'showcase.php' ) && ! is_page_template( 'sidebar-page.php' ) )
		$classes[] = 'singular';

	return $classes;
}
add_filter( 'body_class', 'twentyeleven_body_classes' );


/**
 * Renames special chars like š, č, ž, ć, đ
 *
 * @since Twenty Eleven 1.0
 */
function simarine_rename_upload_file($filename) {

	$spanish_chars = array('/µ/','/þ/','/¦̂/','/î/','/š/','/Š/','/ž/','/Ž/','/č/','/Č/','/ć/','/Ć/','/đ/','/Đ/', '/á/', '/é/', '/í/', '/ó/', '/ú/', '/ü/', '/ñ/', '/Á/', '/É/', '/Í/', '/Ó/', '/Ú/', '/Ü/', '/Ñ/', '/º/', '/ª/' );
	$sanitized_chars = array('','','','','/s/','/S/','/z/','/Z/','/c/','/C/','/c/','/C/','/d/','/D/','a', 'e', 'i', 'o', 'u', 'u', 'n', 'A', 'E', 'I', 'O', 'U', 'U', 'N', 'o', 'a');

	$friendly_filename = preg_replace($spanish_chars, $sanitized_chars, $filename);
	return $friendly_filename;
}
add_filter('sanitize_file_name', 'simarine_rename_upload_file', 1);



/**
 * Builds and shows the standard multy-level menu for pages.
 *
 * @since Twenty Eleven 1.0
 */

function simarine_page_hierarchy_menu($current_id){
	$ancestors = get_ancestors( $current_id, 'page' );
	$ancestors = array_reverse($ancestors);
	if(empty($ancestors))
		$root_id = $current_id;
	else
		$root_id = $ancestors[0];
	echo get_simarine_construct_menu_recursive($root_id, $current_id, $ancestors, 0);
}

function get_simarine_construct_menu_recursive($parent, $active_id, $active_ancestors,  $level){
	$children = get_pages( array( 'child_of' => $parent, 'parent' => $parent, 'order' => 'ASC' ) );
	usort($children, "posts_order_by_menu_order");

	$menu = "";
	foreach($children as $page){
		if($level == 0)
			if(in_array($page->ID, $active_ancestors) || $page->ID == $active_id)
				$menu .= '<div class="active">';
			else
				$menu .= '<div>';

		if($page->ID == $active_id)
			$menu .='<a class="active" href="'.get_permalink($page->ID).'">'.$page->post_title.'</a>';
		else
			$menu .='<a href="'.get_permalink($page->ID).'">'.$page->post_title.'</a>';

		$menu .='<div>'.get_simarine_construct_menu_recursive($page->ID, $active_id, $active_ancestors, $level + 1).'</div>';

		if($level == 0)
			$menu .= '</div>';
	}
	return $menu;
}

function posts_order_by_menu_order($a_x, $b_x){
	$a =$a_x->menu_order;
	$b =$b_x->menu_order;

    if ($a == $b) {
		$a =$a_x->post_title;
		$b =$b_x->post_title;
        return strcmp($a, $b);
    }
    return ($a < $b) ? 1 : -1;
}


/**
 * Builds and shows links to posts in month
 *
 * @since Twenty Eleven 1.0
 */

function simarine_posts_in_month($year = '', $month = '', $cat = ''){

	$args = array(
		'cat'            => $cat,
		'type'            => 'postbypost',
		'format'          => 'custom',
		'limit'          => '',
		'before'          => '',
		'after'           => '',
		'show_post_count' => false,
		'echo'            => 0
	);
	if($year == '' && $month == ''){
		$args['limit'] = '7';
	}
	$posts =  simarine_get_archives( $args, $year, $month);
	$links ="";

	foreach($posts as $post){
		$links .= "<a href='".$post['url']."' title='".$post['text']."' >".$post['text']."</a>";
	}
	echo $links;
}

/**
 * Builds and shows the standard multy-level menu for archive.
 *
 * @since Twenty Eleven 1.0
 */

function simarine_archives_menu($post_date = '', $cat = ''){
	$args = array(
		'cat'            => $cat,
		'type'            => 'yearly',
		'format'          => 'custom',
		'limit'          => '',
		'before'          => '',
		'after'           => '',
		'show_post_count' => false,
		'echo'            => 0
	);
	$years =  simarine_get_archives( $args );

	$active_year = false;  //if false, make first year active!
	$active_month = false;
	if($post_date != ''){
		$time = strtotime($post_date);
		$active_year = date('Y', $time);
		$active_month = date('m', $time);
	}

	$archive="";
	$first_year_in_menu=true;
	foreach($years as $year){
		$args = array(
			'cat'            => $cat,
			'type'            => 'monthly',
			'format'          => 'custom',
			'limit'          => '',
			'before'          => '',
			'after'           => '',
			'show_post_count' => false,
			'echo'            => 0
		);
		$months =  simarine_get_archives( $args, $year['text']);

		if(!$active_year && $first_year_in_menu)
			$archive .= "<div class='active'>";
		elseif($active_year == $year['text'])
			$archive .= "<div class='active'>";
		else
			$archive .= "<div>";

		$archive .= "<a href='".$year['url']."' title='".$year['text']."' >".$year['text']."</a>";
		$archive .= "<div>";
		foreach($months as $month){
			$class="";
			if($active_month == $month['text'])
				$class = "class='active'";

			$archive .= "<a href='".$month['url']."' title='".$month['text']."' $class >".$month['text']."</a>";
		}
		$archive .= "</div>";
		$archive .= "</div>";

		$first_year_in_menu=false;
	}

	echo $archive;
}

/**
 * Display archive links based on type and format.
 *
 * The 'type' argument offers a few choices and by default will display monthly
 * archive links. The other options for values are 'daily', 'weekly', 'monthly',
 * 'yearly', 'postbypost' or 'alpha'. Both 'postbypost' and 'alpha' display the
 * same archive link list, the difference between the two is that 'alpha'
 * will order by post title and 'postbypost' will order by post date.
 *
 * The date archives will logically display dates with links to the archive post
 * page. The 'postbypost' and 'alpha' values for 'type' argument will display
 * the post titles.
 *
 * The 'limit' argument will only display a limited amount of links, specified
 * by the 'limit' integer value. By default, there is no limit. The
 * 'show_post_count' argument will show how many posts are within the archive.
 * By default, the 'show_post_count' argument is set to false.
 *
 * For the 'format', 'before', and 'after' arguments, see {@link
 * get_archives_link()}. The values of these arguments have to do with that
 * function.
 *
 * @since 1.2.0
 *
 * @param string|array $args Optional. Override defaults.
 */
function simarine_get_archives($args = '', $foryear='', $formonth='') {
	global $wpdb, $wp_locale;

	$defaults = array(
		'type' => 'monthly', 'limit' => '',
		 'cat' => '',
		'format' => 'html', 'before' => '',
		'after' => '', 'show_post_count' => false,
		'echo' => 1
	);

	$r = wp_parse_args( $args, $defaults );
	extract( $r, EXTR_SKIP );
	if ( '' == $type )
		$type = 'monthly';
	if ( '' != $limit ) {
		$limit = absint($limit);
		$limit = ' LIMIT '.$limit;
	}

	$simarine_where = "";
	if($foryear != '')
		$simarine_where .= " AND YEAR(post_date) = '".$foryear."'";
	if($formonth != '')
		$simarine_where .= " AND MONTH(post_date) = '".$formonth."'";

	// this is what will separate dates on weekly archive links
	$archive_week_separator = '&#8211;';

	// over-ride general date format ? 0 = no: use the date format set in Options, 1 = yes: over-ride
	$archive_date_format_over_ride = 0;

	// options for daily archive (only if you over-ride the general date format)
	$archive_day_date_format = 'Y/m/d';

	// options for weekly archive (only if you over-ride the general date format)
	$archive_week_start_date_format = 'Y/m/d';
	$archive_week_end_date_format	= 'Y/m/d';

	if ( !$archive_date_format_over_ride ) {
		$archive_day_date_format = get_option('date_format');
		$archive_week_start_date_format = get_option('date_format');
		$archive_week_end_date_format = get_option('date_format');
	}

	//filters
	$where = apply_filters( 'getarchives_where', "WHERE post_type = 'post' AND post_status = 'publish'", $r );
	$join = apply_filters( 'getarchives_join', '', $r );

	$output = array();

	if ( 'monthly' == $type ) {
		$query = "SELECT YEAR(post_date) AS `year`, MONTH(post_date) AS `month`, count(ID) as posts FROM $wpdb->posts $join $where $simarine_where GROUP BY YEAR(post_date), MONTH(post_date) ORDER BY post_date DESC $limit";
		$key = md5($query);
		$cache = wp_cache_get( 'wp_get_archives' , 'general');
		if ( !isset( $cache[ $key ] ) ) {
			$arcresults = $wpdb->get_results($query);
			$cache[ $key ] = $arcresults;
			wp_cache_set( 'wp_get_archives', $cache, 'general' );
		} else {
			$arcresults = $cache[ $key ];
		}
		if ( $arcresults ) {
			$afterafter = $after;
			$count=0;
			foreach ( (array) $arcresults as $arcresult ) {
				$url = get_month_link( $arcresult->year, $arcresult->month );
				/* translators: 1: month name, 2: 4-digit year */
				$text = sprintf(__('%1$s'), $wp_locale->get_month($arcresult->month));
				if ( $show_post_count )
					$after = '&nbsp;('.$arcresult->posts.')' . $afterafter;
				$output[$count]['url'] = $url;
				$output[$count]['text'] = $text;
				$count++;
			}
		}
	} elseif ('yearly' == $type) {
		$query = "SELECT YEAR(post_date) AS `year`, count(ID) as posts FROM $wpdb->posts $join $where $simarine_where GROUP BY YEAR(post_date) ORDER BY post_date DESC $limit";
		$key = md5($query);
		$cache = wp_cache_get( 'wp_get_archives' , 'general');
		if ( !isset( $cache[ $key ] ) ) {
			$arcresults = $wpdb->get_results($query);
			$cache[ $key ] = $arcresults;
			wp_cache_set( 'wp_get_archives', $cache, 'general' );
		} else {
			$arcresults = $cache[ $key ];
		}
		if ($arcresults) {
			$afterafter = $after;
			$count=0;
			foreach ( (array) $arcresults as $arcresult) {
				$url = get_year_link($arcresult->year);
				$text = sprintf('%d', $arcresult->year);
				if ($show_post_count)
					$after = '&nbsp;('.$arcresult->posts.')' . $afterafter;
				$output[$count]['url'] = $url;
				$output[$count]['text'] = $text;
				$count++;
			}
		}
	} elseif ( 'daily' == $type ) {
		$query = "SELECT YEAR(post_date) AS `year`, MONTH(post_date) AS `month`, DAYOFMONTH(post_date) AS `dayofmonth`, count(ID) as posts FROM $wpdb->posts $join $where  $simarine_where GROUP BY YEAR(post_date), MONTH(post_date), DAYOFMONTH(post_date) ORDER BY post_date DESC $limit";
		$key = md5($query);
		$cache = wp_cache_get( 'wp_get_archives' , 'general');
		if ( !isset( $cache[ $key ] ) ) {
			$arcresults = $wpdb->get_results($query);
			$cache[ $key ] = $arcresults;
			wp_cache_set( 'wp_get_archives', $cache, 'general' );
		} else {
			$arcresults = $cache[ $key ];
		}
		if ( $arcresults ) {
			$afterafter = $after;
			$count=0;
			foreach ( (array) $arcresults as $arcresult ) {
				$url	= get_day_link($arcresult->year, $arcresult->month, $arcresult->dayofmonth);
				$date = sprintf('%1$d-%2$02d-%3$02d 00:00:00', $arcresult->year, $arcresult->month, $arcresult->dayofmonth);
				$text = mysql2date($archive_day_date_format, $date);
				if ($show_post_count)
					$after = '&nbsp;('.$arcresult->posts.')'.$afterafter;
				$output[$count]['url'] = $url;
				$output[$count]['text'] = $text;
				$count++;
			}
		}
	} elseif ( 'weekly' == $type ) {
		$week = _wp_mysql_week( '`post_date`' );
		$query = "SELECT DISTINCT $week AS `week`, YEAR( `post_date` ) AS `yr`, DATE_FORMAT( `post_date`, '%Y-%m-%d' ) AS `yyyymmdd`, count( `ID` ) AS `posts` FROM `$wpdb->posts` $join $where $simarine_where GROUP BY $week, YEAR( `post_date` ) ORDER BY `post_date` DESC $limit";
		$key = md5($query);
		$cache = wp_cache_get( 'wp_get_archives' , 'general');
		if ( !isset( $cache[ $key ] ) ) {
			$arcresults = $wpdb->get_results($query);
			$cache[ $key ] = $arcresults;
			wp_cache_set( 'wp_get_archives', $cache, 'general' );
		} else {
			$arcresults = $cache[ $key ];
		}
		$arc_w_last = '';
		$afterafter = $after;
		if ( $arcresults ) {
			$count=0;
				foreach ( (array) $arcresults as $arcresult ) {
					if ( $arcresult->week != $arc_w_last ) {
						$arc_year = $arcresult->yr;
						$arc_w_last = $arcresult->week;
						$arc_week = get_weekstartend($arcresult->yyyymmdd, get_option('start_of_week'));
						$arc_week_start = date_i18n($archive_week_start_date_format, $arc_week['start']);
						$arc_week_end = date_i18n($archive_week_end_date_format, $arc_week['end']);
						$url  = sprintf('%1$s/%2$s%3$sm%4$s%5$s%6$sw%7$s%8$d', home_url(), '', '?', '=', $arc_year, '&amp;', '=', $arcresult->week);
						$text = $arc_week_start . $archive_week_separator . $arc_week_end;
						if ($show_post_count)
							$after = '&nbsp;('.$arcresult->posts.')'.$afterafter;
						$output[$count]['url'] = $url;
						$output[$count]['text'] = $text;
						$count++;
					}
				}
		}
	} elseif ( ( 'postbypost' == $type ) || ('alpha' == $type) ) {
		$orderby = ('alpha' == $type) ? 'post_title ASC ' : 'post_date DESC ';
		$query = "SELECT * FROM $wpdb->posts $join $where $simarine_where ORDER BY $orderby $limit";
		$key = md5($query);
		$cache = wp_cache_get( 'wp_get_archives' , 'general');
		if ( !isset( $cache[ $key ] ) ) {
			$arcresults = $wpdb->get_results($query);
			$cache[ $key ] = $arcresults;
			wp_cache_set( 'wp_get_archives', $cache, 'general' );
		} else {
			$arcresults = $cache[ $key ];
		}
		if ( $arcresults ) {
			$count=0;
			foreach ( (array) $arcresults as $arcresult ) {
				if ( $arcresult->post_date != '0000-00-00 00:00:00' ) {
					$url  = get_permalink( $arcresult );
					if ( $arcresult->post_title )
						$text = strip_tags( apply_filters( 'the_title', $arcresult->post_title, $arcresult->ID ) );
					else
						$text = $arcresult->ID;
					$output[$count]['url'] = $url;
					$output[$count]['text'] = $text;
					$count++;
				}
			}
		}
	}
	return $output;
}

function http_file_exists($url)
{
	$f=@fopen($url,"r");
	if($f)
	{
		fclose($f);
		return true;
	}
	return false;
}


function search_filter($query){
	if (!(!$query->is_admin && $query->is_search)) {
		return $query;
	}
	if(isset($_REQUEST['cats'])){
		$cat = $_REQUEST['cats'];
	}else{
		return get_sz_excluded_id($query);
	}
	if($cat == "news"){
	 	return no_pages($query);
	}elseif($cat == "all"){
		return get_sz_excluded_id($query);
	}else{
		return only_category($query, $cat);
	}
}

add_filter('pre_get_posts','search_filter');

// search filter
//Exlude pages
function no_pages($query) {
	$query->set('post_type', 'post');
	return $query;
}


// Only show specific category
function only_category($query, $cat){
	$id_array = array();
	$id_array[] = $cat;
	get_simarine_child_id_recursive($cat, $id_array);
	$query->set('post__in', $id_array);
	return $query;
}

function get_simarine_child_id_recursive($parent, &$id_array){
	$children = get_pages( array( 'child_of' => $parent, 'parent' => $parent, 'order' => 'ASC' ) );
	usort($children, "posts_order_by_menu_order");
	foreach($children as $page){
		$id_array[] = $page->ID;
		get_simarine_child_id_recursive($page->ID, $id_array);
	}
}

function get_sz_excluded_id($query){
	$id_array = array();
	$args = array(
		'post_type' => 'page',
		'meta_query' => array(
			array(
				'key' => 'exlude_from_search',
				'value' => '1',
				'compare' => 'LIKE'
			)
		)
	);

	$search_category = new WP_Query($args);
	if($search_category->have_posts()){
		while($search_category->have_posts()){
			$search_category->the_post();
			global $id;
			$id_array[] = $id;
		}
	}

	wp_reset_postdata();

	$query->set('post__not_in', $id_array);

	return $query;
}



function url_exists($url) { 
    $hdrs = @get_headers($url); 
    return is_array($hdrs) ? preg_match('/^HTTP\\/\\d+\\.\\d+\\s+2\\d\\d\\s+.*$/',$hdrs[0]) : false; 
}


function sz_changeMediaCreditOutput($val, $attr, $content = null ) {
	$options = get_option( MEDIA_CREDIT_OPTION );
	if ( !empty( $options['credit_at_end'] ) )
		return do_shortcode( $content );

	extract(shortcode_atts(array(
		'id' => -1,
		'name' => '',
		'align'	=> 'alignnone',
		'width'	=> '',
		'attachment_id' => 0,
	), $attr));

	if ($id !== -1){
		//TODO: Implementiraj strani, kjer se bodo pokazale slike avtorjev
		//$author_link = get_media_credit_html_by_user_ID($id);
		$author_link = get_the_author_meta( 'display_name', $id );
	}else
		$author_link = $name;

	$htmlHead = getImageCaptionHtmlBeginning($align);
	$htmlFoot = getImageCaptionHtmlEnding();
	$pos = strpos($content, "media-credit-contain");
	if($pos === false){
		$out = $htmlHead . $content;
		if(strpos($content, "media-credit-contain") === false)
			$out .= getMediaCreditStartTag();
		$out .= '<span class="media-credit-margin">' . __("Photo", "twentyeleven").": ".$author_link . '</span>';
		if(strpos($content, "media-credit-contain") === false)
			$out .= getMediaCreditEndTag();
		$out .= $htmlFoot;
		return do_shortcode($out);
	}else{
		//already have the wrapper, add just the media-credit html
		$posTo = strpos($content, $htmlFoot);
		$before = substr($content, 0, $posTo);

		$out = $before;
		if(strpos($content, "media-credit-contain") === false)
			$out .= getMediaCreditStartTag();
		$out .= '<span class="media-credit-margin">' . __("Photo", "twentyeleven").": ".$author_link . '</span>';
		if(strpos($content, "media-credit-contain") === false)
			$out .= getMediaCreditEndTag();
		$out .= $htmlFoot;
		return  do_shortcode($out);
	}
}
add_filter( 'media_credit_shortcode', 'sz_changeMediaCreditOutput', 10, 3);


function sz_addMediaCreditToCaption( $empty, $attr, $content ){

	$attr = shortcode_atts( array(
		'id'      => '',
		'align'   => 'alignnone',
		'width'   => '',
		'caption' => ''
	), $attr );

	if ( 1 > (int) $attr['width'] || empty( $attr['caption'] ) ) {
		return '';
	}

	if ( $attr['id'] ) {
		$attr['id'] = 'id="' . esc_attr( $attr['id'] ) . '" ';
	}

	$content = do_shortcode($content);
	$htmlHead = getImageCaptionHtmlBeginning(esc_attr( $attr['align'] ));
	$htmlFoot = getImageCaptionHtmlEnding();
	$pos = strpos($content, "media-credit-contain");
	if($pos === false){
		$out = $htmlHead . $content;
		if(strpos($content, "media-credit-contain") === false)
			$out .= getMediaCreditStartTag();
		$out .= '<span class="media-credit-margin-caption">' . $attr['caption'] . '</span>';
		if(strpos($content, "media-credit-contain") === false)
			$out .= getMediaCreditEndTag();
		$out .= $htmlFoot;
		return $out;
	}else{
		//already have the wrapper, add just the caption html
		$posTo = strpos($content, getMediaCreditEndTag());
		$before = substr($content, 0, $posTo);
		$after = substr($content, $posTo);

		$out = $before;
		if(strpos($content, "media-credit-contain") === false)
			$out .= getMediaCreditStartTag();
		$out .= '<br /><span class="media-credit-margin-caption">' . $attr['caption'] . '</span>';
		if(strpos($content, "media-credit-contain") === false)
			$out .= getMediaCreditEndTag();
		$out .= $after;
		return $out;
	}


	/*return '<div ' . $attr['id']
	. 'class="wp-caption ' . esc_attr( $attr['align'] ) . '" '
	. 'style="max-width: ' . ( 10 + (int) $attr['width'] ) . 'px;">'
	. do_shortcode( $content )
	. '<p class="wp-caption-text">' . $attr['caption'] . '</p>'
	. '</div>';*/
}
add_filter( 'img_caption_shortcode', 'sz_addMediaCreditToCaption', 10, 3);
//add_filter( 'disable_captions', create_function('$a', 'return true;') );

function getMediaCreditStartTag(){
	return '<span class="media-credit">';
}
function getMediaCreditEndTag(){
	return '</span><!-- class="media-credit" -->';
}
function getImageCaptionHtmlBeginning($align){
	return '<div class="media-credit-container ' . esc_attr($align) . '" >';
}
function getImageCaptionHtmlEnding(){
	return '</div><!-- end of mceTemp -->';
}
function sz_addMediaCreditToLightbox($title, $id, $title_arg ){
	$mc = trim(get_media_credit($id));
	$out = $title;
	if($title_arg == "caption"){
		if(empty($out)){
			$out = get_the_title( $id ); //Če ni captiona, po defaultu vzami title od slike.
		}
	}

	if(!empty($mc)) {
		if(empty($out))
			$out = __("Photo", "twentyeleven") . ": " . $mc;
		else
			$out .= " (" . __("Photo", "twentyeleven") . ": " . $mc . ")";
	}
	return $out;
}
add_filter( 'rl_get_attachment_title',  'sz_addMediaCreditToLightbox', 10, 3);

/*
 * Izgleda, da WPML še ni implementiral filtra na attachment_url_to_postid in se ID-ji priponk ne kličejo glede na jezik.
 * Ta delček kode odpravi te težave.
 */
function sz_fixWPML_returnIDOfTranslatedAttachment( $post_id, $url ){
	$post_id = sz_wpml_get_idInLanguage($post_id, "attachment");
	return $post_id;
};
add_filter( 'attachment_url_to_postid', 'sz_fixWPML_returnIDOfTranslatedAttachment', 10, 2);


/*
 * Funkcija, ki vrne ID prevedene različice posta/datoteke glede na trenutni aktivni jezik (ali na podani jezik).
 */
function sz_wpml_get_idInLanguage($post_id, $post_type, $returnOriginal=false, $language=null){
	global $wpdb;
	//get images and meta data from page named Slider!
	// automatically get correct page ID for translations
	if(function_exists('icl_object_id')){
		// deprecated but works
		$post_id = icl_object_id($post_id, $post_type, $returnOriginal, $language);
	} else {
		// should work according to documentation when icl_object_id is removed
		$post_id = apply_filters('wpml_object_id', $post_id, $post_type, $returnOriginal, $language);
	}
	return $post_id;
}

function frontpage_widget_init() {
	register_sidebar( array(
		'name' => 'Front Page Calendar Widget Area - SL',
		'id' => 'fp_calendar_widget_area_sl',
		'before_widget' => '<div id="frontpage_calander_widget_area">',
		'after_widget' => '</div>',
		'before_title'  => '<span style="display: none;">',
		'after_title'   => '</span>'
	));
	register_sidebar( array(
			'name' => 'Front Page Calendar Widget Area - IT',
			'id' => 'fp_calendar_widget_area_it',
			'before_widget' => '<div id="frontpage_calander_widget_area">',
			'after_widget' => '</div>',
			'before_title'  => '<span style="display: none;">',
			'after_title'   => '</span>'
	));
	register_sidebar( array(
			'name' => 'Front Page Calendar Widget Area - EN',
			'id' => 'fp_calendar_widget_area_en',
			'before_widget' => '<div id="frontpage_calander_widget_area">',
			'after_widget' => '</div>',
			'before_title'  => '<span style="display: none;">',
			'after_title'   => '</span>'
	));
	register_sidebar( array(
		'name' => 'Front Page Odpiralni Čas Widget Area',
		'id' => 'fp_sz_odpiralni_cas_widget_area',
		'before_widget' => '<div id="frontpage_odpiralni_cas_widget_area">',
		'after_widget' => '</div>'
	));
}
add_action('widgets_init', 'frontpage_widget_init');


//************* Pošlji e-razglednico ******************//
add_action( 'wp_ajax_send_e_card', 'send_e_card' );
add_action( 'wp_ajax_nopriv_send_e_card', 'send_e_card' );
function send_e_card(){
	global $wpdb;

	//Variables used:
	$newsletter = array(
		'title' => $_POST['newsletter_title'],
		'senderName' => $_POST['senders_name'],
		'recipientName' => $_POST['friends_name'],
		'recipientEmail' => $_POST['friends_email'],
		'photoUrl' => $_POST['image_src'],
		'photoTitle' => $_POST['image_title'],
		'photoAuthor' => $_POST['image_author'],
		'senderText' => $_POST['newsletter_text'],
		'isEmail' => true
	);
	do{
		$randId = uniqid();
	}while(get_option($randId) !== FALSE);

	add_option($randId, serialize($newsletter));
	$link = esc_url( add_query_arg('nlid', $randId, $_POST['url']));
	$htmlLink = "";
	if(!empty($_POST['friends_name'])){
		$senderName = (empty($_POST['senders_name']))? __("a friend", "twentyeleven") : $_POST['senders_name'];
		$htmlLink = $_POST['friends_name'].", ".__("you've received an e-card from", "twentyeleven")." ".$senderName."! <br />";
	}
	$htmlLink .=  __("If you are having problems opening this message,", "twentyeleven")." <a href='".$link."'>".__("view it in your browser", "twentyeleven")."</a>.";

	ob_start();
	require_once ('e-razglednice_design.php');
	$html = $htmlLink.ob_get_contents();
	ob_clean();

	$to = $_POST['friends_email'];
//$from = 'no-reply@skocjanski-zatok.org';
	$subject = $_POST['newsletter_title'];
	$body = $html;
	$headers[] = 'From: '.$_POST['senders_name'].' (NRŠZ) <noreply@skocjanski-zatok.org>';
	$headers[] = 'Content-Type: text/html; charset=UTF-8';

	$status = wp_mail( $to, $subject, $body, $headers );

	wp_die($status);
}


/**
 * U-centrix sprememba
 * Pri pluginu Media Credit (2.7.4) je datuma 18.01.2016 prisoten BUG v kodi.
 * Če shranimo Media Credit z IDjem uporabnika, se vnese med tekst direkt html (<a href ...) namesto shortcode.
 * Tukaj preprečimo shranjevanje z IDjem
*/

/**
 * Change the post_author to the entered media credit from add_media_credit() above.
 *
 * @param object $post Object of attachment containing all fields from get_post().
 * @param object $attachment Object of attachment containing few fields, unused in this method.
 */
function sz_save_media_credit($post, $attachment) {
	$wp_user_id = $attachment['media-credit-hidden'];
	$freeform_name = $attachment['media-credit'];
	$url = $attachment['media-credit-url'];

	// we need to update the credit URL in any case
	update_post_meta($post['ID'], MEDIA_CREDIT_URL_POSTMETA_KEY, $url); // insert '_media_credit_url' metadata field

	//U-centrix change: Ne sme se shranit kot user. Zadeva ne deluje pravilno.
	if (false && isset( $wp_user_id ) && $wp_user_id != '' && $freeform_name === get_the_author_meta( 'display_name', $wp_user_id ) ) {
	//if (isset( $wp_user_id ) && $wp_user_id != '' && $freeform_name === get_the_author_meta( 'display_name', $wp_user_id ) ) {
			// a valid WP user was selected, and the display name matches the free-form
		// the final conditional is necessary for the case when a valid user is selected, filling in the hidden field,
		// then free-form text is entered after that. if so, the free-form text is what should be used
		$post['post_author'] = $wp_user_id; // update post_author with the chosen user
		delete_post_meta($post['ID'], MEDIA_CREDIT_POSTMETA_KEY); // delete any residual metadata from a free-form field (as inserted below)
		update_media_credit_in_post($post, true, '', $url);
	} else { // free-form text was entered, insert postmeta with credit. if free-form text is blank, insert a single space in postmeta.
		$freeform = empty( $freeform_name ) ? MEDIA_CREDIT_EMPTY_META_STRING : $freeform_name;
		update_post_meta($post['ID'], MEDIA_CREDIT_POSTMETA_KEY, $freeform); // insert '_media_credit' metadata field for image with free-form text
		update_media_credit_in_post($post, false, $freeform, $url);
	}
	return $post;
}
remove_filter('attachment_fields_to_save', 'save_media_credit', 10);
add_filter('attachment_fields_to_save', 'sz_save_media_credit', 5, 2);


/**
 * U-centrix sprememba
 * Dodana lastna različica funkcije "simarine_posts_in_month" za potrebe izpisa naslovov in datuma
 * novic, kjer je željeno da poleg novice na desni strani prikažemo ostale novice iz tega meseca in leta
 */
function sz_simarine_posts_in_month($year = '', $month = '', $cat = '', $title = '', $getLatest = false){
	// če je nastavljen $getLatest na true bo izpisal naslove elementov za zadnji mesec in označil zadnji element kot aktiven

	$args = array(
			'cat'            => $cat,
			'type'            => 'postbypost',
			'format'          => 'custom',
			'limit'          => '',
			'before'          => '',
			'after'           => '',
			'show_post_count' => false,
			'echo'            => 0
	);
	if($year == '' && $month == ''){
		$args['limit'] = '7';
	}

	if ($getLatest){
		$posts = array();

		// išči toliko časa dokler ne najdemo meseca z novicami (naredi prerez pri letnici 2000, nekje se moramo ustavit)
		do  {
			$posts = simarine_get_archives($args, $year, $month);

			$month--;
			if ($month <= 0) {
				$year--;
				$month = 12;
			}
		} while ((sizeof($posts) <= 0) || ($year <= 2000));
	} else {
		$posts = simarine_get_archives($args, $year, $month);
	}

	$count_post = 0;

	$links ="";
	foreach($posts as $post){
		$count_post++;

		// preverimo ali označimo zadnjo novico
		if (($getLatest) && ($count_post == 1)) {
			$links .= "<a href='" . $post['url'] . "' title='" . $post['text'] . "' style='color: #000000;'>" . $post['text'] . "</a>";
		} else {
			// preverimo ali gledamo trenutno novico in jo ustrezno označimo
			if ((url_to_postid($post['url']) != 0) && ($title == get_the_title(url_to_postid($post['url'])))) {
				$links .= "<a href='" . $post['url'] . "' title='" . $post['text'] . "' style='color: #000000;'>" . $post['text'] . "</a>";
			} else {
				$links .= "<a href='" . $post['url'] . "' title='" . $post['text'] . "' >" . $post['text'] . "</a>";
			}
		}
		// dodamo izpis datuma
		$links .= "<div class='entry-meta' style='margin: 0; margin-bottom: 13px'>".get_the_time('d.m.Y', url_to_postid($post['url']))."</div>";
	}
	echo $links;
}

/**
 * U-centrix sprememba
 * Dodana lastna različica funkcije "simarine_archives_menu" za potrebe pravilne označitve
 * trenutnega prikazovanega meseca
 */
function sz_simarine_archives_menu($post_date = '', $cat = '', $getLatest = false){
	// če je nastavljen $getLatest na true bo označil kot aktivno izbiro zadnje leto in mesec ki še vsebuje element

	$args = array(
			'cat'            => $cat,
			'type'            => 'yearly',
			'format'          => 'custom',
			'limit'          => '',
			'before'          => '',
			'after'           => '',
			'show_post_count' => false,
			'echo'            => 0
	);
	$years =  simarine_get_archives( $args );

	// neuporabljeno ali nedelujoče
	//$active_year = true;  //if false, make first year active!
	//$active_month = true;
	$count_month = 0;
	$count_year = 0;

	if($post_date != ''){
		$time = strtotime($post_date);
		$active_year = date('Y', $time);
		$active_month = date('m', $time);	// tu dobimo številko, dol preverjamo text - to ne dela (hack spodaj)
	}

	$archive="";
	$first_year_in_menu=false;
	foreach($years as $year){
		$count_year++;
		$count_month = 0;

		$args = array(
				'cat'            => $cat,
				'type'            => 'monthly',
				'format'          => 'custom',
				'limit'          => '',
				'before'          => '',
				'after'           => '',
				'show_post_count' => false,
				'echo'            => 0
		);
		$months =  simarine_get_archives( $args, $year['text']);

		// nova pravila v mestu
		//if(!$active_year && $first_year_in_menu)
		//	$archive .= "<div class='active'>";
		//elseif($active_year == $year['text'])
		//	$archive .= "<div class='active'>";
		//else
		//	$archive .= "<div>";

		// preverimo ali označimo zadnje leto v bazi (prvo leto ki se izpiše)
		if (($getLatest) && ($count_year == 1)) {
			$archive .= "<div class='active'>";
		} else {
			if ($active_year == $year['text'])
				$archive .= "<div class='active'>";
			else
				$archive .= "<div>";
		}

		// poiščimo url do zadnje novice v izbranem letu
		$args = array(
				'cat'            => $cat,
				'type'            => 'postbypost',
				'format'          => 'custom',
				'limit'          => '1',
				'before'          => '',
				'after'           => '',
				'show_post_count' => false,
				'echo'            => 0
		);
		// preišči mesece dec->jan in poišči zadnjo novico v letu (lahko se zgodi da ni nobenih novic v decembru, novembru, ...)
		for ($m = 12; $m > 0; $m--){
			$posts_year =  simarine_get_archives( $args, $year['text'], $m);

			if (sizeof($posts_year) > 0)
				break;
		}

		$archive .= "<a href='".$posts_year[0]['url']."' title='".$year['text']."' >".$year['text']."</a>";
		$archive .= "<div>";
		foreach($months as $month){
			$count_month++;

			$class="";

			// "hack" da dobimo številko meseca, da lahko primerjamo ali izpisujemo aktivni mesec (zakaj glej zgoraj)
			// številko meseca pridobimo iz url-ja, kjer je vsak mesec naslovljen z .../leto/mesec/?cat=...
			if (strpos($month['url'], '/?cat') !== false) {
				$current_month = substr($month['url'],  strpos($month['url'], '/?cat') - 2, 2);
			}

			// preverimo ali označimo zadnji mesec v bazi (zadnji mesec ki se izpiše)
			if (($getLatest) && ($count_year == 1)) {
				if ($count_month == 1)
					$class = "class='active'";
			} else {
				//if($active_month == $month['text']) {
				//	$class = "class='active'";
				//}
				if (($active_month == $current_month) && ($active_year == $year['text']))  {
					$class = "class='active'";
				}
			}
			// poiščimo url do zadnje novice v izbranem mesecu
			$args = array(
					'cat'            => $cat,
					'type'            => 'postbypost',
					'format'          => 'custom',
					'limit'          => '1',
					'before'          => '',
					'after'           => '',
					'show_post_count' => false,
					'echo'            => 0
			);
			$posts_month =  simarine_get_archives( $args, $year['text'], $current_month);

			// dodamo kapitalizacijo imena meseca
			$archive .= "<a href='".$posts_month[0]['url']."' title='".ucfirst ($month['text'])."' $class >".ucfirst ($month['text'])."</a>";
		}
		$archive .= "</div>";
		$archive .= "</div>";

		$first_year_in_menu=false;
	}

	echo $archive;
}