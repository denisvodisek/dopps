<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
?>
		<div id="col_2_sidebar_1">
            	<div id="sidebar_left">

                <div class="menu">

                	<?php
						simarine_page_hierarchy_menu(get_the_ID());

					?>
                    &nbsp;
			 </div><!-- .menu -->

                </div><!-- #sidebar_left -->


           		<div id="main_left_col">
                    <span id="nav_pointer">
                    	<a href="<?php echo get_site_url();?>"><?php _e('Home', 'twentyeleven'); ?></a>
                    	<?php $anc = get_ancestors( get_the_ID(), 'page' );
							$anc = array_reverse($anc);
							foreach($anc as $id){
								$current++;
								$p = get_post($id);
								?>
								&gt; <a href="<?php echo get_permalink($p->ID);?>"><?php echo $p->post_title; ?></a>
								<?php
							}
						?>
                    	&gt; <a href="<?php echo get_permalink(get_the_ID()); ?>" class="active"><?php the_title(); ?></a>

                    </span>
                    <div class="clear"></div>

                    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                        <header class="entry-header">
                            <h1><?php the_title(); ?></h1>
                        </header><!-- .entry-header -->

                        <div class="entry-content">
                            <?php
							$content = get_the_content();
						    $content = apply_filters('the_content', $content);
							$content = str_replace("<p>[--section--]</p>", "[--section--]",$content);
							$content = str_replace("<p>[&#8211;section&#8211;]</p>", "[--section--]",$content);

							$page_sections = explode("[--section--]", $content);
							echo $page_sections[0];
							?>




                            <?php wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'twentyeleven' ) . '</span>', 'after' => '</div>' ) ); ?>
                        </div><!-- .entry-content -->
                        <footer class="entry-meta">
                            <?php edit_post_link( __( 'Edit', 'twentyeleven' ), '<span class="edit-link">', '</span>' ); ?>
                        </footer><!-- .entry-meta -->
                    </article><!-- #post-<?php the_ID(); ?> -->


                </div><!-- #main_left_col -->


           		<div id="main_right_col">
               	  <div class="info_board"><?php echo $page_sections[1]; ?>
                    </div><!-- .info_board -->

                </div><!-- #main_right_col -->

            </div><!-- #col_1_sidebar_1 -->

