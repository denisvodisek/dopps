( function( window, undefined ) {
    'use strict';

    jQuery(function ($) {
        /* administration */
        // dodaj dogodek na gum za navigacijo, kateri nastavi filtre za prikaz ($_GET da je ločeno od forme za urejanje posameznega dogodka)
        $( '#sz_vodeni_ogled_admin_navigation_button' ).on( 'click', {step: 'none'}, filterTermList);
        $( '#sz_vodeni_ogled_admin_navigation_button_prev' ).on( 'click', {step: 'prev'}, filterTermList);
        $( '#sz_vodeni_ogled_admin_navigation_button_next' ).on( 'click', {step: 'next'}, filterTermList);



        function filterTermList(pageStep) {
            // generiraj url - kje smo
            var genNavLink = '?page='.concat($('#sz_vodeni_ogled_admin_navigation_URL').html());

            // generiraj url - koliko elementov prikazujemo
            genNavLink = genNavLink.concat('&pageItems='.concat($( '.sz_vodeni_ogled_admin_navigation #pageItems option:selected' ).val()));

            // če smo spremenili število terminov na strani, naložimo prvo stran
            if ($( '.sz_vodeni_ogled_admin_navigation #pageItems option:selected' ).val() == $('#sz_vodeni_ogled_admin_navigation_pageItems').html()) {
                var pageValue = parseInt($('#sz_vodeni_ogled_admin_navigation_currentPage').html());
                var maxPageValue = parseInt($('#sz_vodeni_ogled_admin_navigation_pageMax').html());

                if (pageStep.data.step == 'prev') {
                    if (pageValue > 1) {
                        pageValue = pageValue - 1;
                    }

                    genNavLink = genNavLink.concat('&showPage='.concat(String(pageValue)));
                } else if (pageStep.data.step == 'next') {
                    if (pageValue < maxPageValue) {
                        pageValue = pageValue + 1;
                    }

                    genNavLink = genNavLink.concat('&showPage='.concat(String(pageValue)));
                } else {
                    genNavLink = genNavLink.concat('&showPage='.concat(String(pageValue)));
                }
            } else {
                genNavLink = genNavLink.concat('&showPage=1');
            }

            // generiraj url - kaemo pretekle termine
            if ($('.sz_vodeni_ogled_admin_navigation #showPast').is(':checked'))
            {
                genNavLink = genNavLink.concat('&showPast='.concat($( '.sz_vodeni_ogled_admin_navigation #showPast' ).val()));
            }

            // generiraj url - kažemo zaključene termine
            if ($('.sz_vodeni_ogled_admin_navigation #showConfirmed').is(':checked'))
            {
                genNavLink = genNavLink.concat('&showConfirmed='.concat($( '.sz_vodeni_ogled_admin_navigation #showConfirmed' ).val()));
            }

            location.href = genNavLink;
        }


        /* settings */
        // dodajmo novo vnosno polje za email naslov
        $( '.sz_vodeni_ogled_settings_wrap #sz_vodeni_ogled_registrationSettingsForm #sz_vodeni_ogled_emailNotify_add' ).on( 'click', function () {
            // dodaj vnosno polje
            $( '#sz_vodeni_ogled_emailNotify_table tbody').append(getEmailNotify_table_html());
            // dodaj akcijo za odstranitev vnosnega polja
            addEmailNotify_remove_action();

        });

        // dodaj akcijo za odstranitev vnosnega polja že obstoječim elementom
        addEmailNotify_remove_action();



        function getEmailNotify_table_html() {
            var outHtml = '';

            outHtml += '<tr>';
            outHtml += '<td><input type="text" name="sz_notifyEmailList[]" class="sz_closedDates" /></td>';
            outHtml += '<td><a class="removeEmail"></a> </td>';
            outHtml += '</tr>';

            return outHtml;
        }

        function addEmailNotify_remove_action() {
            // počisti stare dogodke (če dodajamo dogodke novo ustvarjenim elementovm)

            $( '#sz_vodeni_ogled_emailNotify_table .removeEmail').unbind( 'click' );

            // dodaj dogodke
            $( '#sz_vodeni_ogled_emailNotify_table .removeEmail').on( 'click' , function () {
                $( this ).parents( 'tr:first' ).remove();
            });
        }
    });

} )( this );