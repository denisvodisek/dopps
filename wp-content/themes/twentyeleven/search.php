<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

get_header();


?>

<script type="text/javascript">
function get_data() {

	if($("#searchbox").val() == ""){
		alert("<?php _e("Write search string into the category, you wish to search!");?>");
		return false;
	}
	document.searchcntrl.submit();
	return true;
};


</script>
<style>

#searchsubmit:hover{
	background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/images/isci.hover<?php _e("_EN.png", "twentyeleven"); ?>');
}
#searchsubmittwo{
	background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/images/isci<?php _e("_EN.png", "twentyeleven"); ?>');
	background-position: center;
	background-repeat: no-repeat;
	cursor: pointer;
	height: 25px;
	width: 91px;
	border: none;
	float: right;
	margin: 5px 5px 0 0;
}
</style>

		<section id="primary">
			<div id="content" role="main">
				<div id="col_1_sidebar_2">
					<div id="sidebar_left">
	                	<div class="menu">
	                    	&nbsp;
						</div><!-- .menu -->
					</div><!-- #sidebar_left -->
	               	<div id="sidebar_right">
	                	<div class="menu">
							&nbsp;
	                	</div><!-- .menu -->
	                </div><!-- #sidebar_right -->
					<div id="main_left_col">
					 <span id="nav_pointer">
                    	<a href="<?php echo get_site_url();?>"><?php _e('Home', 'twentyeleven'); ?></a> &gt; <?php _e('Search', 'twentyeleven'); ?>

                    </span>
                <header class="page-header">
					<h1 class="page-title"><?php printf( __( 'Search', 'twentyeleven' )); ?></h1>
				</header>
				<div id="srchinput">
					<form name="searchcntrl" method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
						<input id="searchbox" name="s" type="text" style="width: 100%;" />
						<select name="cats" style="width: 100%;">
							<?php $current_cat_name = get_sz_selection_list(); ?>

						</select>
						<!--<div id="srchbutton">&nbsp;</div>-->
						<button type="button" name="search_btn" id="searchsubmittwo" onclick="get_data();" ></button>
					</form>
				</div>
			<?php if ( have_posts() ) : ?>



				<?php twentyeleven_content_nav( 'nav-above' ); ?>

				<?

				add_filter('excerpt_more', 'new_excerpt_more');

				?>




				<div id="srchsectitle">
					<hr style="margin-bottom: 15px;"/>
					<p><?php printf( __( 'Search Results for: \'%s\'', 'twentyeleven' ), '<b>' . get_search_query() . '</b>' );
						if(isset($_GET["catt"]) && $_GET["catt"] != "all"){
							printf(__('searched in %s', 'twentyeleven'), $current_cat_name);
						}
						?>
					</p>

				</div>

				<?php /* Start the Loop */ ?>
				<?php
					$index = 1;
					while ( have_posts() ) : the_post(); ?>

					<?php
						/* Include the Post-Format-specific template for the content.
						 * If you want to overload this in a child theme then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						get_template_part( 'content', get_post_format() );
						$index++;
					?>

				<?php endwhile; ?>

				<?php twentyeleven_content_nav( 'nav-below' ); ?>

			<?php else : ?>

				<article id="post-0" class="post no-results not-found">
					<header class="entry-header">
						<h1 class="entry-title"><?php _e( 'Nothing Found', 'twentyeleven' ); ?></h1>
					</header><!-- .entry-header -->

					<div class="entry-content">
						<p><?php _e( 'Sorry, but nothing matched your search criteria. Please try again with some different keywords.', 'twentyeleven' ); ?></p>
						<?php //get_search_form(); ?>
					</div><!-- .entry-content -->
				</article><!-- #post-0 -->

			<?php endif; ?>
					</div>
				</div>
			</div><!-- #content -->
		</section><!-- #primary -->

<?php get_footer();

function new_excerpt_more($more) {
   	global $post;
		return ' ... <a href="'. get_permalink($post->ID) . '">' . __('Read&nbsp;more&nbsp;', 'twentyeleven') . '<span class="meta-nav">→</span></a>';


}

function get_sz_selection_list(){
	if(isset($_GET['cat'])){
		$selected = $_GET['cat'];
	}

	$args = array(
		'post_type' => 'page',
		'meta_query' => array(
			array(
				'key' => 'is_search_category',
				'value' => '1',
				'compare' => 'LIKE'
			)
		)
	);

	$search_category = new WP_Query($args); ?>
	<option value='all'><?php _e('Everything', 'twentyeleven'); ?></option>
	<option value='news'><?php _e('News', 'twentyeleven'); ?></option>

	<?php
	$current_cat = -1;
	if($search_category->have_posts()){

		while($search_category->have_posts()){
			$search_category->the_post();
			global $id;
			$title = get_the_title();
			if($id == $cat){
				$current_cat = $title;
				?>
			<option value='<?php echo $id; ?>' selected="selected"><?php echo $title; ?></option>
				<?php
			}else{
			?>

			<option value='<?php echo $id; ?>'><?php echo $title; ?></option>
			<?php
			}
		}
	}

	wp_reset_postdata();
	if($current_cat == -1){
		return __('News', 'twentyeleven');
	}else{
		return $current_cat;
	}

}


?>