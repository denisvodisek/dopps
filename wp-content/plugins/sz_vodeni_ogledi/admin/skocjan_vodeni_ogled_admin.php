<?php
/**
 * Text Domain: sz-vodeni-ogledi-admin
 */
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

add_action('admin_menu', 'sz_vodeni_ogled_admin_menu');

function sz_vodeni_ogled_admin_menu(){
    add_menu_page('SZ Vodeni ogledi', 'Vodeni ogledi', 'manage_options', 'sz-vodeni-ogled-administracija', 'sz_vodeni_ogled_administracija', plugin_dir_url(__FILE__).'img/VodeniOgled.png' );
    add_submenu_page('sz-vodeni-ogled-administracija', 'SZ Vodeni ogledi - administracija', 'Administracija', 'manage_options', 'sz-vodeni-ogled-administracija', 'sz_vodeni_ogled_administracija' );
    add_submenu_page('sz-vodeni-ogled-administracija', 'SZ Vodeni ogledi - nastavitve', 'Nastavitve', 'manage_options', 'sz-vodeni-ogled-nastavitve', 'sz_vodeni_ogled_nastavitve' );
}

function sz_vodeni_ogledi_admin_scripts() {
    wp_register_style( 'sz_vodeni_ogledi_admin_style', plugins_url( 'sz_vodeni_ogledi/admin/skocjan_vodeni_ogled_admin.css' ) );
    wp_enqueue_style( 'sz_vodeni_ogledi_admin_style' );

    wp_register_script( 'sz_vodeni_ogledi_admin_script', plugins_url( 'sz_vodeni_ogledi/admin/skocjan_vodeni_ogled_admin.js' ), array('jquery'), '1.0.0', true );
    wp_enqueue_script( 'sz_vodeni_ogledi_admin_script' );
}
add_action( 'admin_enqueue_scripts', 'sz_vodeni_ogledi_admin_scripts' );

function sz_vodeni_ogled_administracija(){
    if (!current_user_can('manage_options'))
        wp_die(__('Nimate pravice spreminjati nastavitev tega plugina', 'sz-vodeni-ogledi-admin'));

    // servisiraj formo

    // preverimo ali dobimo kakšn event/podatke da posodobimo posamezni termin
    if (isset($_POST['sz_vodeni_ogled_admin_navigation_form_term_id'])){
        // preverimo ali spreminjamo status termina
        if (isset($_POST['sz_vodeni_ogled_admin_navigation_form_toggleStatus_id'])){
            sz_vodeni_ogled_administracija_updateTermStatus($_POST['sz_vodeni_ogled_admin_navigation_form_term_id'], 1);
        } else {
            sz_vodeni_ogled_administracija_updateTermStatus($_POST['sz_vodeni_ogled_admin_navigation_form_term_id'], 0);
        }

        sz_vodeni_ogled_administracija_updateNotepad($_POST['sz_vodeni_ogled_admin_navigation_form_term_id'], $_POST['sz_vodeni_ogled_admin_navigation_form_notepad']);
    }

    // preverimo ali dobimo event / podatek da izbrišemo posamezni termin
    if (isset($_POST['sz_vodeni_ogled_admin_navigation_form_deleteTerm_id'])){
        sz_vodeni_ogled_administracija_deleteTerm($_POST['sz_vodeni_ogled_admin_navigation_form_deleteTerm_id']);
    }

    // pripravi izpis
    $pageItems = 25;
    $showPage = 1;

    if (isset($_GET['pageItems'])) {
        $pageItems = $_GET['pageItems'];
    }

    $selected_25 = ($_GET['pageItems'] == 25) ? 'selected' : '';
    $selected_50 = ($_GET['pageItems'] == 50) ? 'selected' : '';
    $selected_100 = ($_GET['pageItems'] == 100) ? 'selected' : '';

    $showPage = (isset($_GET['showPage'])) ? $_GET['showPage'] : 1;

    $show_past = (isset($_GET['showPast']) ) ? 'checked' : '';
    $show_confirmed = (isset($_GET['showConfirmed'])) ? 'checked' : '';


    // prikaži formo
    $genHtml = '<div class="sz_vodeni_ogled_admin_wrap">';
    $genHtml .= '<h1>'.__('Vodeni ogledi NRŠZ - Administracija', 'sz-vodeni-ogledi-admin').'</h1>';


    $genHtml .= '<div class="sz_vodeni_ogled_admin_navigation">';
    $genHtml .= '<table><tr>';
    $genHtml .= '<td>'.__('Število vnosov:', 'sz-vodeni-ogledi-admin').'<select name="pageItems" id="pageItems"><option value="25" '.$selected_25.'>25</option><option value="50" '.$selected_50.'>50</option><option value="100" '.$selected_100.'>100</option></select></td>';

    $genHtml .= '<td id="prevPage"><input type="button" name="sz_vodeni_ogled_admin_navigation_button_prev" id="sz_vodeni_ogled_admin_navigation_button_prev" value="' . __('<<', 'sz-vodeni-ogledi-admin') . '"></td>';
    $genHtml .= '<td id="countPage">' . $showPage . ' / ' . sz_vodeni_ogled_administracija_dobiSteviloStraniOgledov($pageItems, $show_past, $show_confirmed) . '</td>';
    $genHtml .= '<td id="nextPage"><input type="button" name="sz_vodeni_ogled_admin_navigation_button_next" id="sz_vodeni_ogled_admin_navigation_button_next" value="' . __('>>', 'sz-vodeni-ogledi-admin') . '"></td>';

    $genHtml .= '<td>'.__('Prikaži pretekle datume: ', 'sz-vodeni-ogledi-admin').'<input type="checkbox" name="showPast" id="showPast" value="true" '.$show_past.'></td>';
    $genHtml .= '<td>'.__('Prikaži že zaključene termine: ', 'sz-vodeni-ogledi-admin').'<input type="checkbox" name="showConfirmed" id="showConfirmed" value="true" '.$show_confirmed.'></td>';

    $genHtml .= '<td><input type="button" name="sz_vodeni_ogled_admin_navigation_button" id="sz_vodeni_ogled_admin_navigation_button" value="' . __('Filtriraj', 'sz-vodeni-ogledi-admin') . '"></td>';
    $genHtml .= '</tr></table>';

    $genHtml .= '<span style="display: none" id="sz_vodeni_ogled_admin_navigation_URL">'.$_GET['page'].'</span>';
    $genHtml .= '<span style="display: none" id="sz_vodeni_ogled_admin_navigation_currentPage">'.$showPage.'</span>';
    $genHtml .= '<span style="display: none" id="sz_vodeni_ogled_admin_navigation_pageMax">'.sz_vodeni_ogled_administracija_dobiSteviloStraniOgledov($pageItems, $show_past, $show_confirmed).'</span>';
    $genHtml .= '<span style="display: none" id="sz_vodeni_ogled_admin_navigation_pageItems">'.$pageItems.'</span>';
    $genHtml .= '</div>';

    $genHtml .= '<br/>';

    $genHtml .= '<div class="sz_vodeni_ogled_admin_content">';
    $genHtml .= '<table>';
    // header tabele za prikazovanje
    $genHtml .= '<tr class="header_big_top">';
    $genHtml .= '<th>'.__('Datum ogleda', 'sz-vodeni-ogledi-admin').'</th>';
    $genHtml .= '<th>'.__('Čas ogleda', 'sz-vodeni-ogledi-admin').'</th>';
    $genHtml .= '<th>'.__('Naziv naročnika', 'sz-vodeni-ogledi-admin').'</th>';
    $genHtml .= '<th>'.__('Kontaktna oseba', 'sz-vodeni-ogledi-admin').'</th>';
    $genHtml .= '<th>'.__('Telefon', 'sz-vodeni-ogledi-admin').'</th>';
    $genHtml .= '<th>'.__('Email', 'sz-vodeni-ogledi-admin').'</th>';
    $genHtml .= '<th>'.__('Status termina', 'sz-vodeni-ogledi-admin').'</th>';
    $genHtml .= '<th>'.__('Akcija', 'sz-vodeni-ogledi-admin').'</th>';
    $genHtml .= '</tr>';

    $genHtml .= '<tr><td colspan="8" class="composit_spacer"></td></tr>';

    $genHtml .= sz_vodeni_ogled_administracija_dobiKombiniraneTabeleOgledov($showPage, $pageItems, $show_past, $show_confirmed);

    $genHtml .= '</table>';
    $genHtml .= '</div>';

    $genHtml .= '</div> <!-- id="sz_odpiralni_cas_admin_wrap" -->';

    echo $genHtml;
}

function sz_vodeni_ogled_administracija_updateTermStatus($termID, $termStatus){
    global $wpdb;
    $table_name = $wpdb->prefix . "sz_vodeni_ogledi_data";

    // zaupamo in ignoriramo izhod
    $idList = $wpdb->get_results("UPDATE $table_name SET term_confirmed = $termStatus WHERE id = $termID");
}

function sz_vodeni_ogled_administracija_updateNotepad($termID, $notepadText){
    global $wpdb;
    $table_name = $wpdb->prefix . "sz_vodeni_ogledi_data";

    // zaupamo in ignoriramo izhod
    $idList = $wpdb->get_results("UPDATE $table_name SET term_confirmed_text = '$notepadText' WHERE id = $termID");

}

function sz_vodeni_ogled_administracija_deleteTerm($termID){
    global $wpdb;
    $table_name = $wpdb->prefix . "sz_vodeni_ogledi_data";

    // zaupamo in ignoriramo izhod
    $idList = $wpdb->get_results("DELETE FROM $table_name WHERE id = $termID");
}


function sz_vodeni_ogled_administracija_dobiSteviloStraniOgledov($pageItems, $show_past, $show_confirmed) {
    global $wpdb;
    $table_name = $wpdb->prefix . "sz_vodeni_ogledi_data";

    $termSelectCondition = '';

    // preveri če prikazujemo samo ne potrjene termine
    if ($show_confirmed == ''){
        $termSelectCondition .= 'WHERE term_confirmed = 0 ';
    }

    // preveri ali prikazujemo samo termine ki se začnejo kasneje kot ZDAJ
    if ($show_past == '') {
        // preveri ali smo že dodali kakšenb pogoj v iskanje
        if (strlen($termSelectCondition) == 0) {
            $termSelectCondition .= 'WHERE tour_date >= NOW() ';
        } else {
            $termSelectCondition .= 'AND tour_date >= NOW() ';
        }
    }

    $termSelectString = "SELECT * FROM $table_name $termSelectCondition ORDER BY tour_date ASC";

    $idList = $wpdb->get_results($termSelectString);
    $idItemCount = 0;
    foreach ($idList as $idItem) {
        $lastID = $idItem->id;
        $idItemCount++;
    }

    return ceil($idItemCount/$pageItems);
}

function sz_vodeni_ogled_administracija_dobiKombiniraneTabeleOgledov($whichPage, $pageItems, $show_past, $show_confirmed) {
    global $wpdb;
    $table_name = $wpdb->prefix . "sz_vodeni_ogledi_data";

    // koliko ogledov na stran
    $lastID = 0;

    $termSelectCondition = '';

    // preveri če prikazujemo samo ne potrjene termine
    if ($show_confirmed == ''){
        $termSelectCondition .= 'WHERE term_confirmed = 0 ';
    }

    // preveri ali prikazujemo samo termine ki se začnejo kasneje kot ZDAJ
    if ($show_past == '') {
        // preveri ali smo že dodali kakšenb pogoj v iskanje
        if (strlen($termSelectCondition) == 0) {
            $termSelectCondition .= 'WHERE tour_date >= NOW() ';
        } else {
            $termSelectCondition .= 'AND tour_date >= NOW() ';
        }
    }

    $termSelectString = '';
    if ($whichPage > 0) {
        $recordOffset = (($whichPage - 1) * $pageItems);
        $termSelectString = "SELECT * FROM $table_name $termSelectCondition ORDER BY tour_date ASC LIMIT $recordOffset, $pageItems";
    } else {
        // izberi vse
        $termSelectString = "SELECT * FROM $table_name $termSelectCondition ORDER BY tour_date ASC";
    }

    $tableHtml = '';

    $termList = $wpdb->get_results($termSelectString);
    foreach ($termList as $termItem) {
        // najprej izpišemo kompaktni del tabele - 5 celic : datum, čas, naročnik status potrditve, gumb za razširitev
        $tableHtml .= '<tr class="header_small_top">';
        $tableHtml .= '<td>' . $termItem->tour_date . '</td>';
        $tableHtml .= '<td>' . $termItem->tour_time . '</td>';
        $tableHtml .= '<td>' . $termItem->customer_name . '</td>';
        $tableHtml .= '<td>' . $termItem->contact_name . '</td>';
        $tableHtml .= '<td>' . $termItem->contact_phone . '</td>';
        $tableHtml .= '<td>' . $termItem->contact_email . '</tdcol>';

        if ($termItem->term_confirmed == 1) {
            $tableHtml .= '<td>' . __('Zaključem', 'sz-vodeni-ogledi-admin') . '</td>';
        } else {
            $tableHtml .= '<td>' . __('Odprt', 'sz-vodeni-ogledi-admin') . '</td>';
        }

        $tableHtml .= '<td><button class="button_show" type="button" onclick="jQuery(function ($) { $(\'#row_term_'. $termItem->id .'\').fadeToggle(); });">' . __('Prikaži / Skrij', 'sz-vodeni-ogledi-admin') . '</button></td>';
        $tableHtml .= '</tr>';
        // izpišemo del tabele s preostalimi podatki
        $tableHtml .= '<tr id="row_term_'. $termItem->id .'" style="display: none;"><td colspan="8" class="no_padding">';
        $tableHtml .= '<table id="subTable_term_'. $termItem->id .'" style="width: 100%">';
        $tableHtml .= '<tr class="header_small">';
        $tableHtml .= '<th colspan="2">' . __('Termin:', 'sz-vodeni-ogledi-admin') . '</th>';
        $tableHtml .= '<th colspan="2">' . __('Udeleženci:', 'sz-vodeni-ogledi-admin') . '</th>';
        $tableHtml .= '<th colspan="2">' . __('Naročnik:', 'sz-vodeni-ogledi-admin') . '</th>';
        $tableHtml .= '<th colspan="2">' . __('Posebni pogoji:', 'sz-vodeni-ogledi-admin') . '</th>';
        $tableHtml .= '</tr>';

        $tableHtml .= '<tr class="data_composit">';
        // 2 za termin
        $tableHtml .= '<td>' . __('Izbran jezik:', 'sz-vodeni-ogledi-admin') . '</td>';
        $tableHtml .= '<td>' . $termItem->tour_language . '</td>';
        // 2 za udeležence
        $tableHtml .= '<td>' . __('Število obiskovalcev:', 'sz-vodeni-ogledi-admin') . '</td>';
        $tableHtml .= '<td>' . $termItem->visitor_count . '</td>';
        // 2 za za naročnika
        $tableHtml .= '<td>' . __('Naslov:', 'sz-vodeni-ogledi-admin') . '</td>';
        $tableHtml .= '<td>' . $termItem->customer_address . '</td>';
        // 2x5 za posebne pogoje
        $tableHtml .= '<td colspan="2" rowspan="5"><pre>' . $termItem->extra_conditions . '</pre></td>';
        $tableHtml .= '</tr>';
        // ponavljaj vzorec
        $tableHtml .= '<tr class="data_composit">';
        // 2 za termin
        $tableHtml .= '<td>' . __('Program:', 'sz-vodeni-ogledi-admin') . '</td>';
        $tableHtml .= '<td>' . $termItem->tour_type . '</td>';
        // 2 za udeležence
        $tableHtml .= '<td>' . __('Število spremljevalcev:', 'sz-vodeni-ogledi-admin') . '</td>';
        $tableHtml .= '<td>' . $termItem->companion_count . '</td>';
        // 2 za za naročnika
        $tableHtml .= '<td>' . __('Tip naročnika:', 'sz-vodeni-ogledi-admin') . '</td>';
        $tableHtml .= '<td>' . $termItem->person_legal . '</td>';
        $tableHtml .= '</tr>';
        $tableHtml .= '<tr class="data_composit">';
        // 2 za termin
        $tableHtml .= '<td>' . __('Delovni listi:', 'sz-vodeni-ogledi-admin') . '</td>';
        $tableHtml .= '<td>' . $termItem->worksheet_status . '</td>';
        // 2 za udeležence
        $tableHtml .= '<td>' . __('Status obiskovalcev:', 'sz-vodeni-ogledi-admin') . '</td>';
        $tableHtml .= '<td>' . $termItem->visitor_status . '</td>';
        // 2 za za naročnika
        $tableHtml .= '<td>' . __('Davčni zavezanec:', 'sz-vodeni-ogledi-admin') . '</td>';
        $tableHtml .= '<td>' . $termItem->customer_subject_to_vat . '</td>';
        $tableHtml .= '</tr>';
        $tableHtml .= '<tr class="data_composit">';
        // 2 za termin
        $tableHtml .= '<td>' . __('Število listov:', 'sz-vodeni-ogledi-admin') . '</td>';
        $tableHtml .= '<td>' . $termItem->worksheet_count . '</td>';
        // 2 za udeležence
        $tableHtml .= '<td>' . __('Starost od:', 'sz-vodeni-ogledi-admin') . '</td>';
        $tableHtml .= '<td>' . $termItem->visitor_age_from . '</td>';
        // 2 za za naročnika
        $tableHtml .= '<td>' . __('Davčna številka:', 'sz-vodeni-ogledi-admin') . '</td>';
        $tableHtml .= '<td>' . $termItem->customer_tax_number . '</td>';
        $tableHtml .= '</tr>';
        $tableHtml .= '<tr class="data_composit">';
        // 2 za termin
        $tableHtml .= '<td colspan="2"></td>';
        // 2 za udeležence
        $tableHtml .= '<td>' . __('Starost do:', 'sz-vodeni-ogledi-admin') . '</td>';
        $tableHtml .= '<td>' .  $termItem->visitor_age_to . '</td>';
        // 2 za za naročnika
        $tableHtml .= '<td>' . __('Način plačila:', 'sz-vodeni-ogledi-admin') . '</td>';
        $tableHtml .= '<td>' . $termItem->payment_option . '</td>';
        $tableHtml .= '</tr>';

        // izpis meta podatkov
        $tableHtml .= '<tr class="data_composit">';
        $tableHtml .= '<td colspan="2">' . __('Meta podatki:', 'sz-vodeni-ogledi-admin') . '</td>';
        $tableHtml .= '<td colspan="2">' . __('Jezik seje:', 'sz-vodeni-ogledi-admin') . ' ' . $termItem->session_language . '</td>';
        $tableHtml .= '<td colspan="2">' . __('Datum seje:', 'sz-vodeni-ogledi-admin') . ' ' . $termItem->time_submitted . '</td>';
        $tableHtml .= '<td colspan="2">' . __('ID termina:', 'sz-vodeni-ogledi-admin') . ' ' . $termItem->id . '</td>';
        $tableHtml .= '</tr>';

        // izpis kontrol in polja za vnos komentarjev
        $tableHtml .= '<tr class="data_composit">';
        $tableHtml .= '<td colspan="2"><button class="button_show" type="button" onclick="jQuery(function ($) { $(\'#row_term_delete_'. $termItem->id .'\').fadeToggle(); });">' . __('IZBRIŠI TERMIN', 'sz-vodeni-ogledi-admin') . '</button></td>';
        $tableHtml .= '<td colspan="6" rowspan="4"><form name="sz_vodeni_ogled_admin_toggleStatus_form" id="sz_vodeni_ogled_admin_toggleStatus_form" method="post" action=""><textarea name="sz_vodeni_ogled_admin_navigation_form_notepad" id="sz_vodeni_ogled_admin_navigation_form_notepad" class="textarea_notepad">'.$termItem->term_confirmed_text.'</textarea></td>';
        $tableHtml .= '</tr>';

        $tableHtml .= '<tr class="data_composit">';
        $tableHtml .= '<td colspan="2">' . __('Beležka:', 'sz-vodeni-ogledi-admin') . '</td>';
        $tableHtml .= '</tr>';

        $termStatusChecked = ($termItem->term_confirmed == 1 ) ? 'checked' : '';

        $tableHtml .= '<tr class="data_composit">';
        $tableHtml .= '<td colspan="2">';
        $tableHtml .= '<label><input type="checkbox" name="sz_vodeni_ogled_admin_navigation_form_toggleStatus_id" id="sz_vodeni_ogled_admin_navigation_form_toggleStatus_id" value="'.$termItem->id.'" '.$termStatusChecked.'>';
        $tableHtml .= __('Označi termin kot zaključen:', 'sz-vodeni-ogledi-admin') . '<label></td>';
        $tableHtml .= '</tr>';

        $tableHtml .= '<tr class="data_composit">';
        $tableHtml .= '<td colspan="2"><input type="text" name="sz_vodeni_ogled_admin_navigation_form_term_id" id="sz_vodeni_ogled_admin_navigation_form_term_id" value="' . $termItem->id .'" style="display: none;">';
        $tableHtml .= '<input class="button_show" type="submit" name="sz_vodeni_ogled_admin_navigation_form_toggleStatus_button" value="' . __('Shrani beležko in status', 'sz-vodeni-ogledi-admin') . '"></form></td>';
        $tableHtml .= '</tr>';

        $tableHtml .= '<tr class="data_term_delete" id="row_term_delete_'. $termItem->id .'" style="display: none;"><td colspan="8" class="no_padding">';
        $tableHtml .= __('ALI STE PREPRIČANI DA ŽELITE IZBRISATI TERMIN:', 'sz-vodeni-ogledi-admin') . '<br/><br/>';
        $tableHtml .= '<table><tr>';
        $tableHtml .= '<td><form name="sz_vodeni_ogled_admin_deleteTerm_form" id="sz_vodeni_ogled_admin_deleteTerm_form" method="post" action="">';
        $tableHtml .= '<input type="text" name="sz_vodeni_ogled_admin_navigation_form_deleteTerm_id" id="sz_vodeni_ogled_admin_navigation_form_deleteTerm_id" value="' . $termItem->id .'" style="display: none;">';
        $tableHtml .= '<input type="submit" name="sz_vodeni_ogled_admin_navigation_form_deleteTerm_button" value="' . __('DA', 'sz-vodeni-ogledi-admin') . '"></form></td>';
        $tableHtml .= '<td><button type="button" onclick="jQuery(function ($) { $(\'#row_term_delete_'. $termItem->id .'\').fadeToggle(); });">' . __('NE', 'sz-vodeni-ogledi-admin') . '</button></td>';
        $tableHtml .= '</tr></table>';
        $tableHtml .= '</td></tr>';

        $tableHtml .= '</table>';

        $tableHtml .= '<tr><td colspan="8" class="composit_spacer"></td></tr>';

        $tableHtml .= '</td></tr>';
    }

    return $tableHtml;
}

// zastarela funkcija
function sz_vodeni_ogled_administracija_dobiTabeleOgledov($whichPage, $pageItems) {
    global $wpdb;
    $table_name = $wpdb->prefix . "sz_vodeni_ogledi_data";

    // koliko ogledov na stran
    $lastID = 0;

    $termSelectString = '';

    if ($whichPage > 0) {
        // najdi zadnji id prejšnje strani in zgeneriraj select za naslednjo stran
        $idList = $wpdb->get_results("SELECT id	FROM $table_name ORDER BY time_submitted DESC");

        if ($whichPage > 1) {

            $idItemCount = 0;
            foreach ($idList as $idItem) {
                $lastID = $idItem->id;
                $idItemCount++;

                if ($idItemCount >= (($whichPage - 1) * $pageItems))
                    break;
            }

            $termSelectString = "SELECT * FROM $table_name WHERE id < '$lastID' ORDER BY time_submitted DESC LIMIT $pageItems";
        } else {
            $termSelectString = "SELECT * FROM $table_name ORDER BY time_submitted DESC LIMIT $pageItems";
        }
    } else {
        // izberi vse
        $termSelectString = "SELECT * FROM $table_name ORDER BY time_submitted DESC";
    }

    $tableHtml = '';

    $termList = $wpdb->get_results($termSelectString);
    foreach ($termList as $termItem) {
        $tableHtml .= '<table>';

        // glava termina - meta
        $tableHtml .= '<tr>';
        $tableHtml .= '<td>' . __('ID:', 'sz-vodeni-ogledi-admin') . ' ' . $termItem->id . '</td>';
        $tableHtml .= '<td>' . __('Jezik seje:', 'sz-vodeni-ogledi-admin') . ' ' . $termItem->session_language . '</td>';
        $tableHtml .= '</tr>';

        $tableHtml .= '<tr>';
        $tableHtml .= '<td>' . __('Registracija oddana:', 'sz-vodeni-ogledi-admin') . '</td>';
        $tableHtml .= '<td>' . $termItem->time_submitted . '</td>';
        $tableHtml .= '</tr>';

        $tableHtml .= '<tr>';
        $tableHtml .= '<td>' . __('Naročnik:', 'sz-vodeni-ogledi-admin') . '</td>';
        $tableHtml .= '<td>' . $termItem->customer_name . '</td>';
        $tableHtml .= '</tr>';

        $tableHtml .= '<tr>';
        $tableHtml .= '<td>' . __('Naslov:', 'sz-vodeni-ogledi-admin') . '</td>';
        $tableHtml .= '<td>' . $termItem->customer_address . '</td>';
        $tableHtml .= '</tr>';

        $tableHtml .= '<tr>';
        $tableHtml .= '<td>' . __('Tip naročnika:', 'sz-vodeni-ogledi-admin') . '</td>';
        $tableHtml .= '<td>' . $termItem->person_legal . '</td>';
        $tableHtml .= '</tr>';

        $tableHtml .= '<tr>';
        $tableHtml .= '<td>' . __('Davčni zavezanec:', 'sz-vodeni-ogledi-admin') . '</td>';
        $tableHtml .= '<td>' . $termItem->customer_subject_to_vat . '</td>';
        $tableHtml .= '</tr>';

        $tableHtml .= '<tr>';
        $tableHtml .= '<td>' . __('Davčna številka:', 'sz-vodeni-ogledi-admin') . '</td>';
        $tableHtml .= '<td>' . $termItem->customer_tax_number . '</td>';
        $tableHtml .= '</tr>';

        $tableHtml .= '<tr>';
        $tableHtml .= '<td>' . __('Način plačila:', 'sz-vodeni-ogledi-admin') . '</td>';
        $tableHtml .= '<td>' . $termItem->payment_option . '</td>';
        $tableHtml .= '</tr>';

        $tableHtml .= '<tr>';
        $tableHtml .= '<td>' . __('Datum termina:', 'sz-vodeni-ogledi-admin') . '</td>';
        $tableHtml .= '<td>' . $termItem->tour_date . '</td>';
        $tableHtml .= '</tr>';

        $tableHtml .= '<tr>';
        $tableHtml .= '<td>' . __('Čas termina:', 'sz-vodeni-ogledi-admin') . '</td>';
        $tableHtml .= '<td>' . $termItem->tour_time . '</td>';
        $tableHtml .= '</tr>';

        $tableHtml .= '<tr>';
        $tableHtml .= '<td>' . __('Izbran jezik:', 'sz-vodeni-ogledi-admin') . '</td>';
        $tableHtml .= '<td>' . $termItem->tour_language . '</td>';
        $tableHtml .= '</tr>';

        $tableHtml .= '<tr>';
        $tableHtml .= '<td>' . __('Program:', 'sz-vodeni-ogledi-admin') . '</td>';
        $tableHtml .= '<td>' . $termItem->tour_type . '</td>';
        $tableHtml .= '</tr>';

        $tableHtml .= '<tr>';
        $tableHtml .= '<td>' . __('Delovni listi:', 'sz-vodeni-ogledi-admin') . '</td>';
        $tableHtml .= '<td>' . $termItem->worksheet_status . '</td>';
        $tableHtml .= '</tr>';

        $tableHtml .= '<tr>';
        $tableHtml .= '<td>' . __('Število listov:', 'sz-vodeni-ogledi-admin') . '</td>';
        $tableHtml .= '<td>' . $termItem->worksheet_count . '</td>';
        $tableHtml .= '</tr>';

        $tableHtml .= '<tr>';
        $tableHtml .= '<td>' . __('Število obiskovalcev:', 'sz-vodeni-ogledi-admin') . '</td>';
        $tableHtml .= '<td>' . $termItem->visitor_count . '</td>';
        $tableHtml .= '</tr>';

        $tableHtml .= '<tr>';
        $tableHtml .= '<td>' . __('Število spremljevalcev:', 'sz-vodeni-ogledi-admin') . '</td>';
        $tableHtml .= '<td>' . $termItem->companion_count . '</td>';
        $tableHtml .= '</tr>';

        $tableHtml .= '<tr>';
        $tableHtml .= '<td>' . __('Status obiskovalcev:', 'sz-vodeni-ogledi-admin') . '</td>';
        $tableHtml .= '<td>' . $termItem->visitor_status . '</td>';
        $tableHtml .= '</tr>';

        $tableHtml .= '<tr>';
        $tableHtml .= '<td>' . __('Starost od:', 'sz-vodeni-ogledi-admin') . '</td>';
        $tableHtml .= '<td>' . $termItem->visitor_age_from . '</td>';
        $tableHtml .= '</tr>';

        $tableHtml .= '<tr>';
        $tableHtml .= '<td>' . __('Starost dd:', 'sz-vodeni-ogledi-admin') . '</td>';
        $tableHtml .= '<td>' .  $termItem->visitor_age_to . '</td>';
        $tableHtml .= '</tr>';

        $tableHtml .= '<tr>';
        $tableHtml .= '<td>' . __('Odgovorna oseba:', 'sz-vodeni-ogledi-admin') . '</td>';
        $tableHtml .= '<td>' . $termItem->contact_name . '</td>';
        $tableHtml .= '</tr>';

        $tableHtml .= '<tr>';
        $tableHtml .= '<td>' . __('Telefon odgovorne osebe:', 'sz-vodeni-ogledi-admin') . '</td>';
        $tableHtml .= '<td>' . $termItem->contact_phone . '</td>';
        $tableHtml .= '</tr>';

        $tableHtml .= '<tr>';
        $tableHtml .= '<td>' . __('E-pošta odgovorne osebe:', 'sz-vodeni-ogledi-admin') . '</td>';
        $tableHtml .= '<td>' . $termItem->contact_email . '</td>';
        $tableHtml .= '</tr>';

        $tableHtml .= '<tr>';
        $tableHtml .= '<td colspan="2">' . __('Posebni pogoji:', 'sz-vodeni-ogledi-admin') . ' ' . $termItem->extra_conditions . '</td>';
        $tableHtml .= '</tr>';

        $tableHtml .= '</table>';
    }


    return $tableHtml;
}

function sz_vodeni_ogled_nastavitve(){
    if (!current_user_can('manage_options'))
        wp_die(__('Nimate pravice spreminjati nastavitev tega plugina', 'sz-vodeni-ogledi-admin'));

    // servisiraj formo

    // naloži novo datoteko XLS
    if(isset($_POST['sz_vodeni_ogled_xlsUpload_btn'])){
        $exe_time = ini_get('max_execution_time');
        if($exe_time < 60)
            ini_set('max_execution_time', 300); // 300 seconds = 5 minutes
        sz_vodeni_ogled_naloziXLS();
        ini_set('max_execution_time', $exe_time);
    }

    // posodobi stanje vklopa/izklopa registracij
    sz_vodeni_ogled_shrani_registrationMasterSwitch();

    // posodobi stanje vklopa/izklopa povratne informacije naročniku
    sz_vodeni_ogled_shrani_clientFeedbackMessage();

    // posodobi nastavitve za obveščanje
    sz_vodeni_ogled_registrationNotification();

    // posodobi povezavo do pogojev
    sz_vodeni_ogled_shrani_termsAndConditions();

    // posodobi povezavo do forme z registracijo
    sz_vodeni_ogled_shrani_registrationForm();

    // prikaži formo
    $userDefinedRegistrationClosed = get_option(Skocjan_vodeni_ogled::$option_name_registrationClosed);
    $selectedRegistrationClosed = ($userDefinedRegistrationClosed == 'closed') ? 'checked' : '';

    $clientFeedbackMessage = get_option(Skocjan_vodeni_ogled::$option_name_clientFeedbackMessage);
    $selectedClientFeedbackMessage = ($clientFeedbackMessage == 'send') ? 'checked' : '';

    // prikaži naloženo datoteko:
    $xls_file_info = get_option(Skocjan_vodeni_ogled::$option_name_file);

    $xls_fileName = __('Ni naložena', 'sz-vodeni-ogledi-admin');
    $xls_fileUrl = '';
    $missingClass = 'sz_missing';
    if(isset($xls_file_info) && is_array($xls_file_info)) {
        $xls_fileName = $xls_file_info['fileName'];
        $xls_fileUrl = $xls_file_info['fileURL'];
        $missingClass='';
    }

    // pripravi primer XLS datoteke
    $xls_dummyFileUrl = plugin_dir_url(__FILE__) . '../sz_vodeni_ogledi_primer.xlsx';
    $xls_dummyFilePath = plugin_dir_path(__FILE__) . '../sz_vodeni_ogledi_primer.xlsx';
    $dummyLink ='';
    if(file_exists($xls_dummyFilePath))
        $dummyLink = '(<a href="' . $xls_dummyFileUrl . '">' . __('Primer datoteke', 'sz-vodeni-ogledi-admin') . '</a>)';

    $genHtml = '<div class="sz_vodeni_ogled_settings_wrap">';

    $genHtml .= SZ_RegisterNotice_Helper::getNotices();

    $genHtml .= '<h1>'.__('Vodeni ogledi NRŠZ - Nastavitve', 'sz-vodeni-ogledi-admin').'</h1>';
    $genHtml .= '<h2>'.__('Določanje terminov:', 'sz-vodeni-ogledi-admin').'</h2>';
    $genHtml .= '<p>'.__('Če želite spremeniti termine rezervacij vodenih ogledov, potem spodaj ponovno naložite XLS datoteko s podatki.','sz-vodeni-ogledi-admin').'</p>';
    $genHtml .= '<table border="0">';
    $genHtml .= '<tr>';
    $genHtml .= '<td>' . __("XLS datoteka termini rezervacij:", "sz-vodeni-ogledi-admin") . "</td>";
    $genHtml .= '<td colspan="2">';
    if(empty($xls_fileUrl))
        $genHtml .= $xls_fileName . ' ' . $dummyLink;
    else
        $genHtml .= '<a href="' . $xls_fileUrl . '" >' . $xls_fileName . '</a> ' . $dummyLink;
    $genHtml .= '</td></tr>';
    $genHtml .= '<tr class="' . $missingClass . '">';
    $genHtml .= '<td>'.__('Spremeni termine rezervacij','sz-vodeni-ogledi-admin').':</td>';
    $genHtml .= '<td><form name="sz_vodeni_ogled_xls_fileUpload" method="post" action="" enctype="multipart/form-data">
            <input type="file" id="browse" name="sz_vodeni_ogled_xlsUpload" />
            <input type="submit" name="sz_vodeni_ogled_xlsUpload_btn" value="' . __('Naloži', 'sz-vodeni-ogledi-admin') . '" /></form></td>';
    $genHtml .= '</tr>';
    $genHtml .= '</table>';
    $genHtml .= '<br/>';

    $genHtml .= '<form name="sz_vodeni_ogled_registrationSettingsForm" id="sz_vodeni_ogled_registrationSettingsForm" method="post" action="">';
    $genHtml .= '<h2>'.__('Prijava na Vodene oglede:', 'sz-vodeni-ogledi-admin').'</h2>';
    $genHtml .= '<strong><input type="checkbox" name="sz_vodeni_ogled_userRegistrationMasterSwitch" id="sz_vodeni_ogled_userRegistrationMasterSwitch" value="closed"  ' . $selectedRegistrationClosed .' >'.__('Registracija je zaprta.', 'sz-vodeni-ogledi-admin').'</input> </strong><br />';
    $genHtml .= '<strong>'.__('URL do Strani s vsebino, ki se prikaže namesto obrazca, če je registracija zaprta:', 'sz-vodeni-ogledi-admin').'</strong><br/>';
    $genHtml .= '<input type="text" name="sz_vodeni_ogled_registrationClosed_textPage" id="sz_vodeni_ogled_registrationClosed_textPage" value="' . get_permalink(intval(get_option(Skocjan_vodeni_ogled::$option_name_registrationClosed_textPage))) . '" />';
    if (!empty(get_permalink(intval(get_option(Skocjan_vodeni_ogled::$option_name_registrationClosed_textPage))))) {
        $pageEditURL = get_site_url() . '/wp-admin/post.php?post=' . get_option(Skocjan_vodeni_ogled::$option_name_registrationClosed_textPage) . '&action=edit';
        $genHtml .= '<input type="button" onclick="location.href=\''.$pageEditURL.'\';" value="'.__('Uredi stran', 'sz-vodeni-ogledi-admin').'" />';
    }
    $genHtml .= '<br/><br/>';

    $genHtml .= '<h2>'.__('Obveščanje o prejetih rezervacijah:', 'sz-vodeni-ogledi-admin').'</h2>';
    $genHtml .= '<strong>'.__('Seznam email naslovov, kamor bodo poslana obvestila o novi rezervaciji.', 'sz-vodeni-ogledi-admin').'</strong><br/>';
    $genHtml .= '<table id="sz_vodeni_ogled_emailNotify_table" border="0">';
    $genHtml .= '<tr>';
    $genHtml .= '<td>' . __('Email naslovi:', 'sz-vodeni-ogledi-admin') . '</td>';
    $genHtml .= '<td align="right"><input type="button" id="sz_vodeni_ogled_emailNotify_add" name="sz_vodeni_ogled_emailNotify_add" value="' . __('Dodaj', 'sz-vodeni-ogledi-admin') . '" /></td>';
    $genHtml .= '</tr>';
    $notificationEmailListString = get_option(Skocjan_vodeni_ogled::$option_name_sendNotification_emailList);
    $notificationEmailList = explode(';', $notificationEmailListString);
    for ($ei = 0; $ei < sizeof($notificationEmailList); $ei++){
        if ($notificationEmailList[$ei] != '') {
            $genHtml .= '<tr>';
            $genHtml .= '<td><input type="text" name="sz_notifyEmailList[]" class="sz_closedDates" value="' . $notificationEmailList[$ei] . '"/></td>';
            $genHtml .= '<td><a class="removeEmail"></a> </td>';
            $genHtml .= '</tr>';
        }
    }
    $genHtml .= '</table>';
    $genHtml .= '<br/>';

    $genHtml .= '<h1>'.__('Nastavitve sporočil:', 'sz-vodeni-ogledi-admin').'</h1>';
    $genHtml .= '<strong>'.__('Ime pošiljatelja zahvalnega sporočila (uporabljeno v e-mailu):', 'sz-vodeni-ogledi-admin').'</strong><br/>';
    $genHtml .= '<input type="text" name="sz_vodeni_ogled_clientFeedbackMessage_name" id="sz_vodeni_ogled_clientFeedbackMessage_name" value="' . get_option(Skocjan_vodeni_ogled::$option_name_clientFeedbackMessage_name) . '" /><br />';
    $genHtml .= '<strong>'.__('Email naslov zahvalnega sporočila (uporabljeno v e-mailu):', 'sz-vodeni-ogledi-admin').'</strong><br/>';
    $genHtml .= '<input type="text" name="sz_vodeni_ogled_clientFeedbackMessage_email" id="sz_vodeni_ogled_clientFeedbackMessage_email" value="' . get_option(Skocjan_vodeni_ogled::$option_name_clientFeedbackMessage_email) . '" /><br />';
    $genHtml .= '<br/>';

    $genHtml .= '<h2>'.__('Obrazec za prijavo:', 'sz-vodeni-ogledi-admin').'</h2>';
    $genHtml .= '<strong>'.__('URL do Strani, kjer je obrazec za prijavo (s shortcode-om [sz_vodeni_ogled]):', 'sz-vodeni-ogledi-admin').'</strong><br/>';
    $genHtml .= '<input type="text" name="sz_userRegistrationForm" id="sz_userRegistrationForm" value="' . get_permalink(intval(get_option(Skocjan_vodeni_ogled::$option_name_registrationForm))) . '" />';
    if (!empty(get_permalink(intval(get_option(Skocjan_vodeni_ogled::$option_name_registrationForm))))) {
        $pageEditURL = get_site_url() . '/wp-admin/post.php?post=' . get_option(Skocjan_vodeni_ogled::$option_name_registrationForm) . '&action=edit';
        $genHtml .= '<input type="button" onclick="location.href=\''.$pageEditURL.'\';" value="'.__('Uredi stran', 'sz-vodeni-ogledi-admin').'" />';
    }
    $genHtml .= '<br/><br/>';

    $genHtml .= '<h2>'.__('E-mail naročniku po poslani prijavi na voden ogled:', 'sz-vodeni-ogledi-admin').'</h2>';
    $genHtml .= '<strong><input type="checkbox" name="sz_vodeni_ogled_clientFeedbackMessage" id="sz_vodeni_ogled_clientFeedbackMessage" value="send"  ' . $selectedClientFeedbackMessage .' >'.__('Omogoči.', 'sz-vodeni-ogledi-admin').'</input> </strong><br />';
    $genHtml .= '<strong>'.__('URL do Strani z zahvalo za oddano prijavo:', 'sz-vodeni-ogledi-admin').'</strong><br/>';
    $genHtml .= '<input type="text" name="sz_vodeni_ogled_clientFeedbackMessage_page" id="sz_vodeni_ogled_clientFeedbackMessage_page" value="' . get_permalink(intval(get_option(Skocjan_vodeni_ogled::$option_name_clientFeedbackMessage_page))) . '" />';
    if (!empty(get_permalink(intval(get_option(Skocjan_vodeni_ogled::$option_name_clientFeedbackMessage_page))))) {
        $pageEditURL = get_site_url() . '/wp-admin/post.php?post=' . get_option(Skocjan_vodeni_ogled::$option_name_clientFeedbackMessage_page) . '&action=edit';
        $genHtml .= '<input type="button" onclick="location.href=\''.$pageEditURL.'\';" value="'.__('Uredi stran', 'sz-vodeni-ogledi-admin').'" />';
    }
    $genHtml .= '<br/>';
    $genHtml .= '<br/>';

    $genHtml .= '<h2>'.__('Pogoji rezervacije ogledov:', 'sz-vodeni-ogledi-admin').'</h2>';
    $genHtml .= '<strong>'.__('URL do Strani s pogoji za oddajo prijave:', 'sz-vodeni-ogledi-admin').'</strong><br/>';
    $genHtml .= '<input type="text" name="sz_userTermsAndConditions" id="sz_userTermsAndConditions" value="' . get_permalink(intval(get_option(Skocjan_vodeni_ogled::$option_name_termsAndConditions))) . '" />';
    if (!empty(get_permalink(intval(get_option(Skocjan_vodeni_ogled::$option_name_termsAndConditions))))) {
        $pageEditURL = get_site_url() . '/wp-admin/post.php?post=' . get_option(Skocjan_vodeni_ogled::$option_name_termsAndConditions) . '&action=edit';
        $genHtml .= '<input type="button" onclick="location.href=\''.$pageEditURL.'\';" value="'.__('Uredi stran', 'sz-vodeni-ogledi-admin').'" />';
    }
    $genHtml .= '<br/><br/>';


    $genHtml .= '<input type="submit" name="sz_vodeni_ogled_registrationSettingsForm_button" value="' . __('Shrani', 'sz-vodeni-ogledi-admin') . '">';
    $genHtml .= '</form>';

    $genHtml .= '</div> <!-- id="sz_odpiralni_cas_admin_wrap" -->';

    echo $genHtml;
}

function sz_vodeni_ogled_shrani_registrationMasterSwitch() {
    if (isset($_POST['sz_vodeni_ogled_registrationSettingsForm_button'])) {
        $masterSwitch = $_POST['sz_vodeni_ogled_userRegistrationMasterSwitch'];
        if (!isset($masterSwitch) || is_null($masterSwitch))
            $masterSwitch = '';
        update_option(Skocjan_vodeni_ogled::$option_name_registrationClosed, $masterSwitch);

        if (isset($_POST['sz_vodeni_ogled_registrationClosed_textPage'])) {
            // preveri ali imamo celi url (http://...) ali wordpress url (stran/podstran)
            if (strpos($_POST['sz_userRegistrationForm'], 'http://') !== false) {
                update_option(Skocjan_vodeni_ogled::$option_name_registrationClosed_textPage, url_to_postid($_POST['sz_vodeni_ogled_registrationClosed_textPage']));
            } else {
                update_option(Skocjan_vodeni_ogled::$option_name_registrationClosed_textPage, get_page_by_path($_POST['sz_vodeni_ogled_registrationClosed_textPage'])->ID);
            }
        }
    }
}

function sz_vodeni_ogled_shrani_clientFeedbackMessage() {
    if (isset($_POST['sz_vodeni_ogled_registrationSettingsForm_button'])) {
        $notificationSwitch = $_POST['sz_vodeni_ogled_clientFeedbackMessage'];
        if (!isset($notificationSwitch) || is_null($notificationSwitch))
            $notificationSwitch = '';
        update_option(Skocjan_vodeni_ogled::$option_name_clientFeedbackMessage, $notificationSwitch);

        if (isset($_POST['sz_vodeni_ogled_clientFeedbackMessage_page'])) {
            // preveri ali imamo celi url (http://...) ali wordpress url (stran/podstran)
            if (strpos($_POST['sz_userRegistrationForm'], 'http://') !== false) {
                update_option(Skocjan_vodeni_ogled::$option_name_clientFeedbackMessage_page, url_to_postid($_POST['sz_vodeni_ogled_clientFeedbackMessage_page']));
            } else {
                update_option(Skocjan_vodeni_ogled::$option_name_clientFeedbackMessage_page, get_page_by_path($_POST['sz_vodeni_ogled_clientFeedbackMessage_page'])->ID);
            }
        }

        if (isset($_POST['sz_vodeni_ogled_clientFeedbackMessage_name'])) {
            update_option(Skocjan_vodeni_ogled::$option_name_clientFeedbackMessage_name, $_POST['sz_vodeni_ogled_clientFeedbackMessage_name']);
        }

        if (isset($_POST['sz_vodeni_ogled_clientFeedbackMessage_email'])) {
            update_option(Skocjan_vodeni_ogled::$option_name_clientFeedbackMessage_email, $_POST['sz_vodeni_ogled_clientFeedbackMessage_email']);
        }
    }
}

function sz_vodeni_ogled_registrationNotification() {
    if (isset($_POST[sz_notifyEmailList])) {
        $notificationEmailList = '';
        for($ei = 0; $ei < sizeof($_POST[sz_notifyEmailList]); $ei++) {
            $notificationEmailList .= $_POST[sz_notifyEmailList][$ei] .';';
        }

        update_option(Skocjan_vodeni_ogled::$option_name_sendNotification_emailList, $notificationEmailList);
    }
}

function sz_vodeni_ogled_shrani_termsAndConditions() {
    if (isset($_POST['sz_userTermsAndConditions'])) {
        // preveri ali imamo celi url (http://...) ali wordpress url (stran/podstran)
        if (strpos($_POST['sz_userRegistrationForm'], 'http://') !== false) {
            update_option(Skocjan_vodeni_ogled::$option_name_termsAndConditions, url_to_postid($_POST['sz_userTermsAndConditions']));
        } else {
            update_option(Skocjan_vodeni_ogled::$option_name_termsAndConditions, get_page_by_path($_POST['sz_userTermsAndConditions'])->ID);
        }
    }
}

function sz_vodeni_ogled_shrani_registrationForm(){
    if (isset($_POST['sz_userRegistrationForm'])) {
        // preveri ali imamo celi url (http://...) ali wordpress url (stran/podstran)
        if (strpos($_POST['sz_userRegistrationForm'], 'http://') !== false) {
            update_option(Skocjan_vodeni_ogled::$option_name_registrationForm, url_to_postid($_POST['sz_userRegistrationForm']));
        } else {
            update_option(Skocjan_vodeni_ogled::$option_name_registrationForm, get_page_by_path($_POST['sz_userRegistrationForm'])->ID);
        }
    }
}

function sz_vodeni_ogled_naloziXLS(){
    // shrani datoteko na server
    $fileName = basename($_FILES['sz_vodeni_ogled_xlsUpload']['name']);

    $target_dir = plugin_dir_path( __FILE__ ) . 'upload/';
    if(!is_dir($target_dir)){
        // probaj ustvarit mapo, če ne obstaja.
        try{
            mkdir($target_dir, 755);
        }catch (Exception $e){
            SZ_RegisterNotice_Helper::setError(__('Napaka: Ne morem ustvarit mape', 'sz-vodeni-ogledi-admin').' '.$target_dir.' '.__('Podrobnosti:', 'sz-vodeni-ogledi-admin').' '.$e->getMessage(), 1);
            return;
        }
    }
    $filePath = $target_dir . $fileName;
    $fileUrl = plugin_dir_url(__FILE__). 'upload/' . $fileName;

    $fileType = pathinfo($filePath, PATHINFO_EXTENSION);

    if($fileType != 'xlsx' && $fileType != 'xls') {
        SZ_RegisterNotice_Helper::setError(__('Napaka: Sistem podpira samo Excel datoteke (.XLS ali .XLSX).', 'sz-vodeni-ogledi-admin').' '.__('Datoteka je tipa ', 'sz-vodeni-ogledi-admin').$fileType.'.', 1);
        return;
    }

    // preparsaj datoteko in prepisi vrednosti v bazi
    $tmpFile = $_FILES['sz_vodeni_ogled_xlsUpload']['tmp_name'];
    $termini = Skocjan_vodeni_ogled::getInstance();
    $statusOk = $termini->parseXLSFile($tmpFile);
    if($statusOk !== true){
        SZ_RegisterNotice_Helper::setError(__('Prišlo je do napake pri branju podatkov. So podatki v datoteki pravilno formatirani?', "sz-vodeni-ogledi-admin")."<br/>".$statusOk, 1);
        return;
    }

    if (file_exists($filePath)) {
        unlink($filePath);
    }
    if (!move_uploaded_file($_FILES['sz_vodeni_ogled_xlsUpload']['tmp_name'], $filePath)) {
        SZ_RegisterNotice_Helper::setError(__('Prišlo je do napake pri prenosu. Kontaktirajte administratorja.', 'sz-vodeni-ogledi-admin'), 1);
        return;
    }

    update_option(Skocjan_vodeni_ogled::$option_name_file, array('fileName' => $fileName, 'filePath' => $filePath, 'fileURL'  => $fileUrl));
    update_option(Skocjan_vodeni_ogled::$option_name, serialize($termini));

    SZ_RegisterNotice_Helper::setSuccess(__('Datoteka uspešno naložena. Termini registracij vodenih ogledov so posodobljeni.', 'sz-vodeni-ogledi-admin'), 1);
}

/*
 * Class za izpis obvestil
 */
class SZ_RegisterNotice_Helper {
    private static $error;
    private static $info;
    private static $success;

    private static $error_repeat_times;
    private static $info_repeat_times;
    private static $success_repeat_times;

    public static function setError($msg, $repeatTimes = -1){
        SZ_RegisterNotice_Helper::$error = $msg;
        SZ_RegisterNotice_Helper::$error_repeat_times = $repeatTimes;
    }
    public static function setInfo($msg, $repeatTimes = -1){
        SZ_RegisterNotice_Helper::$info = $msg;
        SZ_RegisterNotice_Helper::$info_repeat_times = $repeatTimes;
    }
    public static function setSuccess($msg, $repeatTimes = -1){
        SZ_RegisterNotice_Helper::$success = $msg;
        SZ_RegisterNotice_Helper::$success_repeat_times = $repeatTimes;
    }
    public static function getNotices(){
        $out = '';
        if(!empty(SZ_RegisterNotice_Helper::$error)){
            $out .= '<div class="error notice"><p>'.SZ_RegisterNotice_Helper::$error.'</p></div>';
        }
        if(SZ_RegisterNotice_Helper::$error_repeat_times > 0)
            SZ_RegisterNotice_Helper::$error_repeat_times--;
        if(SZ_RegisterNotice_Helper::$error_repeat_times == 0)
            SZ_RegisterNotice_Helper::$error = "";

        if(!empty(SZ_RegisterNotice_Helper::$info)){
            $out .= '<div class="update-nag notice"><p>'.SZ_RegisterNotice_Helper::$info.'</p></div>';
        }
        if(SZ_RegisterNotice_Helper::$info_repeat_times > 0)
            SZ_RegisterNotice_Helper::$info_repeat_times--;
        if(SZ_RegisterNotice_Helper::$info_repeat_times == 0)
            SZ_RegisterNotice_Helper::$info = "";

        if(!empty(SZ_RegisterNotice_Helper::$success)){
            $out .= '<div class="updated notice"><p>'.SZ_RegisterNotice_Helper::$success.'</p></div>';
        }
        if(SZ_RegisterNotice_Helper::$success_repeat_times > 0)
            SZ_RegisterNotice_Helper::$success_repeat_times--;
        if(SZ_RegisterNotice_Helper::$success_repeat_times == 0)
            SZ_RegisterNotice_Helper::$success = "";
        return $out;
    }
}
?>