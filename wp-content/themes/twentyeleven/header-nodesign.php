<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<link rel="stylesheet" type="text/css" media="all" href="/wp-content/themes/twentyeleven/style_ie7.css" />
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<link rel="stylesheet" type="text/css" media="all" href="/wp-content/themes/twentyeleven/style_ie7.css" />
<![endif]-->
<!--[if IE 8 | IE 9]>
<html id="ie8" <?php language_attributes(); ?>>
<link rel="stylesheet" type="text/css" media="all" href="/wp-content/themes/twentyeleven/style_ie8.css" />
<![endif]-->
<!--[if IE 9]>
<html id="ie8" <?php language_attributes(); ?>>
<link rel="stylesheet" type="text/css" media="all" href="/wp-content/themes/twentyeleven/style_ie9.css" />
<![endif]-->
<!--[if !(IE 6) & !(IE 7) & !(IE 8) & !(IE 9) ]><!-->
<html <?php language_attributes(); ?>>
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html" charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />


<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyeleven' ), max( $paged, $page ) );

	?></title>

<!--Open graph API
<meta property="og:title" content="<?php// echo get_bloginfo('name'); ?>" />
<meta property="og:url" content="<?php// echo get_bloginfo('url'); ?>" />
<meta property="og:image" content="<?php// echo get_bloginfo('url'); ?>/wp-content/themes/twentyeleven/images/skocjanski_zatok_logo_opaque.png" />
<meta property="og:image" content="http://ptice.si/drustvo/dopps_logo.jpg" />
 END Open graph API -->


<link rel="profile" href="http://gmpg.org/xfn/11" />

<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->
<?php
	/* We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.js"></script>
<script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/js/jquery.easing.1.3.js"></script>
<script type="text/javascript">
var templateUrl = '<?php get_site_url(); ?>';
$(document).ready(function() {
	//INIT SLIDERS:
   	$(".slider").each(function(){
		//set heights and widths to masking elements:
		$content_elm =$(this).find(".content_page:first");
		if($content_elm.length != 0){

			var content_height = $content_elm.css("height").replace(/[^-\d\.]/g, '');
			var content_width = $content_elm.css("width").replace(/[^-\d\.]/g, '');
			$content_mask = $(this).find(".content_mask");
			$content_mask.css("height", content_height+"px");
			$content_mask.css("width", content_width+"px");

			//set slider content element to correct width
			var num_of_pages = $(this).find(".content_page").length;
			$(this).find(".content:first").css("width", (num_of_pages * content_width) +"px");

			//make first button active:
			$current_nav = $(this).find(".slider_links:first");
			$current_nav.find(".active").removeClass("active");
			$current_nav.children(":first").addClass("active");
		}
	});

	//INIT Slider buttons:
	$(".slider_links").each(function(){
		$slider_nav = $(this);

		$slider_nav.children().each(function(index){
			$(this).bind("click", function(){
				$current_nav = $(this).parent().parent();
				$slider_root = $current_nav.parent();

				//move
				var width = $slider_root.width();
				$con = $slider_root.find(".content:first").animate(
																	{left: (-index*width)+"px"},
																	"slow",
																	"easeOutExpo");

				//make active btn:
				$current_nav.find(".active:first").removeClass("active");
				$(this).addClass("active");
				$(this).find(".rotated").addClass(".active_background");
			});
		});
	});


	if($(".slider_auto_play").length >0){
		setInterval(function(){
				move_all_sliders();
			}, 10000);
	}

	function move_all_sliders(){
		$(".slider_auto_play").each(function(){
			$slider_root = $(this);
			//get active btn index:
			$navigation = $slider_root.find(".slider_links:first");
			$active_btn = $navigation.children(".active:first");
			var current_index = $navigation.children().index($active_btn);
			var num_of_elements = $navigation.children().length;

			var next_index = (current_index+1)%num_of_elements;
			$navigation.children(":nth-child("+(next_index+1)+")").click();
		});
	}

 });


//Make sure we actually search for something
function check_search(){
	if($("#s").val()){
		$("#searchform").submit();
	}else{
		return false;
	}
}
</script>



</head>

<body <?php body_class(); ?>>

<div id="site_holder" style="min-width:auto; min-height:auto;">


  <div class="skip-link"><a class="assistive-text" href="#content" title="<?php esc_attr_e( 'Skip to primary content', 'twentyeleven' ); ?>"><?php _e( 'Skip to primary content', 'twentyeleven' ); ?></a></div>

        <!-- HEADER ENDS HERE -->

