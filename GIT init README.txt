
Po prvem GIT Clone je treba:

   1. Razpakirat
      wp-content/uploads.zip

   2. Kreirat lokalno bazo in uvozit backup
      backupBaze_livesite_remote.gz

   3. V administraciji z uporabo plugina
      https://wordpress.org/plugins/better-search-replace/
      zamenjat spodnji poti s potmi na svojem strezniku

         siteUrl: http://skocjanski-zatok.org
         path: /var/www/skocjanski-zatok.org

      POMEMBNO: Ne delat search-replace na .sql fajlu, ker so nekatere tabele searizirane


   3. Preimenovat

      wp-config-bak.php
      v
      wp-config.php

      in posodobit link do mysql