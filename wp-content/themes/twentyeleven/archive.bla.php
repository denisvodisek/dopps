<?php
/*

*/
get_header(); ?>

<div id="primary">
			<div id="content" role="main">


				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'content', 'single' ); ?>

				<?php endwhile; // end of the loop. ?>

	</div><!-- #content -->
		</div><!-- #primary -->


	<div id="secondary" class="widget-area" role="complementary">
		<aside id="foo_widget-5" class="widget widget_foo_widget">
		<h1 class="entry-title"><?php the_title(); ?></h1>

		<?php get_search_form(); ?>

		<h2>Archives by Month:</h2>
		<ul>
			<?php wp_get_archives('type=monthly'); ?>
		</ul>


		</aside>
	</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>