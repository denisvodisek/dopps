��    =        S   �      8  (   9     b     h     o     r     �     �     �     �     �     �     �     �     �  $   �  !   !     C     J     P     V     Z     `     u     x  #   |     �     �     �     �     �  +   �                $     (     0     7  /   D  #   t     �     �  %   �     �     �  	   �               0  (   >     g  %   k     �     �  R   �     	     	     	     /	     4	     J	    Y	  )   g
     �
     �
     �
     �
     �
     �
     �
     �
     �
  "   �
           )     8  %   A  *   g     �     �     �     �     �  #   �     �     �  9   �  
        %     ?     W     `  B   x     �     �     �     �     �     �  G   �  '   :     b  %   �  .   �     �     �  	   �  #   �          %  ?   8     x  #   |     �     �  S   �          6     Q     l     p     �     =   ,                            4      9   6   2   (                    %                      .   <   :      
      $   3                	   7      1               *   "      /   ;   8      -   0      '                      !   &             +                    5         #                 )       (zdravstvene, ostale - navedite katere): April Avgust DA Datum vodenja: Davčna številka: Davčni zavezanec: December Delovni list: Do: E-naslov kontaktne osebe: Februar Fizična oseba Gotovina Izbrani datum nima prostih terminov. Izbrani termin je že rezerviran. Januar Julij Junij Maj Marec Morebitne posebnosti NE NED Napaka pri pošiljanju rezervacije. Naslov: Naziv naročnika: Način plačila: November Od tega spremljevalcev: Odgovorna / kontaktna oseba na dan vodenja: Oktober PET PON POŠLJI Po TRR Pravna oseba Prišlo je do napake pri posodobitvi koledarja. Prosim izpolnite manjkajoča polja. Prosim izpolnite polje. Prosim označite izbiro. Rezervacija je bila uspešno poslana. SOB SRE September Starost udeležencev od: Status  udeležencev: Strinjam se s Strinjati se morate s splošnimi pogoji. TOR Telefonska številka kontaktne osebe: Termin Ure vodenja / termin: Uvoziti morate podatke. Prijavite se v administracijo: Vodeni ogledi > Nastavitve. Vodenje v jeziku: Vodenje: splošnimi pogoji. ČET Št. delovnih listov: Število oseb: POT-Creation-Date: 2016-01-29 14:07:28+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2016-02-03 18:17+0100
Project-Id-Version: 
Last-Translator: 
Language-Team: 
Language: it
X-Generator: Poedit 1.5.4
 (problemi di salute, altro – elencate): Aprile Agosto SI Data della visita guidata: Codice fiscale: Soggetto ad IVA: Dicembre Schede didattiche: A: Indirizzo e-mail del responsabile: Febbraio Persona fisica Contanti La data scelta non ha periodi liberi. Il periodo scelto è già stato prenotato. Gennaio Luglio Giugno Maggio Marzo Eventuali particolarità o esigenze NO DOM Si è verificato un errore nell'invio della prenotazione. Indirizzo: Nome dell'organizzazione: Modalità di pagamento: Novembre Tra cui accompagnatori: La persona responsabile del gruppo il giorno della visita guidata: Ottobre VEN LUN INVIARE C/C bancario Persona giuridica Durante l'aggiornamento del calendario si sono verificati degli errori. Si prega di compilare i campi mancanti. Si prega di compilare il campo. Si prega di effettuare una selezione. La prenotazione è stata inviata con successo. SAB MER Settembre Fascia di età dei partecipanti da: Stato dei partecipanti: Sono d'accordo con Dovete essere d'accordo con i termini e le condizioni generali. MAR Numero telefonico del responsabile: Termine Orario della visita guidata: È necessario importare i dati. Entra amministrazione:  Vodeni ogledi > Nastavitve. Lingua della visita guidata: Tipo della visita guidata: i termini e le condizioni. GIO Numero schede didattiche: Numero persone: 