<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

get_header(); ?>

<?php
	// display all posts in the "novice" category with the same sidebars
	$cat_id = get_cat_ID('novice');
	$single_id = get_the_ID();
	if (in_category($cat_id, $single_id)) {
		$postDate = get_the_time('d.m.Y', get_the_ID());
		$postYear = get_the_time('Y', get_the_ID());
		$postMonth = get_the_time('m', get_the_ID());
		$postTitle = get_the_title(get_the_ID());
?>
		<div id="col_1_sidebar_2">
			<div id="sidebar_left">

				<div class="menu">
					<?php
					//$cat_title =single_cat_title('', false);
					//$cat_id = get_cat_ID($cat_title);
					//simarine_page_hierarchy_menu(get_the_ID());
					//	get_sidebar('left');
					//simarine_archives_menu('',$cat_id);
					sz_simarine_archives_menu($postDate,$cat_id);
					?>
				</div><!-- .menu -->

			</div><!-- #sidebar_left -->
			<div id="sidebar_right">

				<div class="menu">
                <span style="font-size:0.9em;">
                <?php

				//simarine_posts_in_month('','', $cat_id);
				sz_simarine_posts_in_month($postYear,$postMonth, $cat_id, $postTitle);
				?>
				</span>
				</div><!-- .menu -->

			</div><!-- #sidebar_right -->

			<div id="main_left_col">
                    <span id="nav_pointer">
                    	<a href="<?php echo get_site_url();?>"><?php _e("Home", "twentyeleven"); ?></a> >
                    	<a href="<?php echo get_category_link($cat_id);?>"><?=get_cat_name (get_cat_ID('novice'))?></a> >
						<a class="active" href="<?php echo get_permalink($single_id)?>"><?=single_post_title()?></a>

                    </span>
				<div class="clear"></div>
				<h1 class="page-title"><?php
					printf( __( '%s', 'twentyeleven' ), '<span>' . single_cat_title( '', false ) . '</span>' );
					?></h1>
				<?php if ( have_posts() ) : ?>

					<?php twentyeleven_content_nav( 'nav-above' ); ?>

					<?php /* Start the Loop */ ?>
					<?php while ( have_posts() ) : the_post(); ?>

						<nav id="nav-single">
							<h3 class="assistive-text"><?php _e( 'Post navigation', 'twentyeleven' ); ?></h3>
							<span class="nav-previous"><?php previous_post_link( '%link', __( '<span class="meta-nav">&larr;</span> Previous', 'twentyeleven' ), true ); ?></span>
							<span class="nav-next"><?php next_post_link('%link', __( 'Next <span class="meta-nav">&rarr;</span>', 'twentyeleven' ), true ); ?></span>
						</nav><!-- #nav-single -->

						<?php
						/* Include the Post-Format-specific template for the content.
						 * If you want to overload this in a child theme then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						get_template_part( 'content', get_post_format() );
						?>

					<?php endwhile; ?>

					<?php twentyeleven_content_nav( 'nav-below' ); ?>

				<?php else : ?>

					<article id="post-0" class="post no-results not-found">
						<header class="entry-header">
							<h1 class="entry-title"><?php _e( 'Nothing Found', 'twentyeleven' ); ?></h1>
						</header><!-- .entry-header -->

						<div class="entry-content">
							<p><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'twentyeleven' ); ?></p>
							<?php get_search_form(); ?>
						</div><!-- .entry-content -->
					</article><!-- #post-0 -->

				<?php endif; ?>


			</div><!-- #main_left_col -->


		</div><!-- #col_1_sidebar_2 -->
<?php
	// display all other single posts with no change
	} else {
?>
		<div id="primary">
			<div id="content" role="main">

				<?php while ( have_posts() ) : the_post(); ?>

					<nav id="nav-single">
						<h3 class="assistive-text"><?php _e( 'Post navigation', 'twentyeleven' ); ?></h3>
						<span class="nav-previous"><?php previous_post_link( '%link', __( '<span class="meta-nav">&larr;</span> Previous', 'twentyeleven' ) ); ?></span>
						<span class="nav-next"><?php next_post_link( '%link', __( 'Next <span class="meta-nav">&rarr;</span>', 'twentyeleven' ) ); ?></span>
					</nav><!-- #nav-single -->

					<?php get_template_part( 'content', 'single' ); ?>

					<?php comments_template( '', true ); ?>

				<?php endwhile; // end of the loop. ?>

			</div><!-- #content -->
		</div><!-- #primary -->
<?php
	}
?>
<?php get_footer(); ?>