<?php
/**
 * Template Name: Sidebar Template
 * Description: A Page Template that adds a sidebar to pages
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

get_header(); ?>

		<div id="primary">
			<div id="content" role="main">

				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'content', 'page' ); ?>

					<?php comments_template( '', true ); ?>

				<?php endwhile; // end of the loop. ?>

		<?php	if(get_post_meta($post->ID, 'pojavnost_graf', true)){	?>
				<div style="background: silver;">
				<script type="text/javascript" src="https://www.google.com/jsapi"></script>
			    <script type="text/javascript">
			      google.load("visualization", "1", {packages:["corechart"]});
			      google.setOnLoadCallback(drawChart);
			      function drawChart() {
			        var data = new google.visualization.DataTable();
			        data.addColumn('string', 'Mesec');
			        data.addColumn('number', 'Pojavnost');
			        data.addRows(<?=get_post_meta($post->ID, 'pojavnost_graf', true)?>);

			        var options = {
						width: 370, height: 187,
						colors: ['green'],
						chartArea:{left:30,top:10,width:275,height:155},
						fontSize: 8,
						vAxis:{maxValue: 200,minValue: 0, gridlines: {count: 11}},
						fontName: 'arial',

			        };

			        var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
			        chart.draw(data, options);
			      }
			    </script>
			    	<h2>Pojavnost</h2>
					<div id="chart_div"></div>
					<h2>Gnezdi&scaron;&#269;a</h2>
					<div id="gnezdisca_slika">
						<img src="<?=get_post_meta($post->ID, 'gnezdisca_slika', true)?>" alt="Gnezdi&scaron;&#269;e" />
					</div>

				</div>
		<?php	}	?>

			</div><!-- #content -->

		</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>