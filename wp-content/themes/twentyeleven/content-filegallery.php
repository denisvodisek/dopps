<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
?>
<script>
$(document).ready(function() {

});

</script>
<style type="text/css">
.filegallery_item{
	float: left;
	width: 185px;
	height: 270px;
	border: solid 1px #B7B7B7;
	margin: 5px 2px;
	overflow: hidden;
}

.filegallery_item h3,
.filegallery_item h3 a{
	font-size: 12px;
}
.filegallery_item h3{
	margin: 8px 0 0 3px;
}

.filegallery_image{
	margin: 0 10px;

}

.filegallery_image p{
	margin: 0px;
	display: block;
}

.filegallery_image img{
	width: 165px;
}


</style>

		<div id="col_1_sidebar_1">
        	<div id="sidebar_left">
                <div class="menu">
                	<?php
						simarine_page_hierarchy_menu(get_the_ID());
					?>
                    &nbsp;
			 	</div><!-- .menu -->
            </div><!-- #sidebar_left -->


       		<div id="main_left_col">
                <span id="nav_pointer">
                	<a href="<?php echo get_site_url();?>"><?php _e("Home", "twentyeleven"); ?></a>
                	<?php $anc = get_ancestors( get_the_ID(), 'page' );
						$anc = array_reverse($anc);
						foreach($anc as $id){
							$current++;
							$p = get_post($id);
							?>
							&gt; <a href="<?php echo get_permalink($p->ID);?>"><?php echo $p->post_title; ?></a>
							<?php
						}
					?>
                	&gt; <a href="<?php echo get_permalink(get_the_ID()); ?>" class="active"><?php the_title(); ?></a>

                </span>
                <div class="clear"></div>

                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <header class="entry-header">
                        <h1><?php the_title(); ?></h1>
                    </header><!-- .entry-header -->

                    <div class="entry-content">
                        <?php
						//get images and meta data from page!
							$args = array(
								'post_type' => 'attachment',
								'numberposts' => null,
								'post_status' => null,
								'post_parent' => $post->ID
							);
							$attachments = get_posts($args);
							if ($attachments) {
								$attachments = array_reverse($attachments);
								foreach ($attachments as $attachment) {
									if($attachment->post_excerpt != ""){
										$img = get_bloginfo( 'template_url' ) . "/images/naslovnice/" . $attachment->post_excerpt . ".jpg";
										if(!url_exists($img)){
											$img = get_bloginfo( 'template_url' ) . "/images/PDF_icon.png";
										}
									}else{
										$img = get_bloginfo( 'template_url' ) . "/images/PDF_icon.png";
									}
									?>
									<div class="filegallery_item">
										<h3><a href="<?php echo wp_get_attachment_url($attachment->ID);?>" title="Prenesi <?php echo ucfirst(apply_filters('the_title', $attachment->post_title));?>"><?php echo ucfirst(apply_filters('the_title', $attachment->post_title));?></a></h3>
										<br />
										<div class="filegallery_image">
											<a href="<?php echo wp_get_attachment_url($attachment->ID);?>" title="Prenesi <?php echo ucfirst(apply_filters('the_title', $attachment->post_title));?>"><img src="<?php echo $img; ?>" /></a>
											<p><?php echo $attachment->post_content ?></p>
										</div>

										<?php /*the_attachment_link($attachment->ID); */?>
									</div>
									<?php

								}
							}

							?>
                    </div><!-- .entry-content -->
                    <footer class="entry-meta">
                        <?php edit_post_link( __( 'Edit', 'twentyeleven' ), '<span class="edit-link">', '</span>' ); ?>
                    </footer><!-- .entry-meta -->
                </article><!-- #post-<?php the_ID(); ?> -->

            </div><!-- #main_left_col -->

        </div><!-- #col_1_sidebar_1 -->

