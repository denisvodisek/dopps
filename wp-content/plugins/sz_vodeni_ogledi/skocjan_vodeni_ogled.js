( function( window, undefined ) {
    'use strict';

    jQuery( function( $ ) {
        // najdi prvi datum ki od vključno danes ki ima gododek
        var dateObject = new Date();
        var dateToday = numZeroPad(dateObject.getDate(), 2) + '.' + numZeroPad((dateObject.getMonth()+1), 2) + '.' + dateObject.getFullYear();
        var firstDateWithTerms = getFirstDayWithTerms(dateToday);

        /* nastavi polje za izbiro datuma (potrebujemo vrednost da označimo izbrani dan ki ga forma prikazuje) */
        $( '#sz_vodeni_ogled_form_date').val(firstDateWithTerms);

        /* dodaj dogodke na kolendar (nastavi dogodke in obarva dneve (današnji, ibrani) */
        setEventsToCal();

        /* nastavi začetno stanje forme na današnji datum (ali prvi datum ki ima dogodek), zato da forma ni nikoli nenastavljena */
        setFormForDate(firstDateWithTerms);

        /* poselij izbiro statusov (enako za vse dogodke) */
        setVisitorStatusForTerm();

        /* dodaj dogodek ki prikaže kolendar za izbiro datuma */
        $( '#sz_vodeni_ogled_form_date' ).on( 'click', function() {
            // prikaži kolendar
            $( '.sz_vodeni_ogled_form_dateContainer #sz_vodeni_ogled_form_calendarContainer' ).show();

            // poskrbi da kolendar izgine če kliknemo izvennjega
            $( document ).ready( function() {
                var mouse_is_inside = false;

                $('.sz_vodeni_ogled_form_dateContainer #sz_vodeni_ogled_form_calendarContainer').hover(function(){
                    mouse_is_inside = true;
                }, function() {
                    mouse_is_inside = false;
                });

                $( 'body' ).mouseup( function() {
                    if (! mouse_is_inside) {
                        if ( $( '#sz_vodeni_ogled_form_calendarContainerMask' ).css('display') == 'none' ){
                            $( '.sz_vodeni_ogled_form_dateContainer #sz_vodeni_ogled_form_calendarContainer' ).hide();
                            $( '.sz_vodeni_ogled_form_dateContainer #sz_vodeni_ogled_form_calendarContainerMask' ).hide();
                        }
                    }
                });
            });
        });

        /* dodaj dogodek na izbiro fizične / pravne osebe */
        $( '#sz_vodeni_ogled_form_formType_person' ).on( 'click', function() {
            // če izberemo fizično osebo - skrij polja z dačno številjo in izbiro zavezanca
            $( '.sz_vodeni_ogled_form_table #tax_number_table' ).hide();
            $( '.sz_vodeni_ogled_form_table #subject_to_vat_table' ).hide();

            $( $( '.sz_vodeni_ogled_form_table' ).find('.sz_vodeni_ogled_form_error#customer_tax_number') ).hide();
            $( $( '.sz_vodeni_ogled_form_table' ).find('.sz_vodeni_ogled_form_error#subject_to_vat') ).hide();

            // preveri ali smo edini z napako, ter ustrezno skrij napis napake nad formo ko skrijemo davčno številjo in izbiro fizične/poslovne osebe
            // najdi vse izpise opozoril
            var arrWarnings_count = 0;
            $( '.sz_vodeni_ogled_form_error' ).each( function( ife, form_error ) {
                // ignoriraj opozorila za davčno številko in izbiro davčnega zavezanca TER opozorilo za manjkajoče termine in strinjanje s pogoji
                // zadnji dve opozorili ne spadata v skupino ki izpiše opozorilo za manjkajoča polja
                if (($(form_error).attr('id') != 'no_available_term') && ($(form_error).attr('id') != 'condition_agree_check')
                && ($(form_error).attr('id') != 'customer_tax_number') && ($(form_error).attr('id') != 'subject_to_vat')) {
                    // preveri ali je opozorilo aktivno
                    if ($(form_error).is( ':visible' )) {
                        arrWarnings_count++;
                    }
                }
            });

            // če smo edini ki javljamo napako, skrij napako ko skrijemo vnosna polja
            if (arrWarnings_count <= 0) {
                $( $( '.sz_vodeni_ogled_form_notice' ).find('#form_missing') ).hide();
            }
        });

        $( '#sz_vodeni_ogled_form_formType_legal' ).on( 'click', function() {
            // če izberemo pravno osebo - prikaži polja z dačno številjo in izbiro zavezanca
            $( '.sz_vodeni_ogled_form_table #tax_number_table' ).show();
            $( '.sz_vodeni_ogled_form_table #subject_to_vat_table' ).show();

            // preveri ali smo že poizkusili pošiljati in smo nakdadno spremenili izbiro fizične/poslovne osebe ter ustrezno označimo manjkajoča polja
            if ($( '.sz_vodeni_ogled_form_table #sz_vodeni_ogled_form_taxNum').hasClass( 'input_error' )) {
                $( $( '.sz_vodeni_ogled_form_table' ).find('.sz_vodeni_ogled_form_error#customer_tax_number') ).show();

                $( '.sz_vodeni_ogled_form_notice' ).show();
                $( $( '.sz_vodeni_ogled_form_notice' ).find('#form_missing') ).show();
            }
            if ($( '.sz_vodeni_ogled_form_table .sz_vodeni_ogled_form_subject_to_vat').hasClass( 'input_error' )) {
                $( $( '.sz_vodeni_ogled_form_table' ).find('.sz_vodeni_ogled_form_error#subject_to_vat') ).show();

                $( '.sz_vodeni_ogled_form_notice' ).show();
                $( $( '.sz_vodeni_ogled_form_notice' ).find('#form_missing') ).show();
            }
        });

        /* dodaj dogodek na vnosno polje za davčni, ki v primeru vpisa ki se začne z SI avtomatsko označi izbiro davčnega zavezanca */
        $( '#sz_vodeni_ogled_form_taxNum' ).on("keyup", function(){
            // če uporabnik še ni naredil dobene izbire
            if ($('input[name=sz_vodeni_ogled_form_subject_to_vat]:checked', '#sz_vodeni_ogled_form').val() == undefined) {
                if ($( '#sz_vodeni_ogled_form_taxNum').val().toLowerCase().substring(0,2) == 'si') {
                    $( '#sz_vodeni_ogled_form_subject_to_vat_yes' ).prop('checked', true);
                }
            }
        });

        /* dodaj dogodek na gumb za registracijo */
        $( '#sz_vodeni_ogled_calendar_submit_button').on( 'click', function() {
            handleSubmitButton();
        });



        function setEventsToCal() {
            // pripravi današnji datum
            var dateObject = new Date();
            var dateToday = numZeroPad(dateObject.getDate(), 2) + '.' + numZeroPad((dateObject.getMonth()+1), 2) + '.' + dateObject.getFullYear();

            // dodaj dogodek na posamezni dan
            $( '.sz_vodeni_ogled_calendar_day' ).each( function( idc, dayCell ) {
                // pripravi datum celice
                var dayCellDate = $( dayCell ).find('#sz_vodeni_ogled_calendar_day_dayData_date').html();

                // prveri ali imamo današnji datum, če je ga označi
                if (compareDate(dateToday, dayCellDate)) {
                    $( dayCell ).addClass('sz_vodeni_ogled_calendar_day_today');
                }

                // preveri ali imamo izbrani datum, če je ga označi
                var selectedDate = $( '#sz_vodeni_ogled_form_date' ).val();
                if (compareDate(selectedDate, dayCellDate)) {
                    $( dayCell ).addClass('sz_vodeni_ogled_calendar_day_selected');
                }

                // poišči možne termine
                var metaTermList = $( dayCell ).find( '.sz_vodeni_ogled_calendar_day_termData .sz_vodeni_ogled_calendar_day_termData_single' );

                // če imamo termin naj obesimo dogodek
                if (metaTermList.length > 0) {
                    $( dayCell ).addClass('sz_vodeni_ogled_calendar_day_hasTerm');

                    $( dayCell ).on( 'click', function() {
                        // označi izbran dan
                        $( '.sz_vodeni_ogled_calendar_day' ).each( function( idcil, dayCellInnerLoop ) {
                            $( dayCellInnerLoop ).removeClass('sz_vodeni_ogled_calendar_day_selected');
                        });
                        $( dayCell ).addClass('sz_vodeni_ogled_calendar_day_selected');

                        // nastavi formo za izbrani dan
                        setFormForDate(dayCellDate);

                        // skrij kolendar za izbiro datuma
                        $( '.sz_vodeni_ogled_form_dateContainer #sz_vodeni_ogled_form_calendarContainer' ).hide();
                    });
                }
            });

            // obesi funkcijo na navigacijske gumbe kolendarja
            // gumb za prejšnji mesec
            $( '#sz_vodeni_ogled_calendar_head_prev_button' ).on( 'click', function() {
                updateCanander(false);
            });
            // gumb za naslednji mesec
            $( '#sz_vodeni_ogled_calendar_head_next_button' ).on( 'click', function() {
                updateCanander(true);
            });
        }

        function getFirstDayWithTerms(dateToday) {
            // najprej preiščemo trenutni mesec (imamo velko možnost da obstaja tak datum)
            var dayCellsList = $( '.sz_vodeni_ogled_calendar').find( '.sz_vodeni_ogled_calendar_day' );
            var todayFound = false;
            var termFound = false;
            var searchDay = '';
            dayCellsList.each( function( idc, dayCell) {
                if (termFound == false) {
                    searchDay = $( $( dayCell ).find( '#sz_vodeni_ogled_calendar_day_dayData_date') ).html();
                    if (compareDate(dateToday, $( $( dayCell ).find( '#sz_vodeni_ogled_calendar_day_dayData_date') ).html())) {
                        todayFound = true;
                    }
                    if (todayFound) {
                        // imamo celico s izbranim datumom, posodobi formo za izbrani datum, pohlej če ima termine

                        // poišči možne termine
                        var metaTermList = $( dayCell ).find( '.sz_vodeni_ogled_calendar_day_termData .sz_vodeni_ogled_calendar_day_termData_single' );

                        // če nimamo termin zaključimo
                        if (metaTermList.length > 0) {
                            termFound = true;
                        }
                    }
                }
            });

            // če smo našli dan s termini
            if (termFound) {
                return searchDay;
            }

            //TODO poišči datum z termini v naslednjih mesecih preko ajaxa če ga ta mesec ne vsebuje

            // če vse spodleti vrni današnji datum da sistem deluje čeprav ipiše formo brez možnih terminov
            return dateToday;
        }

        function setFormForDate(selectedDate) {

            // nastavi datum v polje izbrani datum
            $( '#sz_vodeni_ogled_form_date').val(selectedDate);
            // počisti potencialna prejšnja opozorila
            $( $( '.sz_vodeni_ogled_form_table' ).find('.sz_vodeni_ogled_form_error#no_available_term') ).hide();
            $( '.sz_vodeni_ogled_form_table #sz_vodeni_ogled_form_date' ).removeClass( 'input_error' );

            // najdi celico z izbranim dnem
            var dayCellsList = $( '.sz_vodeni_ogled_calendar').find( '.sz_vodeni_ogled_calendar_day' );
            dayCellsList.each( function( idc, dayCell) {
                if (compareDate(selectedDate, $( $( dayCell ).find( '#sz_vodeni_ogled_calendar_day_dayData_date') ).html())) {

                    // imamo celico s izbranim datumom, posodobi formo za izbrani datum

                    /* zapolni možne termine */

                    // poišči kje prikažemo izbiro terminov
                    var domTermSelect = $( '#sz_vodeni_ogled_form_term' );
                    domTermSelect.html('');

                    // poišči možne termine
                    var metaTermList = $( dayCell ).find( '.sz_vodeni_ogled_calendar_day_termData .sz_vodeni_ogled_calendar_day_termData_single' );

                    // če nimamo terminov na izbrani dan
                    if (metaTermList.length <= 0 ) {
                        // prikažemo prazen seznam in počistimo jezikovne opcije
                        domTermSelect.append('<option disabled selected></option>');
                        // opozori da izbrani datum nima terminov
                        $( $( '.sz_vodeni_ogled_form_table' ).find('.sz_vodeni_ogled_form_error#no_available_term') ).show();
                        $( '.sz_vodeni_ogled_form_table #sz_vodeni_ogled_form_date' ).addClass( 'input_error' );

                        var domLangDiv = $( '.sz_vodeni_ogled_form_lang' );
                        domLangDiv.html('');
                    } else {
                        // če imamo termine, jih izpišemo in pripravimo jezikovne opcije
                        $( metaTermList ).each( function( idt, metaTerm ) {
                            var termLabel = 'Termin';  // privzamemo slovenščino
                            var termLabelTable = $( '.sz_vodeni_ogled_calendar_metaData_dataTable_termLabel');
                            termLabelTable.find('tr').each( function( idtl, termLabelEntry) {
                                if ($( $( termLabelEntry ).find('td')[0] ).html() == 'language') {
                                    termLabel = $( $( termLabelEntry ).find('td')[1] ).html();
                                }
                            });

                            var selectValue = $( $( metaTerm ).find('#sz_vodeni_ogled_calendar_day_termData_single_time')).html();
                            var selectString = termLabel + ' ' + (idt+1) + ' ('+ selectValue + ')';

                            domTermSelect.append('<option value="' + idt + '-' + selectValue + '">' + selectString + '</option>');
                        });

                    }
                    $( domTermSelect ).unbind( 'change' );
                    $( domTermSelect ).change( function() {
                        var selectValSplit = $( '#sz_vodeni_ogled_form_term option:selected' ).val().split('-');
                        setLanguageForTerm(selectedDate, selectValSplit[0]);
                    });

                    /* zapolni možne programe za prvi (izbrani) termin */
                    setProgramForTerm(selectedDate, 0);

                    /* zapolni možne jezike za prvi (izbrani) program */
                    setLanguageForTerm(selectedDate, 0);
                }
            });
        }

        function setVisitorStatusForTerm() {
            // termIdx - štejemo od 0

            // meta: preveri jezik seje - oznaka jezika
            var sessionLanguage = getCalendarSessionLanguage();

            // meta: preberi možne statuse in njihovo ime
            // prva vrstica - type : oznaka
            // vse ostale - koda jezika : ime
            var statusList = {};
            var statusListContainer = $('.sz_vodeni_ogled_calendar_metaData_dataTable_visitorStatus_single');

            $( statusListContainer ).each( function( idse, statusEntry ) {

                var statusElementsSingle = {};
                var statusTypeLabel = '';
                $( $( statusEntry ).find('tr')).each( function ( idse, statusElement ) {
                    if (idse == 0) {
                        statusTypeLabel = $( $( statusElement ).find('td')[1]).html();
                    } else {
                        statusElementsSingle[$( $( statusElement ).find('td')[0]).html()] = $( $( statusElement ).find('td')[1]).html();
                    }
                });
                statusList[statusTypeLabel] = statusElementsSingle;
            });

            var domVisitorStatusSelect = $( '#sz_vodeni_ogled_form_visitorStatus' );
            domVisitorStatusSelect.html('');

            var haveVisitorStatus = false;
            for (var statElement in statusList) {
                domVisitorStatusSelect.append('<option value="' + statElement + '-' + statusList[statElement][sessionLanguage] + '">' + statusList[statElement][sessionLanguage] + '</option>');
                haveVisitorStatus = true;
            }
            if (! haveVisitorStatus) {
                domVisitorStatusSelect.append('<option disabled selected></option>');
            }
        }

        function setProgramForTerm(selectedDate, termIdx) {
            // termIdx - štejemo od 0

            // meta: preveri jezik seje - oznaka jezika
            var sessionLanguage = getCalendarSessionLanguage();

            // meta: preberi možne programe in njihovo ime
            // prva vrstica - type : oznaka
            // vse ostale - koda jezika : ime
            var programList = {};
            var promramListContainer = $('.sz_vodeni_ogled_calendar_metaData_dataTable_program_single');

            $( promramListContainer ).each( function( idpc, progEntry ) {
                var progElementsSingle = {};
                var progTypeLabel = '';
                $( $( progEntry ).find('tr')).each( function ( idpe, progElement ) {
                    if (idpe == 0) {
                        progTypeLabel = $( $( progElement ).find('td')[1]).html();
                    } else {
                        progElementsSingle[$( $( progElement ).find('td')[0]).html()] = $( $( progElement ).find('td')[1]).html();
                    }
                });
                programList[progTypeLabel] = progElementsSingle;
            });

            // najdi celico z izbranim dnem
            var dayCellsList = $( '.sz_vodeni_ogled_calendar').find( '.sz_vodeni_ogled_calendar_day' );
            dayCellsList.each( function( idc, dayCell) {
                if (compareDate(selectedDate, $( $( dayCell ).find( '#sz_vodeni_ogled_calendar_day_dayData_date') ).html())) {

                    // imamo celico s izbranim datumom, posodobi formo za izbrani datum

                    /* zapolni možne programe za izbrani termin */

                    // poišči kje prikažemo izbiro programov
                    var domProgramDiv = $( '.sz_vodeni_ogled_form_termType' );
                    domProgramDiv.html('');

                    // poišči možne termine
                    var metaTermList = $( dayCell ).find( '.sz_vodeni_ogled_calendar_day_termData .sz_vodeni_ogled_calendar_day_termData_single' );

                    // če če imamo kakšen termin
                    if (metaTermList.length > 0 ) {
                        // pridobi vse možne programe za ta termin
                        var termProgList = $( $( $( metaTermList )[termIdx]).find('#sz_vodeni_ogled_calendar_day_termData_single_type')).html();
                        var termProgSplit = termProgList.split(',');


                        $( termProgSplit).each( function(idp, termProg) {
                            var progCode = termProg;
                            var progDescp = programList[progCode][sessionLanguage];

                            // označimo prvi program kot izbran
                            if (idp == 0) {
                                domProgramDiv.append('<div class="radio_top_bottom_pad"><label><input type="radio" name="sz_vodeni_ogled_form_type" value="' + progCode + '" checked>' + progDescp + '</label></div>');
                            } else {
                                domProgramDiv.append('<div class="radio_top_bottom_pad"><label><input type="radio" name="sz_vodeni_ogled_form_type" value="' + progCode + '" >' + progDescp + '</label></div>');
                            }
                        });
                    }
                }
            });
        }

        function setLanguageForTerm(selectedDate, termIdx) {
            // programIdx - štejemo od 0

            // meta: preveri jezik seje - oznaka jezika
            var sessionLanguage = getCalendarSessionLanguage();

            // meta: preberi možne jezike in njihovo ime
            // prva vrstica - label : oznaka
            // vse ostale - koda jezika : ime
            var langList = getLanguageList();

            // najdi celico z izbranim dnem
            var dayCellsList = $( '.sz_vodeni_ogled_calendar').find( '.sz_vodeni_ogled_calendar_day' );
            dayCellsList.each( function( idc, dayCell ) {
                if (compareDate(selectedDate, $( $( dayCell ).find( '#sz_vodeni_ogled_calendar_day_dayData_date') ).html()) ) {

                    // imamo celico s izbranim datumom, posodobi formo z možnimi jeziki termina

                    // poišči kje prikažemo izbiro programov
                    var domLangDiv = $( '.sz_vodeni_ogled_form_lang' );
                    domLangDiv.html('');
                    var domLangDivFirstInput = '';      // shranimo prvo izbiro
                    var boolLangDivFirstInput = false;  // če ni jezika trenutne seje, arbitrarno označimo prvo izbiro

                    // poišči možne termine
                    var metaTermList = $( dayCell ).find( '.sz_vodeni_ogled_calendar_day_termData .sz_vodeni_ogled_calendar_day_termData_single' );

                    // če če imamo kakšen termin
                    if (metaTermList.length > 0 ) {
                        // dobi možne jezike termina
                        var termLangList = $( $( $( metaTermList )[termIdx] ).find('#sz_vodeni_ogled_calendar_day_termData_single_language') ).html();
                        var termLangSplit = termLangList.split(',');

                        $( termLangSplit).each( function(idl, termLang) {
                            var langCode = termLang;
                            var langDescp = langList[langCode][sessionLanguage];

                            var langSelected = '';
                            if (sessionLanguage == langCode) {
                                langSelected = 'checked';
                                boolLangDivFirstInput = true;
                            }

                            // če je to prvi vnos ga shranimo posebaj in dodamo na koncu ko vemo ali imamo jezik seje na izbiro ali ne
                            if (idl == 0) {
                                domLangDivFirstInput = '<div class="radio_top_bottom_pad"><label><input type="radio" name="sz_vodeni_ogled_form_lang" value="' + langCode + '" ' + langSelected + '>' + langDescp + '</label></div>';
                            } else {
                                domLangDiv.append('<div class="radio_top_bottom_pad"><label><input type="radio" name="sz_vodeni_ogled_form_lang" value="' + langCode + '" ' + langSelected + '>' + langDescp + '</label></div>');
                            }
                        });

                        // če imamo jezik seje na voljo ja dodamo prvi element nespremenjen na vrh spiska, dručage ga označimo za izbranega
                        if (boolLangDivFirstInput) {
                            domLangDiv.prepend(domLangDivFirstInput);
                        } else {
                            var langCode = $($(langListTable.find('tr')[0]).find('td')[0]).html();
                            var langDescp = $($(langListTable.find('tr')[0]).find('td')[1]).html();

                            domLangDiv.prepend('<input type="radio" name="sz_vodeni_ogled_form_lang" id="sz_vodeni_ogled_form_lang" value="' + langCode + '" checked>' + langDescp + '<br/>');
                        }
                    }
                }
            });
        }

        function compareDate(dateOne, dateTwo) {
            // vhodni parametri so pričakovani da so v istem formatu (dd.mm.yyyy)
            var dateOneSplit = dateOne.split('.');
            var dateTwoSplit = dateTwo.split('.');

            if ((parseInt(dateOneSplit[0]) == parseInt(dateTwoSplit[0])) && (parseInt(dateOneSplit[1]) == parseInt(dateTwoSplit[1])) && (parseInt(dateOneSplit[1]) == parseInt(dateTwoSplit[1]))) {
                return true
            }

            return false;
        }

        function numZeroPad(num, places) {
            var zero = places - num.toString().length + 1;
            return Array(+(zero > 0 && zero)).join("0") + num;
        }


        function getCalendarSessionLanguage() {
            var sessionLanguage = 'sl';  // privzamemo slovenščino
            var sessionLangTable = $( '.sz_vodeni_ogled_calendar_metaData_dataTable_sessionLanguage');
            sessionLangTable.find('tr').each( function( idx, sesEntry) {
                if ($( $( sesEntry ).find('td')[0] ).html() == 'language') {
                    sessionLanguage = $( $( sesEntry ).find('td')[1] ).html();
                }
            });

            return sessionLanguage;
        }

        function getCalanderDate() {
            var calDate = '';
            var calDateTable = $( '.sz_vodeni_ogled_calendar_metaData_dataTable_calDate');
            calDateTable.find('tr').each( function( idcd, calDateEntry) {
                if ($( $( calDateEntry ).find('td')[0] ).html() == 'date') {
                    calDate = $( $( calDateEntry ).find('td')[1] ).html();
                }
            });

            return calDate;
        }

        function getLanguageList() {
            var langList = {};
            var langListContainer = $('.sz_vodeni_ogled_calendar_metaData_dataTable_language_single');

            $( langListContainer ).each( function( idlc, langEntry ) {
                var langElementsSingle = {};
                var langTypeLabel = '';
                $( $( langEntry ).find('tr')).each( function ( idle, langElement ) {
                    if (idle == 0) {
                        langTypeLabel = $( $( langElement ).find('td')[1]).html();
                    } else {
                        langElementsSingle[$( $( langElement ).find('td')[0]).html()] = $( $( langElement ).find('td')[1]).html();
                    }
                });
                langList[langTypeLabel] = langElementsSingle;
            });

            return langList;
        }

        function updateCanander(goNext) {
            // goNext == true - nadčlenji mesec, čene prejšnji mesec
            // poiščemo datum trenutnega kolendarja
            var calDate = getCalanderDate();
            var calDateSplit = calDate.split('.');

            var newYear = parseInt(calDateSplit[2]);
            var newMonth = parseInt(calDateSplit[1]);

            if (goNext) {;
                newMonth++;

                if (newMonth > 12) {
                    newMonth = 1;
                    newYear++;
                }
            } else {
                newMonth--;

                if (newMonth < 1) {
                    newMonth = 12;
                    newYear--;
                }
            }

            $.ajax({
                url       : sz_vodeni_ogledi.ajax_url,
                method    : 'POST',
                cache     : false,
                data      : {
                    action: 'ajax_get_calendar',
                    month : newMonth,
                    year  : newYear
                },
                beforeSend: function() {
                    $( $( '.sz_vodeni_ogled_form_notice' ).find('#update_calander') ).hide();

                    $( '#sz_vodeni_ogled_form_calendarContainerMask' ).fadeToggle();
                },
                success   : function( ajaxResponse ) {
                    $( '#sz_vodeni_ogled_form_calendarContainer' ).html(ajaxResponse);
                    setEventsToCal();
                    $('#sz_vodeni_ogled_form_calendarContainerMask').fadeToggle();
                },
                error     : function( response ) {
                    $( '#sz_vodeni_ogled_form_calendarContainer' ).hide();
                    $( '#sz_vodeni_ogled_form_calendarContainerMask' ).hide();

                    $( '.sz_vodeni_ogled_form_notice' ).show();
                    $( $( '.sz_vodeni_ogled_form_notice' ).find('#update_calander') ).show();
                }
            } );
        }

        function scroolToNoticeArea() {
            // scrool strani do območja za obvestila kjer prikazujemo napake
            $('html,body').animate({scrollTop: $('.sz_vodeni_ogled_form_notice').offset().top - $('.sz_vodeni_ogled_form_notice').height() }, 'slow');
        }

        function handleSubmitButton() {
            // skrij območje za obvestila
            $( '.sz_vodeni_ogled_form_notice').hide();
            // skrij tudi vse napise
            var noticeArray = $( '.sz_vodeni_ogled_form_notice').find('div');
            $( noticeArray ).each( function(idnt, noticeField) {
                $( noticeField ).hide();
            });
            // skrij vsa opozorila v formi
            $( $( '.sz_vodeni_ogled_form_table' ).find('.sz_vodeni_ogled_form_error') ).hide();
            $( '.sz_vodeni_ogled_form_table input' ).removeClass( 'input_error' );
            $( '.sz_vodeni_ogled_form_table .sz_vodeni_ogled_form_subject_to_vat' ).removeClass( 'input_error' );

            // preveri ali so izpolnjena vsa potrebna polja
            var fieldEmpty = false;

            // ali imamo izbrani termin (se lahko zgodi če ne najdemo dneva s prostim terminom in naložimo prazno formo)
            if (($( '#sz_vodeni_ogled_form_term' ).val() == '') || ($( '#sz_vodeni_ogled_form_term' ).val() == null)) {
                $( '.sz_vodeni_ogled_form_notice' ).show();
                $( $( '.sz_vodeni_ogled_form_notice' ).find('#form_missing') ).show();

                $( $( '.sz_vodeni_ogled_form_table' ).find('.sz_vodeni_ogled_form_error#no_available_term') ).show();
                $( '.sz_vodeni_ogled_form_table #sz_vodeni_ogled_form_date' ).addClass( 'input_error' );

                fieldEmpty = true;
            }

            // ime naročnika
            if ($( '#sz_vodeni_ogled_form_client' ).val() == '') {
                //$( '.sz_vodeni_ogled_form_notice' ).show();
                //$( $( '.sz_vodeni_ogled_form_notice' ).find('#customer_name') ).show();

                $( $( '.sz_vodeni_ogled_form_table' ).find('.sz_vodeni_ogled_form_error#customer_name') ).show();
                $( '.sz_vodeni_ogled_form_table #sz_vodeni_ogled_form_client' ).addClass( 'input_error' );
                $( $( '.sz_vodeni_ogled_form_notice' ).find('#form_missing') ).show();

                fieldEmpty = true;
            }
            // naslov naročnika
            if ($( '#sz_vodeni_ogled_form_address' ).val() == '') {
                $( '.sz_vodeni_ogled_form_notice' ).show();
                $( $( '.sz_vodeni_ogled_form_notice' ).find('#form_missing') ).show();

                ( $( '.sz_vodeni_ogled_form_table' ).find('.sz_vodeni_ogled_form_error#customer_address') ).show();
                $( '.sz_vodeni_ogled_form_table #sz_vodeni_ogled_form_address' ).addClass( 'input_error' );

                fieldEmpty = true;
            }
            // davčno številko - smao če smo pravna oseba
            // preveri v vsakem primeru in primerno označi polja, da se pravilno obarvajo tudi če preklapljamo med fizično/pravno osebo
            if ($( '#sz_vodeni_ogled_form_taxNum' ).val() == '') {
                $( '.sz_vodeni_ogled_form_table #sz_vodeni_ogled_form_taxNum' ).addClass( 'input_error' );

                if ($('input[name=sz_vodeni_ogled_form_formType]:checked', '#sz_vodeni_ogled_form').val() == 'legal') {
                    $( '.sz_vodeni_ogled_form_notice' ).show();
                    $( $( '.sz_vodeni_ogled_form_notice' ).find('#form_missing') ).show();

                    $( $( '.sz_vodeni_ogled_form_table' ).find('.sz_vodeni_ogled_form_error#customer_tax_number') ).show();
                    fieldEmpty = true;
                }
            }
            // davčni zavezanec - samo če smo pravna oseba
            // preveri v vsakem primeru in primerno označi polja, da se pravilno obarvajo tudi če preklapljamo med fizično/pravno osebo
            if ($('input[name=sz_vodeni_ogled_form_subject_to_vat]:checked', '#sz_vodeni_ogled_form').val() == undefined) {
                $( '.sz_vodeni_ogled_form_table .sz_vodeni_ogled_form_subject_to_vat' ).addClass( 'input_error' );

                if ($('input[name=sz_vodeni_ogled_form_formType]:checked', '#sz_vodeni_ogled_form').val() == 'legal') {
                    $( '.sz_vodeni_ogled_form_notice' ).show();
                    $( $( '.sz_vodeni_ogled_form_notice' ).find('#form_missing') ).show();

                    $( $( '.sz_vodeni_ogled_form_table' ).find('.sz_vodeni_ogled_form_error#subject_to_vat') ).show();
                    fieldEmpty = true;
                }
            }
            // število obiskovalcev
            if ($( '#sz_vodeni_ogled_form_numPeople' ).val() == '') {
                $( '.sz_vodeni_ogled_form_notice' ).show();
                $( $( '.sz_vodeni_ogled_form_notice' ).find('#form_missing') ).show();

                $( $( '.sz_vodeni_ogled_form_table' ).find('.sz_vodeni_ogled_form_error#visitor_companion') ).show();
                $( '.sz_vodeni_ogled_form_table #sz_vodeni_ogled_form_numPeople' ).addClass( 'input_error' );

                fieldEmpty = true;
            }
            // število spremljevalcev
            if ($( '#sz_vodeni_ogled_form_numCompanion' ).val() == '') {
                $( '.sz_vodeni_ogled_form_notice' ).show();
                $( $( '.sz_vodeni_ogled_form_notice' ).find('#form_missing') ).show();

                $( $( '.sz_vodeni_ogled_form_table' ).find('.sz_vodeni_ogled_form_error#visitor_companion') ).show();
                $( '.sz_vodeni_ogled_form_table #sz_vodeni_ogled_form_numCompanion' ).addClass( 'input_error' );

                fieldEmpty = true;
            }
            // starost in status obiskovalcev
            if ($( '#sz_vodeni_ogled_form_visitorAgeFrom' ).val() == '') {
                $( '.sz_vodeni_ogled_form_notice' ).show();
                $( $( '.sz_vodeni_ogled_form_notice' ).find('#form_missing') ).show();

                $( $( '.sz_vodeni_ogled_form_table' ).find('.sz_vodeni_ogled_form_error#from_to') ).show();
                $( '.sz_vodeni_ogled_form_table #sz_vodeni_ogled_form_visitorAgeFrom' ).addClass( 'input_error' );

                fieldEmpty = true;
            }
            if ($( '#sz_vodeni_ogled_form_visitorAgeTo' ).val() == '') {
                $( '.sz_vodeni_ogled_form_notice' ).show();
                $( $( '.sz_vodeni_ogled_form_notice' ).find('#form_missing') ).show();

                $( $( '.sz_vodeni_ogled_form_table' ).find('.sz_vodeni_ogled_form_error#from_to') ).show();
                $( '.sz_vodeni_ogled_form_table #sz_vodeni_ogled_form_visitorAgeTo' ).addClass( 'input_error' );

                fieldEmpty = true;
            }
            // število delovnih listov - če smo odgovorili DA
            if ($('input[name=sz_vodeni_ogled_form_worksheet]:checked', '#sz_vodeni_ogled_form').val() == 'yes') {
                if ($( '#sz_vodeni_ogled_form_worksheetNum' ).val() == '') {
                    $( '.sz_vodeni_ogled_form_notice' ).show();
                    $( $( '.sz_vodeni_ogled_form_notice' ).find('#form_missing') ).show();

                    $( $( '.sz_vodeni_ogled_form_table' ).find('.sz_vodeni_ogled_form_error#worksheet_number') ).show();
                    $( '.sz_vodeni_ogled_form_table #sz_vodeni_ogled_form_worksheetNum' ).addClass( 'input_error' );

                    fieldEmpty = true;
                }
            }
            // odgovorna oseba
            if ($( '#sz_vodeni_ogled_form_contactName' ).val() == '') {
                $( '.sz_vodeni_ogled_form_notice' ).show();
                $( $( '.sz_vodeni_ogled_form_notice' ).find('#form_missing') ).show();

                $( $( '.sz_vodeni_ogled_form_table' ).find('.sz_vodeni_ogled_form_error#contact_name') ).show();
                $( '.sz_vodeni_ogled_form_table #sz_vodeni_ogled_form_contactName' ).addClass( 'input_error' );

                fieldEmpty = true;
            }
            // telefonska številka odgovorne osebe
            if ($( '#sz_vodeni_ogled_form_contactNumber' ).val() == '') {
                $( '.sz_vodeni_ogled_form_notice' ).show();
                $( $( '.sz_vodeni_ogled_form_notice' ).find('#form_missing') ).show();

                $( $( '.sz_vodeni_ogled_form_table' ).find('.sz_vodeni_ogled_form_error#contact_phone') ).show();
                $( '.sz_vodeni_ogled_form_table #sz_vodeni_ogled_form_contactNumber' ).addClass( 'input_error' );

                fieldEmpty = true;
            }
            // elektronski naslov odgovorne osebe
            if ($( '#sz_vodeni_ogled_form_contactEmail' ).val() == '') {
                $( '.sz_vodeni_ogled_form_notice' ).show();
                $( $( '.sz_vodeni_ogled_form_notice' ).find('#form_missing') ).show();

                $( $( '.sz_vodeni_ogled_form_table' ).find('.sz_vodeni_ogled_form_error#contact_email') ).show();
                $( '.sz_vodeni_ogled_form_table #sz_vodeni_ogled_form_contactEmail' ).addClass( 'input_error' );

                fieldEmpty = true;
            }


            // preveri ali se strinjamo z pogoji, prvejamo na koncu da izpišemo še obvestila o manjkajočih poljih
            if ($( '#sz_vodeni_ogled_form_conditions').is(':checked') == false) {
                $( '.sz_vodeni_ogled_form_notice' ).show();
                $( $( '.sz_vodeni_ogled_form_notice' ).find('#condition_agree_check') ).show();

                $( $( '.sz_vodeni_ogled_form_table' ).find('.sz_vodeni_ogled_form_error#condition_agree_check') ).show();

                // pogoji so pomembni - this is a no no
                scroolToNoticeArea();
                return;
            }

            // preveri če imamo vse podatke ki jih potrebujemo
            if (fieldEmpty) {
                scroolToNoticeArea();
                return;
            }

            // shrani izpolnjen obrazec v bazo
            var db_session_language = '';
            var db_person_legal = '';
            var db_customer_name = '';
            var db_customer_address = '';
            var db_customer_subject_to_vat = '';
            var db_customer_tax_number = '';
            var db_tour_date = '';
            var db_tour_time = '';
            var db_tour_type = '';
            var db_tour_language = '';
            var db_visitor_count = '';
            var db_companion_count = '';
            var db_visitor_status = '';
            var db_visitor_age_from = '';
            var db_visitor_age_to = '';
            var db_worksheet_status = '';
            var db_worksheet_count = '';
            var db_contact_name = '';
            var db_contact_phone = '';
            var db_contact_email = '';
            var db_payment_option = '';
            var db_extra_conditions = '';

            // pripravi jezik seje
            db_session_language = getCalendarSessionLanguage();

            // pripravi status naročnika
            // zapiši berljiv tekst v bazo
            if ($('input[name=sz_vodeni_ogled_form_formType]:checked', '#sz_vodeni_ogled_form').val() == 'person') {
                db_person_legal = 'Fizična oseba';
            } else {
                db_person_legal = 'Pravna oseba';
            }

            // pripravi ime naročnika
            db_customer_name = $( '#sz_vodeni_ogled_form_client' ).val();

            // pripravi naslov naročnika
            db_customer_address = $( '#sz_vodeni_ogled_form_address' ).val();

            // pripravi davčno številko naročnika naročnika
            db_customer_tax_number = $( '#sz_vodeni_ogled_form_taxNum' ).val();

            // pripravi tip davčnega zavezanca
            // zapiši berljiv text v bazo
            db_customer_subject_to_vat = $('input[name=sz_vodeni_ogled_form_subject_to_vat]:checked', '#sz_vodeni_ogled_form').val();
            // zapiši berljiv tekst v bazo
            if (db_customer_subject_to_vat == 'yes') {
                db_customer_subject_to_vat = 'DA';
            } else {
                db_customer_subject_to_vat = 'NE';
            }

            // pripravi datum vodenja
            db_tour_date = $( '#sz_vodeni_ogled_form_date' ).val();

            // pripravi čas vodenja
            db_tour_time = $( '#sz_vodeni_ogled_form_term' ).val();
            db_tour_time = db_tour_time.split('-')[1];

            // pripravi tip vodenja (program)
            db_tour_type = $('input[name=sz_vodeni_ogled_form_type]:checked', '#sz_vodeni_ogled_form').val();
            // meta: preberi možne programe in njihovo ime
            // prva vrstica - type : oznaka
            // vse ostale - koda jezika : ime
            var programList = {};
            var progmramListContainer = $('.sz_vodeni_ogled_calendar_metaData_dataTable_program_single');
            $( progmramListContainer).each( function( idpc, progEntry) {
                var progElementsSingle = {};
                var progTypeLabel = '';
                $( $( progEntry ).find('tr')).each( function ( idpe, progElement) {
                    if (idpe == 0) {
                        progTypeLabel = $( $( progElement).find('td')[1]).html();
                    } else {
                        progElementsSingle[$( $( progElement).find('td')[0]).html()] = $( $( progElement).find('td')[1]).html();
                    }
                });
                programList[progTypeLabel] = progElementsSingle;
            });
            // zapiši berljiv tekst v bazo
            db_tour_type = programList[db_tour_type]['sl'];

            // pripravi jezik vodenja
            db_tour_language = $('input[name=sz_vodeni_ogled_form_lang]:checked', '#sz_vodeni_ogled_form').val();
            // meta: preberi možne jezike in njihovo ime - oznaka : ime
            var langList = getLanguageList();
            db_tour_language = langList[db_tour_language]['sl'];

            // pripravi število obiskovalcev
            db_visitor_count = $( '#sz_vodeni_ogled_form_numPeople' ).val();

            // pripravi število spremljevalcev
            db_companion_count = $( '#sz_vodeni_ogled_form_numCompanion' ).val();

            // pripravi starost in status obiskovalcev
            db_visitor_status = $( '#sz_vodeni_ogled_form_visitorStatus' ).val().split('-')[1];
            db_visitor_age_from = $( '#sz_vodeni_ogled_form_visitorAgeFrom' ).val();
            db_visitor_age_to = $( '#sz_vodeni_ogled_form_visitorAgeTo' ).val();

            // pripravi status delovnih listov
            db_worksheet_status = $('input[name=sz_vodeni_ogled_form_worksheet]:checked', '#sz_vodeni_ogled_form').val();
            // zapiši berljiv tekst v bazo
            if (db_worksheet_status == 'yes') {
                db_worksheet_status = 'DA';
            } else {
                db_worksheet_status = 'NE';
            }

            // pripravi število delovnih listov
            db_worksheet_count = $( '#sz_vodeni_ogled_form_worksheetNum' ).val();
            if (db_worksheet_count == '') {
                db_worksheet_count = 0;
            }

            // pripravi ime kontakta
            db_contact_name = $( '#sz_vodeni_ogled_form_contactName' ).val();

            // telefonsko številko ime kontakta
            db_contact_phone = $( '#sz_vodeni_ogled_form_contactNumber' ).val();

            // email naslov ime kontakta
            db_contact_email = $( '#sz_vodeni_ogled_form_contactEmail' ).val();

            // pripravi način plačila
            db_payment_option = $('input[name=sz_vodeni_ogled_form_paymentType]:checked', '#sz_vodeni_ogled_form').val();
            // zapiši berljiv tekst v bazo
            if (db_payment_option == 'bill') {
                db_payment_option = 'Po TRR';
            } else {
                db_payment_option = 'Gotovina';
            }

            // pripravi morebitne posebnosti
            db_extra_conditions = $( '#sz_vodeni_ogled_form_specialInstructions' ).val();

            // vstavi podatke v bazo
            $.ajax({
                url       : sz_vodeni_ogledi.ajax_url,
                method    : 'POST',
                cache     : false,
                data      : {
                    action: 'ajax_save_tour',
                    session_language: db_session_language,
                    person_legal: db_person_legal,
                    customer_name: db_customer_name,
                    customer_address: db_customer_address,
                    customer_subject_to_vat: db_customer_subject_to_vat,
                    customer_tax_number: db_customer_tax_number,
                    tour_date: db_tour_date,
                    tour_time: db_tour_time,
                    tour_type: db_tour_type,
                    tour_language: db_tour_language,
                    visitor_count: db_visitor_count,
                    companion_count: db_companion_count,
                    visitor_status: db_visitor_status,
                    visitor_age_from: db_visitor_age_from,
                    visitor_age_to: db_visitor_age_to,
                    worksheet_status: db_worksheet_status,
                    worksheet_count: db_worksheet_count,
                    contact_name: db_contact_name,
                    contact_phone: db_contact_phone,
                    contact_email: db_contact_email,
                    payment_option: db_payment_option,
                    extra_conditions: db_extra_conditions
                },
                beforeSend: function() {
                    $( '.sz_vodeni_ogled_form_mask' ).fadeToggle();
                },
                success   : function( ajaxResponse ) {
                    if (ajaxResponse == 'ok') {
                        // prikaži uspeh
                        $( '.sz_vodeni_ogled_form_notice').show();
                        $( $( '.sz_vodeni_ogled_form_notice').find('#registration_complete') ).show();

                        // skrij formo
                        $( '.sz_vodeni_ogled').hide();
                        // skrij masko
                        $( '.sz_vodeni_ogled_form_mask' ).hide();
                    } else {
                        // prikaži napake
                        // preveri ne splošne napake
                        if (ajaxResponse.split(':')[1] == 'termNotAvailable') {
                            // prikaži da je termin že rezerviran
                            $( '.sz_vodeni_ogled_form_notice').show();
                            $( $( '.sz_vodeni_ogled_form_notice').find('#term_exists') ).show();
                        } else {
                            // prikaži splošno napako
                            $( '.sz_vodeni_ogled_form_notice').show();
                            $( $( '.sz_vodeni_ogled_form_notice').find('#general_error') ).show();
                        }

                        scroolToNoticeArea();

                        $( '.sz_vodeni_ogled_form_mask' ).fadeToggle();
                    }
                },
                error     : function( response ) {
                    // prikaži splošno napako
                    $( '.sz_vodeni_ogled_form_notice').show();
                    $( $( '.sz_vodeni_ogled_form_notice').find('#general_error') ).show();

                    scroolToNoticeArea();

                    $( '.sz_vodeni_ogled_form_mask' ).fadeToggle();
                }
            } );
        }
    });
} )( this );