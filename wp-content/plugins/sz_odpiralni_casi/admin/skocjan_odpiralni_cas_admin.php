<?php
/**
 * Created by PhpStorm.
 * User: urosoresic
 * Date: 1/10/16
 * Time: 10:08 PM
 * Text Domain: sz-odpiralni-casi-admin
 */
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

add_action("admin_menu", "sz_odpiralni_cas_admin_menu");

function sz_odpiralni_cas_admin_menu(){
    add_options_page("SZ Odpiralni čas", "Odpiralni čas", "manage_options", "sz-odpiralni-cas", "sz_odpiralni_cas_nastavitve");
}

function sz_odpiralni_casi_admin_scripts($hook){
    if($hook != 'settings_page_sz-odpiralni-cas')
        return;
    wp_register_script( 'sz_odpiralni_casi_datepicker-sl', plugins_url( 'datepicker-sl.js', __FILE__), array(), '1.0.1', true );

    wp_register_script( 'sz_odpiralni_casi_dateInteraction', plugins_url( 'sz_odpiralni_casi_admin.js', __FILE__), array("jquery","jquery-ui-core", "jquery-effects-core", "jquery-ui-datepicker","sz_odpiralni_casi_datepicker-sl"), '1.0.1', true );
    wp_enqueue_script('sz_odpiralni_casi_dateInteraction');
    wp_register_style('sz_odpiralni_casi_admin_style', plugins_url( 'sz_admin_style.css', __FILE__));
    wp_enqueue_style('sz_odpiralni_casi_admin_style');
}
add_action( 'admin_enqueue_scripts', 'sz_odpiralni_casi_admin_scripts' );

function sz_odpiralni_casi_admin_notices() {
    $instance = Skocjan_odpiralni_cas::getInstance();
    $adminLink = Skocjan_odpiralni_cas::$linkToAdminPanel;
    //Izpiši warning, če še ni definiranega odpiralnega časa:
    if(!$instance->isReady() && basename($_SERVER['REQUEST_URI']) != basename($adminLink)) {
        ?>
        <div class="error">
            <p><?php echo "<strong>".__("Odpiralni čas: ", "sz-odpiralni-casi-admin")."</strong> ".__("Uvoziti morate podatke: ", "sz-odpiralni-casi-admin")."<a href='$adminLink'>".__("Nastavitve > Odpiralni čas.", "sz-odpiralni-casi-admin")."</a>"; ?></p>
        </div>
        <?php
    }
    //Izpiši warning, če je rezervat danes zaprt:
    if($instance->isReady() && (Skocjan_odpiralni_cas::areWeClosedForEver() || Skocjan_odpiralni_cas::areWeClosed(time(),"NRSZ")) && basename($_SERVER['REQUEST_URI']) != basename($adminLink)) {
        ?>
        <div class="notice">
            <p><?php echo "<strong>".__("Odpiralni čas: ", "sz-odpiralni-casi-admin")."</strong> ".__("NRŠZ", "sz-odpiralni-casi-admin")." ".SZ_Schedule::getHourText('is closed today').". <a href='$adminLink'>".__("Nastavitve > Odpiralni čas.", "sz-odpiralni-casi-admin")."</a>"; ?></p>
        </div>

        <?php
    }
}
add_action( 'admin_notices', 'sz_odpiralni_casi_admin_notices' );


function sz_odpiralni_cas_nastavitve()
{
    if (!current_user_can("manage_options"))
        wp_die(__("Nimate pravice spreminjati nastavitev tega plugina", "sz-odpiralni-casi-admin"));

    //Uporabnik hoče naložit nov XLS:
    if(isset($_POST['sz_fileupload_btn'])){
        $exe_time = ini_get('max_execution_time');
        if($exe_time < 60)
            ini_set('max_execution_time', 300); //300 seconds = 5 minutes
        sz_odpiralni_cas_nalozidatoteko();
        ini_set('max_execution_time', $exe_time);
    }

    //Prikaži naloženo datoteko:
    $xls_file_info = get_option(Skocjan_odpiralni_cas::$option_name_file);

    $xls_fileName = __("Ni naložena", "sz-odpiralni-casi-admin");
    $xls_fileUrl = "";
    $missingClass = "sz_missing";
    if(isset($xls_file_info) && is_array($xls_file_info)) {
        $xls_fileName = $xls_file_info['filename'];
        $xls_fileUrl = $xls_file_info['fileurl'];
        $missingClass="";
    }
    //dodaj sample file:
    $xls_dummyFileUrl = plugin_dir_url(__FILE__)."../sz_odpiralni_casi_primer.xlsx";
    $xls_dummyFilePath = plugin_dir_path(__FILE__)."../sz_odpiralni_casi_primer.xlsx";
    $dummyLink ="";
    if(file_exists($xls_dummyFilePath))
        $dummyLink = "(<a href='$xls_dummyFileUrl'>".__("Primer datoteke", "sz-odpiralni-casi-admin")."</a>)";

    //Prikaži seznam nastavitev:
    $out = "<div id='sz_odpiralni_cas_admin_wrap'>";
    $out .= "<h1>".__("Odpiralni čas NRŠZ in COOO", "sz-odpiralni-casi-admin")."</h1>";
    $out .= "<p>".__("Če želite spremeniti odpiralni čas, potem spodaj ponovno naložite XLS datoteko s podatki.","sz-odpiralni-casi-admin")."</p>";

    $out .= SZ_Notice_Helper::getNotices();
    $out .= "<table border='0'>";
    $out .= "<tr>";
    $out .= "<td>" . __("XLS datoteka z odpiralnimi časi:", "sz-odpiralni-casi-admin") . "</td>";
    $out .= "<td colspan='2'>";
    if(empty($xls_fileUrl))
        $out .= "$xls_fileName $dummyLink";
    else
        $out .= "<a href='$xls_fileUrl' >$xls_fileName</a> $dummyLink";
    $out .= "</td></tr>";
    $out .= "<tr class='$missingClass'>";
    $out .= "<td>".__("Spremeni odpiralni čas","sz-odpiralni-casi-admin").":</td>";
    $out .= "<td><form name='sz_xls_fileUpload' method='post' action='' enctype='multipart/form-data'><input type='file' id='browse' name='sz_fileupload' />
            <input type='submit' name='sz_fileupload_btn' value='" . __("Naloži", "sz-odpiralni-casi-admin") . "' /></form></td>";
    $out .= "</tr>";
    $out .= "</table>";


    //Implementiraj dodajanje posameznih datumov, kjer je rezervat zaprt
    if(isset($_POST['sz_datesFromDB'])){
        sz_odpiralni_cas_shraniClosedDates();
    }
    $userDefinedClosedDates = unserialize(get_option(Skocjan_odpiralni_cas::$option_name_closedDates));
    $userDefinedClosedDates_COOO = unserialize(get_option(Skocjan_odpiralni_cas::$option_name_closedDates_COOO));
    $userDefinedThePlaceIsClosed = get_option(Skocjan_odpiralni_cas::$option_name_placeIsClosed);
    $userDefinedWeekendAsWorkdays = get_option(Skocjan_odpiralni_cas::$option_name_useWeekScheduleForWeekends);
    $selectedPlaceIsClosed = ($userDefinedThePlaceIsClosed == "closed") ? "checked" : "";
    $selectedWeekendAsWorkdays = ($userDefinedWeekendAsWorkdays == "yes") ? "checked" : "";

    $userDefinedClosedDatesString = get_option(Skocjan_odpiralni_cas::$option_name_closedDays);
    $selectedCloseDayMonday = (strpos($userDefinedClosedDatesString, 'mon') !== false) ? "checked" : "";
    $selectedCloseDayTuesday = (strpos($userDefinedClosedDatesString, 'tue') !== false) ? "checked" : "";
    $selectedCloseDayWednesday = (strpos($userDefinedClosedDatesString, 'wed') !== false) ? "checked" : "";
    $selectedCloseDayThursday = (strpos($userDefinedClosedDatesString, 'thu') !== false) ? "checked" : "";
    $selectedCloseDayFriday = (strpos($userDefinedClosedDatesString, 'fri') !== false) ? "checked" : "";
    $selectedCloseDaySaturday = (strpos($userDefinedClosedDatesString, 'sat') !== false) ? "checked" : "";
    $selectedCloseDaySunday = (strpos($userDefinedClosedDatesString, 'sun') !== false) ? "checked" : "";

    $userDefinedClosedDatesString_COOO = get_option(Skocjan_odpiralni_cas::$option_name_closedDays_COOO);
    $selectedCloseDayMonday_COOO = (strpos($userDefinedClosedDatesString_COOO, 'mon') !== false) ? "checked" : "";
    $selectedCloseDayTuesday_COOO = (strpos($userDefinedClosedDatesString_COOO, 'tue') !== false) ? "checked" : "";
    $selectedCloseDayWednesday_COOO = (strpos($userDefinedClosedDatesString_COOO, 'wed') !== false) ? "checked" : "";
    $selectedCloseDayThursday_COOO = (strpos($userDefinedClosedDatesString_COOO, 'thu') !== false) ? "checked" : "";
    $selectedCloseDayFriday_COOO = (strpos($userDefinedClosedDatesString_COOO, 'fri') !== false) ? "checked" : "";
    $selectedCloseDaySaturday_COOO = (strpos($userDefinedClosedDatesString_COOO, 'sat') !== false) ? "checked" : "";
    $selectedCloseDaySunday_COOO = (strpos($userDefinedClosedDatesString_COOO, 'sun') !== false) ? "checked" : "";
    $out .= "<h2>".__("Zaprtje rezervata:", "sz-odpiralni-casi-admin")."</h2>";
    $out .= SZ_Notice_Helper::getNotices();
    $out .= __("Tukaj dodajte datume ali označimo dneve, ko bo rezervat zaprt.", "sz-odpiralni-casi-admin");

    $out .= "<form name='sz_closedDates' id='sz_closedDatesForm' method='post' action=''>";
    $out .= "<table><tr><td>";
    $out .= "<label><strong><input type='checkbox' name='sz_userMasterSwitch' id='sz_userMasterSwitch' value='closed'  $selectedPlaceIsClosed >".__("Rezervat je zaprt do odprtja.", "sz-odpiralni-casi-admin")."</input> </strong></label>";
    $out .= "<label><strong><input type='checkbox' name='sz_odpiralnicas_cel_teden_enako' id='sz_odpiralnicas_cel_teden_enako' value='yes'  $selectedWeekendAsWorkdays >".__("Isti delovni čas ves teden.", "sz-odpiralni-casi-admin")."</input> </strong></label>";
    $out .= "</td></tr></table>";

    if(!empty($status))
        $out .= "<h3>".$status."</h3>";

    $out .= '<br/>';
    $out .= "<div class='zaprtiDnevi'>";
    $out .= "<h3>Odpiralni čas NRŠZ</h3>";

    $out .= "<table id='sz_odpiralni_casi_closedDays_table' border='0' width='100%'>";
    $out .= "<tr>";
    $out .= "<td>" . __("Dnevi:", "sz-odpiralni-casi-admin") . "</td>";
    $out .= "</tr>";
    $out .= "<tr><td><label><strong><input type='checkbox' name='sz_odpiralni_casi_closeDay[]' id='sz_odpiralni_casi_closeDay_mon' value='mon' $selectedCloseDayMonday >" . __("Ponedeljek", "sz-odpiralni-casi-admin") . "</input></strong></label></td></tr>";
    $out .= "<tr><td><label><strong><input type='checkbox' name='sz_odpiralni_casi_closeDay[]' id='sz_odpiralni_casi_closeDay_mon' value='tue' $selectedCloseDayTuesday >" . __("Torek", "sz-odpiralni-casi-admin") . "</input></strong></label></td></tr>";
    $out .= "<tr><td><label><strong><input type='checkbox' name='sz_odpiralni_casi_closeDay[]' id='sz_odpiralni_casi_closeDay_mon' value='wed' $selectedCloseDayWednesday >" . __("Sreda", "sz-odpiralni-casi-admin") . "</input></strong></label></td></tr>";
    $out .= "<tr><td><label><strong><input type='checkbox' name='sz_odpiralni_casi_closeDay[]' id='sz_odpiralni_casi_closeDay_mon' value='thu' $selectedCloseDayThursday >" . __("Četrtek", "sz-odpiralni-casi-admin") . "</input></strong></label></td></tr>";
    $out .= "<tr><td><label><strong><input type='checkbox' name='sz_odpiralni_casi_closeDay[]' id='sz_odpiralni_casi_closeDay_mon' value='fri' $selectedCloseDayFriday >" . __("Petek", "sz-odpiralni-casi-admin") . "</input></strong></label></td></tr>";
    $out .= "<tr><td><label><strong><input type='checkbox' name='sz_odpiralni_casi_closeDay[]' id='sz_odpiralni_casi_closeDay_mon' value='sat' $selectedCloseDaySaturday >" . __("Sobota", "sz-odpiralni-casi-admin") . "</input></strong></label></td></tr>";
    $out .= "<tr><td><label><strong><input type='checkbox' name='sz_odpiralni_casi_closeDay[]' id='sz_odpiralni_casi_closeDay_mon' value='sun' $selectedCloseDaySunday >" . __("Nedelja", "sz-odpiralni-casi-admin") . "</input></strong></label></td></tr>";
    $out .= "</table>";

    $out .= "<br/>";

    $out .= "<input type='hidden' name='sz_datesFromDB' value='".json_encode($userDefinedClosedDates)."' >";
    $out .= "<table id='sz_odpiralni_casi_closedDates_table' border='0' width='100%'>";
    $out .= "<tr>";
    $out .= "<td>" . __("Datumi:", "sz-odpiralni-casi-admin") . "</td>";
    $out .= "<td align='right'><input type='button' id='sz_closedDates_add' name='sz_closedDates_add' value='" . __("Dodaj", "sz-odpiralni-casi-admin")."'></td>";
    $out .= "</tr>";
    if(isset($userDefinedClosedDates) && is_array($userDefinedClosedDates)) {
        foreach ($userDefinedClosedDates as $date) {
            /* HTML je generiran v JS!
            $out .= "<tr>";
            $out .= "<td><input type='text' name='sz_closedDates[]' value='$date' /></td>";
            $out .= "<td><input type='button' name='removeDate' class='removeDate' value='" . __("Delete") . "' /> </td>";
            $out .= "</tr>";
            */
        }
    }
    $out .= "</table>";
    $out .= "</div>";



    $out .= "<div class='zaprtiDnevi'>";
    $out .= "<h3>Odpiralni čas COOO</h3>";

    $out .= "<table id='sz_odpiralni_casi_closedDays_table' border='0' width='100%'>";
    $out .= "<tr>";
    $out .= "<td>" . __("Dnevi:", "sz-odpiralni-casi-admin") . "</td>";
    $out .= "</tr>";
    $out .= "<tr><td><label><strong><input type='checkbox' name='sz_odpiralni_casi_closeDay_COOO[]' id='sz_odpiralni_casi_closeDay_mon' value='mon' $selectedCloseDayMonday_COOO >" . __("Ponedeljek", "sz-odpiralni-casi-admin") . "</input></strong></label></td></tr>";
    $out .= "<tr><td><label><strong><input type='checkbox' name='sz_odpiralni_casi_closeDay_COOO[]' id='sz_odpiralni_casi_closeDay_mon' value='tue' $selectedCloseDayTuesday_COOO >" . __("Torek", "sz-odpiralni-casi-admin") . "</input></strong></label></td></tr>";
    $out .= "<tr><td><label><strong><input type='checkbox' name='sz_odpiralni_casi_closeDay_COOO[]' id='sz_odpiralni_casi_closeDay_mon' value='wed' $selectedCloseDayWednesday_COOO >" . __("Sreda", "sz-odpiralni-casi-admin") . "</input></strong></label></td></tr>";
    $out .= "<tr><td><label><strong><input type='checkbox' name='sz_odpiralni_casi_closeDay_COOO[]' id='sz_odpiralni_casi_closeDay_mon' value='thu' $selectedCloseDayThursday_COOO >" . __("Četrtek", "sz-odpiralni-casi-admin") . "</input></strong></label></td></tr>";
    $out .= "<tr><td><label><strong><input type='checkbox' name='sz_odpiralni_casi_closeDay_COOO[]' id='sz_odpiralni_casi_closeDay_mon' value='fri' $selectedCloseDayFriday_COOO  >" . __("Petek", "sz-odpiralni-casi-admin") . "</input></strong></label></td></tr>";
    $out .= "<tr><td><label><strong><input type='checkbox' name='sz_odpiralni_casi_closeDay_COOO[]' id='sz_odpiralni_casi_closeDay_mon' value='sat' $selectedCloseDaySaturday_COOO >" . __("Sobota", "sz-odpiralni-casi-admin") . "</input></strong></label></td></tr>";
    $out .= "<tr><td><label><strong><input type='checkbox' name='sz_odpiralni_casi_closeDay_COOO[]' id='sz_odpiralni_casi_closeDay_mon' value='sun' $selectedCloseDaySunday_COOO >" . __("Nedelja", "sz-odpiralni-casi-admin") . "</input></strong></label></td></tr>";
    $out .= "</table>";

    $out .= "<br/>";

    $out .= "<input type='hidden' name='sz_datesFromDB_COOO' value='".json_encode($userDefinedClosedDates_COOO)."' >";
    $out .= "<table id='sz_odpiralni_casi_closedDates_table_COOO' border='0' width='100%'>";
    $out .= "<tr>";
    $out .= "<td>" . __("Datumi:", "sz-odpiralni-casi-admin") . "</td>";
    $out .= "<td align='right'><input type='button' id='sz_closedDates_add_COOO' name='sz_closedDates_add_COOO' value='" . __("Dodaj", "sz-odpiralni-casi-admin")."'></td>";
    $out .= "</tr>";
    if(isset($userDefinedClosedDatesString_COOO) && is_array($userDefinedClosedDates_COOO)) {
        foreach ($userDefinedClosedDates_COOO as $date) {
            /* HTML je generiran v JS!
            $out .= "<tr>";
            $out .= "<td><input type='text' name='sz_closedDates[]' value='$date' /></td>";
            $out .= "<td><input type='button' name='removeDate' class='removeDate' value='" . __("Delete") . "' /> </td>";
            $out .= "</tr>";
            */
        }
    }
    $out .= "</table>";
    $out .= "</div>";




    $out .= "<input type='submit' value='" . __("Shrani", "sz-odpiralni-casi-admin") . "''>";
    $out .= "</form>";

    $out .= "</div> <!-- id='sz_odpiralni_cas_admin_wrap' -->";
    echo $out;
}

function sz_odpiralni_cas_nalozidatoteko(){
    //shrani datoteko na server
    $fileName = basename($_FILES["sz_fileupload"]["name"]);
    $target_dir = plugin_dir_path( __FILE__ )."upload/";
    if(!is_dir($target_dir)){
        //probaj ustvarit mapo, če ne obstaja.
        try{
            mkdir($target_dir, 755);
        }catch (Exception $e){
            SZ_Notice_Helper::setError(__("Napaka: Ne morem ustvarit mape", "sz-odpiralni-casi-admin")." ".$target_dir." ".__("Podrobnosti:", "sz-odpiralni-casi-admin")." ".$e->getMessage(), 1);
            return;
        }
    }
    $filePath = $target_dir . $fileName;
    $fileUrl = plugin_dir_url(__FILE__)."upload/". $fileName;

    $fileType = pathinfo($filePath,PATHINFO_EXTENSION);

    if($fileType != "xlsx" && $fileType != "xls") {
        SZ_Notice_Helper::setError(__("Napaka: Sistem podpira samo Excel datoteke (.XLS ali .XLSX).", "sz-odpiralni-casi-admin")." ".__("Datoteka je tipa ", "sz-odpiralni-casi-admin").$fileType.".", 1);
        return;
    }

    //Preparsaj datoteko in prepisi vrednosti v bazi.
    $tmpFile = $_FILES["sz_fileupload"]["tmp_name"];
    $urnik = Skocjan_odpiralni_cas::getInstance();
    $statusOk = $urnik->parseXLSFile($tmpFile);
    if($statusOk !== true){
        SZ_Notice_Helper::setError(__("Prišlo je do napake pri branju podatkov. So podatki v datoteki pravilno formatirani?", "sz-odpiralni-casi-admin")."<br/>".$statusOk, 1);
        return;
    }

    if (file_exists($filePath)) {
        unlink($filePath);
    }
    if (!move_uploaded_file($_FILES["sz_fileupload"]["tmp_name"], $filePath)) {
        SZ_Notice_Helper::setError(__("Prišlo je do napake pri prenosu. Kontaktirajte administratorja.", "sz-odpiralni-casi-admin"), 1);
        return;
    }

    update_option(Skocjan_odpiralni_cas::$option_name_file, array('filename' => $fileName, 'filepath' => $filePath, 'fileurl'  => $fileUrl));
    update_option(Skocjan_odpiralni_cas::$option_name, serialize($urnik));

    SZ_Notice_Helper::setSuccess(__("Datoteka uspešno naložena. Odpiralni časi posodobljeni.", "sz-odpiralni-casi-admin"), 1);
}

function sz_odpiralni_cas_shraniClosedDates(){
    $dates = $_POST['sz_closedDates'];
    $dates_COOO = $_POST['sz_closedDates_COOO'];
    if(isset($dates) && is_array($dates)) {
        for ($i = count($dates) - 1; $i >= 0; $i--) {
            if (empty($dates[$i]))
                unset($dates[$i]);
            //Preveri format datumov
            if(preg_match('/^([0-9]|[1-2][0-9]|3[0-1])\.([0-9]|1[0-2])\.20[0-9][0-9]$/', $dates[$i]) == 0){
                //Format ni pravi. Mogoče je 0 v predponi dneva in meseca:
                if(preg_match('/^0?([0-9])\.([0-9]|1[0-2])\.(20[0-9][0-9])$/', $dates[$i]) == 1) {
                    //dan ima predpono 0
                    $dates[$i] = preg_replace('/^0?([0-9])\.([0-9]|1[0-2])\.(20[0-9][0-9])$/', '$1.$2.$3', $dates[$i]);
                }else if(preg_match('/^([0-9]|[1-2][0-9]|3[0-1])\.0?([0-9])\.(20[0-9][0-9])$/', $dates[$i]) == 1) {
                    //mesec ima predpono 0
                    $dates[$i] = preg_replace('/^([0-9]|[1-2][0-9]|3[0-1])\.0?([0-9])\.(20[0-9][0-9])$/', '$1.$2.$3', $dates[$i]);
                }else if(preg_match('/^(0[0-9])\.0([0-9])\.(20[0-9][0-9])$/', $dates[$i]) == 1) {
                    //dan in mesec imata predpono 0
                    $dates[$i] = preg_replace('/^0([0-9])\.0([0-9])\.(20[0-9][0-9])$/', '$1.$2.$3', $dates[$i]);
                }else {
                    unset($dates[$i]);
                }
            }
            unset($tmpDate);
        }
        if(count($dates) > 0)
            $dates = array_values($dates);
    }

    if(isset($dates_COOO) && is_array($dates_COOO)) {
        for ($i = count($dates_COOO) - 1; $i >= 0; $i--) {
            if (empty($dates_COOO[$i]))
                unset($dates_COOO[$i]);
            //Preveri format datumov
            if(preg_match('/^([0-9]|[1-2][0-9]|3[0-1])\.([0-9]|1[0-2])\.20[0-9][0-9]$/', $dates_COOO[$i]) == 0){
                //Format ni pravi. Mogoče je 0 v predponi dneva in meseca:
                if(preg_match('/^0?([0-9])\.([0-9]|1[0-2])\.(20[0-9][0-9])$/', $dates_COOO[$i]) == 1) {
                    //dan ima predpono 0
                    $dates_COOO[$i] = preg_replace('/^0?([0-9])\.([0-9]|1[0-2])\.(20[0-9][0-9])$/', '$1.$2.$3', $dates_COOO[$i]);
                }else if(preg_match('/^([0-9]|[1-2][0-9]|3[0-1])\.0?([0-9])\.(20[0-9][0-9])$/', $dates_COOO[$i]) == 1) {
                    //mesec ima predpono 0
                    $dates_COOO[$i] = preg_replace('/^([0-9]|[1-2][0-9]|3[0-1])\.0?([0-9])\.(20[0-9][0-9])$/', '$1.$2.$3', $dates_COOO[$i]);
                }else if(preg_match('/^(0[0-9])\.0([0-9])\.(20[0-9][0-9])$/', $dates_COOO[$i]) == 1) {
                    //dan in mesec imata predpono 0
                    $dates_COOO[$i] = preg_replace('/^0([0-9])\.0([0-9])\.(20[0-9][0-9])$/', '$1.$2.$3', $dates_COOO[$i]);
                }else {
                    unset($dates_COOO[$i]);
                }
            }
            unset($tmpDate);
        }
        if(count($dates_COOO) > 0)
            $dates_COOO = array_values($dates_COOO);
    }


    $masterSwitch = $_POST['sz_userMasterSwitch'];
    if(!isset($masterSwitch) || is_null($masterSwitch))
        $masterSwitch="";
    update_option(Skocjan_odpiralni_cas::$option_name_placeIsClosed, $masterSwitch);
    update_option(Skocjan_odpiralni_cas::$option_name_closedDates, serialize($dates));
    update_option(Skocjan_odpiralni_cas::$option_name_closedDates_COOO, serialize($dates_COOO));

    $celTedenEnako= $_POST['sz_odpiralnicas_cel_teden_enako'];
    if (!isset($celTedenEnako) || is_null($celTedenEnako))
        $celTedenEnako="";

    update_option(Skocjan_odpiralni_cas::$option_name_useWeekScheduleForWeekends,$celTedenEnako);


    $closedDaysString = "";
    if (isset($_POST['sz_odpiralni_casi_closeDay'])) {
        foreach ($_POST['sz_odpiralni_casi_closeDay'] as $closedDay) {
            $closedDaysString .= $closedDay . ';';
        }
    }
    update_option(Skocjan_odpiralni_cas::$option_name_closedDays, $closedDaysString);

    $closedDaysString_COOO = "";
    if (isset($_POST['sz_odpiralni_casi_closeDay_COOO'])) {
        foreach ($_POST['sz_odpiralni_casi_closeDay_COOO'] as $closedDay) {
            $closedDaysString_COOO .= $closedDay . ';';
        }
    }
    update_option(Skocjan_odpiralni_cas::$option_name_closedDays_COOO, $closedDaysString_COOO);

    SZ_Notice_Helper::setSuccess(__("Nastavitve zaprtja uspešno posodobljeni.", "sz-odpiralni-casi-admin"), 1);
}