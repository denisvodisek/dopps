<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
?>
<script>
$(document).ready(function() {

	$('#newsletter_thumbnails > img').each(function () {
		$(this).bind('click', function() {
  			$("#newsletter_image").attr("src",$(this).attr("id"));
  			$("#newsletter_image").attr("title",$(this).attr("title"));
  			$("#newsletter_author").empty().append($(this).attr("alt"));

  			$("#image_src").attr("value", $(this).attr("id"));
  			$("#image_title").attr("value", $(this).attr("title"));
  			$("#image_author").attr("value", $(this).attr("alt"));

		});
	});

	$("#newsletter_sendbutton").bind('click', function(){

		var data =
		{
			"action": "send_e_card",
			"url": window.location.href,
			"image_src": String($("#image_src").attr("value")),
			"image_title": $("#image_title").attr("value"),
			"image_author": $("#image_author").attr("value"),
			"friends_email": $("#nl_friends_email").val(),
			"friends_name": $("#ns_friends_name").val(),
			"senders_name": $("#nl_senders_name").val(),
			"newsletter_title": $("#newsletter_title").val(),
			"newsletter_text": $("#newsletter_text").val(),
			"ptice": "<?php _e("Birds", "twentyeleven"); ?>",
			"rezervat": "<?php _e("Reserve", "twentyeleven"); ?>",
			"narava": "<?php _e("Nature", "twentyeleven"); ?>",
			"projekti": "<?php _e("Projects", "twentyeleven"); ?>",
			"obisk": "<?php _e("Visit us", "twentyeleven"); ?>",
			"language": "<?php echo ICL_LANGUAGE_CODE; ?>",
			"foto": "<?php _e("Photo", "twentyeleven"); ?>",
			"posiljatelj": "<?php _e("Sender", "twentyeleven"); ?>",
			"prejemnik": "<?php _e("Recipient", "twentyeleven"); ?>"
		};

		$.ajax({
			type: "POST",
			url: "<?php echo admin_url( 'admin-ajax.php' ); ?>",
			data: data,
			success: function(msg){
				$("#fading-background").css("height", $("#newsletter_preview").css("height"));
				if(msg=="1"){
					$("#textbox").css("color", "#FFF");
					$("#textbox").css("background-color", "#016ba7");

					$("#text").empty().append("<?php _e("You have successfully sent the E-card", "twentyeleven"); ?>");
				}else{
					$("#textbox").css("color", "red");
					$("#textbox").css("background-color", "#FFF");

					$("#text").empty().append("<?php _e("There was an error while sending the E-card,</br >please check if the information you entered is correct!", "twentyeleven"); ?>");
				}
				$("#fading-background").fadeIn(400, function(){
					$("#fading").fadeIn(700, function(){
						$("#fading").delay(3000).fadeOut(700, function(){
							$("#fading-background").fadeOut(400);
						});
					});
				});

			}
		});
	});

 });

</script>

<style>
/* Oblikovanje dialoga */
#fading-background{
	position: absolute;
	z-index: 100;
	background-color: #999;
	opacity: 0.80;
	width: 440px;
	height: 100%;
	margin-top: 73px;
	display: none;
	padding: 15px;
	}
#fading{
	position: absolute;
	z-index: 200;
	top: 350px;
	text-align: center;
	width: 460px;
	background-color: #EFEDEE;
	padding: 5px;
	display: none;
}
#textbox{
	background-color: #FFF;
	margin: 10px;
	padding: 10px;
}
#text{
	font-size: 20px;
}

#newsletter_sendbutton{
	cursor: pointer;
	background: no-repeat center url('<?php echo get_stylesheet_directory_uri(); ?>/images/e-razglednice_submit<?php _e("_EN.jpg", "twentyeleven"); ?>');
	height: 25px;
	width: 91px;
	float: right;
}
#newsletter_sendbutton:hover{
	background: no-repeat center url('<?php echo get_stylesheet_directory_uri(); ?>/images/e-razgednice_submit_hover<?php _e("_EN.jpg", "twentyeleven"); ?>');
}


</style>

		<div id="col_2_sidebar_1_newsletter">
            	<div id="main_left_col">
                    <span id="nav_pointer">
                    	<a href="<?php echo get_site_url();?>"><?php _e("Home", "twentyeleven"); ?></a>
                    	<?php $anc = get_ancestors( get_the_ID(), 'page' );
							$anc = array_reverse($anc);
							foreach($anc as $id){
								$current++;
								$p = get_post($id);
								?>
								&gt; <a href="<?php echo get_permalink($p->ID);?>"><?php echo $p->post_title; ?></a>
								<?php
							}
						?>
                    	&gt; <a href="<?php echo get_permalink(get_the_ID()); ?>" class="active"><?php the_title(); ?></a>

                    </span>
                    <div class="clear"></div>

                    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                        <header class="entry-header">
                            <h1><?php the_title(); ?></h1>
                        </header><!-- .entry-header -->

                        <div class="entry-content">
                        	<div id="newsletter_thumbnails">
                            <?php

								global $wpdb;
								//get images and meta data from page!
									//TODO: Preveri, če smo kaj pokvarili z zakomentiranjem tega:
									//	mysql_set_charset('utf8');
									$id = get_the_ID();
									$slo_id = sz_wpml_get_idInLanguage(get_the_ID(), "post", false, "sl");
									$attachment_objects = $wpdb->get_results("SELECT ID FROM $wpdb->posts WHERE post_parent = '".$slo_id."' AND post_type='attachment' ORDER BY menu_order ASC");
									$translatedIDs=array();
									foreach($attachment_objects as $att){
											$translatedIDs[] = sz_wpml_get_idInLanguage($att->ID, "attachment");
									}
									wp_reset_postdata();
									$url_newsletter=array();
									//add newsletter dimensions (640 x 328):
									foreach($translatedIDs as $id){
										if(wp_attachment_is_image($id))
											$url_newsletter[] = wp_get_attachment_image_src($id, 'newsletter_thumb');
									}

									$first_photo_index = rand(0, count($url_newsletter)-1);
									$first_photo_url ="";
									$first_photo_title ="";
									$first_photo_author ="";

									//ECHO IT!
									foreach($url_newsletter as $key => $url){
										$id = $translatedIDs[$key];
										$image_meta = get_post($id);
										$t = $image_meta->post_title;

										$active_class = "";
										$big_url = wp_get_attachment_image_src($id, 'full');

										if($first_photo_index == $key){
											$first_photo_url = $big_url;
											$first_photo_title = $t;
											$first_photo_author = get_media_credit($id);
											$active_class = "class='active'";
										}


											?>
											<img id="<?php echo $big_url[0];?>" <?php echo $active_class; ?> src="<?php echo $url[0]; ?>" title="<?php echo $t; ?>" alt="<?php _e('Photo', 'twentyeleven'); ?>: <?php echo get_media_credit($id); ?>" />
											<?php

									}
									?>
                                    </div><!--#newsletter_thumbnails-->
                        </div><!-- .entry-content -->
                        <footer class="entry-meta">
                            <?php edit_post_link( __( 'Edit', 'twentyeleven' ), '<span class="edit-link">', '</span>' ); ?>
                        </footer><!-- .entry-meta -->
                    </article><!-- #post-<?php the_ID(); ?> -->


                </div><!-- #main_left_col -->


           		<div id="main_right_col">
           			<!-- Dialog stuff -->
           			<div id="fading-background"></div>
					<div id="fading">

						<div id="textbox">
							<p id="text"></p>
						</div>
					</div>
					<!-- End dialog stuff -->

					<?php

					//Variables used:
					$newsletter = array(
						'title' => '',
						'senderName' => '',
						'recipientName' => '',
						'recipientEmail' => '',
						'photoUrl' => $first_photo_url[0],
						'photoTitle' => $first_photo_title,
						'photoAuthor' => $first_photo_author,
						'senderText' => '',
						'isEmail' => false
					);

					require_once ('e-razglednice_design.php');
					?>

                </div><!-- #main_right_col -->

            </div><!-- #col_1_sidebar_1 -->

