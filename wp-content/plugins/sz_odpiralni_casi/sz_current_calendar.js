/**
 * Created by uros on 21/12/15.
 */

/*
This is loaded when [sz_trenutni_odpiralni_cas] is used.
It keeps the 'current opening hours' interactive:
 - it hides the unneeded data and animates the 'current time' pointer.

 */
var $sz_current = $(".sz_odpiralni_cas_current");

/************* functions **************/
var $nrsz_week_name =  $sz_current.find(".sz_odpiralni_cas_leftHeader tr:nth-child(1) td.sz_odpiralni_cas_dayOfWeek");
var $nrsz_week_data =  $sz_current.find(".sz_odpiralni_cas_urnik tr:nth-child(2) span");
var $nrsz_weekend_name =  $sz_current.find(".sz_odpiralni_cas_leftHeader tr:nth-child(2) td.sz_odpiralni_cas_dayOfWeek");
var $nrsz_weekend_data =  $sz_current.find(".sz_odpiralni_cas_urnik tr:nth-child(3) span");


var $cooo_week_name =  $sz_current.find(".sz_odpiralni_cas_leftHeader tr:nth-child(3) td.sz_odpiralni_cas_dayOfWeek");
var $cooo_week_data =  $sz_current.find(".sz_odpiralni_cas_urnik tr:nth-child(4) span");
var $cooo_weekend_name =  $sz_current.find(".sz_odpiralni_cas_leftHeader tr:nth-child(4) td.sz_odpiralni_cas_dayOfWeek");
var $cooo_weekend_data =  $sz_current.find(".sz_odpiralni_cas_urnik tr:nth-child(5) span");


function showWeekData_NRSZ(){
    $nrsz_week_name.css("opacity", 1);
    $nrsz_week_data.css("opacity", 1);
}
function showWeekData_COOO() {
    $cooo_week_name.css("opacity", 1);
    $cooo_week_data.css("opacity", 1);
}
function hideWeekData_NRSZ(){
    $nrsz_week_name.css("opacity", 0);
    $nrsz_week_data.css("opacity", 0);
}
function hideWeekData_COOO() {
    $cooo_week_name.css("opacity", 0);
    $cooo_week_data.css("opacity", 0);
}

function showWeekendData_NRSZ(){
    $nrsz_weekend_name.css("opacity", 1);
    $nrsz_weekend_data.css("opacity", 1);
}
function showWeekendData_COOO() {
    $cooo_weekend_data.css("opacity", 1);
    $cooo_weekend_name.css("opacity", 1);
}
function hideWeekendData_NRSZ(){
    $nrsz_weekend_name.css("opacity", 0);
    $nrsz_weekend_data.css("opacity", 0);
}
function hideWeekendData_COOO() {
    $cooo_weekend_data.css("opacity", 0);
    $cooo_weekend_name.css("opacity", 0);
}

/********** end of functions **********/

var slovenianHolidays = [
    "1.1.",
    "8.2.",
    "27.4.",
    "1.5.",
    "2.5.",
    "8.6.",
    "25.6.",
    "15.8.",
    "17.8.",
    "15.9.",
    "25.10.",
    "31.10.",
    "1.11.",
    "23.11.",
    "25.12.",
    "26.12."
];

 var easterMondays = [
    "28.3.2016",
    "17.4.2017",
    "2.4.2018",
    "22.4.2019",
    "13.4.2020",
    "5.4.2021",
    "18.4.2022",
    "9.4.2023",
    "1.4.2024",
    "21.4.2025",
    "06.4.2026",
    "29.3.2027",
    "17.4.2028",
    "2.4.2029",
    "22.4.2030",
    "14.4.2031",
    "29.3.2032",
    "18.4.2033",
    "10.4.2034",
    "26.4.2035" /* You never know how long this website will be active */
];

var areWeClosedToday = parseInt($("#sz_odpiralni_cas_closedToday").attr("value"));
var areWeClosedToday_COOO = parseInt($("#sz_odpiralni_cas_closedToday_COOO").attr("value"));

var closedText = $("#sz_odpiralni_cas_closedToday").attr("title");
var closedText_COOO = $("#sz_odpiralni_cas_closedToday_COOO").attr("title");

console.log(closedText);
console.log(closedText_COOO);

if(areWeClosedToday){
    $sz_current.find(".sz_odpiralni_cas_urnik_holder tr:not(.sz_odpiralni_cas_ure):nth-child(2) td span").removeClass("schedule_t").removeClass("schedule_o").removeClass("schedule_r").removeClass("schedule_vo");
    $sz_current.find(".sz_odpiralni_cas_urnik_holder tr:not(.sz_odpiralni_cas_ure):nth-child(3) td span").removeClass("schedule_t").removeClass("schedule_o").removeClass("schedule_r").removeClass("schedule_vo");
    $sz_current.find(".sz_odpiralni_cas_urnik_holder tr:not(.sz_odpiralni_cas_ure):nth-child(2) td span").attr("title", closedText).addClass("schedule_closed");
    $sz_current.find(".sz_odpiralni_cas_urnik_holder tr:not(.sz_odpiralni_cas_ure):nth-child(3) td span").attr("title", closedText).addClass("schedule_closed");
}
if(areWeClosedToday_COOO){
    $sz_current.find(".sz_odpiralni_cas_urnik_holder tr:not(.sz_odpiralni_cas_ure):nth-child(4) td span").removeClass("schedule_t").removeClass("schedule_o").removeClass("schedule_r").removeClass("schedule_vo");
    $sz_current.find(".sz_odpiralni_cas_urnik_holder tr:not(.sz_odpiralni_cas_ure):nth-child(5) td span").removeClass("schedule_t").removeClass("schedule_o").removeClass("schedule_r").removeClass("schedule_vo");
    $sz_current.find(".sz_odpiralni_cas_urnik_holder tr:not(.sz_odpiralni_cas_ure):nth-child(4) td span").attr("title", closedText_COOO).addClass("schedule_closed");
    $sz_current.find(".sz_odpiralni_cas_urnik_holder tr:not(.sz_odpiralni_cas_ure):nth-child(5) td span").attr("title", closedText_COOO).addClass("schedule_closed");
}
var currentServerEpoch = parseInt($("#sz_odpiralni_cas_current_epoch").attr("value"));
var currentClientEpoch = (new Date()).getTime()/1000;

var d = new Date(0);
d.setUTCSeconds(currentServerEpoch);


var currentMonthIndex = d.getMonth(); //numeric 0 - 11
var currentDay = d.getDate(); //numeric 1 - 31
var currentDayInWeekIndex = d.getDay(); //0-6  //0 => nedelja!
var currentYear = d.getFullYear(); //2010-2059

//check if it's a weekend or holiday
var itsAWorkDay = true;
if(currentDayInWeekIndex == 0 || currentDayInWeekIndex == 6)
    itsAWorkDay = false;
if(slovenianHolidays.indexOf(currentDay+"."+(currentMonthIndex+1)+".") > -1)
    itsAWorkDay = false;
if(easterMondays.indexOf(currentDay+"."+(currentMonthIndex+1)+"."+currentYear) > -1)
    itsAWorkDay = false;
//check if option weekend as workday is checked
if (document.getElementById('useWeekSchForWeekend').value=='yes')
    itsAWorkDay=true;

//hide undeeded data:
//un-mark the other months
$sz_current.find(".sz_odpiralni_cas_leftHeader .sz_odpiralni_cas_mesec.active").removeClass("active");
if(currentMonthIndex < 5) {
    $sz_current.find(".sz_odpiralni_cas_leftHeader tr:first-child td:nth-child("+(currentMonthIndex+1)+")").addClass("active");
} else {
    //second (actually, third TR) row
    $sz_current.find(".sz_odpiralni_cas_leftHeader tr:nth-child(3) td:nth-child("+(currentMonthIndex-5)+")").addClass("active");
}

if(itsAWorkDay){
    //hide weekend data:
    hideWeekendData_NRSZ();
    hideWeekendData_COOO();

    //show it again if hovered:
    $nrsz_weekend_data.on("mouseover",showWeekendData_NRSZ);
    $nrsz_weekend_data.on("mouseout",hideWeekendData_NRSZ);
    $nrsz_weekend_name.on("mouseover",showWeekendData_NRSZ);
    $nrsz_weekend_name.on("mouseout",hideWeekendData_NRSZ);

    $cooo_weekend_data.on("mouseover",showWeekendData_COOO);
    $cooo_weekend_data.on("mouseout",hideWeekendData_COOO);
    $cooo_weekend_name.on("mouseover",showWeekendData_COOO);
    $cooo_weekend_name.on("mouseout",hideWeekendData_COOO);

    //force show dots on week:
    $nrsz_week_data.addClass("forceShowBefore");
    $cooo_week_data.addClass("forceShowBefore");
}else{
    //hide week data:
    hideWeekData_NRSZ();
    hideWeekData_COOO();

    //show it again if hovered:
    $nrsz_week_data.on("mouseover",showWeekData_NRSZ);
    $nrsz_week_data.on("mouseout",hideWeekData_NRSZ);
    $nrsz_week_name.on("mouseover",showWeekData_NRSZ);
    $nrsz_week_name.on("mouseout",hideWeekData_NRSZ);

    $cooo_week_data.on("mouseover",showWeekData_COOO);
    $cooo_week_data.on("mouseout",hideWeekData_COOO);
    $cooo_week_name.on("mouseover",showWeekData_COOO);
    $cooo_week_name.on("mouseout",hideWeekData_COOO);

    //force show dots on weekend:
    $nrsz_weekend_data.addClass("forceShowBefore");
    $cooo_weekend_data.addClass("forceShowBefore");
}
//********** Hoverer ************//
// Vzami Hoverer in ga postavi nad tabelo časa
$(".sz_odpiralni_cas_hoverer").each(function(){
   $(this).parent().find(".sz_odpiralni_cas_urnik_holder").append($(this));

    setInterval(updateHovererPositionAndData.bind(this), 1500);
});
function updateHovererPositionAndData(){
    var currentTimeOnServer = currentServerEpoch + (new Date()).getTime()/1000 - currentClientEpoch;
    var da = new Date(0);
    da.setUTCSeconds(currentTimeOnServer);
    var currentHour = da.getHours();
    var currentMinute = da.getMinutes() //00-59
    $(this).find(".sz_current_time").html(("0" + currentHour).slice(-2)+":"+("0" + currentMinute).slice(-2));

    //find out min and max hours in schedule
    var $urnik = $(this).parents(".sz_odpiralni_cas_urnik_holder").find(".sz_odpiralni_cas_urnik");
    var minTimeText = $urnik.find("tr.sz_odpiralni_cas_ure td:first-child span").html();
    var maxTimeText = $urnik.find("tr.sz_odpiralni_cas_ure td:last-child span").html();

    var minHours = parseInt(minTimeText.substring(0, 2));
    var maxHours = parseInt(maxTimeText.substring(0, 2))+1;

    var timeSpan = maxHours-minHours;
    var scheduleWidth = $urnik.width();

    var rightoffset = 0;

    $(this).css("right", "0px"); //small hack so that the flag doesn't get resized.
    if(currentHour < minHours){
        rightoffset = scheduleWidth;
        $(this).find(".sz_nrsz_openStatus").html($(".sz_legend_closed").html());
        $(this).find(".sz_cooo_openStatus").html($(".sz_legend_closed").html());
    }else if(currentHour >= maxHours){
        rightoffset = 0;
        $(this).find(".sz_nrsz_openStatus").html($(".sz_legend_closed").html());
        $(this).find(".sz_cooo_openStatus").html($(".sz_legend_closed").html());
    }else{
        timeSpan = timeSpan*100;
        var currentTimeDecimal = currentHour*100+currentMinute*100/60 - minHours*100;

        var procentPosition = currentTimeDecimal/timeSpan;
        procentPosition = 1-procentPosition; //we flip it, because we need position from right edge.

        rightoffset = procentPosition*scheduleWidth;

        var hourOffset = currentHour-minHours;

        var nrsz_current_status = "";
        var cooo_current_status = "";
        if(itsAWorkDay){
            nrsz_current_status = $($nrsz_week_data.get(hourOffset)).attr("title");
            cooo_current_status = $($cooo_week_data.get(hourOffset)).attr("title");
        }else{
            nrsz_current_status = $($nrsz_weekend_data.get(hourOffset)).attr("title");
            cooo_current_status = $($cooo_weekend_data.get(hourOffset)).attr("title");
        }

        $(this).find(".sz_nrsz_openStatus").html(nrsz_current_status);
        $(this).find(".sz_cooo_openStatus_COOO").html(cooo_current_status);
    }
    $(this).css("right", rightoffset+"px");
    $(this).css("display", "block");
    $(this).animate({opacity:1}, 500);

}

//******* End of Hoverer ********//



/* če je "Čez leto" prisotno, mu daj onclick event*/
var $moreBtn = $("#sz_odpiralni_casi_showMore");
if($moreBtn != null && $moreBtn.get(0) != null){
    var $targ = $("#sz_odpiralni_casi_showMore_target");
    $(window).load(function(){
        $targ.data("elmHeight", $targ.height());
        $targ.css("height", "0px");
        $targ.css("display", "block");

        $moreBtn.on('click', function(){

            if($targ.height() > 0){
                $targ.css("height", "0px");
            }else{
                $targ.css("height",$targ.data("elmHeight")+"px");
            }
            return false;
        });
    });
}








