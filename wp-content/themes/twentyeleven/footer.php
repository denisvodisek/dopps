<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
?>


        <!-- FOOTER STARTS HERE -->


        	<div class="clear"></div>
      	</div><!-- #main_inner_margin -->
      <div class="clear"></div>
      </div> <!-- #main -->
  </td>
  <td  id="main_shadow_r">
  </td><!-- #main_shadow_l -->
  <td style="position:relative; vertical-align:top;"><div id="bkg_govedo1">&nbsp;</div>
        <div id="bkg_ptica3">&nbsp;</div>

        <?php
		$this_user_agent = strtoupper ( $_SERVER["HTTP_USER_AGENT"]);
		if (preg_match( '%FIREFOX/([0-9].[0-9]{1,2})%',$this_user_agent,$log_version) != 0) {
 		?> <style>#bkg_ptica3{bottom:124px !important;}</style> <?php
		}
		?>


  </td>
  </tr>
  </table>
<div id="footer_holder">
	<div id="center_footer">
		<div id="footer">
        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
            <?php	if(ICL_LANGUAGE_CODE == 'en'){	?>
            <td id="foot_1">
                <p>DOPPS – BirdLife Slovenia</p>
                <p><span style="font-weight: bold;">Škocjanski zatok Nature Reserve</span><br />
                    Sermin 50, 6000 Koper, Slovenia<br />
                    tel.: +386 5 626 03 70<br />
                    <a href="mailto:info@skocjanski-zatok.org">info@skocjanski-zatok.org</a></p>
            </td>
                <!-- #foot_1 -->
            <td id="foot_2">
                <div class="img_holder">
                    <a href="http://www.dopps.si"><img src="<?php bloginfo( 'template_url' ); ?>/bkg/dopps_logo_new.png" width="50" height="70" alt="DOPPS" /></a>
                </div>
                <div class="img_holder">
                    <a href="http://birdlife.org"><img src="<?php bloginfo( 'template_url' ); ?>/bkg/birdlife_logo_new.png" width="76" height="56" alt="BirdLife" /></a>
                </div>
            </td><!-- #foot_2 -->
            <td id="foot_3">
                <p>Authors:<br />
                    Design: Polonca Peterca<br />
                    Technical preparation: Uroš Orešič</p>

                <p>All rights reserved</p>
            </td><!-- #foot_3 -->
        	<?php	}elseif(ICL_LANGUAGE_CODE == 'sl'){	?>
        	<td id="foot_1">
                <p>Društvo za opazovanje in proučevanje ptic Slovenije (DOPPS)</p>
                <p><span style="font-weight: bold;">Naravni rezervat Škocjanski zatok</span><br />
                    Sermin 50, 6000 Koper<br />
                    tel.: 05 626 03 70<br />
                    <a href="mailto:info@skocjanski-zatok.org">info@skocjanski-zatok.org</a></p>
            </td><!-- #foot_1 -->
            <td id="foot_2">
                    <div class="img_holder">
                        <a href="http://www.dopps.si"><img src="<?php bloginfo( 'template_url' ); ?>/bkg/dopps_logo_new.png" width="50" height="70" alt="DOPPS" /></a>
                    </div>
                    <div class="img_holder">
                        <a href="http://birdlife.org"><img src="<?php bloginfo( 'template_url' ); ?>/bkg/birdlife_logo_new.png" width="76" height="56" alt="BirdLife" /></a>
                    </div>
            </td><!-- #foot_2 -->
            <td id="foot_3">
                    <p>Avtorji:<br />
                        Oblika: Polonca Peterca<br />
                        Izvedba: Uroš Orešič</p>

                    <p>Vse pravice pridržane</p>
            </td><!-- #foot_3 -->

			<?php	}elseif(ICL_LANGUAGE_CODE == 'it'){	?>
                <td id="foot_1">
                    <p>DOPPS – BirdLife Slovenia</p>
                    <p><span style="font-weight: bold;">Riserva Naturale di Val Stagnon</span><br />
                        Sermin/ Sermino 50, 6000 Capodistria, Slovenia<br />
                        tel.: +386 5 626 03 70<br />
                        <a href="mailto:info@skocjanski-zatok.org">info@skocjanski-zatok.org</a></p>
                </td><!-- #foot_1 -->
                <td id="foot_2">
                    <div class="img_holder">
                        <a href="http://www.dopps.si"><img src="<?php bloginfo( 'template_url' ); ?>/bkg/dopps_logo_new.png" width="50" height="70" alt="DOPPS" /></a>
                    </div>
                    <div class="img_holder">
                        <a href="http://birdlife.org"><img src="<?php bloginfo( 'template_url' ); ?>/bkg/birdlife_logo_new.png" width="76" height="56" alt="BirdLife" /></a>
                    </div>
                </td><!-- #foot_2 -->
                <td id="foot_3">
                    <p>Autori:<br />
                        Design: Polonca Peterca<br />
                        Realizzazione: Uroš Orešič</p>

                    <p>Tutti i diritti riservati</p>
                </td><!-- #foot_3 -->

        	<?php	}?>
                <td id="foot_4">
                    <p>
                        <?php $followus = __("Follow us on:", "twentyeleven");
                            echo $followus ?>
                    </p>
                    <a href="https://www.facebook.com/skocjanskizatok/"<div id="fb-icon">
                        <img src="<?php bloginfo( 'template_url' );?>/images/fb-icon.png" width="30" height="30"/>
                    </div></a>
                </td><!-- #foot_4 -->

                <td id="foot_5">
                    <a href="<?php _e("https://luka-kp.si/eng/"); ?>" target="_blank"><span id="luka-koper-logo"></span></a><br />
                    <p><?php _e("The redesign of the site was facilitated by Luka Koper, d.d."); ?></p>
                </td>
        	</tr>
        </table>
    	</div><!-- #footer -->

        <div id="bkg_ptica2"></div>
    </div> <!-- #center_footer -->
</div><!-- #footer_holder -->

<div id="header_bkg"></div>
<div id="header_bkg_3"></div>
</div><!-- #site_holder -->




<?php wp_footer(); ?>

<!-- Start Open Web Analytics Tracker -->
<script type="text/javascript">
//<![CDATA[
var owa_baseUrl = 'http://owa.ptice.si/';
var owa_cmds = owa_cmds || [];
owa_cmds.push(['setSiteId', '725365dbd637a7c882f1a39211d8f6be']);
owa_cmds.push(['trackPageView']);
owa_cmds.push(['trackClicks']);
owa_cmds.push(['trackDomStream']);

(function() {
	var _owa = document.createElement('script'); _owa.type = 'text/javascript'; _owa.async = true;
	owa_baseUrl = ('https:' == document.location.protocol ? window.owa_baseSecUrl || owa_baseUrl.replace(/http:/, 'https:') : owa_baseUrl );
	_owa.src = owa_baseUrl + 'modules/base/js/owa.tracker-combined-min.js';
	var _owa_s = document.getElementsByTagName('script')[0]; _owa_s.parentNode.insertBefore(_owa, _owa_s);
}());
//]]>
</script>
<!-- End Open Web Analytics Code -->
</body>
</html>