<?php
/**
 * User: uros
 * Date: 11/01/16
 * Time: 22:09
 * Text Domain: sz-odpiralni-casi
 */
class SZ_odpiralni_cas_widget extends WP_Widget {

    // constructor
    function __construct() {
        $widget_ops = array(
            'class_name' => '',
            'description' => 'Prikaže odpiralni čas za NRŠZ in COOO obenem.',
        );
        parent::__construct('sz_odpiralni_cas_widget', $name = __('Odpiralni čas', 'sz-odpiralni-casi'), $widget_ops);
    }

    // widget form creation
    function form($instance) {
        global $sitepress;
        // Check values
        if( $instance) {
            $linkToPage = esc_attr($instance['linkToPage']);
            $linkedPostId = esc_attr($instance['linkedPostId']);
            //$title = esc_attr($instance['title']);
        } else {
            $linkToPage = '';
            $linkedPostId = '';
            //$title = '';
        }

        $warning ="";
        if(isset($sitepress)) {
            $currentlang = ICL_LANGUAGE_CODE;
            $defLang = $sitepress->get_default_language();
            if($currentlang != $defLang)
                $warning = "<span style='color:red'>".__("Modul podpira spremembe samo v privzetem jeziku", "sz-odpiralni-casi")." (".$defLang.")!</span><br />";
        }
        ?>
        <p>
        <?= $warning; ?>
        <label for="<?php echo $this->get_field_id('linkToPage'); ?>"><?php _e('Povezava do strani: ', 'sz-odpiralni-casi'); ?></label>
        <input class="widefat" id="<?php echo $this->get_field_id('linkToPage'); ?>" name="<?php echo $this->get_field_name('linkToPage'); ?>" type="text" value="<?php echo $linkToPage; ?>" />
        <input class="widefat" id="<?php echo $this->get_field_id('linkedPostId'); ?>" name="<?php echo $this->get_field_name('linkedPostId'); ?>" type="hidden" value="<?php echo $linkedPostId; ?>" />

        </p>
        <?php
    }

    // widget update
    function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['linkToPage'] = ( ! empty( $new_instance['linkToPage'] ) ) ? strip_tags( $new_instance['linkToPage'] ) : '';
        $instance['linkedPostId'] = url_to_postid($instance['linkToPage']);

        return $instance;
    }

    // widget display
    function widget($args, $instance) {
        $activePluginList = get_option( 'active_plugins' );
        if (in_array('sz_odpiralni_casi/skocjan_odpiralni_cas.php', $activePluginList)) {
            $sz_odpiralni_cas = Skocjan_odpiralni_cas::getInstance();
            $postId = $instance['linkedPostId'];
            $linkOk = false;
            if(!empty($postId) && $postId != 0) {
                // automatically get correct page ID for translations
                if (function_exists('icl_object_id')) {
                    // deprecated but works
                    $langPostID = icl_object_id(intval($postId), 'page', false);
                } else {
                    // should work according to documentation when icl_object_id is removed
                    $langPostID = apply_filters('wpml_object_id', $postId, 'post');
                }
                $link = get_permalink($langPostID);
                $linkOk = true;
            }

            if (!$linkOk && !$sz_odpiralni_cas->isReady()) {
                $link = Skocjan_odpiralni_cas::$linkToAdminPanel;
            }
            $out = $args['before_widget'];
            $out .='<a href="'.$link.'" class="border_orange" id="delovni_cas">';
            $out .='<h3>'.__("Delovni čas", 'sz-odpiralni-casi').'</h3>';

            if(Skocjan_odpiralni_cas::areWeClosedForEver()){
                $out .='<p style="margin-top: 20px;" class="bold_title">'. __("Naravni rezervat Škocjanski zatok", "sz-odpiralni-casi").' '.__("za obiskovalce ponovno odprt", "sz-odpiralni-casi").'</p>';
                $out .='<p> </p>';
                $out .='<div style="width:100%; height: 10px;"></div>';
                $out .='<p class="bold_title">'.__("v ", "sz-odpiralni-casi").'<span class="open_status">'.__("sredo", "sz-odpiralni-casi").'</span>, '.__('2. marca 2016 ob 13. uri', "sz-odpiralni-casi").'.</p>';
                $out .='<p> </p>';
                $out .='</a>';
                $out .= $args['after_widget'];
                echo $out;
                return;
            }

            $out .='<p class="bold_title">'. __("Naravni rezervat Škocjanski zatok", "sz-odpiralni-casi").':</p>';
            $out .='<p>'. sz_odpiralni_cas_summary_rezervat().'</p>';

            $out .='<div style="width:100%; height: 10px;"></div>';
            $out .='<p class="bold_title">'.__("Center za obiskovalce in osrednja opazovalnica", "sz-odpiralni-casi").':</p>';
            $out .='<p>'. sz_odpiralni_cas_summary_opazovalnica() .'</p>';
            $out .='</a>';
            $out .= $args['after_widget'];
            echo $out;
        }
    }
}

// register widget
add_action('widgets_init', create_function('', 'return register_widget("SZ_odpiralni_cas_widget");'));