<?php
/*
 * Plugin name:  Skocjanski zatok - vodeni ogledi
 * Description:  Modul za naročanje vodenih ogledov parka. Modul se prikaže z uporabo shortkod: [sz_vodeni_ogledi]
 * Version:      1.0
 * Plugin URI:   http://www.u-centrix.com/
 * Author:       Jernej Kranjec
 * Author URI:   http://www.u-centrix.com/
 * Text Domain:  sz-vodeni-ogledi
 */
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

require_once "admin/skocjan_vodeni_ogled_admin.php";

/** PHPExcel_IOFactory */
if (!in_array('PHPExcel_IOFactory', get_declared_classes())){
    set_include_path(get_include_path() . PATH_SEPARATOR . 'PHPExcel_1.8.0_doc/Classes');
    include 'PHPExcel_1.8.0_doc/Classes/PHPExcel/IOFactory.php';
}

function sz_vodeni_ogledi_load_textdomain() {
    load_plugin_textdomain( 'sz-vodeni-ogledi', false, plugin_basename( dirname( __FILE__ ) ) . '/languages');
    load_plugin_textdomain( 'sz-vodeni-ogledi-admin', false, plugin_basename( dirname( __FILE__ ) ) . '/languages');
}
add_action( 'plugins_loaded', 'sz_vodeni_ogledi_load_textdomain' );

function sz_vodeni_ogledi_scripts() {
    wp_register_style( 'sz_vodeni_ogledi_style', plugins_url( 'sz_vodeni_ogledi/skocjan_vodeni_ogled.css' ) );
    wp_enqueue_style( 'sz_vodeni_ogledi_style' );

    wp_register_script( 'sz_vodeni_ogledi_script', plugins_url( 'sz_vodeni_ogledi/skocjan_vodeni_ogled.js' ), array('jquery'), '1.0.0', true );
    wp_enqueue_script( 'sz_vodeni_ogledi_script' );

    wp_localize_script( 'sz_vodeni_ogledi_script', 'sz_vodeni_ogledi', array('ajax_url' => admin_url( 'admin-ajax.php')) );
}
add_action( 'wp_enqueue_scripts', 'sz_vodeni_ogledi_scripts' );


function sz_vodeni_ogledi_update_db() {
    global $wpdb;
    $charset_collate = $wpdb->get_charset_collate();

    $table_name = $wpdb->prefix . "sz_vodeni_ogledi_data";

    $sql = "CREATE TABLE $table_name (
		id mediumint(9) unsigned NOT NULL AUTO_INCREMENT,
		time_submitted datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
		session_language varchar(32) DEFAULT '' NOT NULL,
		person_legal varchar(16) DEFAULT '' NOT NULL,
		customer_name varchar(255) DEFAULT '' NOT NULL,
		customer_address varchar(255) DEFAULT '' NOT NULL,
		customer_subject_to_vat varchar(2) DEFAULT '' NOT NULL,
		customer_tax_number varchar(32) DEFAULT '' NOT NULL,
		tour_date date DEFAULT '0000-00-00' NOT NULL,
		tour_time time DEFAULT '00:00:00' NOT NULL,
		tour_type varchar(64) DEFAULT '' NOT NULL,
		tour_language varchar(32) DEFAULT '' NOT NULL,
		visitor_count smallint(3) DEFAULT 0 NOT NULL,
		companion_count smallint(3) DEFAULT 0 NOT NULL,
		visitor_status varchar(255) DEFAULT '' NOT NULL,
		visitor_age_from smallint(3) DEFAULT 0 NOT NULL,
		visitor_age_to smallint(3) DEFAULT 0 NOT NULL,
		worksheet_status varchar(2) DEFAULT '' NOT NULL,
		worksheet_count smallint(3) DEFAULT 0 NOT NULL,
        contact_name varchar(255) DEFAULT '' NOT NULL,
        contact_phone varchar(64) DEFAULT '' NOT NULL,
        contact_email varchar(128) DEFAULT '' NOT NULL,
        payment_option varchar(32) DEFAULT '' NOT NULL,
        extra_conditions text DEFAULT '' NOT NULL,
        term_confirmed tinyint(1) DEFAULT 0 NOT NULL,
        term_confirmed_text text DEFAULT '' NOT NULL,
		UNIQUE KEY id (id)
	) $charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );

    update_option( "sz_vodeni_ogledi_db_version", '1.0' );
}
register_activation_hook( __FILE__, 'sz_vodeni_ogledi_update_db' );



function sz_vodeni_ogled_html(){

    $ogled = Skocjan_vodeni_ogled::getInstance();

    // če nismo pripravljeni izpiši obvestilo - podatke moramo imeti
    if(!$ogled->isReady())
        return '<a href="' . Skocjan_odpiralni_cas::$linkToAdminPanel . '">' . __('Uvoziti morate podatke. Prijavite se v administracijo: Vodeni ogledi > Nastavitve.', 'sz-vodeni-ogledi') . '</a>';

    // če je registracija zaprta izpišemo vsebino strani s tekstom zakaj je tako
    if ($ogled->isRegistrationClosed()) {
        // dobi id strani s tekstom o zaprtju
        $registrationTextPostBaseID = intval(get_option(Skocjan_vodeni_ogled::$option_name_registrationClosed_textPage));

        $the_query = new WP_Query( 'page_id='. $registrationTextPostBaseID );


        while ( $the_query->have_posts() ) {
            $the_query->the_post();
            the_content();
        }
        wp_reset_postdata();
        return;
    }

    // če je vse vredu izpišemo formo
    if (isset($_POST['guideFormDate'])) {
        $formHtml = $ogled->getForm($_POST['guideFormDate']);
    } else {
        $formHtml = $ogled->getForm(date("d.m.Y"));
    }
    return $formHtml;
}
add_shortcode( 'sz_vodeni_ogled', 'sz_vodeni_ogled_html' );



function ajax_get_calendar() {
    $ogled = Skocjan_vodeni_ogled::getInstance();

    $calHtml = $ogled->renderCalander('01.'.$_POST['month'].'.'.$_POST['year']);
    echo $calHtml;

    die();
}
add_action( 'wp_ajax_nopriv_ajax_get_calendar', 'ajax_get_calendar' );
add_action( 'wp_ajax_ajax_get_calendar', 'ajax_get_calendar' );

function ajax_save_tour() {
    $ogled = Skocjan_vodeni_ogled::getInstance();

    $termDateSplit = explode('.', $_POST['tour_date']);
    $termDateSQL = $termDateSplit[2] . '-' . sprintf("%02d", $termDateSplit[1]) . '-' . sprintf("%02d", $termDateSplit[0]);
    $termTimeSQL = $_POST['tour_time'] . ':00';

    // preveri ali je termin še vedno prost
    if ($ogled->checkIfTermIsAvailable($termDateSQL, $termTimeSQL) == false) {
        echo 'error:termNotAvailable';
        die();
    }

    // zapiši termin v bazo
    global $wpdb;

    $table_name = $wpdb->prefix . 'sz_vodeni_ogledi_data';

    $dbResult = $wpdb->insert(
        $table_name,
        array(
            // ICL_LANGUAGE_CODE
            'time_submitted' => current_time( 'mysql' ),
            'session_language' => $_POST['session_language'],
            'person_legal' => $_POST['person_legal'],
            'customer_name' => $_POST['customer_name'],
            'customer_address' => $_POST['customer_address'],
            'customer_subject_to_vat' => $_POST['customer_subject_to_vat'],
            'customer_tax_number' => $_POST['customer_tax_number'],
            'tour_date' => $termDateSQL,
            'tour_time' => $termTimeSQL,
            'tour_type' => $_POST['tour_type'],
            'tour_language' => $_POST['tour_language'],
            'visitor_count' => $_POST['visitor_count'],
            'companion_count' => $_POST['companion_count'],
            'visitor_status' => $_POST['visitor_status'],
            'visitor_age_from' => $_POST['visitor_age_from'],
            'visitor_age_to' => $_POST['visitor_age_to'],
            'worksheet_status' => $_POST['worksheet_status'],
            'worksheet_count' => $_POST['worksheet_count'],
            'contact_name' => $_POST['contact_name'],
            'contact_phone' => $_POST['contact_phone'],
            'contact_email' => $_POST['contact_email'],
            'payment_option' => $_POST['payment_option'],
            'extra_conditions' => $_POST['extra_conditions'],
            'term_confirmed' => 0,
            'term_confirmed_text' => ''
        )
    );

    // če re rezultat vstavljanja v bazo različen od ene vstavljene vrstice, vrni napako
    if ($dbResult != 1) {
        echo 'error:unexpectedDataBaseOperation';
        die();
    }

    // uspešno smo vstavili novo rezervacijo v bazo - pošlji email obvestilo odgovornim
    $notificationEmailListString = get_option(Skocjan_vodeni_ogled::$option_name_sendNotification_emailList);
    $notificationEmailList = explode(';', $notificationEmailListString);
    $textNewLine = "\r\n";
    for ($ei = 0; $ei < sizeof($notificationEmailList); $ei++){
        if ($notificationEmailList[$ei] != '') {
            $to =  $notificationEmailList[$ei];
            $subject = 'NRŠZ - vodeni ogledi: Prejeta nova rezervacija';

            $bodyExtra = '';
            $bodyExtra .= '<p>Jezik uporabljene seje za oddajo rezervacije: ' . $_POST['session_language'] . '</p>';
            $bodyExtra .= '<br/>';

            $formData = array();
            $formData['session_language'] = $_POST['session_language'];
            $formData['person_legal'] = $_POST['person_legal'];
            $formData['customer_name'] = $_POST['customer_name'];
            $formData['customer_address'] = $_POST['customer_address'];
            $formData['customer_tax_number'] = $_POST['customer_tax_number'];
            $formData['customer_subject_to_vat'] = $_POST['customer_subject_to_vat'];
            $formData['tour_date'] = $_POST['tour_date'];
            $formData['tour_time'] = $_POST['tour_time'];
            $formData['visitor_count'] = $_POST['visitor_count'];
            $formData['companion_count'] = $_POST['companion_count'];
            $formData['visitor_status'] = $_POST['visitor_status'];
            $formData['visitor_age_from'] = $_POST['visitor_age_from'];
            $formData['visitor_age_to'] = $_POST['visitor_age_to'];
            $formData['tour_type'] = $_POST['tour_type'];
            $formData['tour_language'] = $_POST['tour_language'];
            $formData['worksheet_status'] = $_POST['worksheet_status'];
            $formData['worksheet_count'] = $_POST['worksheet_count'];
            $formData['contact_name'] = $_POST['contact_name'];
            $formData['contact_phone'] = $_POST['contact_phone'];
            $formData['contact_email'] = $_POST['contact_email'];
            $formData['payment_option'] = $_POST['payment_option'];
            $formData['extra_conditions'] = $_POST['extra_conditions'];

            $headers = 'From: ' . 'Škocjanski zatok' . '<' . 'noreply@skocjanski-zatok.org' . '>' . $textNewLine;

            // pošlji email formatiran v html (da obdržimo oblikovanje vsebine strani)
            add_filter( 'wp_mail_content_type', create_function('', 'return "text/html"; ') );
            wp_mail( $to, $subject, $ogled->renderFormWithData($formData, $bodyExtra, 'above'), $headers );
            remove_filter( 'wp_mail_content_type', create_function('', 'return "text/html"; ') );
        }
    }

    // uspešno smo vstavili novo rezervacijo v bazo - pošlji povratno email obvestilo uporabniku/naročniku
    if (get_option(Skocjan_vodeni_ogled::$option_name_clientFeedbackMessage) == 'send') {
        // dobi naslov strani z pogoji
        $responsePostBaseID = intval(get_option(Skocjan_vodeni_ogled::$option_name_clientFeedbackMessage_page)); // ID strani z pogoji
        if (function_exists('icl_object_id')) {
            // deprecated but works
            $responsePostBaseID = icl_object_id($responsePostBaseID, 'page', false);
        } else {
            // should work according to documentation when icl_object_id is removed
            $responsePostBaseID = apply_filters('wpml_object_id', $responsePostBaseID, 'post');
        }

        $to =  $_POST['contact_email'];

        $post_replyPage = get_post($responsePostBaseID);

        $subject = $post_replyPage->post_title;

        $content = $post_replyPage->post_content;
        $body = apply_filters('the_content', $content);

        $headers = 'From: ' . get_option(Skocjan_vodeni_ogled::$option_name_clientFeedbackMessage_name) . '<' . get_option(Skocjan_vodeni_ogled::$option_name_clientFeedbackMessage_email) . '>' . $textNewLine;

        // pošlji email formatiran v html (da obdržimo oblikovanje vsebine strani)
        add_filter( 'wp_mail_content_type', create_function('', 'return "text/html"; ') );
        wp_mail( $to, $subject, $body, $headers );
        remove_filter( 'wp_mail_content_type', create_function('', 'return "text/html"; ') );
    }



    // prišli sm odo konca brez problema, sporočimo to
    echo 'ok';
    die();
}
add_action( 'wp_ajax_nopriv_ajax_save_tour', 'ajax_save_tour' );
add_action( 'wp_ajax_ajax_save_tour', 'ajax_save_tour' );



/*
 * Class za prikaz regstracijske forme vodenih ogledov.
 * Za uporabo kliči samo: $ogled = Skocjan_vodeni_ogled::getInstance();
*/
class Skocjan_vodeni_ogled {
    public static $option_name = 'sz_vodeni_ogled_data';
    public static $option_name_file = "sz_vodeni_ogled_xlsFile"; // podatki o XLS datoteki

    public static $option_name_registrationForm = 'sz_vodeni_ogled_registrationForm';   // povezava do strani z registracijsko formo (shrani se ID)

    public static $option_name_termsAndConditions = 'sz_vodeni_ogled_termsAndConditions';   // povezava do pogojev

    public static $option_name_registrationClosed = 'sz_vodeni_ogled_registrationClosed';   // rezervacije so zaprte
    public static $option_name_registrationClosed_textPage = 'sz_vodeni_ogled_registrationClosed_textPage'; // povezava do texsta ko so rezervacije zaprte

    public static $option_name_sendNotification_emailList = 'sz_vodeni_ogled_sendNotification_emailList';   // seznam email naslovov za obveščanje (ločeno z ";")

    public static $option_name_clientFeedbackMessage = 'sz_vodeni_ogled_clientFeedbackMessage';             // pošlji povratni email naročniku ob poslani rezervaciji
    public static $option_name_clientFeedbackMessage_page = 'sz_vodeni_ogled_clientFeedbackMessage_page';   // povezava do vsebine ki se pošlje kot povratno sporočilo
    public static $option_name_clientFeedbackMessage_name = 'sz_vodeni_ogled_clientFeedbackMessage_name';   // ime uporabljeno v glavi povratnega sporočila
    public static $option_name_clientFeedbackMessage_email = 'sz_vodeni_ogled_clientFeedbackMessage_email';   // email naslov uporabljen v glavi povratnega sporočila

    private static $instance = null;

    private $xlsSheetOptions; // podatki (iz XLSa) z možnostmi termina
    private $xlsSheetExceptions; // podatki (iz XLSa) z izjemah termina
    private $xlsSheets; // podatki (iz XLSa) s posameznimi termini o vodenih ogledih

    private $isReady;   // je true, ko vsaj enkrat uploadamo XLS.

    function __construct() {
        $this->isReady= false;
    }

    function __destruct() {
        unset($html);
    }

    public static function getInstance(){
        if(Skocjan_vodeni_ogled::$instance == null){

            $fromDb = unserialize(get_option(Skocjan_vodeni_ogled::$option_name));

            if($fromDb !== false)
                Skocjan_vodeni_ogled::$instance = $fromDb;
            else {
                Skocjan_vodeni_ogled::$instance = new Skocjan_vodeni_ogled();
            }
        }

        Skocjan_odpiralni_cas::$linkToAdminPanel = admin_url( 'admin.php?page=sz-vodeni-ogled-nastavitve');

        return Skocjan_vodeni_ogled::$instance;
    }

    public function isReady(){
        return $this->isReady;
    }

    public static function isRegistrationClosed()
    {
        $userDefinedTheRegistrationIsClosed = get_option(Skocjan_vodeni_ogled::$option_name_registrationClosed);
        if ($userDefinedTheRegistrationIsClosed == 'closed')
            return true;
    }

    public function parseXLSFile($inputFileName){
        try{
            unset($this->xlsSheets);

            $objReader = PHPExcel_IOFactory::load($inputFileName);

            // preglej zavihek z nastavitvami termina (nahajajo v prvem zavihku)
            $this->xlsSheetOptions = new SZ_Term_Settings($objReader->getSheet(0));

            // preglej zavihej s izjemami (nahajajo v drugem zavihku)
            $this->xlsSheetExceptions = new SZ_Term_exceptions($objReader->getSheet(1));

            // preglej zavihke z datumi terminov (nahajajo v vseh ostalih zaviskih)
            for($i = 2; $i < $objReader->getSheetCount(); $i++){
                $this->xlsSheets[] = new SZ_Term($objReader->getSheet($i));
            }
            unset($objReader);
            $this->isReady = true;
        }catch(Exception $e){
            return $e->getMessage();
            return false;
        }
        return true;
    }

    public function renderFormWithData($formData, $extraContent = '', $extraContentPlacement = '' ){
        // vrnemo popoln html izpolnjenega obrazca, katerega uporabimo za pošiljanje emaila ali prikaz v administraciji

        // $formData - pričakova tabela z formatum ključ => vrednost ki so uporabljene v obrazcu
        // $extraContent - ekstra vsebina ki jo lahko vstavimo v generiran html, formatirana naj bo v html
        // $extraContentPlacement - kam vstavimo ekstra vsebino - veljavne vrednosti 'above' ali 'below'

        $htmlOut = '';

        if ($extraContentPlacement == 'above'){
            $htmlOut .= $extraContent;
        }

        $htmlOut .= '<table  style="width: 560px; border: 1px solid #b8b8b8; border-collapse: collapse;">';
        $htmlOut .= '<tr>';
        $htmlOut .= '<td style="text-align: right; color: #000000; width: 50%; padding-top: 6px; padding-bottom: 6px; padding-right: 6px; border: 1px solid #b8b8b8;"></td>';
        $htmlOut .= '<td style="text-align: left; color: #000000; width: 50%; padding-top: 6px; padding-bottom: 6px; padding-left: 6px; border: 1px solid #b8b8b8;">' . $formData['person_legal'] . '</td>';
        $htmlOut .= '</tr>';

        $htmlOut .= '<tr>';
        $htmlOut .= '<td style="text-align: right; color: #000000; width: 50%; padding-top: 6px; padding-bottom: 6px; padding-right: 6px; border: 1px solid #b8b8b8;">' . __('Naziv naročnika:', 'sz-vodeni-ogledi') . '</td>';
        $htmlOut .= '<td style="text-align: left; color: #000000; width: 50%; padding-top: 6px; padding-bottom: 6px; padding-left: 6px; border: 1px solid #b8b8b8;">' . $formData['customer_name'] . '</td>';
        $htmlOut .= '</tr>';

        $htmlOut .= '<tr>';
        $htmlOut .= '<td style="text-align: right; color: #000000; width: 50%; padding-top: 6px; padding-bottom: 6px; padding-right: 6px; border: 1px solid #b8b8b8;">' . __('Naslov:', 'sz-vodeni-ogledi') . '</td>';
        $htmlOut .= '<td style="text-align: left; color: #000000; width: 50%; padding-top: 6px; padding-bottom: 6px; padding-left: 6px; border: 1px solid #b8b8b8;">' . $formData['customer_address'] . '</td>';
        $htmlOut .= '</tr>';

        $htmlOut .= '<tr>';
        $htmlOut .= '<td style="text-align: right; color: #000000; width: 50%; padding-top: 6px; padding-bottom: 6px; padding-right: 6px; border: 1px solid #b8b8b8;">' . __('Davčna številka:', 'sz-vodeni-ogledi') . '</td>';
        $htmlOut .= '<td style="text-align: left; color: #000000; width: 50%; padding-top: 6px; padding-bottom: 6px; padding-left: 6px; border: 1px solid #b8b8b8;">' . $formData['customer_tax_number'] . '</td>';
        $htmlOut .= '</tr>';

        $htmlOut .= '<tr>';
        $htmlOut .= '<td style="text-align: right; color: #000000; width: 50%; padding-top: 6px; padding-bottom: 6px; padding-right: 6px; border: 1px solid #b8b8b8;">' . __('Davčni zavezanec:', 'sz-vodeni-ogledi') . '</td>';
        $htmlOut .= '<td style="text-align: left; color: #000000; width: 50%; padding-top: 6px; padding-bottom: 6px; padding-left: 6px; border: 1px solid #b8b8b8;">' . $formData['customer_subject_to_vat'] . '</td>';
        $htmlOut .= '</tr>';

        $htmlOut .= '<tr>';
        $htmlOut .= '<td style="text-align: right; color: #000000; width: 50%; padding-top: 6px; padding-bottom: 6px; padding-right: 6px; border: 1px solid #b8b8b8;">' . __('Datum vodenja:', 'sz-vodeni-ogledi') . '</td>';
        $htmlOut .= '<td style="text-align: left; color: #000000; width: 50%; padding-top: 6px; padding-bottom: 6px; padding-left: 6px; border: 1px solid #b8b8b8;">' . $formData['tour_date'] . '</td>';
        $htmlOut .= '</tr>';

        $htmlOut .= '<tr>';
        $htmlOut .= '<td style="text-align: right; color: #000000; width: 50%; padding-top: 6px; padding-bottom: 6px; padding-right: 6px; border: 1px solid #b8b8b8;">' . __('Ure vodenja / termin:', 'sz-vodeni-ogledi') . '</td>';
        $htmlOut .= '<td style="text-align: left; color: #000000; width: 50%; padding-top: 6px; padding-bottom: 6px; padding-left: 6px; border: 1px solid #b8b8b8;">' . $formData['tour_time'] . '</td>';
        $htmlOut .= '</tr>';

        $htmlOut .= '<tr>';
        $htmlOut .= '<td style="text-align: right; color: #000000; width: 50%; padding-top: 6px; padding-bottom: 6px; padding-right: 6px; border: 1px solid #b8b8b8;">' . __('Število oseb:', 'sz-vodeni-ogledi') . '</td>';
        $htmlOut .= '<td><table style="width: 100%"><tr>';
        $htmlOut .= '<td style="text-align: left; border: none;">' . $formData['visitor_count'] . '</td>';
        $htmlOut .= '<td style="text-align: right; width: 160px; border: none;">' . __('Od tega spremljevalcev:', 'sz-vodeni-ogledi') .'</td>';
        $htmlOut .= '<td style="text-align: left; border: none;">' . $formData['companion_count'] . '</td>';
        $htmlOut .= '</tr></table></td>';
        $htmlOut .= '</tr>';

        $htmlOut .= '<tr>';
        $htmlOut .= '<td style="text-align: right; color: #000000; width: 50%; padding-top: 6px; padding-bottom: 6px; padding-right: 6px; border: 1px solid #b8b8b8;">' . __('Status  udeležencev:', 'sz-vodeni-ogledi') . '</td>';
        $htmlOut .= '<td style="text-align: left; color: #000000; width: 50%; padding-top: 6px; padding-bottom: 6px; padding-left: 6px; border: 1px solid #b8b8b8;">' . $formData['visitor_status'] . '</td>';
        $htmlOut .= '</tr>';

        $htmlOut .= '<tr>';
        $htmlOut .= '<td style="text-align: right; color: #000000; width: 50%; padding-top: 6px; padding-bottom: 6px; padding-left: 6px; border: 1px solid #b8b8b8;">' . __('Starost udeležencev od:', 'sz-vodeni-ogledi') . '</td>';
        $htmlOut .= '<td><table style="width: 100%"><tr>';
        $htmlOut .= '<td style="text-align: left; border: none;">' . $formData['visitor_age_from'] . '</td>';
        $htmlOut .= '<td style="text-align: right; width: 160px; border: none;">' . __('Do:', 'sz-vodeni-ogledi') .'</td>';
        $htmlOut .= '<td style="text-align: left; border: none;">' . $formData['visitor_age_to'] . '</td>';
        $htmlOut .= '</tr></table></td>';
        $htmlOut .= '</tr>';

        $htmlOut .= '<tr>';
        $htmlOut .= '<td style="text-align: right; color: #000000; width: 50%; padding-top: 6px; padding-bottom: 6px; padding-right: 6px; border: 1px solid #b8b8b8;">' . __('Vodenje:', 'sz-vodeni-ogledi') . '</td>';
        $htmlOut .= '<td style="text-align: left; color: #000000; width: 50%; padding-top: 6px; padding-bottom: 6px; padding-left: 6px; border: 1px solid #b8b8b8;">' . $formData['tour_type'] . '</td>';
        $htmlOut .= '</tr>';

        $htmlOut .= '<tr>';
        $htmlOut .= '<td style="text-align: right; color: #000000; width: 50%; padding-top: 6px; padding-bottom: 6px; padding-right: 6px; border: 1px solid #b8b8b8;">' . __('Vodenje v jeziku:', 'sz-vodeni-ogledi') . '</td>';
        $htmlOut .= '<td style="text-align: left; color: #000000; width: 50%; padding-top: 6px; padding-bottom: 6px; padding-left: 6px; border: 1px solid #b8b8b8;">' . $formData['tour_language'] . '</td>';
        $htmlOut .= '</tr>';

        $htmlOut .= '<tr>';
        $htmlOut .= '<td style="text-align: right; color: #000000; width: 50%; padding-top: 6px; padding-bottom: 6px; padding-right: 6px; border: 1px solid #b8b8b8;">' . __('Delovni list:', 'sz-vodeni-ogledi') . '</td>';
        $htmlOut .= '<td><table style="width: 100%"><tr>';
        $htmlOut .= '<td style="text-align: left; border: none;">' . $formData['worksheet_status'] . '</td>';
        $htmlOut .= '<td style="text-align: right; width: 160px; border: none;">' . __('Št. delovnih listov:', 'sz-vodeni-ogledi') .'</td>';
        $htmlOut .= '<td style="text-align: left; border: none;">' . $formData['worksheet_count'] . '</td>';
        $htmlOut .= '</tr></table></td>';
        $htmlOut .= '</tr>';

        $htmlOut .= '<tr>';
        $htmlOut .= '<td style="text-align: right; color: #000000; width: 50%; padding-top: 6px; padding-bottom: 6px; padding-right: 6px; border: 1px solid #b8b8b8;">' . __('Odgovorna / kontaktna oseba na dan vodenja:', 'sz-vodeni-ogledi') . '</td>';
        $htmlOut .= '<td style="text-align: left; color: #000000; width: 50%; padding-top: 6px; padding-bottom: 6px; padding-left: 6px; border: 1px solid #b8b8b8;">' . $formData['contact_name'] . '</td>';
        $htmlOut .= '</tr>';

        $htmlOut .= '<tr>';
        $htmlOut .= '<td style="text-align: right; color: #000000; width: 50%; padding-top: 6px; padding-bottom: 6px; padding-right: 6px; border: 1px solid #b8b8b8;">' . __('Telefonska številka kontaktne osebe:', 'sz-vodeni-ogledi') . '</td>';
        $htmlOut .= '<td style="text-align: left; color: #000000; width: 50%; padding-top: 6px; padding-bottom: 6px; padding-left: 6px; border: 1px solid #b8b8b8;">' . $formData['contact_phone'] . '</td>';
        $htmlOut .= '</tr>';

        $htmlOut .= '<tr>';
        $htmlOut .= '<td style="text-align: right; color: #000000; width: 50%; padding-top: 6px; padding-bottom: 6px; padding-right: 6px; border: 1px solid #b8b8b8;">' . __('E-naslov kontaktne osebe:', 'sz-vodeni-ogledi') . '</td>';
        $htmlOut .= '<td style="text-align: left; color: #000000; width: 50%; padding-top: 6px; padding-bottom: 6px; padding-left: 6px; border: 1px solid #b8b8b8;">' . $formData['contact_email'] . '</td>';
        $htmlOut .= '</tr>';

        $htmlOut .= '<tr>';
        $htmlOut .= '<td style="text-align: right; color: #000000; width: 50%; padding-top: 6px; padding-bottom: 6px; padding-right: 6px; border: 1px solid #b8b8b8;">' . __('Način plačila:', 'sz-vodeni-ogledi') . '</td>';
        $htmlOut .= '<td style="text-align: left; color: #000000; width: 50%; padding-top: 6px; padding-bottom: 6px; padding-left: 6px; border: 1px solid #b8b8b8;">' . $formData['payment_option'] . '</td>';
        $htmlOut .= '</tr>';

        $htmlOut .= '<tr>';
        $htmlOut .= '<td style="text-align: right; color: #000000; width: 50%; padding-top: 6px; padding-bottom: 6px; padding-right: 6px; border: 1px solid #b8b8b8;">' . __('Morebitne posebnosti', 'sz-vodeni-ogledi') . '<br/>' . __('(zdravstvene, ostale - navedite katere):', 'sz-vodeni-ogledi') . '</td>';
        $htmlOut .= '<td style="text-align: left; color: #000000; width: 50%; padding-top: 6px; padding-bottom: 6px; padding-left: 6px; border: 1px solid #b8b8b8;"><pre>' . $formData['extra_conditions'] . '</pre></td>';
        $htmlOut .= '</tr>';

        $htmlOut .= '</table>';

        if ($extraContentPlacement == 'below'){
            $htmlOut .= $extraContent;
        }

        return $htmlOut;
    }

    public function getForm($formDate) {
        // dobi naslov strani z pogoji
        $conditionsPostBaseID = intval(get_option(Skocjan_vodeni_ogled::$option_name_termsAndConditions)); // ID strani z pogoji
        if (function_exists('icl_object_id')) {
            // deprecated but works
            $conditionsPostID = icl_object_id($conditionsPostBaseID, 'page', false);
        } else {
            // should work according to documentation when icl_object_id is removed
            $conditionsPostID = apply_filters('wpml_object_id', $conditionsPostBaseID, 'post');
        }



        $formHtml = '';

        $formHtml .= '<div class="sz_vodeni_ogled_container">';

        $formHtml .= '<div class="sz_vodeni_ogled_form_notice" style="display: none">' . $this->renderNoticeArea() . '</div>';

        $formHtml .= '<div class="sz_vodeni_ogled">';
        $formHtml .= '<form name="sz_vodeni_ogled_form" id="sz_vodeni_ogled_form" method="post" action="">';
        $formHtml .= '<table class="sz_vodeni_ogled_form_table">';
        $formHtml .= '<tr>';
        $formHtml .= '<td></td>';
        $formHtml .= '<td>';
        $formHtml .= '<table class="sz_vodeni_ogled_form_table_formType">';
        $formHtml .= '<tr>';
        $formHtml .= '<td><label><input type="radio" name="sz_vodeni_ogled_form_formType" id="sz_vodeni_ogled_form_formType_person" value="person" checked>'. __('Fizična oseba', 'sz-vodeni-ogledi') .'</label></td>';
        $formHtml .= '<td><label><input type="radio" name="sz_vodeni_ogled_form_formType" id="sz_vodeni_ogled_form_formType_legal" value="legal">'. __('Pravna oseba', 'sz-vodeni-ogledi') .'</label></td>';
        $formHtml .= '</tr>';
        $formHtml .= '</table>';
        $formHtml .= '</td>';
        $formHtml .= '</tr>';
        $formHtml .= '<tr>';
        $formHtml .= '<td colspan="2" class="form_single"><hr></td>';
        $formHtml .= '</tr>';

        $formHtml .= '<tr>';
        $formHtml .= '<td class="form_left">' . __('Naziv naročnika:', 'sz-vodeni-ogledi') . '</td>';
        $formHtml .= '<td class="form_right"><input type="text" name="sz_vodeni_ogled_form_client" id="sz_vodeni_ogled_form_client" class="input_wide"></td>';
        $formHtml .= '</tr>';
        $formHtml .= '<tr class="sz_vodeni_ogled_form_error" id="customer_name" style="display: none">';
        $formHtml .= '<td class="form_left"></td>';
        $formHtml .= '<td class="form_right">' .  __('Prosim izpolnite polje.', 'sz-vodeni-ogledi') . '</td>';
        $formHtml .= '</tr>';

        $formHtml .= '<tr>';
        $formHtml .= '<td class="form_left">' . __('Naslov:', 'sz-vodeni-ogledi') . '</td>';
        $formHtml .= '<td class="form_right"><input type="text" name="sz_vodeni_ogled_form_address" id="sz_vodeni_ogled_form_address" class="input_wide"></td>';
        $formHtml .= '</tr>';
        $formHtml .= '<tr class="sz_vodeni_ogled_form_error" id="customer_address" style="display: none">';
        $formHtml .= '<td class="form_left"></td>';
        $formHtml .= '<td class="form_right">' .  __('Prosim izpolnite polje.', 'sz-vodeni-ogledi') . '</td>';
        $formHtml .= '</tr>';

        $formHtml .= '<tr id="tax_number_table" style="display: none">';
        $formHtml .= '<td class="form_left">' . __('Davčna številka:', 'sz-vodeni-ogledi') . '</td>';
        $formHtml .= '<td class="form_right"><input type="text" name="sz_vodeni_ogled_form_taxNum" id="sz_vodeni_ogled_form_taxNum" class="input_wide"></td>';
        $formHtml .= '</tr>';
        $formHtml .= '</tr>';
        $formHtml .= '<tr class="sz_vodeni_ogled_form_error" id="customer_tax_number" style="display: none">';
        $formHtml .= '<td class="form_left"></td>';
        $formHtml .= '<td class="form_right">' .  __('Prosim izpolnite polje.', 'sz-vodeni-ogledi') . '</td>';
        $formHtml .= '</tr>';

        $formHtml .= '<tr id="subject_to_vat_table" style="display: none">';
        $formHtml .= '<td class="form_left">' . __('Davčni zavezanec:', 'sz-vodeni-ogledi') . '</td>';
        $formHtml .= '<td class="form_right"><div class="sz_vodeni_ogled_form_subject_to_vat">';
        $formHtml .= '<div class="radio_top_bottom_pad"><label><input type="radio" name="sz_vodeni_ogled_form_subject_to_vat" id="sz_vodeni_ogled_form_subject_to_vat_yes" value="yes">'. __('DA', 'sz-vodeni-ogledi') . '</label></div>';
        $formHtml .= '<div class="radio_top_bottom_pad"><label><input type="radio" name="sz_vodeni_ogled_form_subject_to_vat" id="sz_vodeni_ogled_form_subject_to_vat_no" value="no">'. __('NE', 'sz-vodeni-ogledi') . '</label></div>';
        $formHtml .= '</div></td>';
        $formHtml .= '</tr>';
        $formHtml .= '<tr class="sz_vodeni_ogled_form_error" id="subject_to_vat" style="display: none">';
        $formHtml .= '<td class="form_left"></td>';
        $formHtml .= '<td class="form_right">' .  __('Prosim označite izbiro.', 'sz-vodeni-ogledi') . '</td>';
        $formHtml .= '</tr>';

        $formHtml .= '<tr>';
        $formHtml .= '<td class="form_left">' . __('Datum vodenja:', 'sz-vodeni-ogledi') . '</td>';
        $formHtml .= '<td class="form_right"><div class="sz_vodeni_ogled_form_dateContainer">';
        $formHtml .= '<div><input type="text" name="sz_vodeni_ogled_form_date" id="sz_vodeni_ogled_form_date" class="input_wide" readonly></div>';
        $formHtml .= '<div id="sz_vodeni_ogled_form_calendarContainer" style="position: absolute; display: none;">' . $this->renderCalander($formDate) . '</div>';
        $formHtml .= '<div id="sz_vodeni_ogled_form_calendarContainerMask" style="position: absolute; display: none;">' . '<img class="sz_vodeni_ogled_calendar_spinner_center image_spin" src="' . plugin_dir_url(__FILE__) . 'img/spinner_image.png"  width="128" height="128">' . '</div>';
        $formHtml .= '</div></td>';
        $formHtml .= '</tr>';
        $formHtml .= '<tr class="sz_vodeni_ogled_form_error" id="no_available_term" style="display: none">';
        $formHtml .= '<td class="form_left"></td>';
        $formHtml .= '<td class="form_right">' .  __('Izbrani datum nima prostih terminov.', 'sz-vodeni-ogledi') . '</td>';
        $formHtml .= '</tr>';

        $formHtml .= '<tr>';
        $formHtml .= '<td class="form_left">' . __('Ure vodenja / termin:', 'sz-vodeni-ogledi') . '</td>';
        $formHtml .= '<td class="form_right"><select name="sz_vodeni_ogled_form_term" id="sz_vodeni_ogled_form_term" class="input_wide"></select></td>';
        $formHtml .= '</tr>';

        $formHtml .= '<tr>';
        $formHtml .= '<td class="form_left">' . __('Število oseb:', 'sz-vodeni-ogledi') . '</td>';
        $formHtml .= '<td class="form_right no_margins"><table ><tr>';
        $formHtml .= '<td class="no_margins" style="text-align: left; padding-left: 1px !important;"><input type="text" name="sz_vodeni_ogled_form_numPeople" id="sz_vodeni_ogled_form_numPeople" class="input_narrow"></td>';
        $formHtml .= '<td class="no_margins" style="text-align: right; width: 160px;">' . __('Od tega spremljevalcev:', 'sz-vodeni-ogledi') .'</td>';
        $formHtml .= '<td class="no_margins" style="text-align: right; padding-right: 1px !important;"><input type="text" name="sz_vodeni_ogled_form_numCompanion" id="sz_vodeni_ogled_form_numCompanion" class="input_narrow"></td>';
        $formHtml .= '</tr></table></td>';
        $formHtml .= '</tr>';
        $formHtml .= '<tr class="sz_vodeni_ogled_form_error" id="visitor_companion" style="display: none">';
        $formHtml .= '<td class="form_left"></td>';
        $formHtml .= '<td class="form_right">' .  __('Prosim izpolnite polje.', 'sz-vodeni-ogledi') . '</td>';
        $formHtml .= '</tr>';

        $formHtml .= '<tr>';
        $formHtml .= '<td class="form_left">' . __('Status  udeležencev:', 'sz-vodeni-ogledi') . '</td>';
        $formHtml .= '<td class="form_right"><select name="sz_vodeni_ogled_form_visitorStatus" id="sz_vodeni_ogled_form_visitorStatus" class="input_wide"></select></td>';
        $formHtml .= '</tr>';
        $formHtml .= '</tr>';

        $formHtml .= '<tr>';
        $formHtml .= '<td class="form_left">' . __('Starost udeležencev od:', 'sz-vodeni-ogledi') . '</td>';
        $formHtml .= '<td class="form_right no_margins"><table ><tr>';
        $formHtml .= '<td class="no_margins" style="text-align: left; padding-left: 1px !important;"><input type="text" name="sz_vodeni_ogled_form_visitorAgeFrom" id="sz_vodeni_ogled_form_visitorAgeFrom" class="input_narrow"></td>';
        $formHtml .= '<td class="no_margins" style="text-align: right; width: 160px;">' . __('Do:', 'sz-vodeni-ogledi') .'</td>';
        $formHtml .= '<td class="no_margins" style="text-align: right; padding-right: 1px !important;"><input type="text" name="sz_vodeni_ogled_form_visitorAgeTo" id="sz_vodeni_ogled_form_visitorAgeTo" class="input_narrow"></td>';
        $formHtml .= '</tr></table></td>';
        $formHtml .= '</tr>';
        $formHtml .= '<tr class="sz_vodeni_ogled_form_error" id="from_to" style="display: none">';
        $formHtml .= '<td class="form_left"></td>';
        $formHtml .= '<td class="form_right">' .  __('Prosim izpolnite polje.', 'sz-vodeni-ogledi') . '</td>';
        $formHtml .= '</tr>';

        $formHtml .= '<tr>';
        $formHtml .= '<td class="form_left">' . __('Vodenje:', 'sz-vodeni-ogledi') . '</td>';
        $formHtml .= '<td class="form_right"><div class="sz_vodeni_ogled_form_termType"></div></td>';
        $formHtml .= '</tr>';

        $formHtml .= '<tr>';
        $formHtml .= '<td class="form_left">' . __('Vodenje v jeziku:', 'sz-vodeni-ogledi') . '</td>';
        $formHtml .= '<td class="form_right"><div class="sz_vodeni_ogled_form_lang"></div></td>';
        $formHtml .= '</tr>';

        $formHtml .= '<tr>';
        $formHtml .= '<td class="form_left no_margins">';
        $formHtml .= '<table>';
        $formHtml .= '<tr style="height: 21px"><td></td>';
        $formHtml .= '<td class="no_margins" style="text-align: right">' . __('Delovni list:', 'sz-vodeni-ogledi') . '</td></tr>';
        $formHtml .= '<tr style="height: 21px"><td></td><td></td></tr>';
        $formHtml .= '</table>';
        $formHtml .= '</td>';

        $formHtml .= '<td class="form_right no_margins">';
        $formHtml .= '<table class="sz_vodeni_ogled_form_lang_worksheet no_margins move_left">';
        $formHtml .= '<tr style="height: 21px"><td class="no_margins"><label><input type="radio" name="sz_vodeni_ogled_form_worksheet" id="sz_vodeni_ogled_form_worksheet_yes" value="yes" checked>'. __('DA', 'sz-vodeni-ogledi') . '</label></td>';
        $formHtml .= '<td class="no_margins" style="text-align: right">' . __('Št. delovnih listov:', 'sz-vodeni-ogledi') . '   <input type="text" name="sz_vodeni_ogled_form_worksheetNum" id="sz_vodeni_ogled_form_worksheetNum" class="input_narrow">' . '</td></tr>';
        $formHtml .= '<tr style="height: 21px"><td class="no_margins"><label><input type="radio" name="sz_vodeni_ogled_form_worksheet" id="sz_vodeni_ogled_form_worksheet_no" value="no">'. __('NE', 'sz-vodeni-ogledi') . '</label></td>';
        $formHtml .= '<td></td></tr>';
        $formHtml .= '</table>';
        $formHtml .= '</td>';
        $formHtml .= '</tr>';
        $formHtml .= '</tr>';
        $formHtml .= '<tr class="sz_vodeni_ogled_form_error" id="worksheet_number" style="display: none">';
        $formHtml .= '<td class="form_left"></td>';
        $formHtml .= '<td class="form_right">' .  __('Prosim izpolnite polje.', 'sz-vodeni-ogledi') . '</td>';
        $formHtml .= '</tr>';

        $formHtml .= '<tr >';
        $formHtml .= '<td class="form_left">' . __('Odgovorna / kontaktna oseba na dan vodenja:', 'sz-vodeni-ogledi') . '</td>';
        $formHtml .= '<td class="form_right"><input type="text" name="sz_vodeni_ogled_form_contactName" id="sz_vodeni_ogled_form_contactName" class="input_wide"></td>';
        $formHtml .= '</tr>';
        $formHtml .= '<tr class="sz_vodeni_ogled_form_error" id="contact_name" style="display: none">';
        $formHtml .= '<td class="form_left"></td>';
        $formHtml .= '<td class="form_right">' .  __('Prosim izpolnite polje.', 'sz-vodeni-ogledi') . '</td>';
        $formHtml .= '</tr>';

        $formHtml .= '<tr>';
        $formHtml .= '<td class="form_left">' . __('Telefonska številka kontaktne osebe:', 'sz-vodeni-ogledi') . '</td>';
        $formHtml .= '<td class="form_right"><input type="text" name="sz_vodeni_ogled_form_contactNumber" id="sz_vodeni_ogled_form_contactNumber" class="input_wide"></td>';
        $formHtml .= '</tr>';
        $formHtml .= '</tr>';
        $formHtml .= '<tr class="sz_vodeni_ogled_form_error" id="contact_phone" style="display: none">';
        $formHtml .= '<td class="form_left"></td>';
        $formHtml .= '<td class="form_right">' .  __('Prosim izpolnite polje.', 'sz-vodeni-ogledi') . '</td>';
        $formHtml .= '</tr>';

        $formHtml .= '<tr>';
        $formHtml .= '<td class="form_left">' . __('E-naslov kontaktne osebe:', 'sz-vodeni-ogledi') . '</td>';
        $formHtml .= '<td class="form_right"><input type="text" name="sz_vodeni_ogled_form_contactEmail" id="sz_vodeni_ogled_form_contactEmail" class="input_wide"></td>';
        $formHtml .= '</tr>';
        $formHtml .= '</tr>';
        $formHtml .= '<tr class="sz_vodeni_ogled_form_error" id="contact_email" style="display: none">';
        $formHtml .= '<td class="form_left"></td>';
        $formHtml .= '<td class="form_right">' .  __('Prosim izpolnite polje.', 'sz-vodeni-ogledi') . '</td>';
        $formHtml .= '</tr>';

        $formHtml .= '<tr>';
        $formHtml .= '<td class="form_left">' . __('Način plačila:', 'sz-vodeni-ogledi') . '</td>';
        $formHtml .= '<td class="form_right">';
        $formHtml .= '<table class="sz_vodeni_ogled_form_lang_worksheet no_margins move_left">';
        $formHtml .= '<tr><td class="no_margins move_left"><div class="radio_top_bottom_pad"><label><input type="radio" name="sz_vodeni_ogled_form_paymentType" id="sz_vodeni_ogled_form_paymentType_bill" value="bill" checked>'. __('Po TRR', 'sz-vodeni-ogledi') .'</label></div>';
        $formHtml .= '<div class="radio_top_bottom_pad"><label><input type="radio" name="sz_vodeni_ogled_form_paymentType" id="sz_vodeni_ogled_form_paymentType_cash" value="cash">'. __('Gotovina', 'sz-vodeni-ogledi') .'</label></div></td></tr>';
        $formHtml .= '</table>';
        $formHtml .= '</td>';
        $formHtml .= '</tr>';

        $formHtml .= '<tr>';
        $formHtml .= '<td class="form_left">' . __('Morebitne posebnosti', 'sz-vodeni-ogledi') . '<br/>' . __('(zdravstvene, ostale - navedite katere):', 'sz-vodeni-ogledi') . '</td>';
        $formHtml .= '<td class="form_right"><textarea name="sz_vodeni_ogled_form_specialInstructions" id="sz_vodeni_ogled_form_specialInstructions" class="textarea_wide"></textarea></td>';
        $formHtml .= '</tr>';

        $formHtml .= '<tr>';
        $formHtml .= '<td colspan="2" class="form_single"><hr></td>';
        $formHtml .= '</tr>';

        $formHtml .= '<tr>';
        $formHtml .= '<td class="form_left"></td>';
        $formHtml .= '<td class="form_right no_margins move_left">';
        $formHtml .= '<input type="checkbox" name="sz_vodeni_ogled_form_conditions" id="sz_vodeni_ogled_form_conditions" value="agree">';
        $formHtml .= __('Strinjam se s', 'sz-vodeni-ogledi') . ' <a href="' . get_permalink($conditionsPostID) . '" target="_blank">'. __('splošnimi pogoji.', 'sz-vodeni-ogledi') .'</a><br/>';
        $formHtml .= '</td>';
        $formHtml .= '</tr>';
        $formHtml .= '<tr class="sz_vodeni_ogled_form_error" id="condition_agree_check" style="display: none">';
        $formHtml .= '<td class="form_left"></td>';
        $formHtml .= '<td class="form_right">' .  __('Strinjati se morate s splošnimi pogoji.', 'sz-vodeni-ogledi') . '</td>';
        $formHtml .= '</tr>';

        $formHtml .= '<tr>';
        $formHtml .= '<td class="form_left"></td>';
        $formHtml .= '<td class="form_right no margin move_left">' . '<button type="button" id="sz_vodeni_ogled_calendar_submit_button">' . __('POŠLJI', 'sz-vodeni-ogledi') . '<img src="' . plugin_dir_url(__FILE__) . 'img/submit_arrow.png"  width="16" height="10">' . '</button>' . '</td>';
        $formHtml .= '</tr>';
        $formHtml .= '</table>';
        $formHtml .= '</form>';
        $formHtml .= '</div> <!-- <div class="sz_vodeni_ogled"> -->';
        $formHtml .= '<div class="sz_vodeni_ogled_form_mask"> <img class="sz_vodeni_ogled_form_mask_spinner_center image_spin" src="' . plugin_dir_url(__FILE__) . 'img/spinner_image.png"  width="128" height="128"> </div>';
        $formHtml .= '</div> <!-- <div class="sz_vodeni_ogled_container"> -->';

        return $formHtml;
    }

    public function renderCalander($formDate) {
        $formDateExploaded = explode('.', $formDate);   // dd.mm.yyyy

        $dayNameArray = array(
            __('PON', 'sz-vodeni-ogledi'),
            __('TOR', 'sz-vodeni-ogledi'),
            __('SRE', 'sz-vodeni-ogledi'),
            __('ČET', 'sz-vodeni-ogledi'),
            __('PET', 'sz-vodeni-ogledi'),
            __('SOB', 'sz-vodeni-ogledi'),
            __('NED', 'sz-vodeni-ogledi')
        );
        $monthNameArray = array(
            __('Januar', 'sz-vodeni-ogledi'),
            __('Februar', 'sz-vodeni-ogledi'),
            __('Marec', 'sz-vodeni-ogledi'),
            __('April', 'sz-vodeni-ogledi'),
            __('Maj', 'sz-vodeni-ogledi'),
            __('Junij', 'sz-vodeni-ogledi'),
            __('Julij', 'sz-vodeni-ogledi'),
            __('Avgust', 'sz-vodeni-ogledi'),
            __('September', 'sz-vodeni-ogledi'),
            __('Oktober', 'sz-vodeni-ogledi'),
            __('November', 'sz-vodeni-ogledi'),
            __('December', 'sz-vodeni-ogledi')
        );

        $currentWeekday = date('w',mktime(0,0,0,$formDateExploaded[1],1,$formDateExploaded[2]));
        $currentWeekday -= 1;   // php zacne teden z nedeljo, mi z ponedeljkom
        if ($currentWeekday < 0)
            $currentWeekday = 6;

        $monthTotalDays = date('t',mktime(0,0,0,$formDateExploaded[1],1,$formDateExploaded[2]));

        $day_counter = 0;
        $dayInWeek_counter = 0;



        // izris kolendarja
        $calHtml = '';

        $calHtml .= '<table class="sz_vodeni_ogled_calendar">';

        // izrisi glavo z mesecem
        $calHtml .= '<tr class="no_margins sz_vodeni_ogled_calendar_row sz_vodeni_ogled_calendar_head">';
        $calHtml .= '<td colspan="7" class="no_margins"><table class="sz_vodeni_ogled_calendar_head_month"><tr>';
        $calHtml .= '<td class="sz_vodeni_ogled_calendar_head_prev"><button type="button" id="sz_vodeni_ogled_calendar_head_prev_button">' . '<' . '</button></td>';
        $calHtml .= '<td class="sz_vodeni_ogled_calendar_head_title">' . $monthNameArray[date('m',mktime(0,0,0,$formDateExploaded[1],1,$formDateExploaded[2])) - 1] . ' ' . $formDateExploaded[2] . '</td>';
        $calHtml .= '<td class="sz_vodeni_ogled_calendar_head_next"><button type="button" id="sz_vodeni_ogled_calendar_head_next_button">' . '>' . '</button></td>';
        $calHtml .= '</tr></table></td>';
        $calHtml .= '</tr>';
        // izrisi glavo s imeni dnevov
        $calHtml .= '<tr class="sz_vodeni_ogled_calendar_row sz_vodeni_ogled_calendar_head sz_vodeni_ogled_calendar_head_day"><td class="sz_vodeni_ogled_calendar_head_day_name">'
            .implode('</td><td class="sz_vodeni_ogled_calendar_head_day_name">',$dayNameArray).'</td></tr>';

        // izrisi prvi teden (upostevaj zamik zacetka meseca)
        $calHtml.= '<tr class="sz_vodeni_ogled_calendar_row">';
        // izrisi prazne dni
        for($fw = 0; $fw < $currentWeekday; $fw++) {
            $calHtml .= '<td class="sz_vodeni_ogled_calendar_day_empty"> </td>';
            $dayInWeek_counter++;
        }
        // izrisi posamezne dneve
        for($list_day = 1; $list_day <= $monthTotalDays; $list_day++) {
            $calHtml .= '<td class="sz_vodeni_ogled_calendar_day">';

            // stevilka dneva
            $calHtml .= '<div class="sz_vodeni_ogled_calendar_day_number">'.$list_day.'</div>';

            // podatki o dnevu - skrito
            $calHtml .= '<table class="sz_vodeni_ogled_calendar_day_dayData">';
            $calHtml .= '<tr>';
            $calHtml .= '<td id="sz_vodeni_ogled_calendar_day_dayData_date">' . sprintf("%02d", $list_day) . '.' . sprintf("%02d", $formDateExploaded[1]) . '.' . $formDateExploaded[2] . '</td>';
            $calHtml .= '</tr>';
            $calHtml .= '</table>';

            $termMarker = '';

            // poišči podatke o terminih za današnji datum - skrito
            if (new DateTime($formDateExploaded[2] . '-' . $formDateExploaded[1] . '-' . $list_day . ' 23:59:59') >= new DateTime()) {

                // če datum ima termin
                if ($this->dateHasTerm($list_day . '.' . $formDateExploaded[1] . '.' . $formDateExploaded[2])) {
                    // dobi seznam terminov
                    $termListForToday = $this->getTermList($list_day . '.' . $formDateExploaded[1] . '.' . $formDateExploaded[2]);
                    // preveri prostost terminov
                    $availableTermList = $this->parseTermListIfAvailable($termListForToday);

                    // če nam kaj ostane, shranimo podatke o terminih v kolendar
                    if (count($availableTermList) > 0) {
                        $calHtml .= '<table class="sz_vodeni_ogled_calendar_day_termData">';
                        foreach ($availableTermList as $singleTerm) {
                            $calHtml .= '<tr><td>';
                            $calHtml .= '<table class="sz_vodeni_ogled_calendar_day_termData_single">';
                            $calHtml .= '<tr>';
                            $calHtml .= '<td id="sz_vodeni_ogled_calendar_day_termData_single_date">' . $singleTerm[0] . '</td>';
                            $calHtml .= '<td id="sz_vodeni_ogled_calendar_day_termData_single_time">' . $singleTerm[1] . '</td>';
                            $calHtml .= '<td id="sz_vodeni_ogled_calendar_day_termData_single_type">' . $singleTerm[2] . '</td>';
                            $calHtml .= '<td id="sz_vodeni_ogled_calendar_day_termData_single_language">' . $singleTerm[3] . '</td>';
                            $calHtml .= '</tr>';
                            $calHtml .= '</table>';
                            $calHtml .= '</td></tr>';

                            $termMarker .= '•';
                        }
                        $calHtml .=  '</table>';
                    }
                }
            }

            $calHtml .= '<table class="sz_vodeni_ogled_calendar_day_termMarker">';
            $calHtml .= '<tr><td>' . $termMarker . '</td></tr>';
            $calHtml .= '</table>';

            $calHtml .= '</td>';
            if($currentWeekday == 6) {
                $calHtml .= '</tr>';
                if(($day_counter+1) != $monthTotalDays)
                    $calHtml .= '<tr class="sz_vodeni_ogled_calendar_row">';
                $currentWeekday = -1;
                $dayInWeek_counter = 0;
            }
            $dayInWeek_counter++; $currentWeekday++; $day_counter++;
        }
        // izrisi preostale prazne
        if (($dayInWeek_counter > 1) && ($dayInWeek_counter < 8)) {
            for($lw = 1; $lw <= (8 - $dayInWeek_counter); $lw++) {
                $calHtml .= '<td class="sz_vodeni_ogled_calendar_day_empty"> </td>';
            }
        }
        $calHtml .= '</tr>';

        // shranimo še podatke o nastavitvah za vse termine (jeziki, programi, ...)
        $calHtml .= '<tr class="sz_vodeni_ogled_calendar_metaData"><td><table class="sz_vodeni_ogled_calendar_metaData_dataTable">';

        $calHtml .= '<tr><td><table class="sz_vodeni_ogled_calendar_metaData_dataTable_language">';
        // shranimo jezik
        $localLangList = $this->getMetaLanguageList();
        foreach($localLangList as $localLanguage) {
            $calHtml .= '<tr><td><table class="sz_vodeni_ogled_calendar_metaData_dataTable_language_single">';
            foreach($localLanguage as $langVal){
                $calHtml .= '<tr><td>' . $langVal[0] . '</td><td>' . $langVal[1] . '</td></tr>';
            }
            $calHtml .= '</table></td></tr>';
        }
        $calHtml .= '</table></td></tr>';
        // shranimo imena posmeznih programov v vseh jezikih
        $calHtml .= '<tr><td><table class="sz_vodeni_ogled_calendar_metaData_dataTable_program">';
        $localProgramList = $this->getMetaProgramList();
        foreach($localProgramList as $localProgram) {
            $calHtml .= '<tr><td><table class="sz_vodeni_ogled_calendar_metaData_dataTable_program_single">';
            foreach($localProgram as $progVal){
                $calHtml .= '<tr><td>' . $progVal[0] . '</td><td>' . $progVal[1] . '</td></tr>';
            }
            $calHtml .= '</table></td></tr>';
        }
        $calHtml .= '</table></td></tr>';
        // shranimo imena posameznih statusov obiskovalcev v vseh jezikih
        $calHtml .= '<tr><td><table class="sz_vodeni_ogled_calendar_metaData_dataTable_visitorStatus">';
        $localVisitorStatusList = $this->getMetaVisitorStatusList();
        foreach($localVisitorStatusList as $localVisitorStatus) {
            $calHtml .= '<tr><td><table class="sz_vodeni_ogled_calendar_metaData_dataTable_visitorStatus_single">';
            foreach($localVisitorStatus as $statVal){
                $calHtml .= '<tr><td>' . $statVal[0] . '</td><td>' . $statVal[1] . '</td></tr>';
            }
            $calHtml .= '</table></td></tr>';
        }
        $calHtml .= '</table></td></tr>';
        // shranimo jezik trenutne seje
        $calHtml .= '<tr><td><table class="sz_vodeni_ogled_calendar_metaData_dataTable_sessionLanguage">';
        $calHtml .= '<tr><td>language</td><td>' . ICL_LANGUAGE_CODE . '</td></tr>';
        $calHtml .= '</table></td></tr>';
        // shranimo preveden izraz za termin
        $calHtml .= '<tr><td><table class="sz_vodeni_ogled_calendar_metaData_dataTable_termLabel">';
        $calHtml .= '<tr><td>label</td><td>' . __('Termin', 'sz-vodeni-ogledi-admin') . '</td></tr>';
        $calHtml .= '</table></td></tr>';
        // shranimo datum trenutnega kolendarja
        $calHtml .= '<tr><td><table class="sz_vodeni_ogled_calendar_metaData_dataTable_calDate">';
        $calHtml .= '<tr><td>date</td><td>' . $formDate . '</td></tr>';
        $calHtml .= '</table></td></tr>';

        $calHtml .= '</table></td></tr>';
        $calHtml .= '</table> <!-- END calendar table -->';

        return $calHtml;
    }

    private function renderNoticeArea() {
        $noticeHtml = '';

        // opombe pomembnejše od izpolnene forma
        $noticeHtml .= '<div class="sz_vodeni_ogled_notice_error" id="no_available_term" style="display: none">' . __('Izbrani datum nima prostih terminov.', 'sz-vodeni-ogledi') . '</div>';

        // opombe glede izpolnjevanja forme
        $noticeHtml .= '<div class="sz_vodeni_ogled_notice_error" id="form_missing" style="display: none">' . __('Prosim izpolnite manjkajoča polja.', 'sz-vodeni-ogledi') . '</div>';

        // opombe pred pošiljanjem
        $noticeHtml .= '<div class="sz_vodeni_ogled_notice_error" id="condition_agree_check" style="display: none">' . __('Strinjati se morate s splošnimi pogoji.', 'sz-vodeni-ogledi') . '</div>';

        // opombe za shranjevanje rezervacije
        $noticeHtml .= '<div class="sz_vodeni_ogled_notice_error" id="general_error" style="display: none">' . __('Napaka pri pošiljanju rezervacije.', 'sz-vodeni-ogledi') . '</div>';
        $noticeHtml .= '<div class="sz_vodeni_ogled_notice_error" id="term_exists" style="display: none">' . __('Izbrani termin je že rezerviran.', 'sz-vodeni-ogledi') . '</div>';
        $noticeHtml .= '<div class="sz_vodeni_ogled_notice_success" id="registration_complete" style="display: none">' . __('Rezervacija je bila uspešno poslana.', 'sz-vodeni-ogledi') . '</div>';

        // napaka pri posodobitvi kolendarja
        $noticeHtml .= '<div class="sz_vodeni_ogled_notice_error" id="update_calander" style="display: none">' . __('Prišlo je do napake pri posodobitvi koledarja.', 'sz-vodeni-ogledi') . '</div>';

        $noticeHtml .= '<br/>';

        return $noticeHtml;
    }

    private function dateHasTerm($formDate) {

        // preverimo ali smo odprti (in s tem ponujati termine) iz plugina odpiralnih časov (če obstaja)
        if (class_exists('Skocjan_odpiralni_cas')) {
            $urnik = Skocjan_odpiralni_cas::getInstance();

            // preveri če je park zaprt za nedoločen čas
            if ($urnik->areWeClosedForEver())
                return false;

            // preveri ali park odprt za ta datum
            $formDateExploaded = explode('.', $formDate);   // dd.mm.yyyy
            if ($urnik->areWeClosed(strtotime($formDateExploaded[2].'-'.$formDateExploaded[1].'-'.$formDateExploaded[0])))
                return false;
        }

        // najdi stran ki ima datum
        $xlsPageWithDate = -1;
        for($countXLS = 0; $countXLS < count($this->xlsSheets); $countXLS++) {
            if ($this->xlsSheets[$countXLS]->hasThisDate($formDate)) {
                $xlsPageWithDate = $countXLS;
                break;
            }
        }

        if ($xlsPageWithDate < 0)
            return false;

        // preveri ali ima datum prost termin
        return $this->xlsSheets[$xlsPageWithDate]->dateHasTerm($formDate, $this->xlsSheetExceptions->getExceptionsList());
    }

    private function getTermList($formDate) {

        // najdi stran ki ima datum
        $xlsPageWithDate = -1;
        for($countXLS = 0; $countXLS < count($this->xlsSheets); $countXLS++) {
            if ($this->xlsSheets[$countXLS]->hasThisDate($formDate)) {
                $xlsPageWithDate = $countXLS;
                break;
            }
        }

        if ($xlsPageWithDate < 0)
            return array();

        return $this->xlsSheets[$countXLS]->getDateTermList($formDate, $this->xlsSheetExceptions->getExceptionsList());
    }

    private function parseTermListIfAvailable($termList) {
        $ogled = Skocjan_vodeni_ogled::getInstance();

        $termListProcessed = array();

        foreach ($termList as $term) {
            $termDateSplit = explode('.', $term[0]);

            $termDateSQL = $termDateSplit[2] . '-' . sprintf("%02d", $termDateSplit[1]) . '-' . sprintf("%02d", $termDateSplit[0]);
            $termTimeSQL = $term[1] . ':00';

            if ($ogled->checkIfTermIsAvailable($termDateSQL, $termTimeSQL)) {
                $termListProcessed[] = $term;
            }
        }

        return $termListProcessed;
    }

    public function checkIfTermIsAvailable($termDate, $termTime) {
        global $wpdb;

        $table_name = $wpdb->prefix . 'sz_vodeni_ogledi_data';

        $termSelectResult = $wpdb->get_var( "SELECT id FROM $table_name WHERE `tour_date` = '$termDate' AND `tour_time` = '$termTime'" );

        if ($termSelectResult == null)
            return true;

        return false;
    }

    private function getMetaLanguageList() {
        // vrne tabelo tabel (koda jezika, zezik)
        return $this->xlsSheetOptions->getLanguageArray();
    }

    private function getMetaProgramList() {
        // vrne tabelo, ki vsebuje tabele z posameznim programom ([0] type:oznaka, [1]... - jezik:napis)
        return $this->xlsSheetOptions->getProgramArray();
    }

    private function getMetaVisitorStatusList() {
        // vrne tabelo, ki vsebuje tabele z posameznim statusom ([0] type:oznaka, [1]... - jezik:napis)
        return $this->xlsSheetOptions->getVisitorStatusArray();
    }
}

/*
 * Class ki parsa in shrani informacije o nastavitvah in možnostih termina
 */
class SZ_Term_Settings{
    public $programArray;
    public $languageArray;
    public $visitorStatusArray;

    public function __construct($sheet) {
        $sheetData = $sheet->toArray(null, true, true, true);

        $this->programArray = array();

        // parsaj jezike
        $sheetLine = 0;
        while($sheetData[$sheetLine]['A'] != 'Jezik:')
            $sheetLine++;
        $sheetLine++;
        $this->parseLanguages($sheetData, $sheetLine);

        // parsaj programe
        $sheetLine = 0;
        while($sheetData[$sheetLine]['E'] != 'Program:')
            $sheetLine++;
        $sheetLine++;
        $this->parsePrograms($sheetData, $sheetLine);

        // parsaj tipe obiskovalcev
        $sheetLine = 0;
        while($sheetData[$sheetLine]['I'] != 'Status:')
            $sheetLine++;
        $sheetLine++;
        $this->parseVisitorStatus($sheetData, $sheetLine);

        unset($sheetData);
    }

    function __destruct() {
        unset($this->programArray);
        unset($this->languageArray);
        unset($this->visitorStatusArray);
    }

    private function parseLanguages($sheetData, $dataLineStart) {
        $localLineNumber = $dataLineStart;

        $deadSpaceOptionCount = 0;
        while (true) {
            if (($sheetData[$localLineNumber]['A'] == 'Oznaka:') && (($sheetData[$localLineNumber]['B'] != null) && ($sheetData[$localLineNumber]['B'] != ''))) {
                $deadSpaceOptionCount = 0;

                $localLang = array();
                $localLang[] = array('label', $sheetData[$localLineNumber]['B']);

                $langLineNumber = $localLineNumber + 1;
                while (($sheetData[$langLineNumber]['A'] != 'Oznaka:')
                    && ($sheetData[$langLineNumber]['A'] != '') && ($sheetData[$langLineNumber]['A'] != null)) {
                    $localLang[] = array($sheetData[$langLineNumber]['A'], $sheetData[$langLineNumber]['B']);
                    $langLineNumber++;
                }
                $this->languageArray[] = $localLang;
            }

            $localLineNumber++;
            $deadSpaceOptionCount++;

            // če se zgubimo in ne najdemo nobene nove opcije več kot 10 vrstic, potem zaključimo
            if ($deadSpaceOptionCount > 10)
                break;
        }
    }

    private function parsePrograms($sheetData, $dataLineStart) {
        $localLineNumber = $dataLineStart;

        $deadSpaceOptionCount = 0;
        while (true) {
            if (($sheetData[$localLineNumber]['E'] == 'Tip:') && (($sheetData[$localLineNumber]['F'] != null) && ($sheetData[$localLineNumber]['F'] != ''))) {
                $deadSpaceOptionCount = 0;

                $localProg = array();
                $localProg[] = array('type', $sheetData[$localLineNumber]['F']);

                $progLineNumber = $localLineNumber + 1;
                while (($sheetData[$progLineNumber]['E'] != 'Tip:')
                && ($sheetData[$progLineNumber]['E'] != '') && ($sheetData[$progLineNumber]['E'] != null)) {
                    $localProg[] = array($sheetData[$progLineNumber]['E'], $sheetData[$progLineNumber]['F']);
                    $progLineNumber++;
                }
                $this->programArray[] = $localProg;
            }

            $localLineNumber++;
            $deadSpaceOptionCount++;

            // če se zgubimo in ne najdemo nobene nove opcije več kot 10 vrstic, potem zaključimo
            if ($deadSpaceOptionCount > 10)
                break;
        }
    }

    private function parseVisitorStatus($sheetData, $dataLineStart) {
        $localLineNumber = $dataLineStart;

        $deadSpaceOptionCount = 0;
        while (true) {
            if (($sheetData[$localLineNumber]['I'] == 'Tip:') && (($sheetData[$localLineNumber]['J'] != null) && ($sheetData[$localLineNumber]['J'] != ''))) {
                $deadSpaceOptionCount = 0;

                $localStat = array();
                $localStat[] = array('type', $sheetData[$localLineNumber]['J']);

                $statLineNumber = $localLineNumber + 1;
                while (($sheetData[$statLineNumber]['I'] != 'Tip:')
                    && ($sheetData[$statLineNumber]['I'] != '') && ($sheetData[$statLineNumber]['I'] != null)) {
                    $localStat[] = array($sheetData[$statLineNumber]['I'], $sheetData[$statLineNumber]['J']);
                    $statLineNumber++;
                }
                $this->visitorStatusArray[] = $localStat;
            }

            $localLineNumber++;
            $deadSpaceOptionCount++;

            // če se zgubimo in ne najdemo nobene nove opcije več kot 10 vrstic, potem zaključimo
            if ($deadSpaceOptionCount > 10)
                break;
        }
    }

    public function  getLanguageArray() {
        return $this->languageArray;
    }

    public function  getProgramArray() {
        return $this->programArray;
    }

    public function  getVisitorStatusArray() {
        return $this->visitorStatusArray;
    }
}

/*
 * Class ki parsa in shrani informacije o izjemah terminov vodenih ogledov iz xls datoteke
 */
class SZ_Term_exceptions{
    public $exceptionsArray;

    public function __construct($sheet) {
        $sheetData = $sheet->toArray(null, true, true, true);

        $this->exceptionsArray = array();

        // parsaj izjeme
        $sheetLine = 0;
        while ($sheetData[$sheetLine]['A'] != 'Izjeme:')
            $sheetLine++;
        $sheetLine++;
        $this->parseExceptions($sheetData, $sheetLine);

        unset($sheetData);
    }

    function __destruct() {
        unset($exceptionsArray);
    }

    private function parseExceptions($sheetData, $dataLineStart) {
        // beremo izjeme za posamezen datum ki se lahko zgodijo v tem mesecu, premikamo se po posameznem sklopu
        // skol sestavlja vrstica z tagom "Datum:"
        // sledi vrstica z zapisanim datumom v naslednji vrstici
        // sledi vrstica z glavo (preskočimo)
        // sledijo zapisani termini (ura, progam, jezik)

        $localLineNumber = $dataLineStart;

        $deadSpaceExceptionCount = 0;

        $dateThis = null;

        while (true) {
            // če najdemo novi datum, si ga zabeležimo
            if ($sheetData[$localLineNumber]['A'] == 'Datum:') {
                $deadSpaceOptionCount = 0;

                // imamo novi datum, preberemo ga iz naslednje vrstice
                $localLineNumber = $localLineNumber + 1;
                $dateThis = $sheetData[$localLineNumber]['A'];

                // preskočimo header pri posamezni izjemi
                $localLineNumber = $localLineNumber + 2;

                // preverimo ali datum odstrani termine, v tem primeru ga zdaj dodamo sicer ga kasneje zavržemokot neveljavnega
                if ( (($sheetData[$localLineNumber]['A'] == null) || ($sheetData[$localLineNumber]['A'] == ''))
                    && (($sheetData[$localLineNumber]['B'] == null) || ($sheetData[$localLineNumber]['B'] == ''))
                    && (($sheetData[$localLineNumber]['C'] == null) || ($sheetData[$localLineNumber]['C'] == ''))) {

                    // če imamo samo datum brez časa, programa in jezika odstranimo termin
                    $this->exceptionsArray[] = array($dateThis, '', '', '');
                }
            }

            // preveri ali imamo vsa potrebna polja za veljavni termin, čene termin zavržemo
            if (($sheetData[$localLineNumber]['A'] != null) && ($sheetData[$localLineNumber]['A'] != '')
                && ($sheetData[$localLineNumber]['B'] != null) && ($sheetData[$localLineNumber]['B'] != '')
                && ($sheetData[$localLineNumber]['C'] != null) && ($sheetData[$localLineNumber]['C'] != '')) {

                // preverimo ali imamo pričakovan navaden text (11:00) ali excel-ov časovni format (11:00:00 AM)
                $termTime = $sheetData[$localLineNumber]['A'];
                $timeSplit = explode(':', $sheetData[$localLineNumber]['A']);
                if (sizeof($timeSplit) > 2) {
                    // imamo nepričakovan format

                    // preverimo ali imamo samo dolgi zapis ali moramo upoštevati tudi AM/PM zapis
                    if ((strpos($termTime, 'AM') !== false) || (strpos($termTime, 'PM') !== false)) {
                        $termTime = '';
                        if (strpos($termTime, 'PM') !== false) {
                            $termTime .= strval(intval($timeSplit[0]) + 12);
                        } else {
                            $termTime = $timeSplit[0];
                        }

                        $termTime .= ':' . $timeSplit[1];
                    } else {
                        $termTime = $timeSplit[0] . ':' . $timeSplit[1];
                    }
                }
                $this->exceptionsArray[] = array($dateThis, $termTime, $sheetData[$localLineNumber]['B'], $sheetData[$localLineNumber]['C']);
            } else {
                $deadSpaceExceptionCount++;
            }

            $localLineNumber++;

            // če se zgubimo in ne najdemo nobene nove izjeme več kot 10 vrstic, potem zaključimo
            if ($deadSpaceExceptionCount > 10)
                break;
        }
    }

    public function getExceptionsList() {
        return $this->exceptionsArray;
    }
}

/*
 * Class ki parsa in shrani informacije o posameznih terminih vodenih ogledov iz xls datoteke
 */
class SZ_Term{
    public $monthArray;
    public $weekArray;

    private $colNumToName = array(
        0 => 'A',
        1 => 'B',
        2 => 'C',
        3 => 'D',
        4 => 'E',
        5 => 'F',
        6 => 'G',
        7 => 'H',
        8 => 'I',
        9 => 'J',
        10 => 'K',
        11 => 'L',
        12 => 'M',
        13 => 'N',
        14 => 'O',
        15 => 'P',
        16 => 'Q',
        17 => 'R',
        18 => 'S',
        19 => 'T',
        20 => 'U'
    );

    private $monthCodeToNumber = array(
        'jan' => 1,
        'feb' => 2,
        'mar' => 3,
        'apr' => 4,
        'maj' => 5,
        'jun' => 6,
        'jul' => 7,
        'aug' => 8,
        'sep' => 9,
        'okt' => 10,
        'nov' => 11,
        'dec' => 12,
    );

    public function __construct($sheet) {
        $sheetData = $sheet->toArray(null, true, true, true);

        $this->weekArray = array();
        $this->monthArray = array();

        // parsaj mesec
        $sheetLine = 0;
        while ($sheetData[$sheetLine]['A'] != 'Meseci:')
            $sheetLine++;
        $sheetLine++;
        $this->parseMonths($sheetData[$sheetLine]);

        // parsaj termine po dnevih
        while ($sheetData[$sheetLine]['A'] != 'Dnevi:')
            $sheetLine++;
        $sheetLine++; // preskoci vrstico z "Dnevi:"
        $sheetLine++; // preskoci vrstico z "Pon: Tor: ..."
        $sheetLine++; // preskoci vrstico z "Termin:, Jezik, ...."
        $this->parseTerms($sheetData, $sheetLine);

        unset($sheetData);
    }

    function __destruct() {
        unset($monthArray);
        unset($weekArray);
    }

    private function parseMonths($rowOfData) {
        // vrstica, kjer je eno ime meseca v vsakem stolpcu

        foreach($rowOfData as $colName => $monthName) {
            if($monthName != null) {
                $this->monthArray[] = $monthName;
            }
        }
    }

    private function parseTerms($sheetData, $dataLineStart) {
        // beremo segment treh stolpcev v eni vrstici za en termin za en dan
        // za naslednji termin se premaknemo 1 vrstico nizje dokler ne pridemo do prazne vrstice
        // za naslednji dan se premaknemo za 3 stolpce desno (pon(A-C), tor(D-F), sre(G-I), čet(J-L), pet(M-O), sob(P-R), ned(S-U))

        // se prestavljamo po dnevih
        for ($dayCount = 0; $dayCount <= 18; $dayCount += 3) {
            $dayLineNumber = $dataLineStart;
            $dayArray = array();

            while ($sheetData[$dayLineNumber][$this->colNumToName[$dayCount]] != null) {

                // preveri ali imamo vsa potrebna polja, čene termin zavržemo
                if (($sheetData[$dayLineNumber][$this->colNumToName[$dayCount]] != null) && ($sheetData[$dayLineNumber][$this->colNumToName[$dayCount]] != '')
                && ($sheetData[$dayLineNumber][$this->colNumToName[$dayCount+1]] != null) && ($sheetData[$dayLineNumber][$this->colNumToName[$dayCount+1]] != '')
                &&($sheetData[$dayLineNumber][$this->colNumToName[$dayCount+2]] != null) && ($sheetData[$dayLineNumber][$this->colNumToName[$dayCount+2]] != '')) {

                    // preverimo ali imamo pričakovan navaden text (11:00) ali excel-ov časovni format (11:00:00 AM)
                    $termTime = $sheetData[$dayLineNumber][$this->colNumToName[$dayCount]];
                    $timeSplit = explode(':', $sheetData[$dayLineNumber][$this->colNumToName[$dayCount]]);
                    if (sizeof($timeSplit) > 2) {
                        // imamo nepričakovan format

                        // preverimo ali imamo samo dolgi zapis ali moramo upoštevati tudi AM/PM zapis
                        if ((strpos($termTime, 'AM') !== false) || (strpos($termTime, 'PM') !== false)) {
                            $termTime = '';
                            if (strpos($termTime, 'PM') !== false) {
                                $termTime .= strval(intval($timeSplit[0]) + 12);
                            } else {
                                $termTime = $timeSplit[0];
                            }

                            $termTime .= ':' . $timeSplit[1];
                        } else {
                            $termTime = $timeSplit[0] . ':' . $timeSplit[1];
                        }
                    }
                    $dayArray[] = array($termTime, $sheetData[$dayLineNumber][$this->colNumToName[$dayCount+1]], $sheetData[$dayLineNumber][$this->colNumToName[$dayCount+2]]);
                }

                $dayLineNumber++;
            }
            $this->weekArray[] = $dayArray;
        }
    }

    public function hasThisDate($formDate) {
        $formDateExploaded = explode('.', $formDate);   // dd.mm.yyyy

        $hasDateTemp = false;
        foreach($this->monthArray as $monthCode) {
            if (intval($this->monthCodeToNumber[$monthCode]) == intval($formDateExploaded[1])) {
                $hasDateTemp = true;
                break;
            }
        }

        return $hasDateTemp;
    }

    public function dateHasTerm($formDate, $exceptionList) {
        $formDateExploaded = explode('.', $formDate);   // dd.mm.yyyy

        // preveri za vsak slučaj če imamo informacijo o tem datumu
        if ( $this->hasThisDate($formDate) == false)
            return false;

        // preveri če obstaja termin za določen delovni dan in preveri ali slučajno obstaja termin v izjemi
        $hasDateWeekday = false;
        $hasDateException = false;

        // preveri delovni dan
        $dowNum = date('w', strtotime($formDateExploaded[2].'-'.$formDateExploaded[1].'-'.$formDateExploaded[0]));
        $dowNum -= 1;   // php zacne teden z nedeljo, mi z ponedeljkom
        if ($dowNum < 0)
            $dowNum = 6;

        if (count($this->weekArray[$dowNum]) > 0)
            $hasDateWeekday = true;

        // preveri izjeme
        foreach ($exceptionList as $termException) {
            if ($this->compareExceptionDate($termException[0], $formDate))
            {
                // preveri ali izjema odstrani termin
                if (($termException[1] == '') || ($termException[1] == null)) {
                    return false;   // če ni ure ni dogodka, pospravimo
                } else {
                    $hasDateException = true;
                }
            }
        }

        return ($hasDateWeekday || $hasDateException);
    }

    private function compareExceptionDate($exceptionDate, $formDate) {
        $formDateExploaded = explode('.', $formDate);   // dd.mm.yyyy
        $exceptionDateExploaded = explode('.', $exceptionDate);   // dd.mm.yyyy

        if ((intval($formDateExploaded[0]) == intval($exceptionDateExploaded[0])) &&
            (intval($formDateExploaded[1]) == intval($exceptionDateExploaded[1])) &&
            (intval($formDateExploaded[2]) == intval($exceptionDateExploaded[2]))) {
            return true;
        } else {
            return false;
        }
    }

    public function getDateTermList($formDate, $exceptionList) {
        $formDateExploaded = explode('.', $formDate);   // dd.mm.yyyy

        // preveri za vsak slučaj če ta datum nima nobenih terminov
        if ( $this->hasThisDate($formDate) == false)
            return array();

        // preveri ali upoštevamo izjemo
        $dateHasException = false;
        $dateExceptionList = array();
        foreach ($exceptionList as $termException) {
            if ($this->compareExceptionDate($termException[0], $formDate)) {
                $dateHasException = true;
                // preveri ali izjema odstrani termin
                if (($termException[1] != '') || ($termException[1] != null))
                    $dateExceptionList[] = $termException;
            }
        }
        if ($dateHasException) {
            if (count($dateExceptionList) > 0) {
                return $dateExceptionList;
            } else {
                return array();
            }
        }

        // vrni spisek terminov za ta dan
        $dowNum = date('w', strtotime($formDateExploaded[2].'-'.$formDateExploaded[1].'-'.$formDateExploaded[0]));
        $dowNum -= 1;   // php zacne teden z nedeljo, mi z ponedeljkom
        if ($dowNum < 0)
            $dowNum = 6;

        if (count($this->weekArray[$dowNum]) > 0)
            // dodajmo datum tedenskim terminom
            $termListWithDates = array();
            foreach ($this->weekArray[$dowNum] as $singleTerm) {
                $termListWithDates[] = array($formDate, $singleTerm[0], $singleTerm[1], $singleTerm[2]);
            }
            return $termListWithDates; //$this->weekArray[$dowNum];

        // nič nismo našli
        return array();
    }
}
?>