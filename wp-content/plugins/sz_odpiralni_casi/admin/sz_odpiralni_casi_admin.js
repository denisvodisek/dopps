/**
 * Created by urosoresic on 1/11/16.
 */

/*
 Poskrbi za vnos datumov v administraciji
 */

function sz_admin_createRow(date){
    var out = "<tr>";
    out += "<td><input type='text' name='sz_closedDates[]' class='sz_closedDates' value='"+date+"' /></td>";
    out += "<td><a class='removeDate'></a> </td>";
    out += "</tr>";
    return out;
}

function sz_admin_createRow_COOO(date){
    var out = "<tr>";
    out += "<td><input type='text' name='sz_closedDates_COOO[]' class='sz_closedDates' value='"+date+"' /></td>";
    out += "<td><a class='removeDate'></a> </td>";
    out += "</tr>";
    return out;
}

jQuery.datepicker.setDefaults( jQuery.datepicker.regional[ "sl" ] );
jQuery.datepicker.setDefaults( {
    dateFormat:"d.m.yy"
});

jQuery(document).ready(function(){
    //NRSZ
    var dataEncoded = jQuery("#sz_odpiralni_cas_admin_wrap #sz_closedDatesForm input[name='sz_datesFromDB']").attr("value");
    var datesFromDb = jQuery.parseJSON(dataEncoded);
    if(datesFromDb!=null)
    {
        for(var i=0; i<datesFromDb.length; i++){
            jQuery("#sz_closedDatesForm #sz_odpiralni_casi_closedDates_table tbody").append(sz_admin_createRow(datesFromDb[i]));
        }
        jQuery("#sz_odpiralni_cas_admin_wrap #sz_closedDatesForm .sz_closedDates").datepicker();
    }


    //COOO
    var dataEncoded_COOO = jQuery("#sz_odpiralni_cas_admin_wrap #sz_closedDatesForm input[name='sz_datesFromDB_COOO']").attr("value");
    var datesFromDb_COOO = jQuery.parseJSON(dataEncoded_COOO);
    if(datesFromDb_COOO != null)
    {
        for(var i=0; i<datesFromDb_COOO.length; i++){
            jQuery("#sz_closedDatesForm #sz_odpiralni_casi_closedDates_table_COOO tbody").append(sz_admin_createRow_COOO(datesFromDb_COOO[i]));
        }
        jQuery("#sz_odpiralni_cas_admin_wrap #sz_closedDatesForm .sz_closedDates").datepicker();
    }
});


jQuery("#sz_odpiralni_cas_admin_wrap #sz_closedDatesForm .removeDate").live("click", function(){
    jQuery(this).parents("tr:first").remove();
});


//GUMB ZA NRSZ
jQuery("#sz_odpiralni_cas_admin_wrap #sz_closedDatesForm #sz_closedDates_add").on("click",function(){
    //Gumb Dodaj je kliknjen. Prikaži date picker in nato dodaj vrstico v tabelo
    //jQuery(this).datepicker();

    jQuery("#sz_closedDatesForm #sz_odpiralni_casi_closedDates_table tbody").append(sz_admin_createRow(""));
    jQuery("#sz_odpiralni_cas_admin_wrap #sz_closedDatesForm .sz_closedDates").datepicker({
        dateFormat : 'd.m.yy'
    });
});

//GUMB ZA COOO
jQuery("#sz_odpiralni_cas_admin_wrap #sz_closedDatesForm #sz_closedDates_add_COOO").on("click",function(){
    //Gumb Dodaj je kliknjen. Prikaži date picker in nato dodaj vrstico v tabelo
    //jQuery(this).datepicker();

    jQuery("#sz_closedDatesForm #sz_odpiralni_casi_closedDates_table_COOO tbody").append(sz_admin_createRow_COOO(""));
    jQuery("#sz_odpiralni_cas_admin_wrap #sz_closedDatesForm .sz_closedDates").datepicker({
        dateFormat : 'd.m.yy'
    });
});
