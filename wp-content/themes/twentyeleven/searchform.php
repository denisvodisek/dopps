<?php
/**
 * The template for displaying search forms in Twenty Eleven
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
?>
	<form method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">

        <input value="" name="s" id="s" placeholder="<?php _e('Search the site ...', 'twentyeleven'); ?>" />
        <button type="button" name="search_btn" id="searchsubmit" onclick="check_search()" ><?php _e('Search', 'twentyeleven'); ?></button>
	</form>
