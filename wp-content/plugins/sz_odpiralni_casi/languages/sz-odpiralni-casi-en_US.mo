��    J      l  e   �      P  d   Q  \   �       .         K     l     y  g   �     �  "   �  -        E     T  0   [     �  $   �     �     �     �     �     	     	     	     !	     '	     .	     5	     =	     F	     O	     R	     _	  	   g	     q	     t	     {	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
  	   
  
   
  
   
  	   (
  	   2
     <
     Q
     X
     _
     f
     l
     r
     x
     ~
     �
     �
     �
     �
     �
  ^  �
  �     Z   �  	     .        E     e     r  x        �      �          ;     I  )   N     x     �     �     �     �     �     �     �     �     �          	                     )     /     A     J     S     W     _     g     w     |     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �  	   �     �     �  	   �  	             %     .     7  	   @  	   J  	   T     ^     g     p     v     |     �     �        .   4   *                    =   8   ?       3   :   	   2       !   9   A              ,   $      5      +             F   <       7          @          G         J      E           6                   "   -              (   D   /   0   '          &              #                     B   H   )   C                         %      >                  1      I   ;   
    (po programu NRŠZ, za predhodno najavljene skupine in ob terminih po programu najavljenih dogodkov) (sprehodi, fotografiranje, doživljanje narave in podobne tihe oblike individualnega obiska) CO in OO Center za obiskovalce in osrednja opazovalnica Center za obiskovalce je zaprt v Danes zaprto Delovni čas Izjemoma so po predhodni najavi naravni rezervat ali objekti lahko zaprti tudi med državnimi prazniki. NRŠZ Naravni rezervat Škocjanski zatok Naravni rezervat škocjanski zatok je zaprt v Odpiralni čas Odprto Osrednja opazovalnica je odprta vse dni v tednu. Povezava do strani:  Prosim kliknite za več podrobnosti. Trenutno Ure za rekreacijo Ure za tihe oblike obiska Ure za vodene obiske V Zaprto Zaprto: april aprilu avgust avgustu december decembru do do jutri ob  februar februarju in januar januarju je danes zaprt julij juliju junij juniju maj maju marcu marec nedelja nedelje nedeljo november novembru oktober oktobru petek petka pon - pet ponedeljek ponedeljka september septembru sob - ned / prazniki sobota sobote soboto sreda srede sredo torek torka vsak vsaka Čez leto ... četrtek četrtka Project-Id-Version: WPML_EXPORT_sz-odpiralni-casi
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/sz_odpiralni_casi
POT-Creation-Date: 2016-04-15 17:00+0200
PO-Revision-Date: 
Last-Translator: 
Language-Team: 
Language: en
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.7
 (according to the Škocjanski zatok Nature Reserve visitation programme, for previously announced groups and during the planned events according to the programme) (walking, photographing, experience the nature and other silent forms of individual visit) CO and OO The Visitor centre and the central observatory The visitor center is closed on Closed today Opening time Upon previous announcement the nature reserve, visitor centre and central observatory are closed during public holidays. NRŠZ Škocjanski zatok Nature Reserve Nature Reserve is closed on Opening hours Open The central observatory is open all days. Link to page: Click for more information. At the moment Recreation hours Quiet visitation hours Guided tours hours In Closed Closed: April April August August December December until until tomorrow at February February and January January is closed today July July June June May May March March Sunday Sunday Sunday November November October October Friday Friday mon - fri Monday Monday September September sat - sun / Holidays Saturday Saturday Saturday Wednesday Wednesday Wednesday Tueseday Tueseday every every Other time ... Thursday Thursday 