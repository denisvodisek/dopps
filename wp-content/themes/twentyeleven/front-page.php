<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

get_header(); ?>

<div id="frontpage">
	<div class="slider_fp" style="margin-bottom: 15px;">
			<div class="content">
				<?php
				if(function_exists(putRevSlider))
					putRevSlider("frontpage_slider","homepage")
				?>

		</div> <!-- .content -->
	</div> <!-- .slider -->
	<table id="frontpage_table" border="0" cellspacing="0" cellpadding="0" >
		<tr>
			<td>
				<div id="frontpage_calendar_holder">
					<?php
					if(ICL_LANGUAGE_CODE == "sl"){
						if(dynamic_sidebar('fp_calendar_widget_area_sl')) : else : endif;
					}elseif(ICL_LANGUAGE_CODE == "it"){
						if(dynamic_sidebar('fp_calendar_widget_area_it')) : else : endif;
					}else{
						if(dynamic_sidebar('fp_calendar_widget_area_en')) : else : endif;
					}
					?>
				</div>
			</td>
			<td><div id="frontpage_time_holder" >
					<?php dynamic_sidebar('fp_sz_odpiralni_cas_widget_area'); ?>
					<?php
					$registrationFormLink = '';
					// preverimo ali class obstaja in s tem ali imamo plugin aktiviran
					if  (class_exists(Skocjan_vodeni_ogled)) {
						$tourPostBaseID = intval(get_option(Skocjan_vodeni_ogled::$option_name_registrationForm)); // ID strani z ogledi
						if (function_exists('icl_object_id')) {
							// deprecated but works
							$tourPostID = icl_object_id($tourPostBaseID, 'page', false);
						} else {
							// should work according to documentation when icl_object_id is removed
							$tourPostID = apply_filters('wpml_object_id', $tourPostBaseID, 'post');
						}
						$registrationFormLink = get_permalink($tourPostID);
					}
					?>
					<a href="<?php echo $registrationFormLink ?>" class="border_orange" id="vodeni_ogledi">
						<p class="h3_title"><?php _e("Guided tours", 'twentyeleven'); ?></p>
					</a>
					<div class="border_orange" id="napovednik_okvir">
						<div id="napovednik_kontrola">
							<table>
								<tr>
									<td class="napis"><?php _e('DOGODKI', 'twentyeleven'); ?></td>
									<td class="gumb">
										<button class="napovednik_kontrola_zapri" style="cursor: pointer"><img src=" <?php echo get_template_directory_uri() . '/images/simple-calendar/calendar_event_close.png' ; ?>" /></button>
									</td>
								</tr>
							</table>
						</div>
						<div id="napovednik_dogodki"></div>
					</div>

				</div></td>
			<td><div id="frontpage_news_holder" >
					<p class="news_title"><?php _e('News', 'twentyeleven'); ?></p>
					<div class="content">
						<?php
						if(ICL_LANGUAGE_CODE == "sl"){
							$my_query = new WP_Query('category_name=novice&posts_per_page=3');
						}elseif(ICL_LANGUAGE_CODE == "it"){
							$my_query = new WP_Query('category_name=notizie&posts_per_page=3');
						}else{
							$my_query = new WP_Query('category_name=news&posts_per_page=3');
						}
						while ($my_query->have_posts()) : $my_query->the_post();
							?>
							<!-- Do stuff... -->
							<div class="content_page">
								<h2><a href="<?php echo get_permalink($post->ID); ?>"><?php echo $post->post_title; ?></a></h2>
								<p class="meta"><?php
									$date = strtotime(substr($post->post_date, 0, strpos($post->post_date, " ")));
									echo date("d.m.Y", $date);
									?>

									<?php edit_post_link( __( 'Edit', 'twentytwelve' ), '<span class="edit-link">', '</span>' ); ?>
								</p>
							</div> <!-- .content_page -->

						<?php endwhile; ?>
					</div>  <!-- .content -->

				</div></td>
		</tr>
	</table>


            </div><!-- #frontpage -->

<?php get_footer(); ?>