��    I      d  a   �      0  d   1  \   �     �  .   �      +     L     Y  g   f     �  "   �  -   �     %     4  0   ;     l     �     �     �     �     �     �     �     �     �     �     �     �     	     
	     	     	  	   "	     ,	     /	     6	     ?	     N	     T	     [	     a	     h	     l	     q	     w	     }	     �	     �	     �	     �	     �	     �	     �	     �	  	   �	  
   �	  
   �	  	   �	  	   �	     �	     
     
     
     !
     '
     -
     3
     9
     ?
     D
     J
     X
     a
  ^  j
  �   �  `   d     �  .   �     �             t   1     �  "   �     �     �     �  >        C     ]     m     �     �     �     �     �     �     �     �     �     �     �  	   �               !     *     ,     4     <     K     R     Y     `     g     n     u     {     �     �     �     �     �     �     �     �     �  	   �     �     �  	   �  	   �     �               !  
   (  
   3  	   >     H     Q     Z     _     d     z     �         3              ,          -   B      "   .          /                   ?      =   5       >             I              9       7      &   $   +      '       !   #   )       8         E                  6   <   A   2                :      D   G      ;             1                  %   F       H   C   4   0   
          (          *   @   	           (po programu NRŠZ, za predhodno najavljene skupine in ob terminih po programu najavljenih dogodkov) (sprehodi, fotografiranje, doživljanje narave in podobne tihe oblike individualnega obiska) CO in OO Center za obiskovalce in osrednja opazovalnica Center za obiskovalce je zaprt v Danes zaprto Delovni čas Izjemoma so po predhodni najavi naravni rezervat ali objekti lahko zaprti tudi med državnimi prazniki. NRŠZ Naravni rezervat Škocjanski zatok Naravni rezervat škocjanski zatok je zaprt v Odpiralni čas Odprto Osrednja opazovalnica je odprta vse dni v tednu. Povezava do strani:  Trenutno Ure za rekreacijo Ure za tihe oblike obiska Ure za vodene obiske V Zaprto Zaprto: april aprilu avgust avgustu december decembru do do jutri ob  februar februarju in januar januarju je danes zaprt julij juliju junij juniju maj maju marcu marec nedelja nedelje nedeljo november novembru oktober oktobru petek petka pon - pet ponedeljek ponedeljka september septembru sob - ned / prazniki sobota sobote soboto sreda srede sredo torek torka vsak vsaka Čez leto ... četrtek četrtka Project-Id-Version: WPML_EXPORT_sz-odpiralni-casi
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/sz_odpiralni_casi
POT-Creation-Date: 2016-04-15 17:20+0200
PO-Revision-Date: 
Last-Translator: 
Language-Team: 
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.7
 (secondo il programma della Riserva naturale di Val Stagnon, per i gruppi precedentemente annunciati e durante degli eventi previsti secondo il programma) (passeggiare, fotografare, vivere la natura e altre tipologie di visite individuali in silenzio) CO e OO Il Centro visite e l&#39;osservatorio centrale Centro visite chiuso il Oggi chiuso Orario di lavoro Previo avviso la riserva naturale, il centro visite e l'osservatorio centrale sono chiusi durante i giorni festivi.  NRŠZ La Riserva naturale di Val Stagnon Riserva naturale chiuso il Orario di apertura Aperta L'osservatorio centrale aperto tutti i gironi della settimana. Collegamento alla pagina: Momentaneamente Orario di ricreazione Ore per le visite in silenzio Ore per le visite guidate In Chiusi Chiuso: aprile aprile agosto agosto dicembre dicembre fino alle fino a domani alle febbraio febbraio e gennaio gennaio oggi è chiuso luglio luglio giugno giugno maggio maggio marzo marzo domenica domenica domenica novembre novembre ottobre ottobre venerdì venerdì lun - ven lunedi lunedi settembre settembre sab - dom / festività sabato sabato sabato mercoledì mercoledì mercoledi martedì martedì ogni ogni Durante l&#39;anno… giovedì giovedì 