<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<link rel="stylesheet" type="text/css" media="all" href="/wp-content/themes/twentyeleven/style_ie7.css" />
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<link rel="stylesheet" type="text/css" media="all" href="/wp-content/themes/twentyeleven/style_ie7.css" />
<![endif]-->
<!--[if IE 8 | IE 9]>
<html id="ie8" <?php language_attributes(); ?>>
<link rel="stylesheet" type="text/css" media="all" href="/wp-content/themes/twentyeleven/style_ie8.css" />
<![endif]-->
<!--[if IE 9]>
<html id="ie8" <?php language_attributes(); ?>>
<link rel="stylesheet" type="text/css" media="all" href="/wp-content/themes/twentyeleven/style_ie9.css" />
<![endif]-->
<!--[if !(IE 6) & !(IE 7) & !(IE 8) & !(IE 9) ]><!-->
<html <?php language_attributes(); ?>>
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<!--<![endif]-->
<head>
<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html" charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />


<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyeleven' ), max( $paged, $page ) );

	?></title>

<!--Open graph API
<meta property="og:title" content="<?php echo get_bloginfo('name'); ?>" />
<meta property="og:url" content="<?php echo get_bloginfo('url'); ?>" />
<meta property="og:image" content="<?php echo get_bloginfo('url'); ?>/wp-content/themes/twentyeleven/images/skocjanski_zatok_logo_opaque.png" />
<meta property="og:image" content="http://ptice.si/drustvo/dopps_logo.jpg" />
 END Open graph API -->


<link rel="profile" href="http://gmpg.org/xfn/11" />

<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->
<?php
	/* We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.js"></script>
<script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/js/jquery.easing.1.3.js"></script>
<script type="text/javascript">
var templateUrl = '<?php get_site_url(); ?>';

//Make sure we actually search for something
function check_search(){
	if($("#s").val()){
		$("#searchform").submit();
	}else{
		return false;
	}
}
</script>



</head>

<body <?php body_class(); ?>>

<div id="site_holder">

<div id="center_header">
	<!--<div id="dopps_logo_holder"></div>-->
      <div id="header_link_holder" align="right">
<?php
	if(function_exists("icl_get_languages")) {
		?>
		<div id="language_sel" class="margin_lr_30">
			<?php
			$langs = icl_get_languages('skip_missing=N&orderby=KEY&order=DIR&link_empty_to=str');

			?>
			<a href="<?php echo $langs['sl']['url']; ?>" <?php echo $langs['sl']['active'] ? 'class="active"' : ''; ?>>SLO</a>
			|
			<a href="<?php echo $langs['it']['url']; ?>" <?php echo $langs['it']['active'] ? 'class="active"' : ''; ?>>ITA</a>
			|
			<a href="<?php echo $langs['en']['url']; ?>" <?php echo $langs['en']['active'] ? 'class="active"' : ''; ?>>ENG</a>
		</div>
		<?php
	}
?>
           <div id="header_links" class="margin_lr_30 remove_ul_styling">
            	<?php wp_nav_menu( array( 'menu' => 'topmenu') ); ?>
           </div>
  </div><!-- #header_link_holder -->
  <div id="header" align="right">
	  <div id="header_bkg_2"></div>

	  <div id="site_logo_holder"  class="margin_lr_30"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
			  <img src="<?php bloginfo( 'template_url' ); ?>/bkg/skocjanski_zatok_logo_enotni.svg" width="293" height="126" alt="Logo Škocjanski zatok" title="<?php bloginfo( 'description' ); ?>" /></a>
	  </div> <!-- #site_logo_holder -->
   		<div id="header_menu"  class="margin_lr_30 remove_ul_styling">

        	<div class="level_1">
        	<?php wp_nav_menu( array( 'menu' => "mainmenu1", 'after' => '&nbsp;&nbsp;|&nbsp;', 'depth' => 1) ); ?></div>
            <div class="level_2">
            <?php wp_nav_menu( array( 'menu' => 'mainmenu2','after' => '&nbsp;&nbsp;|&nbsp;', 'depth' => 1) ); ?>
           </div>
        </div><!-- #header_menu -->
   	 	<div id="header_search"  class="margin_lr_30">
            <?php get_search_form(); ?>
        </div><!-- #header_search -->

  </div><!-- #header -->

  </div><!-- #center_header-->

  <div class="skip-link"><a class="assistive-text" href="#content" title="<?php esc_attr_e( 'Skip to primary content', 'twentyeleven' ); ?>"><?php _e( 'Skip to primary content', 'twentyeleven' ); ?></a></div>





  <table  id="main_bkg" border="0" cellpadding="0" cellspacing="0" width="100%">
  <col  /><col width="16"  /><col width="1009"  /><col width="16" /><col  />
  <tr>
  <td>&nbsp;

  </td>
  <td id="main_shadow_l">
  </td><!-- #main_shadow_r -->
  <td>
      <div id="main">
      	<div id="main_inner_margin">

        <!-- HEADER ENDS HERE -->

