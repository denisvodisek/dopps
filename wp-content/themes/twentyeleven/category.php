<?php
/**
 * The template for displaying Category Archive pages.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

get_header(); ?>

<?php
	// tu se izpis kategorije zlorablja za prikaz novic, z novo logiko kjer pri izpisu kategorije novic izrišemo samo
	// zadnji post, se je ločil celi del v prikaz kategorije novic in prikaz generične kategorije za vse ostale
	// kot je narejeno v single.php namesto vrivanja posameznih izsekov

	// če prikazujemo novice
	if(get_the_category()[0]->cat_ID = get_cat_ID('novice')) {
		?>
		<div id="col_1_sidebar_2">
			<div id="sidebar_left">

				<div class="menu">
					<?php
					$cat_title = single_cat_title('', false);
					$cat_id = get_cat_ID($cat_title);
					//simarine_page_hierarchy_menu(get_the_ID());
					//	get_sidebar('left');
					//simarine_archives_menu('',$cat_id);


					// poiščemo leto in mesec iz url-ja da pravilno prikažemo aktivni mesec v levem meniju
					if (strpos($_SERVER["REQUEST_URI"], '/?cat') !== false) {
						// preverimo ali imamo leto in mesec ali samo mesec v url
						$activeDate = '01';
						$activeMonth = '';
						$activeYear = '';
						$tmpStr = substr($_SERVER["REQUEST_URI"], strpos($_SERVER["REQUEST_URI"], '/?cat') - 3, 3);
						if ($tmpStr[0] == '/') {
							// imamo mesec in leto
							// dodamo mesec
							$activeDate .= '.' . substr($_SERVER["REQUEST_URI"], strpos($_SERVER["REQUEST_URI"], '/?cat') - 2, 2);
							$activeMonth = substr($_SERVER["REQUEST_URI"], strpos($_SERVER["REQUEST_URI"], '/?cat') - 2, 2);
							// dodamo leto
							$activeDate .= '.' . substr($_SERVER["REQUEST_URI"], strpos($_SERVER["REQUEST_URI"], '/?cat') - 7, 4);
							$activeYear = substr($_SERVER["REQUEST_URI"], strpos($_SERVER["REQUEST_URI"], '/?cat') - 7, 4);
						} else {
							// imamo samo leto
							// izberemo januar - začetek leta
							$activeDate .= '.01';
							$activeMonth = '01';
							// dodamo leto
							$activeDate .= '.' . substr($_SERVER["REQUEST_URI"], strpos($_SERVER["REQUEST_URI"], '/?cat') - 4, 4);
							$activeYear = substr($_SERVER["REQUEST_URI"], strpos($_SERVER["REQUEST_URI"], '/?cat') - 4, 4);
						}


					} else {
						// če nimamo datuma v url (klick cele kategorije novic), pridobimo današnji datum in ga podamo
						// levemu in desnemu stolpcu z parametrom da označita zadnjo pred tem datumom.
						// enako prikažemo samo zadnjo novico kot glavno vsebino

						$activeDate = date('d.m.Y');
						$activeMonth = date('m');
						$activeYear = date('Y');
					}

					sz_simarine_archives_menu($activeDate, $cat_id, true);
					?>
				</div><!-- .menu -->

			</div><!-- #sidebar_left -->
			<div id="sidebar_right">

				<div class="menu">
                <span style="font-size:0.9em;">
                <?php

				//simarine_posts_in_month('','', $cat_id);
				sz_simarine_posts_in_month($activeYear, $activeMonth, $cat_id, '', true);
				?>
                </span>
				</div><!-- .menu -->

			</div><!-- #sidebar_right -->

			<div id="main_left_col">
                    <span id="nav_pointer">
                    	<a href="<?php echo get_site_url(); ?>"><?php _e("Home", "twentyeleven"); ?></a> >
                    	<a class="active" href="<?php echo get_category_link($cat_id); ?>"><?php echo $cat_title; ?></a>

                    </span>

				<div class="clear"></div>
				<h1 class="page-title"><?php
					printf(__('%s', 'twentyeleven'), '<span>' . single_cat_title('', false) . '</span>');
					?></h1>
				<?php if (have_posts()) : ?>

					<?php twentyeleven_content_nav('nav-above'); ?>

					<?php /* Start the Loop */ ?>

					<?php
					// ker smo smo v kategoriji novic, prikaži samo zadnji post
					query_posts('showposts=1&cat=' . get_the_category()[0]->cat_ID);
					?>

					<?php while (have_posts()) : the_post(); ?>
						<?php
						// izriše še navigacijsko pasico od single posta ker v kategoriji prikazujemo zadnjo novico kot single post
						?>
						<nav id="nav-single">
							<h3 class="assistive-text"><?php _e('Post navigation', 'twentyeleven'); ?></h3>
							<span class="nav-previous"><?php previous_post_link('%link', __('<span class="meta-nav">&larr;</span> Previous', 'twentyeleven'), true); ?></span>
							<span class="nav-next"><?php next_post_link('%link', __('Next <span class="meta-nav">&rarr;</span>', 'twentyeleven'), true); ?></span>
						</nav><!-- #nav-single -->
						<?php
						/* Include the Post-Format-specific template for the content.
						 * If you want to overload this in a child theme then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						global $more; $more = 1;
						get_template_part('content', get_post_format());
						?>

					<?php endwhile; ?>

				<?php else : ?>

					<article id="post-0" class="post no-results not-found">
						<header class="entry-header">
							<h1 class="entry-title"><?php _e('Nothing Found', 'twentyeleven'); ?></h1>
						</header><!-- .entry-header -->

						<div class="entry-content">
							<p><?php _e('Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'twentyeleven'); ?></p>
							<?php get_search_form(); ?>
						</div><!-- .entry-content -->
					</article><!-- #post-0 -->

				<?php endif; ?>


			</div><!-- #main_left_col -->


		</div><!-- #col_1_sidebar_2 -->

		<?php get_footer(); ?>

<?php
	} else {

	// prikažemo generični izpis kategorije
?>

		<section id="primary">
			<div id="content" role="main">

				<?php if ( have_posts() ) : ?>

					<header class="page-header">
						<h1 class="page-title"><?php
							printf( __( 'Category Archives: %s', 'twentyeleven' ), '<span>' . single_cat_title( '', false ) . '</span>' );
							?></h1>

						<?php
						$category_description = category_description();
						if ( ! empty( $category_description ) )
							echo apply_filters( 'category_archive_meta', '<div class="category-archive-meta">' . $category_description . '</div>' );
						?>
					</header>

					<?php twentyeleven_content_nav( 'nav-above' ); ?>

					<?php /* Start the Loop */ ?>
					<?php while ( have_posts() ) : the_post(); ?>

						<?php
						/* Include the Post-Format-specific template for the content.
						 * If you want to overload this in a child theme then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						get_template_part( 'content', get_post_format() );
						?>

					<?php endwhile; ?>

					<?php twentyeleven_content_nav( 'nav-below' ); ?>

				<?php else : ?>

					<article id="post-0" class="post no-results not-found">
						<header class="entry-header">
							<h1 class="entry-title"><?php _e( 'Nothing Found', 'twentyeleven' ); ?></h1>
						</header><!-- .entry-header -->

						<div class="entry-content">
							<p><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'twentyeleven' ); ?></p>
							<?php get_search_form(); ?>
						</div><!-- .entry-content -->
					</article><!-- #post-0 -->

				<?php endif; ?>

			</div><!-- #content -->
		</section><!-- #primary -->

		<?php get_sidebar(); ?>
		<?php get_footer(); ?>

<?php
	}
?>