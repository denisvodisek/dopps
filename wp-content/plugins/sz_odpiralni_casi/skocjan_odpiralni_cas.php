<?php
/*
 * Plugin name:  Skocjanski zatok - odpiralni casi
 * Description:  Modul za prikaz odpiralnega časa. Modul se prikaže z uporabo shortkod: [sz_odpiralni_cas [interaktivno]], [sz_trenutni_odpiralni_cas], [NRSZ_odpiralni_cas_status] in [COOO_odpiralni_cas_status]
 * Version:      1.2
 * Plugin URI:   http://www.u-centrix.com/
 * Author:       Uroš Orešič
 * Author URI:   http://www.u-centrix.com/
 * Text Domain:  sz-odpiralni-casi
 */
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

require_once "admin/skocjan_odpiralni_cas_admin.php";
require_once "skocjan_odpiralni_cas_widget.php";
/** Include path **/
set_include_path(get_include_path() . PATH_SEPARATOR . 'PHPExcel_1.8.0_doc/Classes');

/** PHPExcel_IOFactory */
include 'PHPExcel_1.8.0_doc/Classes/PHPExcel/IOFactory.php';


/**
 * Load plugin textdomain.
 *
 * @since 1.1
 */
function sz_odpiralni_casi_load_textdomain() {
    load_plugin_textdomain( 'sz-odpiralni-casi', false, plugin_basename( dirname( __FILE__ ) ) . '/languages');
    load_plugin_textdomain( 'sz-odpiralni-casi-admin', false, plugin_basename( dirname( __FILE__ ) ) . '/languages');
}
add_action( 'plugins_loaded', 'sz_odpiralni_casi_load_textdomain' );


function sz_odpiralni_casi_scripts() {
    wp_register_style( 'sz_odpiralni_casi_style', plugins_url( 'sz_odpiralni_casi/sz_style.css' ) );
    wp_enqueue_style( 'sz_odpiralni_casi_style');
    wp_register_script( 'sz_odpiralni_casi_current', plugins_url( 'sz_odpiralni_casi/sz_current_calendar.js' ), array(), '1.0.0', true );
}
add_action( 'wp_enqueue_scripts', 'sz_odpiralni_casi_scripts' );

function sz_trenutni_odpiralni_cas_html(){
    $urnik = Skocjan_odpiralni_cas::getInstance();
    return $urnik->getCurrentMonthHtml().$urnik->getLegend();
}
add_shortcode( 'sz_trenutni_odpiralni_cas', 'sz_trenutni_odpiralni_cas_html' );

function sz_odpiralni_cas_summary_rezervat(){
    $activePluginList = get_option( 'active_plugins' );
    if (in_array('sz_odpiralni_casi/skocjan_odpiralni_cas.php', $activePluginList)) {
        $schedule = Skocjan_odpiralni_cas::getInstance();
        return $schedule->getStatusHTML("nrsz");
    }
}
add_shortcode( 'NRSZ_odpiralni_cas_status', 'sz_odpiralni_cas_summary_rezervat' );

function sz_odpiralni_cas_summary_opazovalnica(){
    $activePluginList = get_option( 'active_plugins' );
    if (in_array('sz_odpiralni_casi/skocjan_odpiralni_cas.php', $activePluginList)) {
        $schedule = Skocjan_odpiralni_cas::getInstance();
        return $schedule->getStatusHTML("cooo");
    }
}
add_shortcode( 'COOO_odpiralni_cas_status', 'sz_odpiralni_cas_summary_opazovalnica' );


function sz_odpiralni_cas_html( $atts ){
    global $sitepress;
    $urnik = Skocjan_odpiralni_cas::getInstance();

    $a = shortcode_atts( array(
        'interaktivno' => 'false'
    ), $atts );

    if(!$urnik->isReady()){
        return "<a href='".Skocjan_odpiralni_cas::$linkToAdminPanel."'>".__("Uvoziti morate podatke. Prijavite se v administracijo: Nastavitve > Odpiralni čas.", "sz-odpiralni-casi")."</a>";
    }


    if($a['interaktivno'] != "false" || in_array("interaktivno", $atts)) {
        $outHtml = $urnik->getCurrentMonthHtml();
        $outHtml .= "<a href='#' id='sz_odpiralni_casi_showMore'> <img  src='". plugins_url() ."/sz_odpiralni_casi/img/Puscica.svg'><p>" .__("Čez leto ...", "sz-odpiralni-casi")."</p></a>";
        $outHtml .= "<div id='sz_odpiralni_casi_showMore_target'><img src='" . plugins_url() . "/sz_odpiralni_casi/img/UrnikWEB.jpg'></div>" . $urnik->getLegend();

        return $outHtml;
    }
    return $urnik->getHtml() . $urnik->getLegend();
}
add_shortcode( 'sz_odpiralni_cas', 'sz_odpiralni_cas_html' );


/*
 * Class za prikaz odpiralnega časa.
 * Za uporabo kliči smo
 *   $urnik = Skocjan_odpiralni_cas::getInstance();
 *
*/
class Skocjan_odpiralni_cas{
    public static $option_name = "sz_odpiralni_cas_data";
    public static $option_name_file = "sz_odpiralni_cas_xlsFile"; //podatki o XLS datoteki
    public static $option_name_closedDates = "sz_odpiralni_cas_closedByUser"; //ročno vnešeni datumi (preko administracije), ko je rezervat zaprt.
    public static $option_name_closedDates_COOO = "sz_odpiralni_cas_closedByUser_for_COOO"; //ročno vnešeni datumi (preko administracije), ko je opazovalnica zaprta.
    public static $option_name_placeIsClosed = "sz_odpiralni_cas_placeIsClosed"; //rezervat je zaprt.
    public static $option_name_placeIsClosed_COOO = "sz_odpiralni_cas_placeIsClosed_COOO"; //COOO je zaprt.
    public static $option_name_closedDays = "sz_odpiralni_cas_closedDaysByUser"; //ročno vnešeni dnevi v tednu (preko administracije), ko je rezervat zaprt.
    public static $option_name_closedDays_COOO = "sz_odpiralni_cas_closedDaysByUser_for_COOO"; //ročno vnešeni dnevi v tednu (preko administracije), ko je opazovalnica zaprta.
    public static $option_name_useWeekScheduleForWeekends ="sz_cel_teden_enako"; //Enak odpiralni čas cel teden

    public static $linkToAdminPanel;
    public static $slovenianHolidays = array(
        "1.1.",
        "8.2.",
        "27.4.",
        "1.5.",
        "2.5.",
        "8.6.",
        "25.6.",
        "15.8.",
        "17.8.",
        "15.9.",
        "25.10.",
        "31.10.",
        "1.11.",
        "23.11.",
        "25.12.",
        "26.12."
    );
    public static $easterMondays = array(
        "28.3.2016",
        "17.4.2017",
        "2.4.2018",
        "22.4.2019",
        "13.4.2020",
        "5.4.2021",
        "18.4.2022",
        "9.4.2023",
        "1.4.2024",
        "21.4.2025",
        "06.4.2026",
        "29.3.2027",
        "17.4.2028",
        "2.4.2029",
        "22.4.2030",
        "14.4.2031",
        "29.3.2032",
        "18.4.2033",
        "10.4.2034",
        "26.4.2035" /* You never know how long this website will be active */
    );
    private static $instance = null;

    private $schedulesByMonths; //Vsi podatki (iz XLSa) o odpiralnem času

    private $isReady; //Je true, ko vsaj enkrat uploadamo XLS.
    function __construct() {
        $this->isReady= false;
    }
    function __destruct() {
        unset($schedulesByMonths);
        unset($html);
    }

    /*
     * Vrne datume, ko je Rezervat zaprt
     * TODO: preveri delovanje!
     */
    public function getClosedDates($startMonth, $startYear, $endMonth, $endYear,$facility){
        $startTime = strtotime($startYear."-".$startMonth."-01");
        $endTime = strtotime($endYear."-".$endMonth." last day - 1 hour");

        $closedDays = [];
        while($startTime < $endTime){
            if(Skocjan_odpiralni_cas::areWeClosed($startTime,$facility)){
                $closedDays[] = date("d.m.Y", $startTime);
            }
            $startTime = $this->getTimeAfterMidnight($startTime);
        }
        return $closedDays;
    }

    /*
     * Funkcija preveri, če je rezervat odprt na določen čas. Ignorira MasterSwitch.
     * Za celostno sliko odprtja rezervata uporabi najprej areWeClosedForEver() potem pa areWeClosed(time)
       $facility uporabi string COOO ali NRSZ
    */
    public static function areWeClosed($time,$facility){
        $facility=strtolower($facility);

        $currentDay = Date('j', $time); //numeric 1 - 31
        $currentDayInWeekIndex = Date('w', $time); //0-6  //0 => nedelja!
        $currentYear = Date('Y', $time); //2010-2059
        $currentMonth = Date("n", $time); //1-12

        /* Zaenkrat se ne upošteva. Nekatere praznike bo ziher odprto... Vprašaj šefico.
        //Dela prost dan v Sloveniji?
        if(in_array($currentDay.".".$currentMonth.".", Skocjan_odpiralni_cas::$slovenianHolidays))
            return true;
        //Velikonočni ponedeljek?
        if(in_array($currentDay.".".$currentMonth.".".$currentYear, Skocjan_odpiralni_cas::$easterMondays))
            return true;
        */

        //Eksplicitno definirani dnevi v tednu
        if ($facility=="cooo")
            $userDefinedClosedDatesString = get_option(Skocjan_odpiralni_cas::$option_name_closedDays_COOO);
        else
            $userDefinedClosedDatesString = get_option(Skocjan_odpiralni_cas::$option_name_closedDays);

        if (strpos($userDefinedClosedDatesString, strtolower(Date("D", $time))) !== false){
            return true;
        }

        //Eksplicitno definirani datumi zaprtja?
        if ($facility=="cooo")
            $userDefinedClosedDates = unserialize(get_option(Skocjan_odpiralni_cas::$option_name_closedDates_COOO));
        else
            $userDefinedClosedDates = unserialize(get_option(Skocjan_odpiralni_cas::$option_name_closedDates));

        if(isset($userDefinedClosedDates) && is_array($userDefinedClosedDates)){
            if(in_array($currentDay.".".$currentMonth.".".$currentYear, $userDefinedClosedDates))
                return true;
        }

        //Poglej v koledarje, če je na današnji dan ni definiranih ur
        $inst = Skocjan_odpiralni_cas::getInstance();
        $calIndex = $inst->getScheduleIndex($currentMonth);
        if($calIndex < 0) {
            error_log("SZ_odpiralni_casi: Schedule not present for month: ".Date("F", $time).'. In function isNotWorkingDay($time).');
            return false; //ni najdenega koledarja, die quietly.
        }
        //Vrne true, če za ta dan ni zapisa v urniku.
        $schedule = $inst->schedulesByMonths[$calIndex];

        $useWeekScheduleForWeekends= get_option(Skocjan_odpiralni_cas::$option_name_useWeekScheduleForWeekends);

        if($useWeekScheduleForWeekends=="" && ($currentDayInWeekIndex == 0 || $currentDayInWeekIndex == 6)){
            $scheduleDefined = false;
            foreach($schedule->hours as $index => $hour){
                if($schedule->schedule_CO_OO_weekends[$index] != "closed"
                    || $schedule->schedule_NRSZ_weekends[$index] != "closed"){
                    $scheduleDefined = true;
                    continue;
                }
            }
            if($scheduleDefined == false)
                return true;
        }else{
            $scheduleDefined = false;
            foreach($schedule->hours as $index => $hour){
                if($schedule->schedule_CO_OO_weekdays[$index] != "closed"
                    || $schedule->schedule_NRSZ_weekdays[$index] != "closed"){
                    $scheduleDefined = true;
                    continue;
                }
            }
            if($scheduleDefined == false)
                return true;
        }
        return false;
    }

    /*
     * Master switch za zaprtje.
     */

    public static function getInstance(){
        if(Skocjan_odpiralni_cas::$instance == null){
            //optimizacija: Vedno beremo shranjene podatke v bazi, ne iz XLS datoteke.
            $fromDb = unserialize(get_option(Skocjan_odpiralni_cas::$option_name));

            if($fromDb !== false)
                Skocjan_odpiralni_cas::$instance = $fromDb;
            else {
                Skocjan_odpiralni_cas::$instance = new Skocjan_odpiralni_cas();
            }
        }
        Skocjan_odpiralni_cas::$linkToAdminPanel = admin_url( 'options-general.php?page=sz-odpiralni-cas');
        return Skocjan_odpiralni_cas::$instance;
    }

    private function getTimeAfterMidnight($startTime){
        $day = date("j", $startTime);
        do{
            $startTime += 60*60; //+1 hour
        }while(($day) == date("j", $startTime));
        return $startTime;
    }


    /*
     * Vrne HTML tabele celotnega odpiralnega časa.
     */

    public function getHtml(){
        $html = "";
        for($i =0; $i < count($this->schedulesByMonths); $i++){
            $html .= $this->schedulesByMonths[$i]->getHTML();
        }
        return $html;
    }

    public function isReady(){
        return $this->isReady;
    }
    /*
     * Vrne HTML trenutnega stanja rezervata.
     */
    public function ourZone(){
        date_default_timezone_set("Europe/Ljubljana");
        $cas = time();
        return $cas;
    }
    public function getStatusHTML($nrsz_or_cooo){
        if($nrsz_or_cooo != "cooo" && $nrsz_or_cooo != "nrsz")
            $nrsz_or_cooo = "nrsz";

        $openStatus = $this->getCurrentOpenStatus($this->ourZone(), $nrsz_or_cooo);
        if(!is_array($openStatus)){
            if($this->isReady){
                error_log("SZ_odpiralni_casi: getCurrentOpenStatus() did not return an array.");
                return __("Prosim kliknite za več podrobnosti.", "sz-odpiralni-casi");
            }else{
                return __("Uvoziti morate podatke. Prijavite se v administracijo: Nastavitve > Odpiralni čas.", "sz-odpiralni-casi");
            }
        }


        $stat_class = $stat_text = $descText = "";
        if($openStatus["status"] == "closed"){
            $stat_class = "closed_status";
            $stat_text = SZ_Schedule::getHourText($openStatus["status"]);
            if($openStatus["dayOfOpening"] == "today")
                $descText = __("do", "sz-odpiralni-casi")." ".$openStatus["timeOfOpening"];
            else if($openStatus["dayOfOpening"] == "tomorrow")
                $descText = __("do jutri ob ", "sz-odpiralni-casi")." ".$openStatus["timeOfOpening"];
            else if(!empty($openStatus["dayOfOpening"])) {
                //setlocale("LC_ALL", "C.utf8"); Ne dela!!! :/
                //$dayName = Date("l", strtotime($openStatus["dayOfOpening"]);
                $dayName =__(SZ_Schedule::getDayName(Date("w", strtotime($openStatus["dayOfOpening"])), true), "sz-odpiralni-casi");
                //$descText = __("do", "sz-odpiralni-casi") . " " . $dayName .".";
                $descText = __("do", "sz-odpiralni-casi") . " " . $openStatus["dayOfOpening"] .".";
            }
        }else{
            $stat_class = "open_status";
            $stat_text = SZ_Schedule::getHourText("o");
            if($openStatus["status"] == "o")
                $descText = __("do", "sz-odpiralni-casi")." ".$openStatus["timeOfClosing"];
            else
                // če so ure za razl aktivnosti potem ne izpiši nič v oklepaju na widgetu
                $descText = "" . __("do", "sz-odpiralni-casi") . " " . $openStatus["timeOfClosing"];
        }
        return '<span class="'.$stat_class.'">'.$stat_text.'</span> <span>'.$descText.'</span>';
    }

    /*
     * Vrne Array z podatki o trenutnem stanju rezervata. Odprt/zaprt, kdaj se odpre, kdaj se zapre.
     */

    public function getCurrentOpenStatus($time, $facility){
        //for nrsz or cooo:
        $out = array(
            "status" => "closed",
            "dayOfOpening" => "",//today|tomorrow|date
            "timeOfOpening" => "00:00",
            "timeOfClosing" => "00:00"
        );

        if(Skocjan_odpiralni_cas::areWeClosedForEver())
            return $out;

        $currentMonth = Date('n', $time);
        $currentCalIndex = $this->getScheduleIndex($currentMonth);
        if($currentCalIndex < 0) {
            error_log("SZ_odpiralni_casi: Schedule not present for month: ".Date("F", $time).'. In function: getCurrentOpenStatus($time).');
            return ""; //error. Die quietly.
        }
        $monthSchedule = $this->schedulesByMonths[$currentCalIndex];

        //check if it's a weekend or holiday
        $workingDay = Skocjan_odpiralni_cas::isAWorkDay($time);



        // Če je označena opcija isti delovni čas ves teden
        $useWeekScheduleForWeekends= get_option(Skocjan_odpiralni_cas::$option_name_useWeekScheduleForWeekends);
        if ($workingDay==false && $useWeekScheduleForWeekends=="yes" ){
            $workingDay==true;
        }

        //začetna in končna ura v razpredelnici odpiralnega časa
        $minHours = intval(trim(substr($this->schedulesByMonths[$currentCalIndex]->hours[0], 0, 2), ':'));
        $maxHours = intval(substr(end($this->schedulesByMonths[$currentCalIndex]->hours), 0, 2))+1;

        $currentHour = intval(Date("H", $time)); //0 to 31


        if($this->areWeClosed($time,$facility) || $currentHour >= $maxHours){
            //zaprto do jutri/pojutrisnjem/nekega dne:
            $out["status"] = 'closed';

            $nextDay = $time;
            do {
                $nextDay = $this->getTimeAfterMidnight($nextDay);
            }while($this->areWeClosed($nextDay,$facility));

            $openingDay = (strcmp(Date("d.m.Y", $nextDay), Date("d.m.Y", strtotime("tomorrow", $time))) == 0)? "tomorrow" : Date("d.m.Y", $nextDay);
            $out["dayOfOpening"] = $openingDay;

            $currentMonth = Date('n', $nextDay);
            $nextDayCalIndex = $this->getScheduleIndex($currentMonth);
            if($nextDayCalIndex < 0) {
                error_log("SZ_odpiralni_casi: Schedule not present for month: ".Date("F", $nextDay).'. In function: getCurrentOpenStatus($time) near $nextDay = $this->getTimeAfterMidnight($time).');
                return ""; //error. Die quietly.
            }
            $nextDaySchedule = $this->schedulesByMonths[$nextDayCalIndex];
            $nextDayWorkingDay = Skocjan_odpiralni_cas::isAWorkDay($nextDay);

            $i=0;
            $useWeekScheduleForWeekends= get_option(Skocjan_odpiralni_cas::$option_name_useWeekScheduleForWeekends);

            if($facility=="cooo"){
                if($nextDayWorkingDay || $useWeekScheduleForWeekends=="yes"){
                    while($nextDaySchedule->schedule_CO_OO_weekdays[$i] == "closed")
                        $i++;
                }else{
                    while($nextDaySchedule->schedule_CO_OO_weekends[$i] == "closed")
                        $i++;
                }
            }
            else{
                if($nextDayWorkingDay || $useWeekScheduleForWeekends=="yes"){
                    while($nextDaySchedule->schedule_NRSZ_weekdays[$i] == "closed")
                        $i++;
                }else{
                    while($nextDaySchedule->schedule_NRSZ_weekends[$i] == "closed")
                        $i++;
                }
            }
            $out["timeOfOpening"] = $nextDaySchedule->hours[$i];

        }else if($currentHour < $minHours){
            //zaprto do
            $out["dayOfOpening"] = "today";
            $out["status"] = 'closed';
            $i=0;

            $useWeekScheduleForWeekends= get_option(Skocjan_odpiralni_cas::$option_name_useWeekScheduleForWeekends);


            if($facility=="cooo"){
                if($workingDay || $useWeekScheduleForWeekends=="yes"){
                    while($monthSchedule->schedule_CO_OO_weekdays[$i] == "closed")
                        $i++;
                }else{
                    while($monthSchedule->schedule_CO_OO_weekends[$i] == "closed")
                        $i++;
                }
            }
            else{
                if($monthSchedule || $useWeekScheduleForWeekends=="yes"){
                    while($monthSchedule->schedule_NRSZ_weekdays[$i] == "closed")
                        $i++;
                }else{
                    while($monthSchedule->schedule_NRSZ_weekends[$i] == "closed")
                        $i++;
                }
            }

            $out["timeOfOpening"] = $monthSchedule->hours[$i];

        }else {
            //odprto ali zaprto zvečer
            $i=$currentHour-$minHours;
            $c = $i;

            $useWeekScheduleForWeekends= get_option(Skocjan_odpiralni_cas::$option_name_useWeekScheduleForWeekends);

            if ($facility=="cooo"){
                if($workingDay || $useWeekScheduleForWeekends=="yes"){
                    $out["status"] = $monthSchedule->schedule_CO_OO_weekdays[$i];
                    while($monthSchedule->schedule_CO_OO_weekdays[$i] == "closed")
                        $i++;
                    while($monthSchedule->schedule_CO_OO_weekdays[$c] != "closed")
                        $c++;
                }else{
                    $out["status"] = $monthSchedule->schedule_NRSZ_weekends[$i];
                    while($monthSchedule->schedule_CO_OO_weekends[$i] == "closed")
                        $i++;
                    while($monthSchedule->schedule_CO_OO_weekends[$c] != "closed")
                        $c++;
                }
            }
            else {
                if($workingDay || $useWeekScheduleForWeekends=="yes"){
                    $out["status"] = $monthSchedule->schedule_NRSZ_weekdays[$i];
                    while($monthSchedule->schedule_NRSZ_weekdays[$i] == "closed")
                        $i++;
                    while($monthSchedule->schedule_NRSZ_weekdays[$c] != "closed")
                        $c++;
                }else{
                    $out["status"] = $monthSchedule->schedule_NRSZ_weekends[$i];
                    while($monthSchedule->schedule_NRSZ_weekends[$i] == "closed")
                        $i++;
                    while($monthSchedule->schedule_NRSZ_weekends[$c] != "closed")
                        $c++;
                }
            }


            $nextDay =$time ;
            do {
                $nextDay = $this->getTimeAfterMidnight($nextDay);
            }while($this->areWeClosed($nextDay,$facility));
            $nextDayData = $this->getCurrentOpenStatus($nextDay,$facility);
            //nastavi stanje rezervata
            $openingDay = (strcmp(Date("d.m.Y", $nextDay), Date("d.m.Y", strtotime("tomorrow", $time ))) == 0)? "tomorrow" : Date("d.m.Y", $nextDay);

            if(empty($nextDayData)) {
                error_log("SZ_odpiralni_casi: Schedule not present for month: ".Date("F", $nextDay).'. In function: getCurrentOpenStatus($time) near $nextDayData = $this->getCurrentOpenStatus($nextDay).');
                return ""; //die quietly
            }


            if($out["status"] == "closed" && $i >= count($monthSchedule->hours)){
                $out["dayOfOpening"] = $openingDay;
                $out["timeOfOpening"] = $nextDayData["timeOfOpening"];
            }else{
                $out["dayOfOpening"] = "today";
                $out["timeOfOpening"] = $monthSchedule->hours[$i];
                $out["timeOfClosing"] = $monthSchedule->hours[$c];
                // v primeru, da je zadnja ura na urniku delavna
                if ($monthSchedule->hours[$c] == null) {
                    $out["timeOfClosing"] = ($monthSchedule->hours[$c-1]+ 1).":00";
                }
            }


        }
        return $out;
    }

    /*
     * Vrne index Shedula, ki zajema podatke o iskanem mesecu.
     */

    public static function areWeClosedForEver()
    {
        //Master close?
        $userDefinedThePlaceIsClosed = get_option(Skocjan_odpiralni_cas::$option_name_placeIsClosed);
        if($userDefinedThePlaceIsClosed == "closed")
            return true;
    }


    /*
     * Vrne HTML legendo.
     */

    private function getScheduleIndex($currentMonth){
        //get calendar for month
        $currentCalIndex = -1;
        $monthShortNameArray = array_keys(SZ_Schedule::$monthName);
        for($i =0; $i < count($this->schedulesByMonths); $i++){
            if(in_array($monthShortNameArray[intval($currentMonth)-1], $this->schedulesByMonths[$i]->months)){
                $currentCalIndex = $i;
                break;
            }
        }
        return $currentCalIndex;
    }

    /*
     * Helper funkcija za generiranje EPOCH naslednjega dne
     */

    private static function isAWorkDay($time){
        $currentDay = Date('j', $time); //numeric 1 - 31
        $currentDayInWeekIndex = Date('w', $time); //0-6  //0 => nedelja!
        $currentYear = Date('y', $time); //2010-2059
        $currentMonth = Date("n", $time); //1-12

        if($currentDayInWeekIndex == 0 || $currentDayInWeekIndex == 6)
            return false;
        if(in_array($currentDay.".".$currentMonth.".", Skocjan_odpiralni_cas::$slovenianHolidays))
            return false;
        if(in_array($currentDay.".".$currentMonth.".".$currentYear, Skocjan_odpiralni_cas::$easterMondays))
            return false;
        return true;
    }
    /*
     * Pridobi podatke iz XLS ali XLSX datoteke
     */

    public function getCurrentMonthHtml(){
        wp_enqueue_script( 'sz_odpiralni_casi_current');

        $currentMonth = Date('n', $this->ourZone());
        $currentCalIndex = $this->getScheduleIndex($currentMonth);
        if($currentCalIndex < 0) {
            error_log("SZ_odpiralni_casi: Schedule not present for month: ".Date("F", $this->ourZone()).'. In function: getCurrentMonthHtml().');
            return ""; //error. Die quietly.
        }
        $areWeClosed = Skocjan_odpiralni_cas::areWeClosedForEver() || Skocjan_odpiralni_cas::areWeClosed($this->ourZone(),"NRSZ");
        $areWeClosed_COOO = Skocjan_odpiralni_cas::areWeClosedForEver() || Skocjan_odpiralni_cas::areWeClosed($this->ourZone(),"COOO");
        //tukaj je funkcija time(), ker je na serverju nastavljen pravilen čas in mu ni potrebno dodajati 3600 sekund(v datoteki sz_current_calendar.js)
        $additionalHTML = "<input type='hidden' id='sz_odpiralni_cas_current_epoch' value='".time()."' />";
        $additionalHTML .= "<input type='hidden' id='sz_odpiralni_cas_closedToday' value='".$areWeClosed."' title='".SZ_Schedule::getHourText('closed today')."'/>";
        $additionalHTML .= "<input type='hidden' id='sz_odpiralni_cas_closedToday_COOO' value='".$areWeClosed_COOO."' title='".SZ_Schedule::getHourText('closed today')."'/>";
        $trenutnoMsg = __("Trenutno", "sz-odpiralni-casi");

        $outHtml = '<div class="sz_odpiralni_cas_current">';
        $outHtml .= $this->schedulesByMonths[$currentCalIndex]->getHtml($trenutnoMsg).$additionalHTML;
        $outHtml .= '<div class="sz_odpiralni_cas_hoverer"><div class="sz_odpiralni_cas_stick"></div><div class="sz_text_holder">';
        $outHtml .= '<p class="sz_current_time">15:00</p>';
        $outHtml .= '<p><strong>'.__("NRŠZ", "sz-odpiralni-casi").':</strong> <span class="sz_nrsz_openStatus"></span></p>';
        $outHtml .= '<p><strong>'.__("CO in OO", "sz-odpiralni-casi").':</strong> <span class="sz_cooo_openStatus_COOO"> </span></p>';
        $outHtml .= '</div></div>';

        $outHtml .= '</div>';
        $outHtml .= '</div>';
        return $outHtml;
    }
    /*
     * Vrne true, če je EPOCH med tednom.
     */

    public function getLegend(){
        $htmlOut = "";

        // izspi opozorila da je park zaprt vsak X dan v tednu
        $userDefinedClosedDatesString = get_option(Skocjan_odpiralni_cas::$option_name_closedDays);
        $closedDaysString = "";
        $closedDaysString .= (strpos($userDefinedClosedDatesString, 'mon') !== false) ? $this->addStringToStringWithSeparator($closedDaysString, __("ponedeljek", "sz-odpiralni-casi"), ",") : "";
        $closedDaysString .= (strpos($userDefinedClosedDatesString, 'tue') !== false) ? $this->addStringToStringWithSeparator($closedDaysString, __("torek", "sz-odpiralni-casi"), ",") : "";
        $closedDaysString .= (strpos($userDefinedClosedDatesString, 'wed') !== false) ? $this->addStringToStringWithSeparator($closedDaysString, __("sredo", "sz-odpiralni-casi"), ",") : "";
        $closedDaysString .= (strpos($userDefinedClosedDatesString, 'thu') !== false) ? $this->addStringToStringWithSeparator($closedDaysString, __("četrtek", "sz-odpiralni-casi"), ",") : "";
        $closedDaysString .= (strpos($userDefinedClosedDatesString, 'fri') !== false) ? $this->addStringToStringWithSeparator($closedDaysString, __("petek", "sz-odpiralni-casi"), ",") : "";
        $closedDaysString .= (strpos($userDefinedClosedDatesString, 'sat') !== false) ? $this->addStringToStringWithSeparator($closedDaysString, __("soboto", "sz-odpiralni-casi"), ",") : "";
        $closedDaysString .= (strpos($userDefinedClosedDatesString, 'sun') !== false) ? $this->addStringToStringWithSeparator($closedDaysString, __("nedeljo", "sz-odpiralni-casi"), ",") : "";

        $userDefinedClosedDatesString_COOO = get_option(Skocjan_odpiralni_cas::$option_name_closedDays_COOO);
        $closedDaysString_COOO = "";
        $closedDaysString_COOO .= (strpos($userDefinedClosedDatesString_COOO, 'mon') !== false) ? $this->addStringToStringWithSeparator($closedDaysString_COOO, __("ponedeljek", "sz-odpiralni-casi"), ",") : "";
        $closedDaysString_COOO .= (strpos($userDefinedClosedDatesString_COOO, 'tue') !== false) ? $this->addStringToStringWithSeparator($closedDaysString_COOO, __("torek", "sz-odpiralni-casi"), ",") : "";
        $closedDaysString_COOO .= (strpos($userDefinedClosedDatesString_COOO, 'wed') !== false) ? $this->addStringToStringWithSeparator($closedDaysString_COOO, __("sredo", "sz-odpiralni-casi"), ",") : "";
        $closedDaysString_COOO .= (strpos($userDefinedClosedDatesString_COOO, 'thu') !== false) ? $this->addStringToStringWithSeparator($closedDaysString_COOO, __("četrtek", "sz-odpiralni-casi"), ",") : "";
        $closedDaysString_COOO .= (strpos($userDefinedClosedDatesString_COOO, 'fri') !== false) ? $this->addStringToStringWithSeparator($closedDaysString_COOO, __("petek", "sz-odpiralni-casi"), ",") : "";
        $closedDaysString_COOO .= (strpos($userDefinedClosedDatesString_COOO, 'sat') !== false) ? $this->addStringToStringWithSeparator($closedDaysString_COOO, __("soboto", "sz-odpiralni-casi"), ",") : "";
        $closedDaysString_COOO .= (strpos($userDefinedClosedDatesString_COOO, 'sun') !== false) ? $this->addStringToStringWithSeparator($closedDaysString_COOO, __("nedeljo", "sz-odpiralni-casi"), ",") : "";
        /* sklanjajmo pravilno, ker slovenščina
        $everyText = __("v", "sz-odpiralni-casi");
        if ((substr($userDefinedClosedDatesString, 0, 3) == "wed") || (substr($userDefinedClosedDatesString, 0, 3) == "sat") || (substr($userDefinedClosedDatesString, 0, 3) == "sun"))
            $everyText = __("v", "sz-odpiralni-casi");*/

        // preverimo ali prikazujemo posametne dneve, če ne ne prikažemo oznake "***" v legendi
        //$closedDaysLegendAsterisk = "*** ";
        if (get_option(Skocjan_odpiralni_cas::$option_name_useWeekScheduleForWeekends))
            $closedDaysLegendAsterisk = "";

        if (strpos($closedDaysString,",")!=false){
            $position=strripos($closedDaysString,",");
            $closedDaysString=substr_replace($closedDaysString, __("in","sz-odpiralni-casi"), $position, 1 );
        }

        if (strpos($closedDaysString_COOO,",")!=false){
            $position=strripos($closedDaysString_COOO,",");
            $closedDaysString_COOO=substr_replace($closedDaysString_COOO, __("in","sz-odpiralni-casi"), $position, 1 );
        }

        if (strlen($closedDaysString) > 0 || strlen($closedDaysString_COOO) > 0){
            //$htmlOut .= "<div class='sz_odpiralni_cas_closedDays'>";
            $htmlOut .= "<div class='sz_odpiralni_cas_closedDays'>";
            if (strlen($closedDaysString)>0){
                $htmlOut .= $closedDaysLegendAsterisk . __("Naravni rezervat škocjanski zatok je zaprt v", "sz-odpiralni-casi") . " ";
                $htmlOut .= "<span>" . " " . $closedDaysString . "</span>";
                $htmlOut .= "</br>";
            }
            else{
                //$htmlOut .= $closedDaysLegendAsterisk . __("Naravni rezervat škocjanski zatok je odprt vse dni v tednu.", "sz-odpiralni-casi") . " ";
            }
            if (strlen($closedDaysString_COOO)>0){
                $htmlOut .=  __("Center za obiskovalce je zaprt v", "sz-odpiralni-casi");
                $htmlOut .= "<span>" . " " . $closedDaysString_COOO . "</span>";
                $htmlOut .= "</br>";
                //$htmlOut .= ". ". __("Osrednja opazovalnica je odprta vse dni v tednu.", "sz-odpiralni-casi");
            }
            else {
                //$htmlOut .= __("Center za obiskovalce je odprt vse dni v tednu.", "sz-odpiralni-casi") . " ";
                //$htmlOut .= __("Osrednja opazovalnica je odprta vse dni v tednu", "sz-odpiralni-casi");
            }
            $htmlOut .= "</br>";
            $htmlOut .= __("Izjemoma so po predhodni najavi naravni rezervat ali objekti lahko zaprti tudi med državnimi prazniki.", "sz-odpiralni-casi") . " ";
            $htmlOut .= "</div>";
            $htmlOut .= "<div class='sz_odpiralni_cas_legend'></div>";
        }

        // stalna legenda
        $htmlOut .= "<p>* ".__("Naravni rezervat Škocjanski zatok", "sz-odpiralni-casi")."<br />";
        $htmlOut .= "** ".__("Center za obiskovalce in osrednja opazovalnica", "sz-odpiralni-casi")."</p>";

        $htmlOut .= "<div  class='sz_odpiralni_cas_legend'><ul>";
        $htmlOut .= "<li class='sz_legend_t'>";
        $htmlOut .= "<span class='sz_shortTitle'>".SZ_Schedule::getHourText("t")."</span> ";
        $htmlOut .= "<span class='sz_desc'>".__("(sprehodi, fotografiranje, doživljanje narave in podobne tihe oblike individualnega obiska)", "sz-odpiralni-casi")."</span>";
        $htmlOut .= "</li>";
        //$htmlOut .= "<li class='sz_legend_vo'>";
        //$htmlOut .= "<span class='sz_shortTitle'>".SZ_Schedule::getHourText("vo")."</span> ";
        //$htmlOut .= "<span class='sz_desc'>".__("(po programu NRŠZ, za predhodno najavljene skupine in ob terminih po programu najavljenih dogodkov)", "sz-odpiralni-casi")."</span>";
        //$htmlOut .= "</li>";
        $htmlOut .= "<li class='sz_legend_r'>";
        $htmlOut .= "<span class='sz_shortTitle'>".SZ_Schedule::getHourText("r")."</span> ";
        //$htmlOut .= "<span class='sz_desc'>".__("(razlaga ur za rekreacijo)", "sz-odpiralni-casi")."</span>";
        $htmlOut .= "</li>";
        $htmlOut .= "<li class='sz_legend_closed' style='display:none'>";
        $htmlOut .= "<span class='sz_shortTitle'>".SZ_Schedule::getHourText("closed")."</span> ";
        $htmlOut .= "</li>";
        $htmlOut .= "</ul></div>";
        return $htmlOut;
    }
    /*
     * Vrne true, če je rezervat zaprt na podan EPOCH
     */

    public function parseXLSFile($inputFileName){
        try{
            unset($this->schedulesByMonths);

            $objReader = PHPExcel_IOFactory::load($inputFileName);

            for($i =0; $i < $objReader->getSheetCount(); $i++){
                $this->schedulesByMonths[] = new SZ_Schedule($objReader->getSheet($i));
            }
            unset($objReader);
            $this->isReady = true;
        }catch(Exception $e){
            return $e->getMessage();
            return false;
        }
        return true;
    }

    /*
     * Helper funkcija za dodajanje novih nizov v obstoječ niz z drugim vmesnim nizom
     */
    private function addStringToStringWithSeparator($stringIn, $stringAdd, $separator){
        if (strlen($stringIn) == 0) {
            return $stringAdd;
        } else {
            return " ".$separator." ".$stringAdd;
        }
    }
}

/*
 * Class, ki drži posamezno verzijo odpiralnega časa (posamezen Sheet znotraj XLS-a)
 */
class SZ_Schedule{
    public static $monthName = array(
        "jan" => "januar",
        "feb" => "februar",
        "mar" => "marec",
        "apr" => "april",
        "maj" => "maj",
        "jun" => "junij",
        "jul" => "julij",
        "avg" => "avgust",
        "sep" => "september",
        "okt" => "oktober",
        "nov" => "november",
        "dec" => "december");
    public static $monthNameSklanjano = array(
        "jan" => "januarju",
        "feb" => "februarju",
        "mar" => "marcu",
        "apr" => "aprilu",
        "maj" => "maju",
        "jun" => "juniju",
        "jul" => "juliju",
        "avg" => "avgustu",
        "sep" => "septembru",
        "okt" => "oktobru",
        "nov" => "novembru",
        "dec" => "decembru");

    public static $dayName = array(
        "0" => "nedelja",
        "1" => "ponedeljek",
        "2" => "torek",
        "3" => "sreda",
        "4" => "četrtek",
        "5" => "petek",
        "6" => "sobota"
    );
    public static $dayNameSklanjano = array(
        "0" => "nedelje",
        "1" => "ponedeljka",
        "2" => "torka",
        "3" => "srede",
        "4" => "četrtka",
        "5" => "petka",
        "6" => "sobote"
    );
    public $months;
    public $hours;

    public $schedule_NRSZ_weekdays;
    public $schedule_NRSZ_weekends;
    public $schedule_CO_OO_weekdays;
    public $schedule_CO_OO_weekends;

    /*
     * Parse the XSL file and store the values this class
     */
    public function __construct($sheet){
        //$this->_sheetObject = $sheet;
        $sheetData = $sheet->toArray(null, true, true, true);

        //get months:
        $i = 0;
        while($sheetData[$i]['A'] != "Meseci:")
            $i++;
        $i++;
        $this->parseMonths($sheetData[$i]);

        //get hours
        while($sheetData[$i]['A'] != "Med tednom:")
            $i++;
        $i++;
        $this->parseHours($sheetData[$i]);

        //get schedules:
        $i++;
        $this->schedule_NRSZ_weekdays = $this->parseSchedule($sheetData[$i]);
        $i += 4;
        $this->schedule_NRSZ_weekends = $this->parseSchedule($sheetData[$i]);
        $i += 5;
        $this->schedule_CO_OO_weekdays = $this->parseSchedule($sheetData[$i]);
        $i += 4;
        $this->schedule_CO_OO_weekends = $this->parseSchedule($sheetData[$i]);

        unset($sheetData);
    }

    private function parseMonths($rowOfData){
        $this->months = array();
        foreach($rowOfData as $colName => $monthName){
            if($monthName != null){
                $this->months[] = $monthName;
            }
        }
    }

    private function parseHours($rowOfData){
        $this->hours = array();
        foreach($rowOfData as $colName => $time){
            if($time != null){
                $this->hours[] = $time;
            }
        }
    }

    private function parseSchedule($rowOfData){
        $schedule = array();
        foreach($rowOfData as $colName => $openOrClosed){
            if($openOrClosed != null){
                $schedule[] = $openOrClosed;
            }else{
                $schedule[] = 'closed';
            }
        }
        return $schedule;
    }

    public static function getDayName($dayIndex, $sklanjano=false){
        if($sklanjano == true) {
            return SZ_Schedule::$dayNameSklanjano[$dayIndex];
        }else{
            return SZ_Schedule::$dayName[$dayIndex];
        }
        return "";
    }

    /*
     * HTML tabela za en mesec. 
     */

    function __destruct()
    {
        unset($months);
        unset($hours);
        unset($schedule_NRSZ_weekdays);
        unset($schedule_NRSZ_weekends);
        unset($schedule_CO_OO_weekdays);
        unset($schedule_CO_OO_weekends);
    }

    /*
     * Vrne seznam mesecev.
     */

    public function getHTML($title = ""){
        $htmlout = "<div>";

        //Vrednost za javascript oblikovanje tabele
        $useWeekScheduleForWeekends= get_option(Skocjan_odpiralni_cas::$option_name_useWeekScheduleForWeekends);
        $htmlout.="<input type='hidden' id='useWeekSchForWeekend' value='".$useWeekScheduleForWeekends ."'/>";

        if($title == "")
            $htmlout .= "<h3>".__("V", "sz-odpiralni-casi")." ".$this->getMonthsString()."</h3>";
        else
            $htmlout .= "<h3>".$title."</h3>";
        $useWeekScheduleForWeekends= get_option(Skocjan_odpiralni_cas::$option_name_useWeekScheduleForWeekends);
        $addShrinkingClass = ($useWeekScheduleForWeekends != "") ? "sz_odpiralni_cas_week_schedule_for_weekends" : "";


        $htmlout .= "<table class='sz_odpiralni_cas_all $addShrinkingClass' cellspacing='0' border='0' cellpadding='0'>";
        $htmlout .= "<tr>";
        $htmlout .= "<td valign='bottom'>";
        $htmlout .= $this->getHTML_part_meseci();
        $htmlout .= "</td>";
        $htmlout .= "<td valign='bottom' class='sz_odpiralni_cas_urnik_holder'>";
        $htmlout .= $this->getHTML_part_urnik();
        $htmlout .= "</td>";
        $htmlout .= "</tr>";
        $htmlout .= "</table>";
        $htmlout .= "</div>";
        return $htmlout;
    }

    /*
     * Helper funkcija za pravilno sklanjatev meseca.
     */

    private function getMonthsString(){
        $monthsNames = array();
        for($i =0; $i<count($this->months); $i++){
            $monthsNames[] = __($this->getMonthName($this->months[$i], true), "sz-odpiralni-casi");
        }
        return implode(", ", $monthsNames);
    }
    /*
     * Helper funkcija za pravilno sklanjatev tedna.
     */

    public static function getMonthName($monthCode, $sklanjano=false){
        if(!array_key_exists($monthCode, SZ_Schedule::$monthName))
            return $monthCode;

        if($sklanjano == true) {
            return SZ_Schedule::$monthNameSklanjano[$monthCode];
        }else{
            return SZ_Schedule::$monthName[$monthCode];
        }
    }
    /*
     * Vrne del tabele, kjer so napisani meseci
     */

    private function getHTML_part_meseci(){
        $useWeekScheduleForWeekends= get_option(Skocjan_odpiralni_cas::$option_name_useWeekScheduleForWeekends);

        $userDefinedClosedDatesString = get_option(Skocjan_odpiralni_cas::$option_name_closedDays);
        $closedWeekdaysAsterisk = "";
        $closedWeekendsAsterisk = '';
        if (strlen($userDefinedClosedDatesString) > 0) {

            if ((strpos($userDefinedClosedDatesString, 'mon') !== false)
                || (strpos($userDefinedClosedDatesString, 'tue') !== false)
                || (strpos($userDefinedClosedDatesString, 'wed') !== false)
                || (strpos($userDefinedClosedDatesString, 'thu') !== false)
                || (strpos($userDefinedClosedDatesString, 'fri') !== false))
                $closedWeekdaysAsterisk = ' ***';

            if ((strpos($userDefinedClosedDatesString, 'sun') !== false)
                || (strpos($userDefinedClosedDatesString, 'sat') !== false))
                $closedWeekendsAsterisk = ' ***';
        }


        $htmlout = "<table class='sz_odpiralni_cas_leftHeader' cellspacing='0' border='0' cellpadding='0'>";
        $htmlout .= "<tr>";
        //Meseci
        $htmlout .= $this->getHTML_part_meseci_fromTo(0,6);
        //NRŠZ

        $htmlout .= "<td rowspan='2' class='sz_odpiralni_cas_title'>";
        $htmlout .= "<span  title='".__("Naravni rezervat Škocjanski zatok", "sz-odpiralni-casi")."'>";
        $htmlout .= __("NRŠZ", "sz-odpiralni-casi")."*";
        $htmlout .= "</span>";
        $htmlout .= "</td>";
        //MON-FRI
        if ($useWeekScheduleForWeekends==""){
            $htmlout .= "<td class='sz_odpiralni_cas_dayOfWeek'>";

            $htmlout .= "<span>";
            $htmlout .= __("pon - pet", "sz-odpiralni-casi") . $closedWeekdaysAsterisk;
            $htmlout .= "</span>";

            $htmlout .= "</td>";
        }
        $htmlout .= "</tr>";
        //Meseci že od prej
        //NRŠZ že od prej
        $htmlout .= "<tr>";
        //SAT-SUN
        if ($useWeekScheduleForWeekends=="") {
            $htmlout .= "<td class='sz_odpiralni_cas_dayOfWeek'>";

            $htmlout .= "<span>";
            $htmlout .= __("sob - ned / prazniki", "sz-odpiralni-casi") . $closedWeekendsAsterisk;
            $htmlout .= "</span>";

            $htmlout .= "</td>";
        }
        $htmlout .= "</tr>";
        $htmlout .= "<tr>";
        //Meseci
        $htmlout .= $this->getHTML_part_meseci_fromTo(6,12);
        //NRŠZ
        $htmlout .= "<td rowspan='2' class='sz_odpiralni_cas_title'>";
        $htmlout .= "<span  title=\"".__("Center za obiskovalce in osrednja opazovalnica", "sz-odpiralni-casi")."\">";
        $htmlout .= __("CO in OO", "sz-odpiralni-casi")."**";
        $htmlout .= "</span>";
        $htmlout .= "</td>";
        //MON-FRI
        if ($useWeekScheduleForWeekends=="") {
            $htmlout .= "<td class='sz_odpiralni_cas_dayOfWeek'>";
            $htmlout .= "<span>";
            $htmlout .= __("pon - pet", "sz-odpiralni-casi") . $closedWeekdaysAsterisk;
            $htmlout .= "</span>";
            $htmlout .= "</td>";
        }
        $htmlout .= "</tr>";

        //Tu zbrišemo tudi tr, da je spodnja črta v tabeli odebeljena
        if ($useWeekScheduleForWeekends=="") {
            $htmlout .= "<tr>";

            //Meseci že od prej
            //NRŠZ že od prej

            //SAT-SUN
            $htmlout .= "<td  class='sz_odpiralni_cas_dayOfWeek'>";
            $htmlout .= "<span>";
            $htmlout .= __("sob - ned / prazniki", "sz-odpiralni-casi") . $closedWeekendsAsterisk;
            $htmlout .= "</span>";
            $htmlout .= "</td>";
            $htmlout .= "</tr>";
        }

        // "popravek" za FF border collapse bug - tu ga zmoti zaradi uporabe dvovrstičnih celic
        // dodamo prazno vrstico da zna pravilno zračunat višino tabele tako da ne odreže spodnjega roba (dela tudi v ostalih brskalnikih)
        $htmlout .= "<tr></tr>";

        $htmlout .= "</table>";
        return $htmlout;
    }

    /*
     * Helper funkcija, ki vrne nekaj zaporednih TD-jev z meseci 
     */
    private function getHTML_part_meseci_fromTo($from, $to){
        $htmlout = "";
        $shortNames = array_keys(SZ_Schedule::$monthName);
        for($i=$from; $i<$to; $i++){
            $activeClass = in_array($shortNames[$i], $this->months)? "active": "";
            $htmlout .= "<td rowspan='2' class='sz_odpiralni_cas_mesec ".$activeClass."'>";
            $htmlout .= "<span  title='".ucfirst(__(SZ_Schedule::getMonthName($shortNames[$i]), "sz-odpiralni-casi"))."'>";
            $htmlout .= substr(__(SZ_Schedule::getMonthName($shortNames[$i]), "sz-odpiralni-casi"),0, 1);
            $htmlout .= "</span>";
            $htmlout .= "</td>";
        }
        return $htmlout;
    }
    /*
     * Vrne del tabele, kjer je napisan odpiralni čas.
     */
    private function getHTML_part_urnik(){
        $useWeekScheduleForWeekends= get_option(Skocjan_odpiralni_cas::$option_name_useWeekScheduleForWeekends);

        $htmlout = "<table class='sz_odpiralni_cas_urnik' cellspacing='0' border='0' cellpadding='0'>";
        $htmlout .= "<tr class='sz_odpiralni_cas_ure'>";
        $htmlout .= $this->getHTML_part_urnik_ure();
        $htmlout .= "</tr>";

        $htmlout .= "<tr>";
        $htmlout .= $this->getHTML_schedule($this->schedule_NRSZ_weekdays);
        $htmlout .= "</tr>";
        $htmlout .= "<tr>";
        if ($useWeekScheduleForWeekends=="") {
            $htmlout .= $this->getHTML_schedule($this->schedule_NRSZ_weekends);
            $htmlout .= "</tr>";
        }
        $htmlout .= "<tr>";
        $htmlout .= $this->getHTML_schedule($this->schedule_CO_OO_weekdays);
        $htmlout .= "</tr>";
        if ($useWeekScheduleForWeekends=="") {
            $htmlout .= "<tr>";
            $htmlout .= $this->getHTML_schedule($this->schedule_CO_OO_weekends);
            $htmlout .= "</tr>";
        }
        $htmlout .= "</table>";
        return $htmlout;
    }

    /*
     * Vrne del tabele, kjer so napisane ure
     */
    private function getHTML_part_urnik_ure(){
        $htmlout = "";
        foreach($this->hours as $h){
            $htmlout .= "<td>";
            $htmlout .= "<span>";
            $htmlout .= $h;
            $htmlout .= "</span>";
            $htmlout .= "</td>";
        }
        return $htmlout;
    }

    /*
     * Vrne del tabele, kjer je označeno, če je rezervat odprt in katere ure so.
     */
    private function getHTML_schedule($scheduleData){
        $htmlout = "";
        foreach($this->hours as $index => $hour){
            $htmlout .= "<td>";
            $htmlout .= "<span  class='schedule_$scheduleData[$index]' title='".SZ_Schedule::getHourText($scheduleData[$index])."'></span>";
            $htmlout .= "</td>";
        }
        return $htmlout;
    }
    /*
     * Helper funkcija za poimenovanje ur.
     */
    public static function getHourText($openOrClosed_code){
        switch($openOrClosed_code){
            case "t":
                //Ure za tihe oblike obiska
                return __("Ure za tihe oblike obiska", "sz-odpiralni-casi");
            case "vo":
                return __("Ure za vodene obiske", "sz-odpiralni-casi");
            case "r":
                return __("Ure za rekreacijo", "sz-odpiralni-casi");
            case "o":
                return __("Odprto", "sz-odpiralni-casi");
            case "closed today":
                return __("Danes zaprto", "sz-odpiralni-casi");
            case "is closed today":
                return __("je danes zaprt", "sz-odpiralni-casi");
            case "closed":
            default: {
                return __("Zaprto", "sz-odpiralni-casi");
                break;
            }
        }
    }
}

class SZ_Notice_Helper
{
    private static $error;
    private static $info;
    private static $success;

    private static $error_repeat_times;
    private static $info_repeat_times;
    private static $success_repeat_times;

    public static function setError($msg, $repeatTimes = -1){
        SZ_Notice_Helper::$error = $msg;
        SZ_Notice_Helper::$error_repeat_times = $repeatTimes;
    }
    public static function setInfo($msg, $repeatTimes = -1){
        SZ_Notice_Helper::$info = $msg;
        SZ_Notice_Helper::$info_repeat_times = $repeatTimes;
    }
    public static function setSuccess($msg, $repeatTimes = -1){
        SZ_Notice_Helper::$success = $msg;
        SZ_Notice_Helper::$success_repeat_times = $repeatTimes;
    }
    public static function getNotices(){
        $out = "";
        if(!empty(SZ_Notice_Helper::$error)){
            $out .= "<div class='error notice'>
                <p>".SZ_Notice_Helper::$error."</p>
            </div>";
        }
        if(SZ_Notice_Helper::$error_repeat_times > 0)
            SZ_Notice_Helper::$error_repeat_times--;
        if(SZ_Notice_Helper::$error_repeat_times == 0)
            SZ_Notice_Helper::$error = "";

        if(!empty(SZ_Notice_Helper::$info)){
            $out .= "<div class='update-nag notice'>
                <p>".SZ_Notice_Helper::$info."</p>
            </div>";
        }
        if(SZ_Notice_Helper::$info_repeat_times > 0)
            SZ_Notice_Helper::$info_repeat_times--;
        if(SZ_Notice_Helper::$info_repeat_times == 0)
            SZ_Notice_Helper::$info = "";

        if(!empty(SZ_Notice_Helper::$success)){
            $out .= "<div class='updated notice'>
                <p>".SZ_Notice_Helper::$success."</p>
            </div>";
        }
        if(SZ_Notice_Helper::$success_repeat_times > 0)
            SZ_Notice_Helper::$success_repeat_times--;
        if(SZ_Notice_Helper::$success_repeat_times == 0)
            SZ_Notice_Helper::$success = "";
        return $out;
    }
}
?>
