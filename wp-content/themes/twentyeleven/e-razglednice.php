<?php
/**
 * Template Name: E-razglednice
 * Description: A Page Template that showcases Sticky Posts, Asides, and Blog Posts
 *
 * The showcase template in Twenty Eleven consists of a featured posts section using sticky posts,
 * another recent posts area (with the latest post shown in full and the rest as a list)
 * and a left sidebar holding aside posts.
 *
 * We are creating two queries to fetch the proper posts and a custom widget for the sidebar.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

parse_str($_SERVER['QUERY_STRING'], $args);
if(!empty($args['nlid'])){

	//check if ID is in DB
	/*$newsletter = array(
		'title' => $_POST['newsletter_title'],
		'senderName' => $_POST['senders_name'],
		'recipientName' => $_POST['friends_name'],
		'recipientEmail' => $_POST['friends_email'],
		'photoUrl' => $_POST['image_src'],
		'photoTitle' => $_POST['image_title'],
		'photoAuthor' => $_POST['image_author'],
		'senderText' => $_POST['newsletter_text'],
		'isEmail' => true
	)*/
	$newsletter = unserialize(get_option($args['nlid']));

	if(isset($newsletter) && is_array($newsletter)) {
		get_header("nodesign"); //don't display header
		require_once('e-razglednice_design.php');
		get_footer("nodesign"); //don't display header
		wp_die();
	}

}

get_header();
?>

		<div id="primary">
			<div id="content" role="main">

			<?php if ( have_posts() ) : ?>

				<?php twentyeleven_content_nav( 'nav-above' ); ?>

				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>

					<?php  get_template_part( 'content', 'e-razglednice' ); ?>

				<?php endwhile; ?>

				<?php twentyeleven_content_nav( 'nav-below' ); ?>

			<?php else : ?>

				<article id="post-0" class="post no-results not-found">
					<header class="entry-header">
						<h1 class="entry-title"><?php _e( 'Nothing Found', 'twentyeleven' ); ?></h1>
					</header><!-- .entry-header -->

					<div class="entry-content">
						<p><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'twentyeleven' ); ?></p>
						<?php get_search_form(); ?>
					</div><!-- .entry-content -->
				</article><!-- #post-0 -->

			<?php endif; ?>

			</div><!-- #content -->
		</div><!-- #primary -->

<?php //get_sidebar(); ?>
<?php get_footer(); ?>