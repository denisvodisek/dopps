<?php
/**
 * Template Name: Empty content
 * Description: A Page Template that showcases Sticky Posts, Asides, and Blog Posts
 *
 * The showcase template in Twenty Eleven consists of a featured posts section using sticky posts,
 * another recent posts area (with the latest post shown in full and the rest as a list)
 * and a left sidebar holding aside posts.
 *
 * We are creating two queries to fetch the proper posts and a custom widget for the sidebar.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

//display first child with another template!

get_header();
global $wp_query;


$template_name ='empty_content.php';
do{
	//get first child:
	$p = $wp_query->get_posts();
	//TODO: odstrani, ko sčistimo bazo!
	if ( is_user_logged_in() ){
		if (count($p) > 1) {
			$i = 0;
			$duplicatedPosts = "Podvojena objava: ";
			foreach ($p as $post) {
				$duplicatedPosts .= $i++ . " => " . $post->ID . " (" . $post->post_title . ") ";
			}
			error_log("Empty_content.php: ".$duplicatedPosts);
			echo "<pre id='problem-vec-IDjev' style='display: block;'>";
			echo $duplicatedPosts;
			echo "</pre>";
		}
	}
	$child = get_children(array(
				'order'=> 'DESC',
				'orderby'=> 'menu_order',
				'post_parent' => $wp_query->post->ID,
	));
	if(!$child){ //if there's no child, break out if this while loop
		continue;
	}
	
	$first_child = reset($child);
	if($first_child->menu_order == 0){ //if order is 0, reorder by name ('orderby'=> 'post_title' is not working correctly)
		usort($child, "posts_order_by_menu_order");
	}
	//get the first object:
	$child = reset($child);
			
	$new_pagename = $wp_query->query['pagename']."/".$child->post_name;
	$wp_query = new WP_Query(array('page'=> '','pagename'=> $new_pagename));
	
	$template_name = get_post_meta( $wp_query->post->ID, '_wp_page_template', true );

}while($template_name == 'empty_content.php');

$template_name = substr($template_name, 0, strrpos($template_name, '.', -1)); //remove .php on the end

if(empty($template_name))
	$template_name = 'page';
	
?>

		<div id="primary">
			<div id="content" role="main">

			<?php if ( have_posts() ) : ?>

				<?php twentyeleven_content_nav( 'nav-above' ); ?>

				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'content', $template_name ); ?>

				<?php endwhile; ?>

				<?php twentyeleven_content_nav( 'nav-below' ); ?>

			<?php else : ?>

				<article id="post-0" class="post no-results not-found">
					<header class="entry-header">
						<h1 class="entry-title"><?php _e( 'Nothing Found', 'twentyeleven' ); ?></h1>
					</header><!-- .entry-header -->

					<div class="entry-content">
						<p><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'twentyeleven' ); ?></p>
						<?php get_search_form(); ?>
					</div><!-- .entry-content -->
				</article><!-- #post-0 -->

			<?php endif; ?>

			</div><!-- #content -->
		</div><!-- #primary -->

<?php //get_sidebar(); ?>
<?php get_footer(); ?>